///@function __luaRousr_instance_builtin_get_ystart()
///@desc builtin getter for ystart
///@returns {*} builtin
///@extensionizer { "docs": false }
return ystart;
