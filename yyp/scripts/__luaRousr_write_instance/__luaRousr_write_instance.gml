///@func __luaRousr_write_instance(_id, _force)
///@desc sync the instance
///@param {Real}         _id   instance to sync
///@param {Boolean}   _force   should we force update every argument (full flush)
///@extensionizer { "docs": false }
var _id        = argument0;
var _force     = argument1;

var variables =  [ ];
var bind = undefined;
with (LuaSr) {
  var boundVariablesData = GMLBindings[__ELuaRousrGMLBindData.BoundVariables];
  var boundVariables     = boundVariablesData[ERousrArray.Data];
  var numBoundVariables  = boundVariablesData[ERousrArray.Count];
  
  var id_map = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  var bind = id_map[? _id];
  
  var syncVals = bind[__ELuaRousrBindEntity.Values];
  var numSync  = sr_array_size(syncVals);

  var valueCount  = max(numSync, numBoundVariables);
  var dirtyValues = array_create(valueCount);	// Create greedy arrays for performance
  var dirtyIndex  = 0;
  
  with (_id) {
    var  variableIndex = 0;
    for (var i = 0; i < numBoundVariables; ++i) {
      var boundVariable = boundVariables[i];
      var getter = boundVariable[__ELuaRousrBoundValue.Getter];
      var val = script_execute(getter, boundVariable[__ELuaRousrBoundValue.Name], variableIndex);
      dirtyIndex = __luaRousr_write_val(syncVals, variableIndex, val, dirtyValues, dirtyIndex, _force);
      variableIndex++;
    }
    
    if (bind != undefined) {  
      var customBinds = bind[__ELuaRousrBindEntity.CustomMembers];
      var numCustom = sr_array_size(customBinds);
      var customVars = sr_array_data(customBinds);
  
      for (var i = 0; i < numCustom ; ++i) {
        var customVariable = customVars[i];
        var getter = customVariable[__ELuaRousrBoundValue.Getter];
        var val = script_execute(getter, customVariable[__ELuaRousrBoundValue.Name], variableIndex);
        dirtyIndex = __luaRousr_write_val(syncVals, variableIndex, val, dirtyValues, dirtyIndex, _force);
        variableIndex++;
      }
    }
  }

  if (dirtyIndex > 0) {
  	var payloads = GMLBindings[__ELuaRousrGMLBindData.Payloads];
  	ds_list_add(payloads, [ _id, dirtyValues, dirtyIndex ]);
  }
}