var _width = 1278,
	_height = 180,
	_y = display_get_gui_height() - _height,
	_x = display_get_gui_width() - _width -1;

draw_set_font(fnt_12px);
draw_set_halign(fa_left);
draw_set_valign(fa_bottom);

var _text_y_start = _y + _height-2,
	_text_y = _text_y_start,
	_text_x = _x + 6,
	_i=ds_list_size(text)-1;

repeat(ds_list_size(text)){
	draw_text(_text_x,_text_y,text[|_i]);
	_text_y -= string_height(text[| _i]);
	--_i;
	if(abs(_text_y_start-_text_y) > _height - 6){
		break;
	}
}

draw_set_colour(c_black);
draw_set_alpha(0.2);
draw_rectangle(_x,_y,_x+_width,_y+_height,false);
draw_set_colour(c_white);
draw_set_alpha(1);
draw_rectangle(_x,_y,_x+_width,_y+_height,true);
