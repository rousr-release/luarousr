///@function __luaRousr_instance_builtin_set_path_positionprevious(_val)
///@desc builtin setter for path_positionprevious
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_positionprevious = argument0;
