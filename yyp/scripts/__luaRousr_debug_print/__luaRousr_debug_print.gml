///@func __luaRousr_debug_print(_msg)
///@desc print a message from the Lua debug call
///@param {string} _msg   messge from Lua
///@extensionizer { "docs": false }
var _msg = argument0;
var to_console = true;

with (LuaSr) {
  var num_listeners = ds_list_size(DebugListeners);
  for (var i = 0; i < num_listeners; ++i) {
    script_execute(DebugListeners[| i], _msg);
  }
  to_console = DebugToConsole;
}

if (to_console)
  show_debug_message(_msg);
