// vcxproj.js
//   babyjeans
//
// parse the vcxproj and vcxproj.filter files, and insert headers / cpp files into them
///
var fs = require("fs");

// "Private" functions
function insertVcxprojLine(searchText, searchKey, itemArray, itemCallback) {
	var includeIndex = searchText.lastIndexOf(searchKey);
	var insertIndex  = includeIndex + searchText.slice(includeIndex).indexOf("\n");
	var beginLine    = searchText.slice(0, includeIndex).lastIndexOf("\n");
	var spacer       = searchText.slice(beginLine, includeIndex);
	
	var newText     = searchText.slice(0, insertIndex);
	var textPostFix = searchText.slice(insertIndex);

	for (let i = 0; i < itemArray.length; i++) {
		newText += itemCallback(itemArray[i], spacer, searchKey, searchText);
	}
	
	newText += textPostFix;
	return newText;
}

class vcxProj {
	constructor(generator) {
		let vcxConfig = generator.vcxConfig;
		let vcxPath   = vcxConfig.path;

		this.vcxConfig      = vcxConfig;
		this.vcxproj        = vcxPath + vcxConfig.proj;
		this.vcxprojFilters = this.vcxproj + ".filters";
		this.filterFolder   = vcxConfig.filterFolder || "";
		this.resources      = [ ];

		this.relCppPath = this.vcxConfig.relativePath;
		this.headers = [this.relCppPath + vcxConfig.bindingsHeader ];
		this.cpp     = [this.relCppPath + vcxConfig.bindingsCpp    ];
		this.hRegistration = [ ];
		this.cppRegistration = [ ];
	}

	addResource(boundResource) {
		this.resources[this.resources.length] = boundResource;
		this.headers[this.headers.length] = this.relCppPath + boundResource.h.fileName;
		this.cpp[this.cpp.length]         = this.relCppPath + boundResource.cpp.fileName;
		
		this.hRegistration[this.hRegistration.length] = "./" + boundResource.h.fileName;
		this.cppRegistration[this.cppRegistration.length] = boundResource.className + "::RegisterLua(_Lua);";
	}

	processVcxProj() {
		let fileData = null;
		try {
			fileData = fs.readFileSync(this.vcxproj);
		} catch(err) {
			console.log("\tFailed to Generate: " + this.vcxproj + "\n\t\tCouldn't open for read.");
			throw err;
		}
		
		let itemCb = function(rawItem, spacer, searchKey, searchText) {
			let item = rawItem.replace(/\//g, "\\"); // swap the slashes for the proj files
			if (searchText.indexOf(item) > 0 || searchText.indexOf(rawItem) > 0)
				return "";

			return spacer + searchKey + "\"" + item + "\" />";
		};

		var vcxProjData = insertVcxprojLine(fileData,    "<ClCompile Include=", this.cpp,     itemCb);
		vcxProjData     = insertVcxprojLine(vcxProjData, "<ClInclude Include=", this.headers, itemCb);
		
		try {
			fs.writeFileSync(this.vcxproj, vcxProjData); 
			console.log("\tGenerated: " + this.vcxproj);
		} catch (err) {
			console.log("\tFailed to Generate: " + this.vcxproj + "\n\t\tError writing file.");
			throw err;
		}
	}

	processVcxProjFilter() {
		let fileData = null;
		try {
			fileData = fs.readFileSync(this.vcxprojFilters)
		} catch (err) {
			console.log("\tFailed to Generate: " + this.vcxprojFilters + "\n\t\tCouldn't open for read.");
			throw err;
		}
		
		var vcxProjData = insertVcxprojLine(fileData, "</ClCompile>",  this.cpp, function(rawItem, spacer, searchKey, searchText) {
			let item = rawItem.replace(/\//g, "\\"); // swap the slashes for the proj files
			if (searchText.indexOf(item) >= 0 || searchText.indexOf(rawItem) >= 0)
				return "";
			
			return spacer + "<ClCompile Include=\"" + item + "\">" + spacer + "  <Filter>" + this.filterFolder + "</Filter>" + spacer + "</ClCompile>";
		}.bind(this));
		
		vcxProjData     = insertVcxprojLine(vcxProjData, "</ClInclude>", this.headers, function(rawItem, spacer, searchKey, searchText) {
			let item = rawItem.replace(/\//g, "\\"); // swap the slashes for the proj files
			if (searchText.indexOf(item) >= 0 || searchText.indexOf(rawItem) >= 0)
				return "";
			
			return spacer + "<ClInclude Include=\"" + item + "\">" + spacer + "  <Filter>" + this.filterFolder + "</Filter>" + spacer + "</ClInclude>";
		}.bind(this));
		
		try {
			fs.writeFileSync(this.vcxprojFilters, vcxProjData); 
			console.log("\tGenerated: " + this.vcxprojFilters);
		} catch (err) {
			console.log("\tFailed to Generate: " + this.vcxprojFilters + "\n\t\tError writing file.");
			throw err;
		}
	}

	generateCallBinder() {
		let vcxConfig  = this.vcxConfig;
		let hFilePath   = vcxConfig.exportPath + vcxConfig.bindingsHeader;
		let cppFilePath = vcxConfig.exportPath + vcxConfig.bindingsCpp;

		let registerH = "";
		registerH	+= "#pragma once\n\n";
		registerH   += "#include \"../luaRousr.h\"\n\n";
		
		for (let reg = 0; reg < this.hRegistration.length; reg++) 
			registerH += "#include \"" + this.hRegistration[reg] + "\"\n";
		
		registerH   += "\n";
		registerH	+= "class GMLBindings {\n"
		registerH	+= "public:\n"
		registerH	+= '\t' + "static void Register(CLuaRousr& _lua);\n";
		registerH	+= "};\n"

		let registerCpp = "";
		registerCpp += "#include \"" + hFilePath.slice(hFilePath.lastIndexOf('/') + 1) + "\"\n\n";
		registerCpp += "void GMLBindings::Register(CLuaRousr& _Lua) {\n\n"
		
		for (let reg = 0; reg < this.cppRegistration.length; reg++) 
			registerCpp += '\t' + this.cppRegistration[reg] + '\n';
		
		registerCpp += "\n}\n";

		try { 
			fs.writeFileSync(hFilePath,   registerH);    // write the header
			fs.writeFileSync(cppFilePath, registerCpp);  // write the cpp file
			console.log("\tGenerated: " + hFilePath + " and " + cppFilePath);
		} catch (err) {
			console.log(err);
		}
	}

	generate() {
		try {
			this.generateCallBinder();
			this.processVcxProj();
			this.processVcxProjFilter();
		} catch (err) { console.log("\tFailed to regenerate vcxproj and vcxproj.filters:\n\t" + err);	}
	}
}

module.exports = vcxProj;