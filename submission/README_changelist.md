----

### Lua Functions 

#### `thread(function)`
|  |  |
--------------- | ---------------------- 
**callbackId**  | the id of the callback passed from Lua
**returns:** a thread object to hold on to.

A `Lua Thread` can be "yielded" to pause execution, resuming from the point of the `yield` the next time the `Lua Thread` is resumed. (LuaRousr automatically resumes threads once per step in the `oLuaRousr` object).

##### ***TIP*** This is useful for scripts that need to wait for things to occur, such as telling an NPC to move to a location, and then checking they've finished their move each step. i.e,:

```
local move_thread = thread(function()
  local npc = GMLInstance("npc")
  npc_move(npc, 200, 200)
  while (npc_is_moving(npc)) do
    thread.yield()
  end
end)

```
###### Note: The npc_* functions are just for the example, and not actual API.

##### **Note:** We're storing the returned thread in a local variable. This is to access it with later, but LuaRousr will also keep a Lua reference to the thread while it's active to prevent it from being garbage collected.

---

#### `thread.yield()`

Yield tells a Lua Thread to stop executing, resuming from this point next step. See the explanation in `thread()`

---

#### `toFunction(callbackId)`
|  |  |
--------------- | ---------------------- 
**callbackId**  | the id of the callback passed from Lua to GML and back to Lua
**returns:** function `callbackId` represents

If you've passed a function to GML, but want to pass it back to Lua and use it, `toFunction` returns the actual function from the numeric id.

---

#### `print(string)`
|  |  |
----------- | ---------------------- 
**string**  | the text to show_debug_message

a debug, logging, helper function. calls `show_debug_message` in GML with the given text.

---

#### `GMLInstance(instanceName)`
|  |  |
----------------- | ---------------------- 
**instanceName**  | the string name given to the instance when it was bound to Lua.
**returns:** a `GMLInstance` Lua object representing the named instance, or nil on error.

Retrieves a GML `instance` resource that was bound using `luaRousr_bindInstance.` Each `GMLInstance` Lua object created this way does point to the same GML resource.

##### Built-ins

All instance built-in GML variables are bound to `GMLInstance` and accessible as properties:

```
local inst = GMLInstance("tester")
inst.x = 10
print (inst.x)
```

#### `GMLResource(resourceName)`
|  |  |
----------------- | ---------------------- 
**instanceName**  | the string name given to the resource when it was bound to Lua.
**returns:** at `index` value for the resource, so you can pass it to your functions

Retrieves a GML Resource `index` for a resource that was bound using `luaRousr_bindResource.` 

---

### Changelog

*	v1.1.0
	*	Updated LuaRousr to use [rousrSuite](https://marketplace.yoyogames.com/assets/6319/rousrsuite)
	*	Updated internal `rousrGML` lib.
	* 	Hid API in a GML extension to keep the resource tree clean.

* 	v1.0.0
	* **API Changes:**
		*	Renamed `luaRousr_killThread` to `luaRousr_killThreads` to more accurately match its function, no longer accepts a contextId
		*	`luaRousr_executeFile` and `luaRousr_executeString` no longer return a `script_context_id` and just return `true` on success.
		*	Added 
			*	`luaRousr_init` - optional script to create LuaRousr
			*	`luaRousr_bindInstanceVariable(instance_id, memberName, defaultVal, readOnly)` - bind your own instance variables to your bound instance
			*	`luaRousr_bindResource(_name, _index)` and `luaRousr_unbindResource(_indexOrName)` to manage resource binds (access in Lua as `GMLResource("name")`)
			*	`luaRousr_retainCallback` and `luaRousr_releaseCallback` for managing functions passed _from_ Lua's Lifetime.
			*	`luaRousr_addDebugPrintListener` and `luaRousr_removeDebugPrintListener` added, allowing you to add custom print handlers.
	* Fixes:
		*	Fixed calling Lua from Lua called GML functions
			* 	Nesting Lua->Gml->Lua->Gml should now call functions correctly. 
		*	Fixed `luaRousr_call` not returning values properly
		*	Rewrote Instance Syncing
			*	`rousr_sync` was not actually being called after initial instantiation, so GML updates to objects wouldn't be seen by Lua
			*	_Smart_ Syncing that caches reads/writes to minimize communication between GML/Lua
	* New Features:
		*	Added support for passing functions as arguments to GML, i.e.,:
			```lua
			-- Lua Code
			set_game_callback(function(str)
				print (str)
			end)
			```
		*	Added support for calling those functions, i.e.,: 
			```javascript
			// GML code
			///@func set_game_callback(_callbackId)
			var _callbackId = argument0; // got from the set_game_callback lua function
			luaRousr_callLua(callbackId, "hello");
			```
*	v0.7.0RC 
	*	Excellent, superb demo by @net8floz
		*	Added outsideTheBox (v0.9.3) to the demo
	*	Fixed id not being bound to instances.
	*	Fixed rebinding to the same named instance.
	*	Added `luaRousr_unbindInstance`
	*	Added `GMLInstance`
		*	Integrated [sol2](https://github.com/ThePHd/sol2) for dynamic member getter/setter
*   v0.6.1 - Fixed YYC compile
*	v0.6.0
	*	rousr_sync_val system added
	*	Added `luaRousr_bindInstance(instance_id)`
* 	v0.5.1 - Added `luaRousr_killThreads` 
*	v0.5.0 
	*	Initial Version
		*	GML to DLL: Use Async Map to transfer data on a request basis
		*	DLL Lua Integration using [lua-intf](https://github.com/SteveKChiu/lua-intf)

### Upcoming

*	Support for more platforms (MacOs, Linux first)
*	Configuration:
	*	Filter instance variable properties

### Known Issues (Expect fixes soon)
*	`layer = -1` disables rendering, currently syncing it is disabled.
*	If LuaRousr is not persistent, recreating it can sometimes cause issues.
