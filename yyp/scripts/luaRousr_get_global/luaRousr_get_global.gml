///@function luaRousr_get_global(_name)
///@desc get the value of `_name` in the Lua Globals table (global namespace)
///@param {String} _name   the name of the Lua global variable
///@returns {Real|String} global value or undefined
if (!__luaRousr_check_init())
  return undefined;
  
var _name = argument0;
var returnBuffer = __luaRousr_pop_buffer();

__extLuaRousrGetGlobal(_name, _RousrDefaultBufferSize, buffer_get_address(returnBuffer));
var val = sr_buffer_read_val(returnBuffer);

__luaRousr_push_buffer(returnBuffer);
return val;