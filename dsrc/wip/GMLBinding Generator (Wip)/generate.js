// generate.js
//   babyjeans
//
// generate C++ and GML files for binding GML properties and functions
// to code.
//
var path = require('path');
var fs = require('fs');

var BoundResource = require('./script/boundresource.js');
var VCXProj = require('./script/vcxproj.js');
var YYProj = require('./script/yyproj.js');

class LuaRousrBindingGenerator {
	constructor(bindings) {
		this.bindings = bindings;
		let config = bindings.config;

		///
		// Load VS config
		let vcxConfig  = config.vcx;
		let relCppPath = vcxConfig.cppPath || './exports/'
		
		// sanitize path
		if (relCppPath.indexOf('./') == 0) relCppPath = relCppPath.slice(2);
		if (relCppPath[relCppPath.length - 1] != '/') relCppPath += '/';
		if (vcxConfig.path[vcxConfig.path.length - 1] != '/') vcxConfig.path += '/';
		
		vcxConfig.exportPath   = vcxConfig.path + relCppPath;
		vcxConfig.relativePath = relCppPath;
		vcxConfig.classPrefix  = vcxConfig.classPrefix || "";
		vcxConfig.filePrefix   = vcxConfig.filePrefix || classPrefix;
		
		///
		// Load GML Config
		let gmlConfig = config.gml;	
		gmlConfig.path         = gmlConfig.path || "./exports/";
		gmlConfig.fallbackPath = gmlConfig.fallbackPath || "./exports/";
		gmlConfig.filePrefix   = gmlConfig.filePrefix || "";
		gmlConfig.callName     = gmlConfig.callName || gmlFilePrefix + "_call";
		
		this.gmlConfig = gmlConfig;
		this.vcxConfig = vcxConfig;
		this.vcxProj         = new VCXProj(this);
		this.yyProj          = new YYProj(this);

		this.boundResources = [ ];
	}
	
	generate() {
		let gmlConfig = this.gmlConfig;
		let vcxConfig = this.vcxConfig;

		console.log("Configuring Binding Paths: \n" +
			"\tvcxproj path:        " + vcxConfig.path + "\n" +
			"\tcppExportPath:       " + vcxConfig.exportPath + "\n" +
			"\tgmlExportPath:       " + gmlConfig.path + "\n" +
			"\tgmlFallbackPath:     " + gmlConfig.fallbackPath + "\n"
		);

		try { fs.mkdirSync(vcxConfig.exportPath); } catch(err) {
			if (err && err.code != "EEXIST") {
				console.log("Couldn't open folder for CPP export");
				vcxConfig.exportPath = null;
			}
		}

		try { fs.mkdirSync(gmlConfig.fallbackPath); } catch(err) {
			if (err && err.code != "EEXIST") {
				console.log("Couldn't open fallback GML folder for export");
				gmlConfig.fallbackPath = null;
			}
		}

		console.log("Processing Types: ");
		let bindings = this.bindings;
		let boundResources = this.boundResources;

		// first pass, create and number so that we can reference each other
		for (let resourceName in bindings.resources) {
			let resource = bindings.resources[resourceName];
			let resourceIndex = boundResources.length;
			boundResources[resourceIndex] = new BoundResource(resourceName, resourceIndex, this);
		}

		// second pass: generate .cpp, .h, and .gml content
		for (let resourceIndex = 0; resourceIndex < boundResources.length; resourceIndex++) {
			let boundResource = boundResources[resourceIndex];

			console.log("\tresource: " + boundResource.name);
			if (boundResource.abstract) {
				console.log("\t\tSkipped abstract (base) resource.");
				continue;
			}

			boundResource.generate();
			boundResource.writeFiles();

			this.vcxProj.addResource(boundResource);
			this.yyProj.addResource(boundResource);
		}

		// third: update vcxProj / vcxProjFilters
		console.log("Generating Visual Studio Project Files: ");
		this.vcxProj.generate();
		
		// fourth: generate entries in __luaRousr_callBinding
		console.log("Generating YYP and GML Binding Core: ");
		this.yyProj.generate();
	}

	generateProject() {
		// third: generate the entries in the GMLBinding.cpp
		this.generateCPPRegistar(cppExportPath + vcxConfig.bindingsHeader, cppExportPath + vcxConfig.bindingsCpp, cppRegister);	
		
		vcxProj.generate(projectFiles);
		
		this.generateGMLCaller(gmlExportPath + "scripts/" + gmlCallName + "/" + gmlCallName + ".gml", gmlCalls);
		
		// sixth: update YYP file with new files (unsupported, for now)
		console.log("\tSkipping YYP updated (currently unsupported).");
		//this.updateYYP();
	}
};

let luaRousrBindings= new LuaRousrBindingGenerator(require("./bindings.json"));
luaRousrBindings.generate();