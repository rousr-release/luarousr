///@function __luaRousr_instance_builtin_get_path_positionprevious()
///@desc builtin getter for path_positionprevious
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_positionprevious;
