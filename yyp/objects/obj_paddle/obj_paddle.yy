{
    "id": "6622a31a-ffda-45d5-ab0d-10221f12535f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_paddle",
    "eventList": [
        {
            "id": "68a82591-919f-42dc-9117-3f044db42709",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6622a31a-ffda-45d5-ab0d-10221f12535f"
        },
        {
            "id": "9d2cb0da-ceb9-4cb8-8b60-065c71b843f0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "6622a31a-ffda-45d5-ab0d-10221f12535f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "82ab8dc6-40cf-46ed-9f83-30695c2889d5",
    "visible": true
}