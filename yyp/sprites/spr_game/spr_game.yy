{
    "id": "bdc0aa3f-40c9-4bff-a352-bb100f55c6d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_game",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 352,
    "bbox_left": 403,
    "bbox_right": 630,
    "bbox_top": 319,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "4fd133ef-8363-44c5-996f-d0c09bc8cfa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdc0aa3f-40c9-4bff-a352-bb100f55c6d6",
            "compositeImage": {
                "id": "4dee6e18-4f01-4096-a74c-d49f114229d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4fd133ef-8363-44c5-996f-d0c09bc8cfa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea39c804-533d-41da-bbac-588bee3fbe5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4fd133ef-8363-44c5-996f-d0c09bc8cfa5",
                    "LayerId": "79d73956-32c4-4547-8c6e-0a75e455f89f"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 360,
    "layers": [
        {
            "id": "79d73956-32c4-4547-8c6e-0a75e455f89f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdc0aa3f-40c9-4bff-a352-bb100f55c6d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}