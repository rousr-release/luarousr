///@function __luaRousr_process_buffer(_cmd_buffer, _cmd_id)
///@description process the buffer from Lua, until we're done
///@param {Real} _cmd_buffer   Id of the buffer passed to LuaSr to process Lua commands
///@param {Real} _cmd_id       Id of the command to ensure we didn't corrupt our processing buffer.
///@extensionizer { "docs": false }
var _cmd_buffer = argument0;
var _cmd_id     = argument1;
if (_cmd_id < 0)
  return;

buffer_seek(_cmd_buffer, buffer_seek_start, 0); 

var cmdId = undefined;
var status = __ELuaRousrStatus.Waiting;
while (status != __ELuaRousrStatus.Done) {
  __extLuaRousrWaitForLua(_cmd_id);
  status = buffer_read(_cmd_buffer, buffer_u8);
 	cmdId  = buffer_read(_cmd_buffer, buffer_u32);
  switch (status) {
    case __ELuaRousrStatus.Ready: {
  	  var callType     = buffer_read(_cmd_buffer, buffer_s8);
      
  	  switch(callType) {
        case __ELuaRousrCmd.DebugPrint: {
          var msg = buffer_read(_cmd_buffer, buffer_string);
          __luaRousr_debug_print(msg)
          __luaRousr_reset_buffer(_cmd_buffer, _cmd_id);
        } break;
        
        case __ELuaRousrCmd.GMLBound: {
          __luaRousr_sync_read(_cmd_buffer);
          var script_index = buffer_read(_cmd_buffer, buffer_u32);
					
    			var argCount = buffer_read(_cmd_buffer, buffer_u8);
    			var args = array_create(argCount)
    			for (var argIndex = 0; argIndex < argCount; ++argIndex) {
    				args[argIndex] = sr_buffer_read_val(_cmd_buffer)
    			}
					
    			__luaRousr_executeGML(_cmd_buffer, script_index, args, argCount);
          __luaRousr_reset_buffer(_cmd_buffer, _cmd_id);
        } break;
        
        // Lua requesting an instance sync
        case __ELuaRousrCmd.RequestSync: {
          var num = buffer_read(_cmd_buffer, buffer_u32);
          if (num > 0) {
            for (var i = 0; i < num; ++i) {
              var _id = buffer_read(_cmd_buffer, buffer_f64);
              __luaRousr_write_instance(_id, false);
            }
          } else {
            show_debug_message("luaRousr - warning: Got a sync request with no ids.");
          }
          
          __luaRousr_sync_write(_cmd_buffer);
          __luaRousr_reset_buffer(_cmd_buffer, _cmd_id);
        } break;
      } 
    } break;
    
    case __ELuaRousrStatus.Done: { 
      if (cmdId == _cmd_id) {
        __luaRousr_sync_read(_cmd_buffer);
        break;
      }
      
      show_debug_message("lua Rousr - ERROR: cmd buffer corrupt! Attempting to recover.");
      status = __ELuaRousrStatus.Waiting
    } break;
  } 
  
  buffer_seek(_cmd_buffer, buffer_seek_start, 0);
}

__extLuaRousrFinished(_cmd_id);