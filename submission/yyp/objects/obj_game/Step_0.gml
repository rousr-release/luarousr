if(surface_get_width(application_surface) != window_get_width()){
	if(window_get_width() > 100){
		surface_resize(application_surface,window_get_width(),window_get_height());
	}
}

if(keyboard_check_pressed(vk_escape)){
	game_end();
}

if(keyboard_check_pressed(ord("R"))){
	//Kill all active lua threads
	luaRousr_kill_threads();
	//Reload scripts
	luaRousr_execute_string(otb_file_to_string(exe_path, "ball.lua"));
	luaRousr_execute_string(otb_file_to_string(exe_path, "paddle.lua"));

	with(obj_ball){
		luaRousr_bind_instance(lua_key,id); 
		luaRousr_call("ballStart",lua_id);
	}
	
	with(obj_paddle){
		luaRousr_bind_instance(lua_key,id);
		luaRousr_call("paddleStart",lua_id);
	}

}

if(keyboard_check_pressed(vk_f1)){
	grid_set_grid_type(grid_types.normal);
}

if(keyboard_check_pressed(vk_f2)){
	grid_set_grid_type(grid_types.fancy);
}

if(keyboard_check_pressed(vk_f3)){
	grid_set_grid_type(grid_types.rainbow);
}

if(keyboard_check_pressed(vk_f4)){
	grid_set_grid_type(grid_types.null);
}

