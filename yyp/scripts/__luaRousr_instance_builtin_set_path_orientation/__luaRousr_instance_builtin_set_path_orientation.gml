///@function __luaRousr_instance_builtin_set_path_orientation(_val)
///@desc builtin setter for path_orientation
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_orientation = argument0;
