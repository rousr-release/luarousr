///@function __luaRousr_instance_builtin_get_friction()
///@desc builtin getter for friction
///@returns {*} builtin
///@extensionizer { "docs": false }
return friction;
