///@func otb_file_to_string(_exe, _fname)
///@param _exe
///@param _fname
var _exe    = argument0,
    _fname  = argument1,
	  _string = "";

var _filePath = string(_exe + "/" + _fname);
var _file = oTB_fileOpen(_filePath, oTB.Read);
if (_file == -1 && os_type == os_linux) {
  _exe += "/assets";
  _file = oTB_fileOpen(string(_exe + "/" + _fname), oTB.Read);
}

var normal = false;
if(_file == -1){
  _file = file_text_open_read(_fname);
  normal = true;
  if (_file == -1) {
	  show_message("File not found! You have either moved the file or are using GMS2 IDE. Please check the comments in obj_game for more information about file loading");
	  game_end();
    return;
  }
}

while(!file_text_eof(_file)){
	_string += file_text_readln(_file);
}

if (normal)
  file_text_close(_file)
else
  oTB_fileClose(_file);


return _string;

  
