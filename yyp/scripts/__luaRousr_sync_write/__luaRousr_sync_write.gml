///@function __luaRousr_sync_write(_buffer)
///@desc write all pending payloads to the DLL buffer, and clear the payload list
///@param {Real) buffer   buffer being processed
///@extensionizer { "docs": false }
var buffer = argument0;

with (LuaSr) {
  var payloads     = GMLBindings[__ELuaRousrGMLBindData.Payloads];
  var payloadCount = ds_list_size(payloads);

  buffer_seek(buffer, buffer_seek_start, 1);
  buffer_write(buffer, buffer_u32, payloadCount);

  // Write any dirty contexts since last update
  if (payloadCount > 0) {
    
  	for (var payloadIndex = 0; payloadIndex < payloadCount; ++payloadIndex) {	
  		var payload = payloads[| payloadIndex];
  		var _id         = payload[__ELuaRousrPayload.Id];
  		var vals        = payload[__ELuaRousrPayload.Payload];
      var payloadSize = payload[__ELuaRousrPayload.PayloadSize];
    
    	buffer_write(buffer, buffer_f64, _id);
    	buffer_write(buffer, buffer_u32, payloadSize / 2);

    	for (var i = 0; i < payloadSize - 1; i+=2) {
    		var valIndex = vals[i];
    		var val =      vals[i + 1];
    		buffer_write(buffer, buffer_u32, valIndex);
    		sr_buffer_write_val(buffer, val);
    	}
    }
	
    ds_list_clear(payloads);
  }
}