///@param force_x
///@param force_y
///@param position x
///@param position y
///@param radius_squared real
var _fx = argument0 * 10,
	_fy = argument1 * 10,
	_x = argument2,
	_y = argument3,
	_radius = argument4;
	

var _i=0;
repeat(global.points_size){
	var _point = global.points_array[_i],
		_dx = (_point[point.x] - _x),
		_dy = (_point[point.y] - _y),
		_dist = (_dx * _dx) + (_dy * _dy);

	if(_dist <  _radius){
		_dist = 10 + sqrt(_dist);
		point_apply_force(_point,_fx/_dist,_fy/_dist);
	}
	++_i;
}
