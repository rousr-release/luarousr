{
    "id": "48d474a8-113b-4084-9e36-99f24c8a090f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ball",
    "eventList": [
        {
            "id": "23918bd0-fe38-457c-90de-69fd77fb79d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "48d474a8-113b-4084-9e36-99f24c8a090f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8c8b9120-cf00-402b-86f9-3a13e31e5278",
    "visible": true
}