{
    "id": "b5843d2f-b278-46b8-953d-435cd3338b76",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_line_rainbow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e98b7dd6-dbf5-4a4b-90ba-290dcba55d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5843d2f-b278-46b8-953d-435cd3338b76",
            "compositeImage": {
                "id": "cd956461-f8a6-458e-b294-a48aecc277ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e98b7dd6-dbf5-4a4b-90ba-290dcba55d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c766ca12-678d-4dda-a929-e80697107571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e98b7dd6-dbf5-4a4b-90ba-290dcba55d9d",
                    "LayerId": "e07dfd6e-45bc-4329-a979-7ead0a6e8390"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "e07dfd6e-45bc-4329-a979-7ead0a6e8390",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5843d2f-b278-46b8-953d-435cd3338b76",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}