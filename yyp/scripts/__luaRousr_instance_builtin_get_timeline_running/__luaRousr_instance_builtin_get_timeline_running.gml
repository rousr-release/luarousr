///@function __luaRousr_instance_builtin_get_timeline_running()
///@desc builtin getter for timeline_running
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_running;
