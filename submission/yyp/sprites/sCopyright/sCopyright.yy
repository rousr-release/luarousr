{
    "id": "3f18846b-077a-4d03-af0b-1bbd5bfda5eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCopyright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 0,
    "bbox_right": 8,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fdfb3475-5d71-4b9a-ab1b-ade77865d345",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f18846b-077a-4d03-af0b-1bbd5bfda5eb",
            "compositeImage": {
                "id": "304648e0-cb05-4838-a40d-998b1636dc01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fdfb3475-5d71-4b9a-ab1b-ade77865d345",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10b07791-5518-4f00-89ea-35b756f6261b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fdfb3475-5d71-4b9a-ab1b-ade77865d345",
                    "LayerId": "17429ee4-431d-4fa0-bd36-a27e73760cec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "17429ee4-431d-4fa0-bd36-a27e73760cec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f18846b-077a-4d03-af0b-1bbd5bfda5eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 9,
    "xorig": 0,
    "yorig": 0
}