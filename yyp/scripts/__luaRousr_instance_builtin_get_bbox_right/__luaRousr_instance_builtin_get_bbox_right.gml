///@function __luaRousr_instance_builtin_get_bbox_right()
///@desc builtin getter for bbox_right
///@returns {*} builtin
///@extensionizer { "docs": false }
return bbox_right;
