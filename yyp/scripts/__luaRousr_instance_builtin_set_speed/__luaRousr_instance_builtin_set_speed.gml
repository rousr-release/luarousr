///@function __luaRousr_instance_builtin_set_speed(_val)
///@desc builtin setter for speed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
speed = argument0;
