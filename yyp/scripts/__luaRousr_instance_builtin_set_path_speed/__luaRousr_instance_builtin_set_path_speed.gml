///@function __luaRousr_instance_builtin_set_path_speed(_val)
///@desc builtin setter for path_speed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_speed = argument0;
