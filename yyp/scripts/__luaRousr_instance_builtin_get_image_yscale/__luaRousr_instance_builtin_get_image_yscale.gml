///@function __luaRousr_instance_builtin_get_image_yscale()
///@desc builtin getter for image_yscale
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_yscale;
