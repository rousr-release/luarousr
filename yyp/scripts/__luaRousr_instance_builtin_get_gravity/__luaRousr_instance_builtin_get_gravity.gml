///@function __luaRousr_instance_builtin_get_gravity()
///@desc builtin getter for gravity
///@returns {*} builtin
///@extensionizer { "docs": false }
return gravity;
