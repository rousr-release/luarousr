///@func luaRousr_bind_instance([_lua_name], _instance_id)
///@desc Binds an instance to Lua using the given name. All the members can now be accessed i.e,:
///      ```
///      inst.x = 10
///      print(inst.y)
///      ```
///      **NOTE:** You must unbind an instance in its `CleanUp` event if it's still bound at that point.
///
///@param {String} [_lua_name]  - Name to lookup this instance with in Lua, can omit, and just pass id, when using for return values
///@param {Real}   _instance_id - instance we're sync'ing (some history: original API required both, and unfortunately passed id second, in order to not break everything, first param is now optional)
///@returns {Real} noone on unsuccessful bind, or id of instance (for chaining in function returns)
var _lua_name = argument_count > 1 ? argument[0] : "";
var _id      = argument_count == 1 ? argument[0] : argument[1];

with (LuaSr) {
  // ensure its not already bound by id or by name
  luaRousr_unbind_instance(_id);
  luaRousr_unbind_instance(_lua_name);
  
  // bind the instance
  __extLuaRousrBindInstance(_lua_name, _id);

  // get the next free bind index to track our context with
  var binds = GMLBindings[__ELuaRousrGMLBindData.Binds];
  var bind = sr_pool_create(binds)
  
  bind[@ __ELuaRousrBindEntity.Id]            = _id;
  bind[@ __ELuaRousrBindEntity.LuaName]       = _lua_name;
  bind[@ __ELuaRousrBindEntity.Values]        = sr_array_create();
  bind[@ __ELuaRousrBindEntity.CustomMembers] = sr_array_create();
  
  var id_map      = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  id_map[? _id]            = bind;
  
  var name_map    = GMLBindings[__ELuaRousrGMLBindData.NameToBind];
  name_map[? _lua_name]     = bind;
  
  return _id;
}

return noone;