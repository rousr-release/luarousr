///@function __luaRousr_instance_builtin_get_yprevious()
///@desc builtin getter for yprevious
///@returns {*} builtin
///@extensionizer { "docs": false }
return yprevious;
