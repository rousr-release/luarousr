// GMLWrapperBuffer.h
//  babyjeans
//
// a simple class to hold a global buffer to use for reading wrapped arguments and sending returns back.
////
#pragma once

#include "types.h"

class CGMLBuffer;

class GMLWrapperBuffer {
public:
    static void SetWrapperBuffer(std::shared_ptr<CGMLBuffer> _buffer);
    static std::shared_ptr<CGMLBuffer> WrapperBuffer() { return mInstance != nullptr ? mInstance->mBuffer : nullptr; }

private:
    static std::shared_ptr<GMLWrapperBuffer> mInstance;

    std::shared_ptr<CGMLBuffer> mBuffer;
};