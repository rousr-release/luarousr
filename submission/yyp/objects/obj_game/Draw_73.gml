///@description Insert description here
// You can write your code in this editor
draw_set_font(fnt_12px);
draw_set_colour(c_gray);
draw_set_alpha(0.8);
var _text = ["Lua","Rousr","   - ","  Demo ","    by ","     @net8floz","","          " + string(fps)+"/"+string(fps_real)];
var _i=0;

repeat(array_length_1d(_text)){
	var _point = obj_grid.points[# 1+_i,1];
	draw_text(_point[point.x],_point[point.y]-16,_text[_i]);
	++_i;
}

draw_set_alpha(1);
