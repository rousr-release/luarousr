// boundresource.js
//  babyjeans
//
// wrap up the resources
///
var fs = require('fs');

var cppFile = require('./cppgenerator.js');
var hFile   = require('./hgenerator.js');
var gmlFile = require('./gmlgenerator.js');

function mergeObjects(obj1, obj2) {
	obj1 = obj1 || { };
	obj2 = obj2 || { };
	
	for (let name in obj2) 	if (obj1[name] == null) obj1[name] = obj2[name];
	return obj1;
}
		
class BoundResource {
	constructor(resourceName, resourceIndex, generator) {
		let bindings        = generator.bindings;
		let vcxConfig       = generator.vcxConfig;
		let gmlConfig       = generator.gmlConfig;
		let classPrefix     = vcxConfig.classPrefix || "";
		let cppFilePrefix   = vcxConfig.filePrefix || classPrefix;

		let resource = bindings.resources[resourceName];

		this.vcxConfig = vcxConfig
		this.gmlConfig = gmlConfig;
		this.boundResources = generator.boundResources;
		this.name            = resourceName;
		this.bindingIndex    = 0;
		this.index           = resourceIndex;
		this.type            = resource.type;
		this.resource        = resource;
		this.abstract        = resource.abstract;
		this.staticFunctions = resource.static_functions || { };

		this.functions       = resource.functions || { };
		this.variables       = resource.variables || { };

		let baseResourceDef = resource.baseResource || "";
		
		// inherit base resource
		if (bindings.resources[baseResourceDef] !== undefined) {
			let baseResource     = bindings.resources[baseResourceDef];

			this.hasId  = (baseResource.hasId || false);
			this.module = (baseResource.module || null);
			this.staticFunctions = mergeObjects(this.staticFunctions, baseResource.static_functions);
			this.functions       = mergeObjects(this.functions, baseResource.functions);
			this.variables       = mergeObjects(this.variables, baseResource.variables);
		}

		// allow for overrides
		this.hasId    = resource.hasId  || this.hasId;
		this.module   = resource.module || this.module;
		
		// naming schemes
		this.className      = resource.cppName  || classPrefix + resourceName;
		this.luaName        = resource.luaName  || resourceName;
		this.cppFileName    = resource.fileName || cppFilePrefix + resourceName;
		this.headerIncludes = vcxConfig.headerIncludes || [];

		// generate the file parsers
		this.cpp = new cppFile(this);
		this.h   = new hFile(this);
		this.gml = new gmlFile(this);

		// generate is a secondary step, we need all the resources created to reference each other
	}

	convertType(variableName, variable) {
		let v = { };

		let vtype = variable.type;
		let isRes = false;
		for (let i = 0; i < this.boundResources.length; i++) {
			if (this.boundResources[i].name == variable.type) {
				isRes = true;
				v.resource = this.boundResources[i];
				break;
			}
		}
				
		// let returnType = vcxConfig.types[ret]
		// if (returnType == null) {
		// 	if (resources[ret] != null)	ret = "std::shared_ptr<" + className +">";
		// 	else                        ret = "void";
		// 	returnType = vcxConfig.types[ret];
		// }

		// if (returnType) returnType = returnType.gettype || (returnType.memberType || ret);
		// else returnType = ret;

		if (isRes) vtype = 'int';

		let vcxType = this.vcxConfig.types[vtype] || this.vcxConfig.types['void'] || { };
		let gmlType = this.gmlConfig.types[vtype] || this.gmlConfig.types['void'] || { };
		
		v.name          = variableName,
		v.memberType    = vcxType.membertype || vtype;
		v.getReturnType = vcxType.gettype || v.memberType;
		v.setReturnType = vcxType.settype || v.getReturnType;
		
		v.casts = { 
			castFrom: vcxType.castFrom || null,
			castTo:   vcxType.castTo   || null
		};

		if (v.casts.castFrom != null && v.casts.castTo == null) v.casts.castTo = v.memberType;

		v.flags = { }
		variable.flags = variable.flags || [ ];
		for (let f = 0; f < variable.flags.length; f++)
			v.flags[variable.flags[f]] = true;

		v.gmlUpdate = { 
			func: gmlType.func,
			type: gmlType.type,
			flags: { }
		};

		if (gmlType.flags)
			for (let i = 0; i < gmlType.flags.length; i++)
				v.gmlUpdate.flags[gmlType.flags[i]] = true;
		
		return v;
	}

	processFunction(functionName, func, isStatic) {
		let f = { };
		f.name     = functionName;
		f.isStatic = isStatic;
		
		f.cppName  = func.cpp || f.name;
		f.luaName  = func.lua || f.name;
		f.gmlName  = func.gml || f.name;

		f.return   = this.convertType("return", { type: func.return});
		f.args = [ ];
		for (var i = 0; func.args != null && i < func.args.length; i++)
			f.args[i] = this.convertType("arg" + i, { type: func.args[i] });
		
		f.bindingIndex = this.bindingIndex;
		this.bindingIndex++;

		this.cpp.processFunction(f);
		this.h.processFunction(f);
		this.gml.processFunction(f);
	}

	processVariable(variableName, variable) {
		let v = this.convertType(variableName, variable);
		if (v.flags.unsupported)
			return;

		v.bindingIndex = this.bindingIndex;
		
		this.bindingIndex++;
		if (!v.flags.readonly) this.bindingIndex++;

		this.cpp.processVariable(v);
		this.h.processVariable(v);
		this.gml.processVariable(v);
	}

	generate() {
		for (let staticFunction in this.staticFunctions) this.processFunction(staticFunction, this.staticFunctions[staticFunction], true);
		for (let func in this.functions)                 this.processFunction(func,           this.functions[func],                 false);
		for (let variableName in this.variables)         this.processVariable(variableName,   this.variables[variableName]);

		this.cppFile = this.cpp.generate();
		this.hFile   = this.h.generate();
		this.gmlFile = this.gml.generate();
	}

	writeFiles() {
		let gmlConfig = this.gmlConfig;
		let vcxConfig = this.vcxConfig;

		let cppFilePath = this.cpp.filePath;
		let hFilePath   = this.h.filePath;
		let gmlFilePath = this.gml.filePath;
		
		let writeHelper = function(path, file, label) {
			try {
				fs.writeFileSync(path, file);
				console.log("\t\tGenerated " + label + ": " + path);
			} catch(err) { console.log("\t\tError generating " + label + ": " + path + "\n" + err); }
		}

		writeHelper(hFilePath,   this.hFile,   "header");
		writeHelper(cppFilePath, this.cppFile, "cpp");
		writeHelper(gmlFilePath, this.gmlFile, "gml");
	}

	generateGMLCall()       { return this.gml.generateGMLCall();    }
	generateHCallBinder()   { return this.h.generateCallBinder();   }
	generateCPPCallBinder() { return this.cpp.generateCallBinder(); }

	generateCPPFilePath()   { return this.cpp.getFilePath(); }
	generateHFilePath()     { return this.h.getFilePath();   }
	generateGMLFilePath()   { return this.gml.getFilePath(); }
}

module.exports = BoundResource;