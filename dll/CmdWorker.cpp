#include "CmdWorker.h"

CmdWorker* CmdWorker::Instance = nullptr;

void CmdWorker::PushJobInternal(std::function<void()>&& _job) {
    std::shared_ptr<WorkerThread> pWorker(nullptr);
    for (auto workerThread : mThreads) {
        if (!workerThread->isReady())
            continue;
        
        pWorker = workerThread;
        break;
    }

    if (pWorker == nullptr) {
        mThreads.emplace_back(std::make_shared<WorkerThread>());
        pWorker = mThreads.back();
    }

    if (pWorker == nullptr)
        throw MsgException("Can't create new worker thread for job");

    pWorker->DoWork(_job);
}

CmdWorker::WorkerThread::WorkerThread() {
    using namespace std::chrono_literals;

    std::thread([this]() {
        std::mutex workerMutex;
        std::unique_lock<std::mutex> workerLock(workerMutex);

        while (!mShutdown) {
            mWaitForJob.wait_for(workerLock, 10ms, [this] {
                return mShutdown || (mJobPending && mJob != nullptr);
            });
            
            if (!mJobPending || mJob == nullptr || mShutdown)
                continue;
            
            mWorking = true;

            mJob();
            mJob = nullptr;
            mJobPending = false;
            mWorking = false;
        }

        mShutdown = false;
    }).detach();
}

void CmdWorker::WorkerThread::DoWork(const std::function<void()>& _job) {
    if (!isReady())
        throw MsgException("Trying to start a job on a busy thread!");

    mJob = _job;
    mJobPending = true;
    mWaitForJob.notify_one();   
}