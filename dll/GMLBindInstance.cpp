#include "GMLBindInstance.h"

#include "rousrGML/GMLVal.h"
#include "rousrGML/GMLBuffer.h"
#include "GMLBind.h"

#include "luaRousr.h"
#include "LuaContext.h"

#include "GMLCmdLayer.h"

CGMLBindInstance::CGMLBindInstance(const std::string& _instanceName, double _gmlId) 
	: mName(_instanceName)
    , mGmlId(_gmlId)
{
    updateMembers();
}

size_t CGMLBindInstance::GetMemberIndex(const std::string& _memberName) {
    auto& bind(CLuaRousr::Bind());
    size_t memberIndex(bind.GetMemberIndex(_memberName));
    if (memberIndex == size_t_max) {
        // check custom binds
        auto itMember(mCustomMemberMap.find(_memberName));
        if (itMember != mCustomMemberMap.end()) {
            size_t bindCount(bind.GetMemberCount());
            memberIndex = itMember->second + bindCount;
        }
    }

    return memberIndex;
}

std::shared_ptr<IGMLLuaVal> CGMLBindInstance::GetMember(size_t _memberIndex) {
    UpdateReadIndex(); 
    
    if (_memberIndex == size_t_max)
        return nullptr;

    if (_memberIndex >= mVals.size()) {
        updateMembers();
        if (_memberIndex >= mVals.size())
            return nullptr;
    }

    return mVals[_memberIndex];
}

void CGMLBindInstance::updateMembers() {
    auto& bind(CLuaRousr::Bind());
    bind.UpdateInstanceMembers(mVals);
}

void CGMLBindInstance::UpdateReadIndex() {
    auto sequenceIndex(CLuaRousr::SequenceIndex());
    if (mReadSequence == sequenceIndex)
        return;
    
    mReadSequence = sequenceIndex;
    
    auto active(GMLCmdLayer::Active());
    auto cmdBuffer(active != nullptr ? active->CmdBuffer() : nullptr);
    if (!cmdBuffer)
        return;
   
    cmdBuffer->Seek(LuaRousr::SeekData);
    cmdBuffer->Write<int8_t>(static_cast<int8_t>(LuaRousr::Cmd::RequestSync));
    cmdBuffer->Write<uint32_t>(1u);  // todo: possibly cache more at once? not sure how.
    cmdBuffer->Write<double>(mGmlId);  // todo: possibly cache more at once? not sure how.
    active->SendBuffer();

    cmdBuffer->Seek(1);
    
    CGMLBind& bind(CLuaRousr::Bind());
    bind.SyncRead(*cmdBuffer);
}

size_t CGMLBindInstance::AddCustomMember(const std::string& _name, std::shared_ptr<IGMLLuaVal> _val, bool _readOnly) {
    size_t memberIndex(~0);
    auto itMember(mCustomMemberMap.find(_name));
    if (itMember == mCustomMemberMap.end()) {
        memberIndex = mCustomMembers.size();
        mCustomMembers.push_back(std::make_tuple(_name, _val, _readOnly));
        mCustomMemberMap[_name] = memberIndex;
    } else {
        memberIndex = itMember->second;
        auto& memberValue(mCustomMembers[memberIndex]);
        std::get<0>(memberValue) = _name;
        std::get<1>(memberValue) = _val;
        std::get<2>(memberValue) = _readOnly;
    }

    return memberIndex;
}

void CGMLBindInstance::MarkDirty(size_t _memberIndex) {
    mDirty.insert(_memberIndex);

    CGMLBind& bind(CLuaRousr::Bind());
    bind.MarkDirty(mIndex);
}

bool CGMLBindInstance::Read(CGMLBuffer& _buffer) {
    size_t valSize(mVals.size());
    if (mLastSync.size() != valSize)
        mLastSync.resize(valSize);

    for (uint32_t i = 0, updateCount(_buffer.Read<uint32_t>()); i < updateCount; ++i) {
        size_t valIndex(static_cast<size_t>(_buffer.Read<uint32_t>()));
        if (valIndex >= valSize) {
            mVals.resize(valIndex + 1);
            mLastSync.resize(valIndex + 1);
            valSize = valIndex + 1;
        }

        auto newVal(GMLLua::ReadVal(_buffer));
        if (mVals[valIndex] == nullptr || !mVals[valIndex]->Set(newVal))
            mVals[valIndex] = std::dynamic_pointer_cast<IGMLLuaVal>(newVal->Clone());

        if (mLastSync[valIndex] == nullptr || !mLastSync[valIndex]->Set(mVals[valIndex]))
            mLastSync[valIndex] = std::dynamic_pointer_cast<IGMLLuaVal>(mVals[valIndex]->Clone());
    }

    return true;
}

bool CGMLBindInstance::Write(CGMLBuffer& _buffer) {
    if (mDirty.empty())
        return false;
      
    size_t start(_buffer.Tell());
    _buffer.SeekOffset(sizeof(double));
    _buffer.SeekOffset(sizeof(uint32_t));
    uint32_t count(0);
    
    for (auto valIndex : mDirty) {
        if (valIndex >= mLastSync.size())
            mLastSync.resize(valIndex + 1);

        if (!mVals[valIndex]->Equals(mLastSync[valIndex])) {
            _buffer.Write<uint32_t>(valIndex);
            mVals[valIndex]->WriteBuffer(_buffer);
            if (mLastSync[valIndex] == nullptr || !mLastSync[valIndex]->Set(mVals[valIndex]))
                mLastSync[valIndex] = std::dynamic_pointer_cast<IGMLLuaVal>(mVals[valIndex]->Clone());
            ++count;
        }
    }
    mDirty.clear();
    size_t end(_buffer.Tell());
    _buffer.Seek(start);

    if (count == 0)
        return false;
    
    _buffer.Write<double>(mGmlId);
    _buffer.Write<uint32_t>(count);
    _buffer.Seek(end);
    return true;
}