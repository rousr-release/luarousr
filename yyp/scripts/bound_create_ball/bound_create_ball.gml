///@param x
///@param y
///@param delay_in_steps
var _data = array_create(ball_queue_data.__size);
_data[@ ball_queue_data.x] = argument0;
_data[@ ball_queue_data.y] = argument1;
_data[@ ball_queue_data.delay] = argument2;
ds_list_add(obj_game.ball_queue,_data);

if(obj_game.alarm[0] == -1){
	obj_game.alarm[0] = _data[@ ball_queue_data.delay];
}
