///@function __luaRousr_instance_builtin_get_image_number()
///@desc builtin getter for image_number
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_number;
