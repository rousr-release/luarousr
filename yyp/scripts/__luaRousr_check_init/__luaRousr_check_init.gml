///@function __luaRousr_check_init([_no_warn=false])
///@desc return true if we're ready to roll, warn if not initialized
///@param {Boolean} [_no_warn=false]   pass true it suppress warning
///@returns {Boolean} true if luaRousr is initialized
///@extensionizer { "docs": false }
gml_pragma("forceinline");

with (LuaSr)
   return true;

if (argument_count == 0 || !argument[0])
  show_debug_message("luaRousr Error - not initialized");

return false;