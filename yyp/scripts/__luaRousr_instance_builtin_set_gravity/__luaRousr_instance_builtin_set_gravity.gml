///@function __luaRousr_instance_builtin_set_gravity(_val)
///@desc builtin setter for gravity
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
gravity = argument0;
