///@function __luaRousr_instance_builtin_get_path_orientation()
///@desc builtin getter for path_orientation
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_orientation;
