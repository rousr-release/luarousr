// cppgenerator.js
//   babyjeans
//
///
var tab = '\t';
	
class CPPGenerator {
	constructor(boundResource) {
		let vcxConfig = boundResource.vcxConfig;
		this.resource = boundResource;
		this.className = boundResource.className;
		
		let cppPath   = vcxConfig.exportPath;
		this.fileName   = boundResource.cppFileName + ".cpp";
		this.filePath   = cppPath + this.fileName;

		let luaName = boundResource.luaName;
		let className = this.className;

		this.idStr = "";
		if (this.resource.hasId) this.idStr = ", m_id";

		this.cppHeading = "";
		this.luaRegistrations = "";
		this.cppBody = "";
		this.endLua = "";

		this.cppHeading += "#include \"" + this.resource.cppFileName + ".h\"\n\n"
		this.cppHeading += "#include <stdio.h>\n\n"
		this.cppHeading += "#include \"LuaIntf/LuaIntf.h\"\n";
		this.cppHeading += "#include \"../LuaError.h\"\n";
		this.cppHeading += "#include \"../GMLLib.h\"\n";
		this.cppHeading += "#include \"../GMLEvent.h\"\n";
		this.cppHeading += "#include \"../GMLDSMap.h\"\n";
		this.cppHeading += "#include \"../GMLVal.h\"\n\n";

		this.luaRegistrations += "void " + className + "::RegisterLua(CLuaRousr& _Lua) {\n";
		this.luaRegistrations += tab + "LuaIntf::LuaState& L(*_Lua.L);\n"
		this.luaRegistrations += tab + "LuaIntf::LuaBinding(L).";
			
		if (this.resource.type == "module") {
			this.luaRegistrations += "beginModule(\"" + (this.resource.module || className) + "\")\n";
			this.endLua +=  tab + ".endModule()";
		} else {
			this.endLua += tab;
			this.endLua += ".endClass()";
			if (this.resource.module) {
				this.luaRegistrations += "beginModule(\"" + this.resource.module + "\").";
				this.endLua += ".endModule()";
			} 
			this.luaRegistrations += "beginClass<" + className + ">(\"" + luaName + "\")\n";
		}
		this.endLua += ";\n}\n\n";
	}

	addLuaRegistration(v) {
		let regLua = tab + tab + ".addProperty(\"" + v.name + "\", &" + this.className + "::" + v.name;
		if (!v.flags.readonly) regLua += ", " + "&" + this.className + "::Set" + v.name;
		regLua += ")\n";
		return regLua;
	}

	addCppGet(v) {
		let resourceIndex = this.resource.index;
		let bindingIndex  = v.bindingIndex;
		
		let memberName = "m_" + v.name;

		var cpp = "// variable: " + v.name + "\n";
		cpp += v.getReturnType + " " + this.className + "::" + v.name + "() {\n";
					
		// CGMLEvent(resourceIndex, bindingIndex, m_id).Trigger([this](double/string& _val) { memberName = _val; });
		cpp += tab + "CGMLEvent(" + resourceIndex + ", " + bindingIndex + this.idStr + ").Trigger([this](";
		
		// set string / double
		if (v.casts.castFrom == null) cpp += v.getReturnType + " _val";
		else                          cpp += v.casts.castFrom   + " _val";

		cpp += ") { " + memberName + " = ";

		if (v.casts.castTo == null) cpp += "_val; });\n";
		else                        cpp += "static_cast<" + v.casts.castTo + ">(_val); });\n";

		cpp += tab + "return " + memberName + ";\n";
		cpp += "}\n";

		return cpp;
	}			


	addCppSet(v) {
		let cpp = "";
		if (!v.flags.readonly) {
			let resourceIndex = this.resource.index;
			let bindingIndex  = v.bindingIndex + 1;

			cpp  = "void " + this.className +"::Set" + v.name + "(const " + v.setReturnType + " _val) {\n";
			cpp += tab + "CGMLEvent(" + resourceIndex + ", " + bindingIndex + this.idStr + ")\n";
			cpp += tab + tab + ".Add(\"arg0\", _val)\n";
			cpp += tab + tab + ".Trigger();\n";
			cpp += tab + "m_" + v.name + " = _val;\n";
			cpp += "}\n";
		}

		return cpp;
	}

	processVariable(v) {
		this.luaRegistrations += this.addLuaRegistration(v);
		this.cppBody          += this.addCppGet(v);
		this.cppBody          += this.addCppSet(v);

		// put a space between each variable
		this.cppBody += '\n';
	}

	processFunction(f) {
		let returnType = f.return.getReturnType;
		if (f.return.resource)
			returnType = "std::shared_ptr<" + f.return.resource.className + ">";

		// function signature
		let cppBinding = returnType + " " + this.className + "::" + f.cppName + "(";
	
		for (let argIndex = 0; argIndex < f.args.length; ++argIndex) {
			let argType = f.args[argIndex];
			cppBinding += argType.setReturnType + " ";
			cppBinding += "_arg" + argIndex;
			if (argIndex + 1 < f.args.length) cppBinding += ", ";
		}
	
		cppBinding += ") {\n";
			
			// function body
		if (returnType != "void") cppBinding += tab + returnType + " retVal;\n";
		cppBinding += tab + "CGMLEvent(" + this.resource.index + ", " + f.bindingIndex + ")\n" // TriggerType
		for (let argIndex = 0; argIndex < f.args.length; ++argIndex) {
			cppBinding += tab + tab + ".Add(\"arg" + argIndex + "\", _arg" + argIndex + ")\n";
		}

		if (returnType == "void") {
			cppBinding += tab + tab + ".Trigger();\n";
		} else {

			let triggerParamType = f.return.casts.castFrom || f.return.getReturnType;
			cppBinding += tab + tab + ".Trigger([&retVal](" + triggerParamType + "& _val) { retVal = ";
			let val = "_val"; 
			if (f.return.casts.castTo) val = "static_cast<" + f.return.casts.castTo + ">(_val)";
			if (f.return.resource) val = "std::make_shared<" + f.return.resource.className + ">(static_cast<int>(_val))";

			cppBinding += val + "; });\n";
			cppBinding += tab + "return retVal;\n";
		}
		cppBinding += "}\n\n";
		
		// Lua registration
		let add = ".addFunction(";
		if (f.isStatic) add = ".addStaticFunction(";
		let luaRegistration = tab + tab + add + "\"" + f.luaName + "\", &" + this.className + "::" + f.cppName + ")\n";

		this.cppBody += cppBinding;
		this.luaRegistrations += luaRegistration;
	}

	generate() {
		return this.cppHeading + this.luaRegistrations + this.endLua + this.cppBody;
	}


// // variable: sprite_index
// sprite CGMLinstance::sprite_index() {
//         CGMLEvent(2, 25, m_id).Trigger([this](sprite _val) { m_sprite_index = _val; });
//         return m_sprite_index;
// }
// void CGMLinstance::Setsprite_index(const sprite _val) {
//         CGMLEvent(2, 26, m_id)
//                 .Add("arg0", _val)
//                 .Trigger();
//         m_sprite_index = _val;
// }

}

module.exports = CPPGenerator;