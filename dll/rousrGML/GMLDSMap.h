#pragma once
#include "rousrGML.h"

// From https://help.yoyogames.com/hc/en-us/articles/216755258-Returning-Values-From-An-Extension-Asynchronously-GMS-v1-3-
#define EVENT_OTHER_SOCIAL (70)	

enum class GMLConstant : int {
	Noone = -4,
	Undefined = -5,
};

class IGMLVal;
template<typename T> class TGMLVal;

class CGMLDSMap {
public:
	CGMLDSMap() { ;  }
	virtual ~CGMLDSMap() { ;  }

    static bool IsRegistered() { return mRegistered; }

	virtual void AddDouble(const std::string& _key, double _val);
	virtual void AddString(const std::string& _key, const std::string& _val);

	virtual void TriggerAsyncEvent(int _event = EVENT_OTHER_SOCIAL);
	
	using MapData = std::tuple<std::shared_ptr<TGMLVal<std::string>>, std::shared_ptr<IGMLVal>>;
	std::map<std::string, MapData>& GetMap()             { return mMap; }
	const std::map<std::string, MapData>& GetMap() const { return mMap; }

	static void RegisterCallbacks(char*, char*, char*, char*);

	static void WriteDouble(int _mapId, const std::string& _key, double _val)              { DSMapAddDouble(_mapId, const_cast<char*>(_key.c_str()), _val); }
	static void WriteString(int _mapId, const std::string& _key, const std::string& _val)  { DSMapAddString(_mapId, const_cast<char*>(_key.c_str()),  const_cast<char*>(_val.c_str())); }
		
private:
	std::map<std::string, MapData> mMap;
	
	using FnCreateAsyncEventWithDSMap = void(*)(int, int);
	using FnCreateDsMap = int(*)(int, ...);
	using FnDsMapAddDouble = bool(*)(int, char*, double);
	using FnDsMapAddString = bool(*)(int, char*, char*);

	static bool mRegistered;
	static FnCreateAsyncEventWithDSMap CreateAsyncEventWithDSMap;
	static FnCreateDsMap               CreateDSMap;
	static FnDsMapAddDouble            DSMapAddDouble;
	static FnDsMapAddString            DSMapAddString;
};
