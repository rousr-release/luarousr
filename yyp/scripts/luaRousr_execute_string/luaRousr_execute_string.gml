///@function lua_rousr_executeString(_script)
///@description Execute Lua script
///@param {String} _script   string containing Lua script
///@returns {Boolean} true on call successfully returned
if (!__luaRousr_check_init())
  return undefined;

var _luaScript = argument0;
var cmd_buffer = __luaRousr_pop_buffer();
var cmdId = __extLuaRousrExecuteLua(buffer_get_address(cmd_buffer), _luaScript);
__luaRousr_process_buffer(cmd_buffer, cmdId);
__luaRousr_push_buffer(cmd_buffer);

return cmdId >= 0;