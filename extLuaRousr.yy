{
	"id": "66446719-3655-406f-8f56-0eb5f225f9cf",
	"modelName": "GMExtension",
	"mvc": "1.0",
	"name": "extLuaRousr",
	"IncludedResources": [],
	"androidPermissions": [],
	"androidProps": false,
	"androidactivityinject": "",
	"androidclassname": "",
	"androidinject": "",
	"androidmanifestinject": "",
	"androidsourcedir": "",
	"author": "",
	"classname": "",
	"copyToTargets": 194,
	"date": "2017-13-06 02:02:32",
	"description": "",
	"extensionName": "",
	"files": [
		{
			"id": "57ac44a2-3c2d-4c88-a0eb-30158ce221ab",
			"modelName": "GMExtensionFile",
			"mvc": "1.0",
			"ProxyFiles": [
				{
					"id": "063599ed-bb2c-4fc3-ae44-357f81f4f992",
					"modelName": "GMProxyFile",
					"mvc": "1.0",
					"TargetMask": 1,
					"proxyName": "rousrlua"
				}
			],
			"constants": [],
			"copyToTargets": 194,
			"filename": "rousrlua.dll",
			"final": "",
			"functions": [
				{
					"id": "ea9493af-7ed6-4eb7-8f0d-6b2a2291fd47",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "",
					"help": "nofunction",
					"hidden": false,
					"kind": 1,
					"name": "_dummyFunc",
					"returnType": 1
				},
				{
					"id": "e843a18e-9f6f-4254-a5b8-981d59592464",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "Init",
					"help": "Internally Used: pass the command buffer to Lua. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrInit",
					"returnType": 2
				},
				{
					"id": "851819f1-1008-47e9-beba-c79dfec96fd1",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						1
					],
					"externalName": "ExecuteLua",
					"help": "Internally used: Run the string passed to Lua as Lua script. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrExecuteLua",
					"returnType": 2
				},
				{
					"id": "daf82077-9959-461a-913e-55423db9b2f3",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						1,
						1,
						1
					],
					"externalName": "RegisterCallbacks",
					"help": "",
					"hidden": false,
					"kind": 1,
					"name": "RegisterCallbacks",
					"returnType": 2
				},
				{
					"id": "d4c73b36-0cfb-4f26-b365-4543610753b6",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "CleanUp",
					"help": "Internally used: clean up the background processes. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrCleanUp",
					"returnType": 2
				},
				{
					"id": "6c211c24-04ec-41a7-b98c-78e9b44f14d2",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						1,
						2,
						1
					],
					"externalName": "CallLua",
					"help": "Internally used: call's a lua function and returns the values by writing them to the passed buffer. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrCallLua",
					"returnType": 2
				},
				{
					"id": "531c47a7-c6df-4458-ae6a-e12a7bb6cc28",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2
					],
					"externalName": "BindGMLFunction",
					"help": "Internally used: Bind a GML script Id to a named Lua function. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrBindScriptFunction",
					"returnType": 2
				},
				{
					"id": "17e41819-d1a2-4546-9124-27f451c975d2",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1
					],
					"externalName": "ResumeThreads",
					"help": "Internally used: called every step to update each lua coroutine or thread. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrResumeThreads",
					"returnType": 2
				},
				{
					"id": "0b164d8b-5650-4716-862b-bf68ebe66767",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "KillThreads",
					"help": "Internally used: kill all threads. returns -1.0 on failure",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrKillThreads",
					"returnType": 2
				},
				{
					"id": "c6097444-1f7f-427e-b0bc-607b4a5c5495",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2
					],
					"externalName": "BindInstance",
					"help": "Internal Use: bind an instance",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrBindInstance",
					"returnType": 2
				},
				{
					"id": "f6c34a65-8f21-467a-a691-98537cd08902",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1
					],
					"externalName": "BindMember",
					"help": "internal use: binds a named \"member\" value to a bindId",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrBindMember",
					"returnType": 2
				},
				{
					"id": "d48c70a2-ae3f-486c-b246-8d696f914912",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "UnbindInstance",
					"help": "",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrUnbindInstance",
					"returnType": 2
				},
				{
					"id": "b04f759e-8448-4a7c-8608-c8372be1f394",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2,
						2,
						1
					],
					"externalName": "CallLuaScriptId",
					"help": "Internally used: call a lua script callback ID given to us from Lua",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrCallLuaScriptId",
					"returnType": 2
				},
				{
					"id": "a156357c-83de-4ce1-9bab-4f9fcf9c0229",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "WaitForLua",
					"help": "Internally Used: Waits for the cmd buffer to be ready - returns 0 if no Lua initialized.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrWaitForLua",
					"returnType": 2
				},
				{
					"id": "21c819c4-5f20-41a3-9ad8-34a11aeb65fe",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "ReadyForLua",
					"help": "Internally Used: signal the buffer is ready for Lua to write to",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrReadyForLua",
					"returnType": 2
				},
				{
					"id": "1b7d9f6c-9479-4205-892c-80fb48bc5daf",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "ReleaseCallback",
					"help": "Internally Used: release a callback id",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrReleaseScriptCallback",
					"returnType": 2
				},
				{
					"id": "75974402-894e-44d0-9bef-0c27c2712190",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "RetainCallback",
					"help": "Internally Used: increment the callback ref count",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrRetainScriptCallback",
					"returnType": 2
				},
				{
					"id": "776e76f2-c38b-4101-bc3c-8f3d204b9aaa",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "CollectGarbage",
					"help": "Internally Used: collects garbage - currently, no longer ref'd scripts. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrCollectGarbage",
					"returnType": 2
				},
				{
					"id": "d578f0ad-fe61-4e86-815a-34f9c9e69fb1",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2,
						2,
						1
					],
					"externalName": "BindCustomMember",
					"help": "Internally Used: bind a custom member to a context. Returns memberIndex or -1",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrBindCustomMember",
					"returnType": 2
				},
				{
					"id": "28ef3b86-c61a-427e-b2a2-b13e82acb064",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2
					],
					"externalName": "SetGlobalDbl",
					"help": "sets a global with a double val",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrSetGlobalReal",
					"returnType": 2
				},
				{
					"id": "d7d13b07-debd-419d-b73a-5a53c01e5a1b",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						1
					],
					"externalName": "SetGlobalString",
					"help": "sets a global with a string type",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrSetGlobalString",
					"returnType": 2
				},
				{
					"id": "2c56504d-365a-4c23-95a4-e8054fdf3d3c",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2,
						1
					],
					"externalName": "GetGlobal",
					"help": "returns a global value",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrGetGlobal",
					"returnType": 2
				},
				{
					"id": "775dc6cf-8ea5-423b-82d6-8287fe5d8258",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "Finished",
					"help": "",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrFinished",
					"returnType": 2
				},
				{
					"id": "d2cbc024-dc3d-4cb3-9238-16ea59245fec",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "HasThreads",
					"help": "returns 1.0 if there's threads pending",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrHasThreads",
					"returnType": 2
				}
			],
			"init": "",
			"kind": 1,
			"order": [
				"ea9493af-7ed6-4eb7-8f0d-6b2a2291fd47",
				"daf82077-9959-461a-913e-55423db9b2f3",
				"e843a18e-9f6f-4254-a5b8-981d59592464",
				"776e76f2-c38b-4101-bc3c-8f3d204b9aaa",
				"17e41819-d1a2-4546-9124-27f451c975d2",
				"851819f1-1008-47e9-beba-c79dfec96fd1",
				"6c211c24-04ec-41a7-b98c-78e9b44f14d2",
				"b04f759e-8448-4a7c-8608-c8372be1f394",
				"a156357c-83de-4ce1-9bab-4f9fcf9c0229",
				"21c819c4-5f20-41a3-9ad8-34a11aeb65fe",
				"531c47a7-c6df-4458-ae6a-e12a7bb6cc28",
				"c6097444-1f7f-427e-b0bc-607b4a5c5495",
				"f6c34a65-8f21-467a-a691-98537cd08902",
				"d578f0ad-fe61-4e86-815a-34f9c9e69fb1",
				"d48c70a2-ae3f-486c-b246-8d696f914912",
				"75974402-894e-44d0-9bef-0c27c2712190",
				"1b7d9f6c-9479-4205-892c-80fb48bc5daf",
				"d4c73b36-0cfb-4f26-b365-4543610753b6",
				"0b164d8b-5650-4716-862b-bf68ebe66767",
				"28ef3b86-c61a-427e-b2a2-b13e82acb064",
				"d7d13b07-debd-419d-b73a-5a53c01e5a1b",
				"2c56504d-365a-4c23-95a4-e8054fdf3d3c",
				"775dc6cf-8ea5-423b-82d6-8287fe5d8258",
				"d2cbc024-dc3d-4cb3-9238-16ea59245fec"
			],
			"origname": "",
			"uncompress": false
		},
		{
			"id": "b809dbab-ac82-46c6-9bf5-0c47f61abcb1",
			"modelName": "GMExtensionFile",
			"mvc": "1.0",
			"ProxyFiles": [],
			"constants": [],
			"copyToTargets": 35184372089026,
			"filename": "extLuaRousr.gml",
			"final": "",
			"functions": [],
			"init": "",
			"kind": 2,
			"order": [],
			"origname": "",
			"uncompress": false
		},
		{
			"id": "30a739ee-78e0-4053-812a-290cc84a06b6",
			"modelName": "GMExtensionFile",
			"mvc": "1.0",
			"ProxyFiles": [],
			"constants": [],
			"copyToTargets": 194,
			"filename": "rousrlua.dylib",
			"final": "",
			"functions": [
				{
					"id": "1a36b938-b68d-4b2d-97d0-e6145385157a",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "",
					"help": "nofunction",
					"hidden": false,
					"kind": 1,
					"name": "_dummyFunc",
					"returnType": 1
				},
				{
					"id": "593fa4de-03a6-4eca-ae5c-07c114f58c7d",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "Init",
					"help": "Internally Used: pass the command buffer to Lua. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrInit",
					"returnType": 2
				},
				{
					"id": "d2ff52b7-0b6d-431a-bd52-3f03dda58d1b",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						1
					],
					"externalName": "ExecuteLua",
					"help": "Internally used: Run the string passed to Lua as Lua script. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrExecuteLua",
					"returnType": 2
				},
				{
					"id": "feb2cac8-3666-46d3-9c65-6ebe9886b954",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						1,
						1,
						1
					],
					"externalName": "RegisterCallbacks",
					"help": "",
					"hidden": false,
					"kind": 1,
					"name": "RegisterCallbacks",
					"returnType": 2
				},
				{
					"id": "d1afbce1-2936-4d61-b0f4-78df386239ee",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "CleanUp",
					"help": "Internally used: clean up the background processes. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrCleanUp",
					"returnType": 2
				},
				{
					"id": "d9a43338-93a1-4830-835b-10cd6c3f430a",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						1,
						2,
						1
					],
					"externalName": "CallLua",
					"help": "Internally used: call's a lua function and returns the values by writing them to the passed buffer. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrCallLua",
					"returnType": 2
				},
				{
					"id": "ad91be45-b8c6-4b6d-8194-616e90858e3a",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2
					],
					"externalName": "BindGMLFunction",
					"help": "Internally used: Bind a GML script Id to a named Lua function. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrBindScriptFunction",
					"returnType": 2
				},
				{
					"id": "323d1891-f2c7-41bb-8064-b359d0965b6c",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1
					],
					"externalName": "ResumeThreads",
					"help": "Internally used: called every step to update each lua coroutine or thread. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrResumeThreads",
					"returnType": 2
				},
				{
					"id": "9bbb00a1-317b-40fc-80fb-784d87f58253",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "KillThreads",
					"help": "Internally used: kill all threads. returns -1.0 on failure",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrKillThreads",
					"returnType": 2
				},
				{
					"id": "a4477e41-1ef3-4119-a755-6d6415b1a437",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2
					],
					"externalName": "BindInstance",
					"help": "Internal Use: bind an instance",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrBindInstance",
					"returnType": 2
				},
				{
					"id": "132683ef-e1b8-48d6-870c-df4fd767cea5",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1
					],
					"externalName": "BindMember",
					"help": "internal use: binds a named \"member\" value to a bindId",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrBindMember",
					"returnType": 2
				},
				{
					"id": "63d7cd8a-26fc-4b7f-986c-7809f8715f9c",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "UnbindInstance",
					"help": "",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrUnbindInstance",
					"returnType": 2
				},
				{
					"id": "5274234f-669e-4bf9-b46e-422817ff1db4",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2,
						2,
						1
					],
					"externalName": "CallLuaScriptId",
					"help": "Internally used: call a lua script callback ID given to us from Lua",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrCallLuaScriptId",
					"returnType": 2
				},
				{
					"id": "79fda282-484b-4f85-a30f-58db3b15e638",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "WaitForLua",
					"help": "Internally Used: Waits for the cmd buffer to be ready - returns 0 if no Lua initialized.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrWaitForLua",
					"returnType": 2
				},
				{
					"id": "ea81c811-4613-4866-b865-57f2ac8c9964",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "ReadyForLua",
					"help": "Internally Used: signal the buffer is ready for Lua to write to",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrReadyForLua",
					"returnType": 2
				},
				{
					"id": "178157ba-5adc-4a92-976a-9389a56d1044",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "ReleaseCallback",
					"help": "Internally Used: release a callback id",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrReleaseScriptCallback",
					"returnType": 2
				},
				{
					"id": "1fa02572-e41b-4eda-92cf-fe3615f08f7e",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "RetainCallback",
					"help": "Internally Used: increment the callback ref count",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrRetainScriptCallback",
					"returnType": 2
				},
				{
					"id": "935f607b-5520-4201-9483-a9c38add0ceb",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "CollectGarbage",
					"help": "Internally Used: collects garbage - currently, no longer ref'd scripts. Returns -1.0 on failure.",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrCollectGarbage",
					"returnType": 2
				},
				{
					"id": "b923aa72-34f0-4559-a2a9-d83ea821d8a2",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2,
						2,
						1
					],
					"externalName": "BindCustomMember",
					"help": "Internally Used: bind a custom member to a context. Returns memberIndex or -1",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrBindCustomMember",
					"returnType": 2
				},
				{
					"id": "1be036f5-a7ba-47c6-afa0-80c256cf3d19",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2
					],
					"externalName": "SetGlobalDbl",
					"help": "sets a global with a double val",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrSetGlobalReal",
					"returnType": 2
				},
				{
					"id": "44e1929d-3a30-4ff0-acdb-9e9ff650e6a9",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						1
					],
					"externalName": "SetGlobalString",
					"help": "sets a global with a string type",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrSetGlobalString",
					"returnType": 2
				},
				{
					"id": "68a6859c-fbd3-47fd-929c-754458935d45",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						1,
						2,
						1
					],
					"externalName": "GetGlobal",
					"help": "returns a global value",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrGetGlobal",
					"returnType": 2
				},
				{
					"id": "38239ea7-c767-46c2-99ac-fd00c49ce08c",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [
						2
					],
					"externalName": "Finished",
					"help": "",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrFinished",
					"returnType": 2
				},
				{
					"id": "6e23902c-b4f7-4130-a327-066b3755a300",
					"modelName": "GMExtensionFunction",
					"mvc": "1.0",
					"argCount": 0,
					"args": [],
					"externalName": "HasThreads",
					"help": "returns 1.0 if there's threads pending",
					"hidden": false,
					"kind": 1,
					"name": "__extLuaRousrHasThreads",
					"returnType": 2
				}
			],
			"init": "",
			"kind": 1,
			"order": [
				"1a36b938-b68d-4b2d-97d0-e6145385157a",
				"593fa4de-03a6-4eca-ae5c-07c114f58c7d",
				"d2ff52b7-0b6d-431a-bd52-3f03dda58d1b",
				"feb2cac8-3666-46d3-9c65-6ebe9886b954",
				"d1afbce1-2936-4d61-b0f4-78df386239ee",
				"d9a43338-93a1-4830-835b-10cd6c3f430a",
				"ad91be45-b8c6-4b6d-8194-616e90858e3a",
				"323d1891-f2c7-41bb-8064-b359d0965b6c",
				"9bbb00a1-317b-40fc-80fb-784d87f58253",
				"a4477e41-1ef3-4119-a755-6d6415b1a437",
				"132683ef-e1b8-48d6-870c-df4fd767cea5",
				"63d7cd8a-26fc-4b7f-986c-7809f8715f9c",
				"5274234f-669e-4bf9-b46e-422817ff1db4",
				"79fda282-484b-4f85-a30f-58db3b15e638",
				"ea81c811-4613-4866-b865-57f2ac8c9964",
				"178157ba-5adc-4a92-976a-9389a56d1044",
				"1fa02572-e41b-4eda-92cf-fe3615f08f7e",
				"935f607b-5520-4201-9483-a9c38add0ceb",
				"b923aa72-34f0-4559-a2a9-d83ea821d8a2",
				"1be036f5-a7ba-47c6-afa0-80c256cf3d19",
				"44e1929d-3a30-4ff0-acdb-9e9ff650e6a9",
				"68a6859c-fbd3-47fd-929c-754458935d45",
				"38239ea7-c767-46c2-99ac-fd00c49ce08c",
				"6e23902c-b4f7-4130-a327-066b3755a300"
			],
			"origname": "",
			"uncompress": false
		}
	],
	"gradleinject": "",
	"helpfile": "",
	"installdir": "",
	"iosProps": false,
	"iosSystemFrameworkEntries": [],
	"iosThirdPartyFrameworkEntries": [],
	"iosplistinject": "",
	"license": "",
	"maccompilerflags": "",
	"maclinkerflags": "",
	"macsourcedir": "",
	"packageID": "",
	"productID": "",
	"sourcedir": "",
	"version": "1.1.1"
}