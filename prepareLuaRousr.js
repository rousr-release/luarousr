// buildImGuiGML.js
//  babyjeans
///
let scryyptPath = "../scryypt/";

const fs               = require("fs-extra");
const child_process    = require('child_process');
const MarkDownIt       = require('markdown-it');
const MarkDownItAnchor = require('./submission/scripts/markdown-it-anchor.js');
const YYP              = require(scryyptPath + "script/gms2/yyp.js");

let DoCopy           = true;
let DoBuildExt       = true;
let DoBuildReadMe    = true;
let DoMarkdownReadMe = true;

if (DoCopy) {
    console.log("Removing old submission yyp...");
    child_process.execSync('rm -rf submission/yyp'); 
    console.log("Copying yyp to submission...");
    child_process.execSync('cp -R yyp submission');
}

if (DoBuildExt) {
    console.log("Building gml and yy for luaRousr...");
    child_process.execSync("node " + scryyptPath + "scryypt.js --extensionize luaRousr.json yyp/luaRousr.yyp submission/yyp/extensions/extLuaRousr/extLuaRousr.gml");
    console.log("Moving reference doc");
    child_process.execSync('mv submission/yyp/extensions/extLuaRousr/extLuaRousr.md ./submission/README_ref.md')
}

let readme = "";
if (DoBuildReadMe) {
    console.log("Building README")
    let readme_top    = fs.readFileSync("submission/README_doc.md");
    let readme_ref    = fs.readFileSync("submission/README_ref.md");
    let readme_bottom = fs.readFileSync("submission/README_changelist.md");

    console.log("  - writing README.md");
    readme = readme_top + readme_ref + readme_bottom;
    fs.writeFileSync("README.md", readme);

    console.log("  - writing luaRousr_read_me.gml");
    let readmegml = "/*\n" + readme_top + readme_ref + readme_bottom + "\n*/";
    fs.writeFileSync("yyp/scripts/luaRousr_read_me/luaRousr_read_me.gml", readmegml);
    
    console.log("Copying yyp/scripts/luaRousr_read_me/luaRousr_read_me.gml...")
    child_process.execSync("cp yyp/scripts/luaRousr_read_me/luaRousr_read_me.gml submission/yyp/scripts/luaRousr_read_me/luaRousr_read_me.gml");
}

if (DoBuildReadMe && DoMarkdownReadMe) {
    console.log("Updating http://lua.rou.sr/")
    let md = MarkDownIt({html:true}).use(MarkDownItAnchor, { });

    let indexhtml = md.render(readme);
    fs.writeFileSync("index.html", indexhtml);
}

console.log("TODO:\n" +
    "   *  `scp index.html grogshack:/home/webhost/lua.rou.sr/index.html`\n" +
    "   *  `start submission/yyp/luaRousr.yyp'\n" +
    "   *  Remove Scripts from submission yyp\n" + 
    "   *  Upload new yymp\n" +
    "      *  Upload to itch.io\n" +
    "      *  Update http://forum.rou.sr/\n" +
    "      *  Update http://babyjeans.rou.sr/\n" +
    "      *  Update gmc forum\n" +
    "   *  Build new demo (optional)"
);  
