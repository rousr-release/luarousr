{
    "id": "869e5be6-f7dc-467c-8dd9-5be8587f42b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWhiteBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b648f560-e583-43f0-94da-3d977cfa9f16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "869e5be6-f7dc-467c-8dd9-5be8587f42b2",
            "compositeImage": {
                "id": "e589f69f-d29e-4cbf-9707-cf2034f11371",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b648f560-e583-43f0-94da-3d977cfa9f16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cae31494-343e-4d95-bf6e-df420a0d0bb7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b648f560-e583-43f0-94da-3d977cfa9f16",
                    "LayerId": "25f688dc-59b9-4b27-880f-f982210c59fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "25f688dc-59b9-4b27-880f-f982210c59fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "869e5be6-f7dc-467c-8dd9-5be8587f42b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 0,
    "yorig": 0
}