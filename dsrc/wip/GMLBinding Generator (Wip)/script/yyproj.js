// yyproj.js
//  babyjeans
//
///
var fs = require('fs');
var GMLGenerator = require('./gmlgenerator.js')
var tab = '  '

class YYProj {
	constructor(generator) {
		this.boundResources = [ ];
		this.gmlConfig = generator.gmlConfig;
	}

	addResource(boundResource) {
		this.boundResources[this.boundResources.length] = boundResource;
	}

	generateGMLCaller() {
		let gmlCalls = "";
		let gmlConfig = this.gmlConfig;

		for (let r in this.boundResources) 
			gmlCalls += tab + this.boundResources[r].generateGMLCall() + '\n';

		var callScript = "";
		callScript += "///@desc Process the binding appropriately\n";
		callScript += "///NOTE: This is a generated file\n";
		callScript += "///@param async_load\n";
		callScript += "var _data = argument0;\n";
		callScript += "\n";
		callScript += "// call info\n";
		callScript += "var _callId      = _data[? \"_callId\"];\n";
		callScript += "var _gmlResource = _data[? \"_gmlResource\"];\n";
		callScript += "var _bind        = _data[? \"_bind\"];\n";
		callScript += "\n";
		callScript += "// arguments\n";
		callScript += "var _id       = _data[? \"_id\"];\n";
		callScript += "var argRead = array_create(32);\n";
		callScript += "var arg  = async_load[? \"arg0\"];\n";
		callScript += "var argCount = 0;\n";
		callScript += "while(arg != undefined) { \n";
		callScript += "  argRead[argCount] = arg;\n";
		callScript += "  argCount++;\n";
		callScript += "  arg = async_load[? \"arg\" + string(argCount)];\n";
		callScript += "}\n";
		callScript += "\n";
		callScript += "var args = array_create(argCount);\n";
		callScript += "array_copy(args, 0, argRead, 0, argCount);\n";
		callScript += "\n";
		callScript += "switch (_gmlResource) {\n";
		callScript += gmlCalls;
		callScript += "  default:break;\n"
		callScript += "}\n\n";

		let callFile = GMLGenerator.getGMLPath(gmlConfig.callName, gmlConfig, true);
		try {
		//	fs.writeFileSync(gmlConfig.callFile, callScript);
			console.log("\tGenerated: " + callFile);
		} catch(err) {
			console.log(err);
		}
	}

	updateYYP() {
		// TODO
	}

	generate() {
		// generate call script
		this.generateGMLCaller();
		// update project
		this.updateYYP();
	}
}

module.exports = YYProj;