{
    "id": "a17d594a-7b77-45f3-b6e6-0539cd6ee977",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "outsideTheBox",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 194,
    "date": "2017-21-05 08:02:26",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "c5df2406-c6f0-4bf2-afd9-d029de455bd6",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                {
                    "id": "d06c4824-135c-4e60-ace7-b57b672227bc",
                    "modelName": "GMProxyFile",
                    "mvc": "1.0",
                    "TargetMask": 1,
                    "proxyName": "otb"
                }
            ],
            "constants": [
                
            ],
            "copyToTargets": 194,
            "filename": "otb.dll",
            "final": "",
            "functions": [
                {
                    "id": "8d5f0963-d7b2-40f7-b878-29e051b4865a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "nofunc",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__dummyFunc2",
                    "returnType": 1
                },
                {
                    "id": "43ddba6a-1569-4ae1-b2dd-d8cea84f37c6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "OSTBInit",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__oTB_init",
                    "returnType": 2
                },
                {
                    "id": "041258fd-1325-4495-a0e1-fb2766fadba1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "OSTBOpenFile",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__oTB_openFile",
                    "returnType": 2
                },
                {
                    "id": "519d8a34-7944-4dea-8a5a-0215decd15ee",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "OSTBGetFilename",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__oTB_getFileName",
                    "returnType": 1
                },
                {
                    "id": "9463c021-7dbd-42ef-8b00-5f94d1e26ab9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "OSTBCloseFile",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__oTB_closeFile",
                    "returnType": 2
                },
                {
                    "id": "f6dbe525-b1a7-4f83-b30b-f833bc06f386",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "GetExecutablePath",
                    "help": "Returns a string to where the executable is running from",
                    "hidden": false,
                    "kind": 1,
                    "name": "get_executable_path",
                    "returnType": 1
                },
                {
                    "id": "3248fc85-ae50-41f7-88a6-cdd5ebb25138",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "GetAppDataPath",
                    "help": "Returns a string to the path that the app data is saved to",
                    "hidden": false,
                    "kind": 1,
                    "name": "__oTB_getAppDataPath",
                    "returnType": 1
                }
            ],
            "init": "",
            "kind": 1,
            "order": [
                "8d5f0963-d7b2-40f7-b878-29e051b4865a",
                "43ddba6a-1569-4ae1-b2dd-d8cea84f37c6",
                "041258fd-1325-4495-a0e1-fb2766fadba1",
                "9463c021-7dbd-42ef-8b00-5f94d1e26ab9",
                "519d8a34-7944-4dea-8a5a-0215decd15ee",
                "3248fc85-ae50-41f7-88a6-cdd5ebb25138",
                "f6dbe525-b1a7-4f83-b30b-f833bc06f386"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "0.10.2"
}