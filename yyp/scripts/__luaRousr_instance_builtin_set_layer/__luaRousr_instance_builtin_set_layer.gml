///@function __luaRousr_instance_builtin_set_layer(_val)
///@desc builtin setter for layer
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
layer = argument0;
