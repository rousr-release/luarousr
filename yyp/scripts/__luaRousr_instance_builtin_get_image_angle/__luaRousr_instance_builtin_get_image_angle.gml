///@function __luaRousr_instance_builtin_get_image_angle()
///@desc builtin getter for image_angle
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_angle;
