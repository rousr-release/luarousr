///@function __luaRousr_instance_builtin_get_image_speed()
///@desc builtin getter for image_speed
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_speed;
