///@function __luaRousr_instance_builtin_get_image_alpha()
///@desc builtin getter for image_alpha
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_alpha;
