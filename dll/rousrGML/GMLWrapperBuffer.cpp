#include "GMLWrapperBuffer.h"

std::shared_ptr<GMLWrapperBuffer> GMLWrapperBuffer::mInstance;

void GMLWrapperBuffer::SetWrapperBuffer(std::shared_ptr<CGMLBuffer> _buffer) {
    if (mInstance == nullptr)
        mInstance = std::make_shared<GMLWrapperBuffer>();
    
    mInstance->mBuffer = _buffer;
}
