///@function __luaRousr_instance_builtin_set_direction(_val)
///@desc builtin setter for direction
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
direction = argument0;
