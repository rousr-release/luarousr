///@function __luaRousr_instance_builtin_get_object_index()
///@desc builtin getter for object_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return object_index;
