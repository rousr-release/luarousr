#include "TaskRunner.h"

TaskRunner::Task& TaskRunner::Run(std::function<void(TaskRunner::Task&)>&& _func) {
    size_t taskIndex(0);
    if (mFreeTasks.empty()) {
        taskIndex = mTasks.size();
        mTasks.emplace_back(std::make_unique<TaskRunner::Task>(*this, taskIndex));
    } else {
        taskIndex = mFreeTasks.top();
        mFreeTasks.pop();
    }

    auto& task(*mTasks[taskIndex]);
    task.SetReady(std::forward<std::function<void(TaskRunner::Task&)>>(_func));
    return task;
}

TaskRunner::Task::Task(TaskRunner& _runner, size_t _index)
    : index(_index)
    , active(false)
    , taskReady(false)
    , cancelled(false)
    , taskFunc(nullptr)
    , runner(_runner)
{
    std::thread([this, &_runner]() {
        while (!_runner.mShutdown) {
            while (!taskReady && !_runner.mShutdown)
                std::this_thread::yield();

            taskLock.lock();
            if (!cancelled && taskReady) {
                active = true;
                
                taskFunc(*this);
                taskReady = false;
                taskFunc = nullptr;
                _runner.mFreeTasks.push(index);
                
                active = false;
            }
            taskLock.unlock();
        }
    }).detach();
}

void TaskRunner::Task::SetReady(std::function<void(Task&)>&& _func) {
    taskLock.lock();
    taskFunc = std::move(_func);
    taskReady = true;
    cancelled = false;
    taskLock.unlock();
}

void TaskRunner::Task::Cancel() {
    taskLock.lock();
    cancelled = true;
    taskLock.unlock();

    while (active)
        std::this_thread::yield();        
}