#pragma once

#include "types.h"

class IGMLLuaVal;
class CLuaThread;
class CGMLBuffer;

class CLuaContext {
public:
	static std::shared_ptr<CLuaThread> NewThread(lua_State* L);

    CLuaContext();
	virtual ~CLuaContext();

	LuaIntf::LuaState& L()           { return mL; }
	bool isActive() const            { return mActive; }
	bool isShuttingDown() const      { return mSignalShutdown; }
	
    void RegisterFunction(const std::string& _funcName, uint32_t _scriptId);
    void DoString(const std::string& _script);
    void CallLua(const std::string& _functionName, const std::vector<std::shared_ptr<IGMLLuaVal>>& _vArgs, std::shared_ptr<CGMLBuffer> _returnBuffer);
    void CallLua(int32_t _luaRef,                  const std::vector<std::shared_ptr<IGMLLuaVal>>& _vArgs, std::shared_ptr<CGMLBuffer> _returnBuffer);
    
    std::shared_ptr<IGMLLuaVal> GetGlobal(const std::string& _name);
    void SetGlobal(const std::string& _name, double _val);
    void SetGlobal(const std::string& _name, const std::string& _val);

    int32_t CaptureScriptCallback(LuaIntf::LuaState& _L, int _stackIndex = -1);
    void AddScriptRef(int32_t _ref);
    void RemoveScriptRef(int32_t _ref);

    void ResumeThreads(); 
    void CollectGarbage();
    void KillThreads();
	
	void Shutdown() { mSignalShutdown = true; }
	bool EmptyThreads() { return mActiveThreads.empty(); }

private:
    void callLuaStack(const std::vector<std::shared_ptr<IGMLLuaVal>>& _vArgs, std::shared_ptr<CGMLBuffer>& _buffer);
	std::shared_ptr<CLuaThread> newThread();
	
private:
	std::atomic<bool> mActive            = { false };
	std::atomic<bool> mSignalShutdown    = { false };
	
	LuaIntf::LuaState mL;
    
	std::vector<std::shared_ptr<CLuaThread>>  mActiveThreads;
    std::map<int32_t, int32_t> mScriptCallbacks;
    std::set<int32_t> mDeadRefs;
};