{
    "id": "53dc0120-24ca-4982-875b-b61a29b8e918",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "extRousrDs",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 194,
    "date": "2017-27-29 11:12:18",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "3ae7c6c2-f6e3-42f0-af35-c5ef7550694c",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 194,
            "filename": "extRousrDs.gml",
            "final": "",
            "functions": [
                {
                    "id": "829e6dad-8f00-45f9-a96f-01e18dbb80b9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array",
                    "help": "return an index, or set an index in a `_rousr_array` ( _rousr_array , _index , [_val] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array",
                    "returnType": 2
                },
                {
                    "id": "7e0f4509-7b34-464d-a58c-d69b5949aa10",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_create",
                    "help": "create a `rousr style` array, with the size as the first element, and the actual array as second ( [_array] , [_arraySize] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_create",
                    "returnType": 2
                },
                {
                    "id": "1effc9e0-341b-460e-8f09-eed05ff9894d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_data",
                    "help": "return the data of the `rousr_array` ( - )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_data",
                    "returnType": 2
                },
                {
                    "id": "4cbd9984-e78a-4e1e-9cb8-d3a3b9505fd8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_destroy",
                    "help": "destroy a `rousr style` array - currently does nothing, but future proofing by including. ( _array )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_destroy",
                    "returnType": 2
                },
                {
                    "id": "c15a3a66-364b-4a46-8c7f-444005c35b31",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_find",
                    "help": "returns the first occurence of _val beginning at _startIndex ( _rousr_array , _val , [_startIndex=0] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_find",
                    "returnType": 2
                },
                {
                    "id": "01f2ff38-a46a-4c85-b56a-d2603864c739",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_insert",
                    "help": "insert a value into a rousr array ( _rousr_array , _index , _val )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_insert",
                    "returnType": 2
                },
                {
                    "id": "01921bb7-3d49-41c0-a8b2-bf8f190775b7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_pop_back",
                    "help": "pop the back of a rousr array and return it ( _rousr_array )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_pop_back",
                    "returnType": 2
                },
                {
                    "id": "7a17b543-3de2-4054-ab78-1ecd3bb81f7c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_pop_front",
                    "help": "pop the front of an array and return it ( _rousr_array )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_pop_front",
                    "returnType": 2
                },
                {
                    "id": "0a77083f-2cfa-4cad-b38e-bdff1eda3c7f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_push_back",
                    "help": "push a value on the end of a rousr_array ( _rousr_array , _val )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_push_back",
                    "returnType": 2
                },
                {
                    "id": "a88fad7d-f432-496a-ac36-f04cbfb2d7aa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_push_front",
                    "help": "push a value on the front of a rousr array ( _array , _val )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_push_front",
                    "returnType": 2
                },
                {
                    "id": "86375a1c-f73f-44ee-bcdb-74a729ed19ee",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_push_unique",
                    "help": "push a value on the end of a rousr array, but only if value isn't in the array already ( _rousr_array , _val , [_returnIndexOnExists=false] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_push_unique",
                    "returnType": 2
                },
                {
                    "id": "14b963bf-808d-4d4d-b598-6ef2c076dea0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_remove",
                    "help": "generate a new array with _index removed from the `_rousr_array` ( _rousr_array , _index )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_remove",
                    "returnType": 2
                },
                {
                    "id": "8825d763-6235-47a5-92da-ac2f0fbda4a1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_remove_multi",
                    "help": "generate a new array with _index removed from the `_rousr_array` ( _rousr_array , _index_array , [_index_array_size] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_remove_multi",
                    "returnType": 2
                },
                {
                    "id": "98bf13fc-d25a-482e-b178-0d857c23aafc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_size",
                    "help": "return the size of the `rousr_array` ( - , [_new_size] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_size",
                    "returnType": 2
                },
                {
                    "id": "cfb386f3-3721-46de-961c-c463b3318918",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_array_clear",
                    "help": "emtpy a rousr_array ( _rousr_array , [_flush_mem=false] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_array_clear",
                    "returnType": 2
                },
                {
                    "id": "b0c8bbff-d1d1-43bc-8ac6-317b1f6904aa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_stack_array_create",
                    "help": "create a `rousr_stack_array` - a stack purely made of arrays (  )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_stack_array_create",
                    "returnType": 2
                },
                {
                    "id": "81bc193b-4045-4834-8cd9-c41c02a78c65",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_stack_array_destroy",
                    "help": "destroy a `rousr_stack_array` - since they're made up of arrays, this is unnecessary to call, but provided future API use or re-using stack arrays. ( _rousr_stack_array )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_stack_array_destroy",
                    "returnType": 2
                },
                {
                    "id": "d80ff12e-ddee-49e2-abff-cd8a5efda692",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_stack_array_empty",
                    "help": "check if a stack is empty ( _rousr_stack_array )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_stack_array_empty",
                    "returnType": 2
                },
                {
                    "id": "af703b26-82ca-407a-a040-01d12072f7a9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_stack_array_pop",
                    "help": "pop the last `val`  from a `_rousr_stack_array` ( _rousr_stack_array )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_stack_array_pop",
                    "returnType": 2
                },
                {
                    "id": "684e66a5-3841-4e78-a53b-c6ce4c449a28",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_stack_array_push",
                    "help": "push a `_val` onto the stack of a `_rousr_stack_array` ( _rousr_stack_array , _val )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_stack_array_push",
                    "returnType": 2
                },
                {
                    "id": "c8635865-bbb3-4c10-8a52-183f7db81b89",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_stack_array_top",
                    "help": "return the top index of a `rousr_stack_array` ( _rousr_stack_array )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_stack_array_top",
                    "returnType": 2
                },
                {
                    "id": "d2ec8f56-6293-45c4-8540-c32d86d7bb53",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_buffer_read_val",
                    "help": "Reads a value from a packed buffer, first reading data type to expect. ( _buffer )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_buffer_read_val",
                    "returnType": 2
                },
                {
                    "id": "e4d31710-1cb0-4e70-9784-09b42d4ef52f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_buffer_write_val",
                    "help": "writes a value to the buffer, but first writes the type ( _buffer , _val , [type=ERousrData.Invalid] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_buffer_write_val",
                    "returnType": 2
                },
                {
                    "id": "02650a98-40fc-46ee-89a7-b891624c3b9a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_buffer_rousr_type_to_buffer_type",
                    "help": "return a buffer type from a luarousr data type ( _type )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_buffer_rousr_type_to_buffer_type",
                    "returnType": 2
                },
                {
                    "id": "463d5411-f229-4d2e-8a0c-0bc95a079012",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_pool_create_pool",
                    "help": "create a `rousr_pool`, using the `_alloc_script` to create them. use `_destroy_script` to remove datastructures that are pooled. `rousr_pools` allow you to create generic pools of similiar objects for pooling resources, rather than allocating brand new ones at run-time. ( _alloc_script , [_destroy_script] , [_reset_script] , [_constructor] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_pool_create_pool",
                    "returnType": 2
                },
                {
                    "id": "a94dbc81-53c3-4713-ab8a-266e13b4f75e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_pool_destroy_pool",
                    "help": "destroy a `rousr_pool` and free its memory use [data structures] ( _rousr_pool )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_pool_destroy_pool",
                    "returnType": 2
                },
                {
                    "id": "54b8e264-9e41-4d90-a0f6-a2e96115b148",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_pool_create",
                    "help": "get a fresh item, optionally call the passed constructor on the object. ( _rousr_pool , [_countructor] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_pool_create",
                    "returnType": 2
                },
                {
                    "id": "522da0a0-7d96-4fa6-bb44-3ccc99653228",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_pool_release",
                    "help": "returns element to the pool to be stored and reused ( _rousr_pool , _element )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_pool_release",
                    "returnType": 2
                },
                {
                    "id": "3dfbd3de-1467-4bec-a54b-8dd5536b0022",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_buffer_pool",
                    "help": "helper function to create a `rousr_pool` of buffers ( _buffer_size , _buffer_type , _buffer_alignment )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_buffer_pool",
                    "returnType": 2
                },
                {
                    "id": "d8d7b042-bbd0-428f-85b7-a1b6880a4ebf",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__sr_buffer_pool_alloc",
                    "help": "create a new buffer ( _rousr_pool )",
                    "hidden": false,
                    "kind": 1,
                    "name": "__sr_buffer_pool_alloc",
                    "returnType": 2
                },
                {
                    "id": "c5e60321-9b02-44cc-b737-942ea6896111",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__sr_buffer_pool_destroy",
                    "help": "destroy a pooled buffer ( _rousr_pool , _buffer )",
                    "hidden": false,
                    "kind": 1,
                    "name": "__sr_buffer_pool_destroy",
                    "returnType": 2
                },
                {
                    "id": "7baad2bc-8e7f-4d8c-ba9a-fd659264f5f0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__sr_buffer_pool_reset",
                    "help": "reset a buffer, after being returned to the pool ( _rousr_pool , _buffer )",
                    "hidden": false,
                    "kind": 1,
                    "name": "__sr_buffer_pool_reset",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 2,
            "order": [
                "829e6dad-8f00-45f9-a96f-01e18dbb80b9",
                "7e0f4509-7b34-464d-a58c-d69b5949aa10",
                "1effc9e0-341b-460e-8f09-eed05ff9894d",
                "4cbd9984-e78a-4e1e-9cb8-d3a3b9505fd8",
                "c15a3a66-364b-4a46-8c7f-444005c35b31",
                "01f2ff38-a46a-4c85-b56a-d2603864c739",
                "01921bb7-3d49-41c0-a8b2-bf8f190775b7",
                "7a17b543-3de2-4054-ab78-1ecd3bb81f7c",
                "0a77083f-2cfa-4cad-b38e-bdff1eda3c7f",
                "a88fad7d-f432-496a-ac36-f04cbfb2d7aa",
                "86375a1c-f73f-44ee-bcdb-74a729ed19ee",
                "14b963bf-808d-4d4d-b598-6ef2c076dea0",
                "8825d763-6235-47a5-92da-ac2f0fbda4a1",
                "98bf13fc-d25a-482e-b178-0d857c23aafc",
                "cfb386f3-3721-46de-961c-c463b3318918",
                "b0c8bbff-d1d1-43bc-8ac6-317b1f6904aa",
                "81bc193b-4045-4834-8cd9-c41c02a78c65",
                "d80ff12e-ddee-49e2-abff-cd8a5efda692",
                "af703b26-82ca-407a-a040-01d12072f7a9",
                "684e66a5-3841-4e78-a53b-c6ce4c449a28",
                "c8635865-bbb3-4c10-8a52-183f7db81b89",
                "d2ec8f56-6293-45c4-8540-c32d86d7bb53",
                "e4d31710-1cb0-4e70-9784-09b42d4ef52f",
                "02650a98-40fc-46ee-89a7-b891624c3b9a",
                "463d5411-f229-4d2e-8a0c-0bc95a079012",
                "a94dbc81-53c3-4713-ab8a-266e13b4f75e",
                "54b8e264-9e41-4d90-a0f6-a2e96115b148",
                "522da0a0-7d96-4fa6-bb44-3ccc99653228",
                "3dfbd3de-1467-4bec-a54b-8dd5536b0022",
                "d8d7b042-bbd0-428f-85b7-a1b6880a4ebf",
                "c5e60321-9b02-44cc-b737-942ea6896111",
                "7baad2bc-8e7f-4d8c-ba9a-fd659264f5f0"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "0.6.0"
}