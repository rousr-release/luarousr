///@function __luaRousr_instance_builtin_set_xprevious(_val)
///@desc builtin setter for xprevious
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
xprevious = argument0;
