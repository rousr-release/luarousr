{
    "id": "94b7c6c5-c20c-4a36-97fb-d1e4da5c716d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCopyright_sm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 0,
    "bbox_right": 6,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54b2de10-47df-4e9f-8711-d7aa6f9db4e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "94b7c6c5-c20c-4a36-97fb-d1e4da5c716d",
            "compositeImage": {
                "id": "308a8d5e-1623-49bf-b4bd-884c1ffde965",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54b2de10-47df-4e9f-8711-d7aa6f9db4e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81ee9536-a082-4a9c-8221-2bc94828d6d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54b2de10-47df-4e9f-8711-d7aa6f9db4e4",
                    "LayerId": "4b86acb7-ad64-4972-9c28-a829f2c56c87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "4b86acb7-ad64-4972-9c28-a829f2c56c87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "94b7c6c5-c20c-4a36-97fb-d1e4da5c716d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 0,
    "yorig": 0
}