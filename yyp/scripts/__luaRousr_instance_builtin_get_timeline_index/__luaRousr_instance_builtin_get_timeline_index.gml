///@function __luaRousr_instance_builtin_get_timeline_index()
///@desc builtin getter for timeline_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_index;
