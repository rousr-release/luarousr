///@function __luaRousr_instance_builtin_get_vspeed()
///@desc builtin getter for vspeed
///@returns {*} builtin
///@extensionizer { "docs": false }
return vspeed;
