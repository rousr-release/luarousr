///@function __luaRousr_instance_builtin_set_vspeed(_val)
///@desc builtin setter for vspeed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
vspeed = argument0;
