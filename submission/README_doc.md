<link rel = "stylesheet" type = "text/css" href = "retro.css" />  

![luuuuuuaaaaaaaaah-ah-ah-ah](https://i.imgur.com/dpH6TbF.png)  
[Available for Purchase at the YoYo Games Marketplace]()

# LuaRousr

LuaRousr is a GameMaker Studio 2 extension that integrates Lua with GameMaker Studio 2

## Installation Notes

*  ​Import Assets from the `LuaRousr` groups in each Resource
*  Import Assets from the "(required)" marked groups (See note below)

**​​Note:**  LuaRousr uses functions from [rousrSuite​​](https://marketplace.yoyogames.com/assets/6319/rousrsuite). When importing assets from the extension, make sure to include any `rousr (required)` group. If you have any other rousr​extensions, you only need to keep one copy of each of these resources. If you do import multiples, GameMaker Studio 2 will rename the duplicates with a `_1` at the end. You can simply delete any of the resources after importing the asset.

### Quick Start

*	Drop an instance of `LuaRousr` into your room. (**NOTE:** It's best to keep it persistent)
*	Execute Lua files;     `luaRousr_executeFile(filename)`
*	Execute Lua strings:   `luaRousr_executeString(scriptstring)`
*	Bind GML script:       `luaRousr_bindScript(lua_name, script_index, [optional]noreturns)`
*	Bind/Unbind Instances: `luaRousr_bindInstance(lua_name, instance_id)`/`luaRousr_unbindInstance(instance_id)`

###### _Note_: LuaRousr uses [Lua 5.3](https://www.lua.org/versions.html#5.3)

---

## The Demo
![boingggggg](http://i.imgur.com/yRzgkk7.gif)

###### The [demo](http://static.rou.sr/LuaRousr/Demo.zip) was lovingly handcrafted, and provided by [@net8floz](https://twitter.com/net8floz)
[Download Here](http://static.rou.sr/LuaRousr/Demo.zip)

### Messin' with the Demo

First and foremost: Use the `F1`-`F4` keys so that you can experience the latest in Grid fashion.

The demo uses the 4 Lua files, `game.lua`, `paddle.lua`, `ball.lua`, and `math.lua` to pit two AI pong paddles against one another, each of these files can be edited WHILE the game is running to completely change the behavior of the game itself. Once you've edited the Lua files, press `R` and the Lua will reload. Hurray!

### Working with the Demo Example (included with LuaRousr)

Lua files are expected to be right next to the .exe in this demo - though you don't have to do it that way.  To run the demo from GMS2 you must add the lua files to the `executable path` **first**. 

This path is something like the following for VM (and something ridiculous on YYC):
```
C:\ProgramData\GMS2\Cache\runtimes\runtime-x-x-x\windows
```

Alternatively just put the Lua files on `C:/lua` (or anywhere else) and use that instead of `get_executable_path`

**See: obj_game - Create for more instruction**

### Credit

* [@net8floz](http://www.twitter.com/net8floz) for the friggin cool [demo]()
	* Makes use of [spline](http://www.gmlscripts.com/script/spline) from [GMLscripts.com](http://www.gmlscripts.com)
* [Lua 5.3](http://www.lua.org)!
* [lua-intf](https://github.com/SteveKChiu/lua-intf) for a C++ 11 Lua C API Wrapper!
* [moodycamel:concurrentqueue](https://github.com/cameron314/concurrentqueue) for lockless queues!
* [sol2](https://github.com/ThePHd/sol2) for GMLBindInstance dynamic getter/setter

**Please see LICENSE.md for license information**  
**Extension itself is covered by [YoYo Games's EULA]()**

---

## API Reference

### GML
