#include "LuaContext.h"
#include "LuaThread.h"

#include "GMLLuaVal.h"
#include "GMLBindInstance.h"

#include "GMLCmdLayer.h"
#include "GMLCmds.h"

#include "rousrGML/GMLBuffer.h"
#include "rousrGML/GMLTypedBuffer.h"

namespace {
    std::shared_ptr<TGMLLuaVal<double>> GetGMLBindInstanceId(LuaIntf::LuaState& _L, int _index) {
        sol::stack_object so(_L, _index);
        if (so.is<CGMLBindInstance>()) {
            auto& inst(so.as<std::shared_ptr<CGMLBindInstance>>());
            if (inst != nullptr)
                return std::make_shared<TGMLLuaVal<double>>(inst->Id());
        }
        return nullptr;
    }
}

CLuaContext::CLuaContext() {
	mL = luaL_newstate();
	mL.openLibs();
	
	CLuaThread::RegisterLua(*this);
	LuaIntf::LuaBinding(L()).addFunction("print", [](const std::string& _s) {
		CLuaRousr::DebugPrint(_s);
	});

    sol::state_view lua(L());
    LuaIntf::LuaBinding(L()).addFunction("toFunction", [this](lua_State* _L) {
        LuaIntf::LuaState L(_L);
        
        int32_t ref(static_cast<int32_t>(L.toNumber(-1)));
        L.getRef(ref);
        
        return 1;
    });
}

CLuaContext::~CLuaContext() {
	Shutdown();
	mActiveThreads.clear();
	mL.close();
}

///
// Register GML script resource `_scriptId` with Lua as `_funcName` 
void CLuaContext::RegisterFunction(const std::string& _funcName, uint32_t _scriptId) {
    LuaIntf::LuaBinding(L()).addFunction(_funcName.c_str(), [this, _funcName, _scriptId](lua_State* l) -> int {
        LuaIntf::LuaState L(l);

        // Get the arguments
        const uint32_t top(L.top());
        const uint32_t bottom(0);
        std::vector<std::shared_ptr<IGMLLuaVal>> vArgs(top - bottom);
    
        for (int argIndex(top); argIndex > bottom; --argIndex) {
            if (L.isBool(argIndex))          vArgs[argIndex - (1 + bottom)] = std::make_shared<TGMLLuaVal<double>>(L.toBool(argIndex) ? 1.0 : 0.0);
            else if (L.isNumber(argIndex))   vArgs[argIndex - (1 + bottom)] = std::make_shared<TGMLLuaVal<double>>(L.toNumber(argIndex));
            else if (L.isString(argIndex))   vArgs[argIndex - (1 + bottom)] = std::make_shared<TGMLLuaVal<std::string>>(L.toString(argIndex));
            else if (L.isFunction(argIndex)) vArgs[argIndex - (1 + bottom)] = std::make_shared<TGMLLuaVal<double>>(static_cast<double>(CaptureScriptCallback(L, argIndex)));
            else {
                auto instId(GetGMLBindInstanceId(L, argIndex));
                vArgs[argIndex - 1] = instId != nullptr ? std::dynamic_pointer_cast<IGMLLuaVal>(instId) : std::make_shared<GMLLuaValVoid>();
            }
        }

        GMLCmds::GMLCall(_scriptId, std::move(vArgs), [&L](std::shared_ptr<IGMLLuaVal> _val) { _val->Push(L); });
        return L.top() - top;
    });
}

///
// Execute the passed Lua script
void CLuaContext::DoString(const std::string& _script) {
    CLuaRousr::IncrementSequence();

    try {
        if (mL.doString(_script.c_str()))
            throw LuaError(mL);
    }
    catch (std::exception& e) {
        std::string errorMsg(e.what());
        CLuaRousr::Error(errorMsg);
    }
}

///
// Call the passed function name
void CLuaContext::CallLua(const std::string& _functionName, const std::vector<std::shared_ptr<IGMLLuaVal>>& _vArgs, std::shared_ptr<CGMLBuffer> _returnBuffer) {
    mL.getGlobal(_functionName.c_str());
    if (!mL.isFunction(1) && !mL.isCFunction(1)) {
        CLuaRousr::Error("luaRousr - warning: Invalid lua function `" + _functionName + "`");
        mL.pop();
        return;
    }

    callLuaStack(_vArgs, _returnBuffer);
}

///
// Call the passed callback ID, usually something you've called retain on.
void CLuaContext::CallLua(int32_t _luaRef, const std::vector<std::shared_ptr<IGMLLuaVal>>& _vArgs, std::shared_ptr<CGMLBuffer> _returnBuffer) {
    mL.getRef(_luaRef);
    if (!mL.isFunction(1) && !mL.isCFunction(1)) {
        CLuaRousr::Error("luaRousr - warning: Invalid lua function. No function with index: " + std::to_string(_luaRef));
        mL.pop();
        return;
    }

    callLuaStack(_vArgs, _returnBuffer);
}

void CLuaContext::callLuaStack(const std::vector<std::shared_ptr<IGMLLuaVal>>& _vArgs, std::shared_ptr<CGMLBuffer>& _buffer) {
    CLuaRousr::IncrementSequence();

    for (auto arg : _vArgs)
        arg->Push(mL);

    try {
        if (mL.pcall(static_cast<int>(_vArgs.size()), LUA_MULTRET, 0))
            throw LuaError(mL);
    }
    catch (std::exception& e) {
        std::string errorMsg(e.what());
        CLuaRousr::Error(errorMsg);
        return;
    }

    // TODO: Handle Returns
    if (_buffer != nullptr) {
        
        _buffer->Seek(LuaRousr::LuaCallReturn);
        CGMLTypedBuffer typedBuff(*_buffer, false);
        int8_t numRet(1);
        if (mL.isBool(-1))          typedBuff.Write<double>(mL.toBool(-1));
        else if (mL.isNumber(-1))   typedBuff.Write<double>(mL.toNumber(-1));
        else if (mL.isString(-1))   typedBuff.Write<std::string>(mL.toString(-1));
        else if (mL.isFunction(-1)) typedBuff.Write<double>(CaptureScriptCallback(mL, -1));
        else {
            auto instId(GetGMLBindInstanceId(mL, -1));
            if (instId != nullptr) typedBuff.Write<double>(instId->Val());
            else 
                numRet = 0;
        }
        
        _buffer->Seek(LuaRousr::LuaCallNumReturn);
        _buffer->Write<int8_t>(numRet);

        while (mL.top() > 0)
            mL.pop(); // pop the return
    }
}

std::shared_ptr<IGMLLuaVal> CLuaContext::GetGlobal(const std::string& _name) {
    sol::state_view l(mL);
    std::shared_ptr<IGMLLuaVal> ret(nullptr);
    sol::object valObj(l.get<sol::object>(_name));
    
    switch (valObj.get_type()) {
        case sol::type::string:  ret = std::make_shared<TGMLLuaVal<std::string>>(valObj.as<std::string>());  break;
        case sol::type::boolean: ret = std::make_shared<TGMLLuaVal<bool>>(valObj.as<bool>()); break;
        case sol::type::number:  ret = std::make_shared<TGMLLuaVal<double>>(valObj.as<double>()); break;
        
        default:
            CLuaRousr::DebugPrint("luaRousr GetGlobal: unsupported Lua type for global: " + _name);
        break;
    }    

    return ret;
}

void CLuaContext::SetGlobal(const std::string& _name, double _val) {
    sol::state_view l(mL);
    l[_name] = _val;
}

void CLuaContext::SetGlobal(const std::string& _name, const std::string& _val) {
    sol::state_view l(mL);
    l[_name] = _val;
}

int32_t CLuaContext::CaptureScriptCallback(LuaIntf::LuaState& _L, int _stackIndex) {
    if (!_L.isFunction(_stackIndex))
        return -1;

    _L.pushValueAt(_stackIndex);
    int32_t ref(_L.ref());
    mDeadRefs.insert(ref);
    
    return ref;
}

void CLuaContext::AddScriptRef(int32_t _ref) {
    auto itFind(mScriptCallbacks.find(_ref));
    if (itFind == mScriptCallbacks.end())
        return;

    int32_t& ref(mScriptCallbacks[_ref]);
    ref++;

    mDeadRefs.erase(_ref);
}

void CLuaContext::RemoveScriptRef(int32_t _ref) {
    auto itFind(mScriptCallbacks.find(_ref));
    if (itFind == mScriptCallbacks.end())
        return;

    int32_t& ref(mScriptCallbacks[_ref]);
    ref--;

    if (ref <= 0) mDeadRefs.insert(_ref);
}

///
// Resume Lua threads up to their next yield, remove them from the list of active threads if they finish
void CLuaContext::ResumeThreads() {
    CLuaRousr::IncrementSequence();

    for (size_t threadIndex(0), threadEnd(mActiveThreads.size()); threadIndex < threadEnd; ++threadIndex) {
        auto& activeThread(*mActiveThreads[threadIndex]);
        if (activeThread.IsRunning())
            continue;

        if (activeThread.IsFinished() || !activeThread.Resume()) {
            mActiveThreads.erase(mActiveThreads.begin() + threadIndex);
            threadIndex++;
            threadEnd--;
        }
    }
}

void CLuaContext::CollectGarbage() {
    if (mDeadRefs.empty())
        return;

    for (int32_t luaRef : mDeadRefs) {
        mL.unref(luaRef);
    }

    mDeadRefs.clear();
}

void CLuaContext::KillThreads() {
	for (size_t threadIndex(0), threadCount(mActiveThreads.size()); threadIndex < threadCount; ++threadIndex) {
		auto& luaThread(mActiveThreads[threadIndex]);
		luaThread->Kill();
	}
}

std::shared_ptr<CLuaThread> CLuaContext::NewThread(lua_State* L) {
	auto context(CLuaRousr::Instance().GetContextFromState(L));
	if (context == nullptr)
		return nullptr;

	return context->newThread();
}

std::shared_ptr<CLuaThread> CLuaContext::newThread() {
    std::shared_ptr<CLuaThread> newThread(std::make_shared<CLuaThread>(mL));

	LuaIntf::LuaState& threadState(newThread->L());
	mL.pushValueAt(2);
	threadState.xmove(mL, 1);
    newThread->Resume();
		
	mActiveThreads.push_back(newThread);
	return newThread;
}

extern "C" {
    DLL_API double BindGMLFunction(const char* _luaName, double _scriptId) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::MainContext());
        
        std::string luaName(_luaName);
        uint32_t scriptId(static_cast<uint32_t>(_scriptId));
        mainContext->RegisterFunction(luaName, scriptId);

        return 1.0;
    }

    DLL_API double SetGlobalDbl(char* _name, double _val) {
        if (!CLuaRousr::Initialized())
            return -1.0;
        
        auto mainContext(CLuaRousr::MainContext());

        std::string globalName(_name);
        mainContext->SetGlobal(globalName, _val);
    
        return 1.0;
    }

    DLL_API double SetGlobalString(char* _name, char* _val) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::MainContext());
        
        std::string globalName(_name);
        std::string globalVal(_val);
        mainContext->SetGlobal(globalName, _val);
        
        return 1.0;
    }

    DLL_API double GetGlobal(char* _name, double _returnBufferLen, char* _returnBuffer) {
        if (!CLuaRousr::Initialized())
            return -1.0;
        
        auto mainContext(CLuaRousr::MainContext());
        
        std::string globalName(_name);
        std::shared_ptr<CGMLBuffer> returnBuffer(std::make_shared<CGMLBuffer>(static_cast<size_t>(_returnBufferLen), _returnBuffer));

        auto gmlVal(mainContext->GetGlobal(globalName));
        gmlVal->WriteBuffer(*returnBuffer);

        return 1.0;
    }

    DLL_API double KillThreads() {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::MainContext());
        if (mainContext == nullptr || mainContext->EmptyThreads())
            return -1.0;

        mainContext->KillThreads();
        return 1.0;
    }

    DLL_API double RetainScript(double _scriptId) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::MainContext());
        if (mainContext == nullptr)
            return -1.0;

        mainContext->AddScriptRef(static_cast<int32_t>(_scriptId));
        return 1.0;
    }

    DLL_API double ReleaseScript(double _scriptId) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::MainContext());
        if (mainContext == nullptr)
            return -1.0;

        mainContext->RemoveScriptRef(static_cast<int32_t>(_scriptId));
        return 1.0;
    }

    DLL_API double CollectGarbage() {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::MainContext());
        if (mainContext == nullptr || mainContext->EmptyThreads())
            return -1.0;

        mainContext->CollectGarbage();
        return 1.0;
    }
}