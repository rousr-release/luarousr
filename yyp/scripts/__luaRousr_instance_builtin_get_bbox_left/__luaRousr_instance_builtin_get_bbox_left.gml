///@function __luaRousr_instance_builtin_get_bbox_left()
///@desc builtin getter for bbox_left
///@returns {*} builtin
///@extensionizer { "docs": false }
return bbox_left;
