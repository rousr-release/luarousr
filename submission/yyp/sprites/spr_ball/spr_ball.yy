{
    "id": "8c8b9120-cf00-402b-86f9-3a13e31e5278",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ball",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f246b840-e1d8-408d-a356-cfb08d900dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8c8b9120-cf00-402b-86f9-3a13e31e5278",
            "compositeImage": {
                "id": "63e62a1e-dc93-4f14-9bdb-84898f712918",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f246b840-e1d8-408d-a356-cfb08d900dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b84abc6-f795-464e-a18a-b62ad1f795f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f246b840-e1d8-408d-a356-cfb08d900dc2",
                    "LayerId": "9089ce6d-4043-4ebc-93a6-5bb1f1b9f27c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "9089ce6d-4043-4ebc-93a6-5bb1f1b9f27c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8c8b9120-cf00-402b-86f9-3a13e31e5278",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 6,
    "yorig": 6
}