{
    "id": "5e17cc4d-464b-491e-b1f6-195b0b9340cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_opening",
    "eventList": [
        {
            "id": "abe7fb0f-36a7-425e-a438-29a92995e048",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5e17cc4d-464b-491e-b1f6-195b0b9340cb"
        },
        {
            "id": "38090c5b-5f44-4cb0-a38d-4175ebc0017e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5e17cc4d-464b-491e-b1f6-195b0b9340cb"
        },
        {
            "id": "eb765a41-8056-48ea-b458-5eaf3e9f3fc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5e17cc4d-464b-491e-b1f6-195b0b9340cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}