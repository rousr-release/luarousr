///@function luaRousr_execute_file(_lua_file)
///@desc Load a .Lua file and call `luaRousr_execute_string` on it
///@param {String} _lua_file   file name of text file containing Lua.
///@returns {Real} undefined on failure, or the script context Id for all threads running from this execute.
if (!__luaRousr_check_init())
  return undefined;
  
var _lua_file = argument0;
var script = "";

var readFile = file_text_open_read(_lua_file);
if (readFile == -1)
  return undefined;

while (!file_text_eof(readFile)) script += file_text_readln(readFile);

file_text_close(readFile);
if (string_length(script) <= 0) 
  return undefined;
  
return luaRousr_execute_string(script);