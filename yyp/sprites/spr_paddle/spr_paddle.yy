{
    "id": "82ab8dc6-40cf-46ed-9f83-30695c2889d5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_paddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "b83e5d1d-d137-40e6-8016-10107b67583d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82ab8dc6-40cf-46ed-9f83-30695c2889d5",
            "compositeImage": {
                "id": "7d5b9a9b-0063-48c6-8e3f-763fd5ca1f4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b83e5d1d-d137-40e6-8016-10107b67583d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c88f88c-ee38-4545-b9da-0f5fdac92f70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b83e5d1d-d137-40e6-8016-10107b67583d",
                    "LayerId": "ce11b26d-4321-47c2-b321-fee5bf34c485"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ce11b26d-4321-47c2-b321-fee5bf34c485",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82ab8dc6-40cf-46ed-9f83-30695c2889d5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 32
}