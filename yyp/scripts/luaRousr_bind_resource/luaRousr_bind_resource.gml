///@func luaRousr_bind_resource([_lua_name], _object_index);
///@desc bind an Object to Lua
///@param {String}     [_lua_name] - Name to lookup this Object with in Lua
///@param {Real}   _object_index  - Object we're sync'ing
var _lua_name = argument_count > 1 ? argument[0] : "";
var _index   = argument_count == 1 ? argument[0] : argument[1];

with (LuaSr) {
  // ensure its not already bound by id or by name
  luaRousr_unbind_resource(_lua_name);
  luaRousr_unbind_resource(_index);
  
  var name_map  = GMLBindings[__ELuaRousrGMLBindData.ResourceNameMap];
  var index_map = GMLBindings[__ELuaRousrGMLBindData.ResourceIndexMap];

  var bind = [ _lua_name, _index ];
  name_map[? _lua_name] = bind;
  index_map[? _index]  = bind;
}