#pragma once

#include "types.h"

namespace rousrGML {
    const size_t MaxGMLStringLen = 1024;

    enum class DataType : int8_t {
        Terminate = -1,

        Byte = 0,
        Bool,

        Int8,
        Int16,
        Int32,
        Int64,

        Uint8,
        Uint16,
        Uint32,
        Uint64,

        Float,
        Double,
        String,

        Num
    };

    enum class Status : int8_t {
        Invalid = -2,
        Error   = -1,
        
        Done = 0,
        Init,
        Ready, 
        Working,
        
        Num
    };


    const size_t SeekBegin(0);
    const size_t SeekStatus(0);
    const size_t SeekCmdId(1);
    const size_t SeekData(SeekCmdId + sizeof(uint32_t));

    template <typename T> inline int8_t GetDataType()    { }
    template <> inline int8_t GetDataType<void>()        { return static_cast<int8_t>(DataType::Num); }
    template <>	inline int8_t GetDataType<char>()        { return static_cast<int8_t>(DataType::Byte); }
    template <>	inline int8_t GetDataType<bool>()        { return static_cast<int8_t>(DataType::Bool); }
    template <>	inline int8_t GetDataType<int8_t>()      { return static_cast<int8_t>(DataType::Byte); }
    template <>	inline int8_t GetDataType<int16_t>()     { return static_cast<int8_t>(DataType::Int16); }
    template <>	inline int8_t GetDataType<int32_t>()     { return static_cast<int8_t>(DataType::Int32); }
    template <>	inline int8_t GetDataType<int64_t>()     { return static_cast<int8_t>(DataType::Int64); }
    template <>	inline int8_t GetDataType<uint8_t>()     { return static_cast<int8_t>(DataType::Uint8); }
    template <>	inline int8_t GetDataType<uint16_t>()    { return static_cast<int8_t>(DataType::Uint16); }
    template <>	inline int8_t GetDataType<uint32_t>()    { return static_cast<int8_t>(DataType::Uint32); }
    template <>	inline int8_t GetDataType<uint64_t>()    { return static_cast<int8_t>(DataType::Uint64); }
    template <>	inline int8_t GetDataType<float>()       { return static_cast<int8_t>(DataType::Float); }
    template <>	inline int8_t GetDataType<double>()      { return static_cast<int8_t>(DataType::Double); }
    template <> inline int8_t GetDataType<std::string>() { return static_cast<int8_t>(DataType::String); }
}