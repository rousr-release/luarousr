///@function __luaRousr_alloc_bind(_rousr_pool)
///@desc allocate a new array for binds
///@param {Array} _rousr_pool - pool being allocated for
///@returns {Array} new bind data array
///@extensionizer { "docs": false }
var _rousr_pool = argument0;
return array_create(__ELuaRousrBindEntity.Num);