function ballStart(ballIndex)  
	local ball = { } 
	local instance = GMLInstance("ball"..ballIndex)
	local xMin = -32
	local xMax = game.roomWidth + 32
	local yMin = 6
	local yMax = game.roomHeight - 6
	local bounceForce = 40
	local deathForce = 50
	local startForce = 50
	local maxSpeed = 4
	
	local function addGridForce(fx,fy,radius)
		gmlAddGridForce(fx,fy,instance.x,instance.y,radius)
	end

	local function init()
		if game.balls[ballIndex] == nil then 
			ball.instance = instance
			ball.dx = gmlRandomSign()
			ball.dy = gmlRandomSign()
			ball.accel = 2+math.random(0,1)
			ball.radiusSquared = 32 * 32
			game.balls[ballIndex] = ball 
			addGridForce(-ball.dx * startForce,0,ball.radiusSquared)
		else 
			ball = game.balls[ballIndex]
			ball.instance = instance
		end 
	end 

	local function bounce(x,y)
		if x then 
			addGridForce(-ball.dx * bounceForce,0,ball.radiusSquared)
			ball.dx = -ball.dx
		end 

		if y then 
			addGridForce(0,-ball.dy * bounceForce,ball.radiusSquared)
			ball.dy = -ball.dy
		end 

		ball.accel = ball.accel + 0.1
		ball.accel = math.min(ball.accel,maxSpeed)
	end 


	local function bounceOffPaddles()
		local paddle = game.instancePlacePaddle(instance.id,instance.x,instance.y)
		if paddle ~= nil then
			if instance.x > game.roomCenterX then 
				instance.x = paddle.instance.x - 16
			else
				instance.x = paddle.instance.x + 16
			end

			bounce(true,false) 
		end
	end

	local function bounceOffWalls() 
		local dy = ball.dy
		if instance.y <= yMin then
			instance.y = 6
			bounce(false,true)
		elseif instance.y >= yMax then 
			instance.y = game.roomHeight-6
			bounce(false,true)
		end
	end 

	local function update()
		local xSpeed = ball.dx * ball.accel
		local ySpeed = ball.dy * ball.accel 
		instance.x = instance.x + xSpeed
		instance.y = instance.y + ySpeed
		addGridForce(xSpeed,ySpeed,ball.radiusSquared)

		if instance.x < xMin or instance.x > xMax then
			addGridForce(-xSpeed*deathForce,0,ball.radiusSquared*8)
			gmlInstanceDestroy(instance.id)
			instance = nil
			game.balls[ballIndex] = nil
			gmlCreateBall(game.roomWidth/2,game.roomHeight/2,60*2) 
		else
			bounceOffWalls()
			bounceOffPaddles()
			instance.image_angle = instance.image_angle - xSpeed
		end 
	end

	init()
	local newThread = thread(function()
		while(instance ~= nil) do   
			update()
			thread.yield()  
		end		
		ball = nil
		newThread = nil 
	end)	
end