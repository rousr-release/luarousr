///@functino luaRousr_call(_function, [_args...])
///@desc Call a Lua function with the given name, that simple!  
///      **NOTE: Returns are not functioning in the current version! No returns as of yet.**
///      [luaRousr_retain_callback](#luaRousr_retain_callback)
///      [luaRousr_release_callback](#luaRousr_release_callback)
///@param {String|Real}       _function    name of function defined in Lua or callback id previously retained 
///@param {...Real|...String} [_args...]   arguments to pass to Lua function
///@returns {Real|String} value lua function returns
///@todo - return value is not functioning
var _function = argument[0];

var arg_buffer    = __luaRousr_pop_buffer();
buffer_write(arg_buffer, buffer_u32, argument_count - 1);
for (var i = 1; i < argument_count; i++) {
  var arg = argument[i];
  if (!sr_buffer_write_val(arg_buffer, arg)) {
    show_debug_message("luaRousr_Call: Invalid argument type!")
    __luaRousr_push_buffer(arg_buffer);
    return undefined;
  }
}

var cmd_buffer = __luaRousr_pop_buffer();
if (is_string(_function))
  __luaRousr_process_buffer(cmd_buffer, __extLuaRousrCallLua(buffer_get_address(cmd_buffer), _function, _RousrDefaultBufferSize, buffer_get_address(arg_buffer)));
else if (is_real(_function))
  __luaRousr_process_buffer(cmd_buffer, __extLuaRousrCallLuaScriptId(buffer_get_address(cmd_buffer), _function, _RousrDefaultBufferSize, buffer_get_address(arg_buffer)));
var returnBuffer = arg_buffer;
buffer_seek(returnBuffer, buffer_seek_start, 0);
var validReturn = buffer_read(returnBuffer, buffer_s8) > 0;
var returnValue = validReturn ? sr_buffer_read_val(returnBuffer) : undefined;

__luaRousr_push_buffer(cmd_buffer);
__luaRousr_push_buffer(arg_buffer);

return returnValue;
