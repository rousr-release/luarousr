{
    "id": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "imgui",
    "eventList": [
        {
            "id": "982cbe28-c4cf-4c64-aa9b-977d83ca6704",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "9c884080-8339-4d0a-9c29-287b77991ff5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "4d557e32-23a5-462d-a07d-9a33f59b83cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "7c42b22c-8a24-4719-a57a-af5407b93feb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "febcbea2-d039-49bd-9536-8a51182bade7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "a2a58f03-75ad-4719-9c56-75267a2f6a11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        },
        {
            "id": "7144db7b-57e4-4355-bc2a-c7d0ebd8fe78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "3e2c6ad6-35f5-4437-ac55-13a60009c3a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}