///@function __luaRousr_instance_builtin_set_visible(_val)
///@desc builtin setter for visible
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
visible = argument0;
