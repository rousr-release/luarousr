///@function __luaRousr_instance_builtin_set_persistent(_val)
///@desc builtin setter for persistent
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
persistent = argument0;
