///@function __luaRousr_instance_builtin_get_sprite_width()
///@desc builtin getter for sprite_width
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_width;
