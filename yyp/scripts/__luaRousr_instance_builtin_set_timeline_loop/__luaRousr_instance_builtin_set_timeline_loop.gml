///@function __luaRousr_instance_builtin_set_timeline_loop(_val)
///@desc builtin setter for timeline_loop
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
timeline_loop = argument0;
