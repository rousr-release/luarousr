#pragma once

#include "luaRousr.h"

class IGMLVal;

class CGMLValBuffer : CGMLBuffer 
{
	template <typename T> friend class TGMLVal;
public:
	CGMLBuffer(const char* _bufferPtr);
	CGMLBuffer(size_t _bufferSize, const char* _bufferPtr);
	virtual ~CGMLBuffer() { ; }

	const char*              Raw() const { return mBuffer; }
	char*                    Raw()       { return mBuffer; }

	size_t Size() const               { return mBufferSize; }
	size_t Tell() const               { return mSeekPos; }
	void   Seek(size_t _pos)          { mSeekPos = _pos; }
	void   SeekOffset(size_t _offset) { mSeekPos += _offset; }

	int8_t PeekByte() const;
	int8_t PeekByte(uint32_t _seekPos) const;

	void   Write(std::shared_ptr<IGMLVal> _val);
	void   WriteString(const std::string& _val, bool _writeType = false, bool _writeSize = false);
	
	template <typename T, typename=std::enable_if_t<!std::is_same<T, std::string>::value>>
	void Write(const T& _val, bool _writeType = false) {
        size_t totalSize(mSeekPos + sizeof(T) + (_writeType ? 1 : 0));
		if (totalSize >= mBufferSize)
			return;

		if (_writeType)
			mBuffer[mSeekPos++] = static_cast<int8_t>(LuaRousr::GetDataType<T>());

		*reinterpret_cast<T*>(&mBuffer[mSeekPos]) = _val;
		mSeekPos += sizeof(T);
	}
	template <typename T, typename=std::enable_if_t<std::is_same<T, std::string>::value>>
	void Write(const std::string& _val, bool _writeType) { 
		WriteString(_val, _writeType); 
	}

	int8_t                   ReadByte();
	
	template <typename T>
	typename std::enable_if<!std::is_same<T, std::string>::value, T>::type Read() {
		T _val(*reinterpret_cast<T*>(&mBuffer[mSeekPos]));
		mSeekPos += sizeof(T);
		return _val;
	}
	
	template <typename T>
	typename std::enable_if<std::is_same<T, std::string>::value, T>::type Read() {
		char buffer[MaxGMLStringLen];
		char* val = &buffer[0];

		for (size_t& pos(mSeekPos), end(Size()); pos < end && mBuffer[pos] != 0; ++pos) {
			*val = mBuffer[pos];
			val++;
		}
		*val = 0;
		mSeekPos++; // account for terminator

		return buffer;
	}
	
	std::shared_ptr<IGMLVal> ReadVal();
	void					 Clear() { mSeekPos = mBufferSize = 0; mBuffer = nullptr; }

private:
	char*  mBuffer;
	size_t mSeekPos;
	size_t mBufferSize;
};