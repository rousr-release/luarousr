///@function luaRousr_bind_script(_lua_name, _script_index, [_user_data])
///@desc Call this for all of your GML script functions you'd like Lua to have access to.  
///      **Note:** This must be done _before_ you load your scripts that use the functions. Otherwise, it'sunreliable that they'll be able to "see" your functions.  
///      **Note About Arguments:** In Lua, you're able to treat most anything like a variable and pass it to a function as an argument. At this time, LuaRousr doesn't support accepting `Lua tables` as arguments. You may pass functions, however. If passing a function to GML, you must retain the `callbackid` in order to call it outside of the scope of the bound script function. See `luaRousr_retain_callback` and `luaRousr_release_callback`.
///@param {String} _lua_name     name to refer to this script by in Lua `_lua_name()`
///@param {Real} _script_index   script index that represents this function
///@param {*} [_user_data]       user data optionally passed with the script_execute
var _lua_name     = argument[0];
var _script_index = argument[1];
var _user_data    = [ ];

with(LuaSr) {
  var argCount = argument_count - 2, arg = 2, i = 0;
  repeat(argCount)
    _user_data[i++] = argument[arg++];
  
  ScriptUserData[? _script_index] = [ array_length_1d(_user_data), _user_data ];
  __extLuaRousrBindScriptFunction(_lua_name, _script_index);
}