///@param x
///@param y
///@param inverse_mass
var _point = array_create(point.__size);
_point[@ point.x] = argument0;
_point[@ point.y] = argument1;
_point[@ point.vx] = 0;
_point[@ point.vy] = 0;
_point[@ point.ax] = 0;
_point[@ point.ay] = 0;
_point[@ point.inverse_mass] = argument2;
_point[@ point.damping] = 0.98;
return _point;
