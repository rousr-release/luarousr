///@param id
///@param x
///@param y
with(argument0){
	var _col = instance_place(argument1,argument2,obj_paddle);
	if(_col){
		return _col.lua_id;
	}
}
return -1;