///#function __luaRousr_read_instance(_id, _contextValues, _dirtyIndices, _dirtyCount)
///@desc internal function to read an instance's values from a sync
///@param {Real}  _id              instance id
///@param {Array} _contextValues   Array of values to set instance built-ins to
///@param {Array} _dirtyIndices    Array of dirty values
///@param {Real}  _dirtyCount      Amount of dirty values in array already, add new dirty indices at the end [deprecated]
///@extensionizer { "docs": false }
var _id            = argument0;
var _contextValues = argument1;
var _dirtyIndices  = argument2;
var _dirtyCount    = argument3;

with (LuaSr) {
  var id_map = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  var bind = id_map[? _id];
  if (bind == undefined)
    return;

  
}