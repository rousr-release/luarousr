///@function luaRousr_bind_instance_variable(_id, _member_name, _default, [_read_only=false], [_getter=_luaRousr_default_custom_getter], [_setter=_luaRousr_default_custom_setter], [_force_type=undefined])
///@desc Binds an `instance` variable to `_member_name` in Lua. Once `instance` is bound in Lua, the variable is accessible by `_member_name.` 
///      In LuaRousr's current version, this feature uses `variable_instance_set` and `variable_instance_get` to keep the value in sync with Lua, so `varName` **must** match the `instance` variable name.
///@param {Real}        _id              instance id to bind variable to
///@param {String}      _member_name     name of this member to refer to it by in Lua i.e., `instance._member_name`
///@param {Real|String} _default         default value of this member, if _forceType is undefined (not passed), the type written is based on this value.
///@param {Boolean} [_read_only=false]   can you write to this value?
///@param {Real}           [_getter]     getter function for this variable
///@param {Real}           [_setter]     setter function for this variable
///@param {Real|String} [_force_type]    ERousrData or string representing type to optionally force use for this value
///@returns {Real} index of variable
#region Args
var _instance_id = argument[0];
var _member_name = argument[1];
var _default = argument[2];
var _read_only   = argument_count > 3 ? argument[3] : false;
var _getter     = argument_count > 4 ? argument[4] : luaRousr_default_getter;
var _setter     = argument_count > 5 ? argument[5] : luaRousr_default_setter;
var _forceType  = argument_count > 6 ? argument[4] : undefined;
#endregion

#region Determine member type

if (_forceType == undefined) {
  if      (is_bool(_default))   _forceType = ERousrData.Bool;
  else if (is_real(_default))  	_forceType = ERousrData.Double;
  else if (is_string(_default))	_forceType = ERousrData.String;
}

if (is_string(_forceType)) {
  switch (_forceType) {
    case "real":   _forceType = ERousrData.Double; break;
    case "string": _forceType = ERousrData.String; break;
    case "bool":   _forceType = ERousrData.Bool; break;
  }
}

#endregion
#region Bind To Instance

var bind = undefined;
with (LuaSr) {
  var id_map = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  bind = id_map[? _instance_id]
  if (bind == undefined) {
    show_debug_message("luaRousr - warning: can't bind " + _member_name + " to instance (id: " + _instance_id + "). Instance isn't bound to Lua!");
    return;
  }
}  

if (bind == undefined) {
  show_debug_message("luaRousr - error: can't bind instance");
  return;
}

with (_instance_id) {
  
  switch (_forceType) {
    case ERousrData.Double:
    case ERousrData.String:
    case ERousrData.Bool:
      var customBinds = bind[ __ELuaRousrBindEntity.CustomMembers];
      var customBind = [ _member_name, _default, _read_only, _getter, _setter, _forceType ];
      sr_array_push_back(customBinds, customBind);
      
      // todo: buffer pool / scracth buffer
      var buffer = buffer_create(_RousrDefaultBufferSize, buffer_fixed, 1);
      buffer_write(buffer, buffer_string, _member_name);
      sr_buffer_write_val(buffer, _default, _forceType);
      buffer_write(buffer, buffer_u8, _read_only);
      
      with (LuaSr)
        __extLuaRousrBindCustomMember(_instance_id, _RousrDefaultBufferSize, buffer_get_address(buffer));
        
      buffer_delete(buffer);
    break;
  	default: show_debug_message("luaRousr - warning: can't bind member '" + string(_member_name) + "' invalid type passed for default value (can only be bool/real/string).");
  }
}

#endregion