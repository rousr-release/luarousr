///@func unit_test_write(_msg)
///@desc write the test message to the terminal and console
///@param {string} _msg   msg to write
var _msg = argument0;
show_debug_message(_msg);
terminal_write_line(_msg);