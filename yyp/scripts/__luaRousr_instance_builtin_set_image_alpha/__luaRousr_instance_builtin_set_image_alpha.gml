///@function __luaRousr_instance_builtin_set_image_alpha(_val)
///@desc builtin setter for image_alpha
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
image_alpha = argument0;
