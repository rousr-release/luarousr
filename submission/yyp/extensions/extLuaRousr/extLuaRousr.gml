#define luaRousr_init
///@function luaRousr_init()
///@desc create LuaRousr if it doesn't already exist. [helper function]
///@returns {Real} LuaRousr instance id
if (instance_number(LuaRousr) != 0) {
  with (LuaRousr)
    return id;
}

return instance_create_depth(0, 0, 0, LuaRousr);

#define luaRousr_add_debug_print_listener
///@function luaRousr_add_debug_print_listener(_script_index)
///@desc Adds a script index to the list of debug listeners internal to LuaRousr.  
///      **Note:** Use `with(LuaRousr) DebugToConsole = false` to shut off default print behavior
///@param {real} _script_index   index of script to call... expects `callback(string_parameter)` to be valid
var _script_index = argument0;
with (LuaSr)
  ds_list_add(_script_index);

#define luaRousr_remove_debug_print_listener
///@function luaRousr_remove_debug_print_listener(_script_index)
///@desc Removes a script index from the list of debug listeners internal to LuaRousr.
///      **Note:** Use `with(LuaRousr) DebugToConsole = false` to shut off default print behavior
///@param {Real} _script_index   index of script to remove
var _script_index = argument0;
with (LuaSr) {
  var index = ds_list_find_index(DebugListeners, _script_index);
  if (index >= 0) {
    ds_list_delete(DebugListeners, index);
  }
}

#define luaRousr_tick
///@function luaRousr_tick()
///@desc force lua to process a frame / tick /step

// todo: check if there are threads after calling Lua, and set a "there are threads" flag, rather
//       than hitting the DLL every step

__extLuaRousrCollectGarbage();
if (__extLuaRousrHasThreads()) {
  var cmd_buffer = __luaRousr_pop_buffer();
  __luaRousr_process_buffer(cmd_buffer, __extLuaRousrResumeThreads(buffer_get_address(cmd_buffer)));
  __luaRousr_push_buffer(cmd_buffer);
}

#define luaRousr_default_getter
///@func __luaRousr_default_custom_getter([_variable_name], [_id_or_index])
///@desc called `with(instance)` 
///@param {String} _variable_name   name given to variable
///@param {Real}           _index   index assigned the variable
///@returns {*} return value
///@notes optionally take the name, or index, as helper ways to identify a gotten variable, 
///       or just use one script per variable

/// by default, variable_instance_get
var _name = argument[0];
var _index = argument[1];

return variable_instance_get(id, _name);

#define luaRousr_default_setter
///@func __luaRousr_default_custom_setter(_val, [_variable_name], [_id_or_index])
///@desc called `with(instance)`
///@param {*}                _val   value to assign
///@param {String} _variable_name   name given to variable
///@param {Real}           _index   index assigned the variable
///@notes optionally take the name, or index, as helper ways to identify a variable - _val is first and required to be used
///       or just use one script per variable

/// by default, variable_instance_get
var _val = argument[0];
var _name = argument[1];
var _index = argument[2];

return variable_instance_set(id, _name, _val);

#define luaRousr_bind_script
///@function luaRousr_bind_script(_lua_name, _script_index, [_user_data])
///@desc Call this for all of your GML script functions you'd like Lua to have access to.  
///      **Note:** This must be done _before_ you load your scripts that use the functions. Otherwise, it'sunreliable that they'll be able to "see" your functions.  
///      **Note About Arguments:** In Lua, you're able to treat most anything like a variable and pass it to a function as an argument. At this time, LuaRousr doesn't support accepting `Lua tables` as arguments. You may pass functions, however. If passing a function to GML, you must retain the `callbackid` in order to call it outside of the scope of the bound script function. See `luaRousr_retain_callback` and `luaRousr_release_callback`.
///@param {String} _lua_name     name to refer to this script by in Lua `_lua_name()`
///@param {Real} _script_index   script index that represents this function
///@param {*} [_user_data]       user data optionally passed with the script_execute
var _lua_name     = argument[0];
var _script_index = argument[1];
var _user_data    = [ ];

with(LuaSr) {
  var argCount = argument_count - 2, arg = 2, i = 0;
  repeat(argCount)
    _user_data[i++] = argument[arg++];
  
  ScriptUserData[? _script_index] = [ array_length_1d(_user_data), _user_data ];
  __extLuaRousrBindScriptFunction(_lua_name, _script_index);
}

#define luaRousr_bind_instance_variable
///@function luaRousr_bind_instance_variable(_id, _member_name, _default, [_read_only=false], [_getter=_luaRousr_default_custom_getter], [_setter=_luaRousr_default_custom_setter], [_force_type=undefined])
///@desc Binds an `instance` variable to `_member_name` in Lua. Once `instance` is bound in Lua, the variable is accessible by `_member_name.` 
///      In LuaRousr's current version, this feature uses `variable_instance_set` and `variable_instance_get` to keep the value in sync with Lua, so `varName` **must** match the `instance` variable name.
///@param {Real}        _id              instance id to bind variable to
///@param {String}      _member_name     name of this member to refer to it by in Lua i.e., `instance._member_name`
///@param {Real|String} _default         default value of this member, if _forceType is undefined (not passed), the type written is based on this value.
///@param {Boolean} [_read_only=false]   can you write to this value?
///@param {Real}           [_getter]     getter function for this variable
///@param {Real}           [_setter]     setter function for this variable
///@param {Real|String} [_force_type]    ERousrData or string representing type to optionally force use for this value
///@returns {Real} index of variable
#region Args
var _instance_id = argument[0];
var _member_name = argument[1];
var _default = argument[2];
var _read_only   = argument_count > 3 ? argument[3] : false;
var _getter     = argument_count > 4 ? argument[4] : __extluarousr_script_index(luaRousr_default_getter);
var _setter     = argument_count > 5 ? argument[5] : __extluarousr_script_index(luaRousr_default_setter);
var _forceType  = argument_count > 6 ? argument[4] : undefined;
#endregion

#region Determine member type

if (_forceType == undefined) {
  if      (is_bool(_default))   _forceType = ERousrData.Bool;
  else if (is_real(_default))  	_forceType = ERousrData.Double;
  else if (is_string(_default))	_forceType = ERousrData.String;
}

if (is_string(_forceType)) {
  switch (_forceType) {
    case "real":   _forceType = ERousrData.Double; break;
    case "string": _forceType = ERousrData.String; break;
    case "bool":   _forceType = ERousrData.Bool; break;
  }
}

#endregion
#region Bind To Instance

var bind = undefined;
with (LuaSr) {
  var id_map = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  bind = id_map[? _instance_id]
  if (bind == undefined) {
    show_debug_message("luaRousr - warning: can't bind " + _member_name + " to instance (id: " + _instance_id + "). Instance isn't bound to Lua!");
    return;
  }
}  

if (bind == undefined) {
  show_debug_message("luaRousr - error: can't bind instance");
  return;
}

with (_instance_id) {
  
  switch (_forceType) {
    case ERousrData.Double:
    case ERousrData.String:
    case ERousrData.Bool:
      var customBinds = bind[ __ELuaRousrBindEntity.CustomMembers];
      var customBind = [ _member_name, _default, _read_only, _getter, _setter, _forceType ];
      sr_array_push_back(customBinds, customBind);
      
      // todo: buffer pool / scracth buffer
      var buffer = buffer_create(_RousrDefaultBufferSize, buffer_fixed, 1);
      buffer_write(buffer, buffer_string, _member_name);
      sr_buffer_write_val(buffer, _default, _forceType);
      buffer_write(buffer, buffer_u8, _read_only);
      
      with (LuaSr)
        __extLuaRousrBindCustomMember(_instance_id, _RousrDefaultBufferSize, buffer_get_address(buffer));
        
      buffer_delete(buffer);
    break;
  	default: show_debug_message("luaRousr - warning: can't bind member '" + string(_member_name) + "' invalid type passed for default value (can only be bool/real/string).");
  }
}

#endregion

#define luaRousr_bind_instance
///@func luaRousr_bind_instance([_lua_name], _instance_id)
///@desc Binds an instance to Lua using the given name. All the members can now be accessed i.e,:
///      ```
///      inst.x = 10
///      print(inst.y)
///      ```
///      **NOTE:** You must unbind an instance in its `CleanUp` event if it's still bound at that point.
///
///@param {String} [_lua_name]  - Name to lookup this instance with in Lua, can omit, and just pass id, when using for return values
///@param {Real}   _instance_id - instance we're sync'ing (some history: original API required both, and unfortunately passed id second, in order to not break everything, first param is now optional)
///@returns {Real} noone on unsuccessful bind, or id of instance (for chaining in function returns)
var _lua_name = argument_count > 1 ? argument[0] : "";
var _id      = argument_count == 1 ? argument[0] : argument[1];

with (LuaSr) {
  // ensure its not already bound by id or by name
  luaRousr_unbind_instance(_id);
  luaRousr_unbind_instance(_lua_name);
  
  // bind the instance
  __extLuaRousrBindInstance(_lua_name, _id);

  // get the next free bind index to track our context with
  var binds = GMLBindings[__ELuaRousrGMLBindData.Binds];
  var bind = sr_pool_create(binds)
  
  bind[@ __ELuaRousrBindEntity.Id]            = _id;
  bind[@ __ELuaRousrBindEntity.LuaName]       = _lua_name;
  bind[@ __ELuaRousrBindEntity.Values]        = sr_array_create();
  bind[@ __ELuaRousrBindEntity.CustomMembers] = sr_array_create();
  
  var id_map      = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  id_map[? _id]            = bind;
  
  var name_map    = GMLBindings[__ELuaRousrGMLBindData.NameToBind];
  name_map[? _lua_name]     = bind;
  
  return _id;
}

return noone;

#define luaRousr_bind_resource
///@func luaRousr_bind_resource([_lua_name], _object_index);
///@desc bind an Object to Lua
///@param {String}     [_lua_name] - Name to lookup this Object with in Lua
///@param {Real}   _object_index  - Object we're sync'ing
var _lua_name = argument_count > 1 ? argument[0] : "";
var _index   = argument_count == 1 ? argument[0] : argument[1];

with (LuaSr) {
  // ensure its not already bound by id or by name
  luaRousr_unbind_resource(_lua_name);
  luaRousr_unbind_resource(_index);
  
  var name_map  = GMLBindings[__ELuaRousrGMLBindData.ResourceNameMap];
  var index_map = GMLBindings[__ELuaRousrGMLBindData.ResourceIndexMap];

  var bind = [ _lua_name, _index ];
  name_map[? _lua_name] = bind;
  index_map[? _index]  = bind;
}

#define luaRousr_unbind_instance
///@dfunction luaRousr_unbind_instance(_id_or_name)
///@desc Removes an instance binding so that Lua no longer updates the GML instance.
///      **Note:** Any references to the **Lua** version of the object are still 'valid' in that they will act like a real instance... but they no longer are attached to the GML rendering them useless.
///@param {Real|String} _id_or_name   instance id OR Lua string Name of the instance to unbind
var _id_or_name = argument0;
 
with (LuaSr) {
  var id_map      = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  var name_map    = GMLBindings[__ELuaRousrGMLBindData.NameToBind];
  
  var bind_map = is_string(_id_or_name) ? name_map : id_map;
  var bind = bind_map[? _id_or_name];

  if (bind != undefined) {
    var _id        = bind[@ __ELuaRousrBindEntity.Id];
    var name       = bind[@ __ELuaRousrBindEntity.LuaName]
  
    // Clear the bind
    var binds  = GMLBindings[__ELuaRousrGMLBindData.Binds];
    sr_pool_release(binds, bind);
  
    // Remove it from datastructures
    ds_map_delete(id_map,      _id);
    ds_map_delete(name_map,    name);
  
    // Notify Lua its unhooked
    __extLuaRousrUnbindInstance(_id);
  }
}

#define luaRousr_unbind_resource
///@function luaRousr_unbind_resource(_index_or_lua_name)
///@desc unbind an object from Lua
///@param {Real|String} _index_or_lua_name - object index OR Lua string Name of the object to unbind
var _index_or_lua_name = argument0;

with (LuaSr) {
  var name_map  = GMLBindings[__ELuaRousrGMLBindData.ResourceNameMap];
  var index_map  = GMLBindings[__ELuaRousrGMLBindData.ResourceIndexMap];
  
  var map = undefined;
  if (is_string(_index_or_lua_name)) map = name_map;
  else if (is_real(_index_or_lua_name)) map = index_map;

  if (map == undefined)
    return;
  
  var bind = map[? _index_or_lua_name];
  if (bind == undefined)
    return;
    
  var _index = bind[@ __ELuaRousrResourceBind.Index];
  var _name  = bind[@ __ELuaRousrResourceBind.Name];

  name_map[? _name]   = undefined;
  index_map[? _index] = undefined;
}

#define luaRousr_set_global
///@func luaRousr_set_global(_name, _val)
///@desc set a value with `_name` in the Lua Globals table (global namespace)
///@param {String}      _name   name of global to be used in Lua
///@param {Real|String} _val    value to assign global
///@returns {Boolean} true if set
if (!__luaRousr_check_init())
  return undefined;
  
var _name = argument0,
    _val = argument1;

if (!is_real(_val)) {
  if (!is_string(_val))
    return undefined;
  
	__extLuaRousrSetGlobalString(_name, _val);
	return true;
}

__extLuaRousrSetGlobalReal(_name, _val);
return true;


#define luaRousr_get_global
///@function luaRousr_get_global(_name)
///@desc get the value of `_name` in the Lua Globals table (global namespace)
///@param {String} _name   the name of the Lua global variable
///@returns {Real|String} global value or undefined
if (!__luaRousr_check_init())
  return undefined;
  
var _name = argument0;
var returnBuffer = __luaRousr_pop_buffer();

__extLuaRousrGetGlobal(_name, _RousrDefaultBufferSize, buffer_get_address(returnBuffer));
var val = sr_buffer_read_val(returnBuffer);

__luaRousr_push_buffer(returnBuffer);
return val;

#define luaRousr_execute_file
///@function luaRousr_execute_file(_lua_file)
///@desc Load a .Lua file and call `luaRousr_execute_string` on it
///@param {String} _lua_file   file name of text file containing Lua.
///@returns {Real} undefined on failure, or the script context Id for all threads running from this execute.
if (!__luaRousr_check_init())
  return undefined;
  
var _lua_file = argument0;
var script = "";

var readFile = file_text_open_read(_lua_file);
if (readFile == -1)
  return undefined;

while (!file_text_eof(readFile)) script += file_text_readln(readFile);

file_text_close(readFile);
if (string_length(script) <= 0) 
  return undefined;
  
return luaRousr_execute_string(script);

#define luaRousr_execute_string
///@function lua_rousr_executeString(_script)
///@description Execute Lua script
///@param {String} _script   string containing Lua script
///@returns {Boolean} true on call successfully returned
if (!__luaRousr_check_init())
  return undefined;

var _luaScript = argument0;
var cmd_buffer = __luaRousr_pop_buffer();
var cmdId = __extLuaRousrExecuteLua(buffer_get_address(cmd_buffer), _luaScript);
__luaRousr_process_buffer(cmd_buffer, cmdId);
__luaRousr_push_buffer(cmd_buffer);

return cmdId >= 0;

#define luaRousr_call
///@functino luaRousr_call(_function, [_args...])
///@desc Call a Lua function with the given name, that simple!  
///      **NOTE: Returns are not functioning in the current version! No returns as of yet.**
///      [luaRousr_retain_callback](#luaRousr_retain_callback)
///      [luaRousr_release_callback](#luaRousr_release_callback)
///@param {String|Real}       _function    name of function defined in Lua or callback id previously retained 
///@param {...Real|...String} [_args...]   arguments to pass to Lua function
///@returns {Real|String} value lua function returns
///@todo - return value is not functioning
var _function = argument[0];

var arg_buffer    = __luaRousr_pop_buffer();
buffer_write(arg_buffer, buffer_u32, argument_count - 1);
for (var i = 1; i < argument_count; i++) {
  var arg = argument[i];
  if (!sr_buffer_write_val(arg_buffer, arg)) {
    show_debug_message("luaRousr_Call: Invalid argument type!")
    __luaRousr_push_buffer(arg_buffer);
    return undefined;
  }
}

var cmd_buffer = __luaRousr_pop_buffer();
if (is_string(_function))
  __luaRousr_process_buffer(cmd_buffer, __extLuaRousrCallLua(buffer_get_address(cmd_buffer), _function, _RousrDefaultBufferSize, buffer_get_address(arg_buffer)));
else if (is_real(_function))
  __luaRousr_process_buffer(cmd_buffer, __extLuaRousrCallLuaScriptId(buffer_get_address(cmd_buffer), _function, _RousrDefaultBufferSize, buffer_get_address(arg_buffer)));
var returnBuffer = arg_buffer;
buffer_seek(returnBuffer, buffer_seek_start, 0);
var validReturn = buffer_read(returnBuffer, buffer_s8) > 0;
var returnValue = validReturn ? sr_buffer_read_val(returnBuffer) : undefined;

__luaRousr_push_buffer(cmd_buffer);
__luaRousr_push_buffer(arg_buffer);

return returnValue;


#define luaRousr_retain_callback
///@function luaRousr_retain_callback(_callback_id) 
///@desc Increments the reference count for a callbackId. As long as the reference count is above 0, the callback is kept in memory.
///@desc When a `callbackId` is passed to GML you must retain it during that step, as it begins with a reference count of 0. If you're going to call it immediately, there's no need to retain it.
///@param {Real} _callback_id   callback Id passed to us from Lua
///@returns {Real} _callback_id or undefined
var _callbackid = argument0;
return __extLuaRousrRetainScriptCallback(_callbackid) >= 0 ? _callbackid : undefined;

#define luaRousr_release_callback
///@function luaRousr_release_callback(_callback_id)
///@desc Decrements the reference count for the callbackId. If it's completely released (refcount = 0), it queues the callbackId for deletion. The next `step` called by `LuaRousr` will release this callback from memory.
///@param {Real} _callback_id - callback Id passed to us from Lua
var _callback_id = argument0;
__extLuaRousrReleaseScriptCallback(_callback_id);

#define luaRousr_kill_threads
///@function luaRousr_kill_threads()
///@desc Kill all threads created in Lua script using the `thread(function)` Lua function.
///@desc When reloading scripts, it's definitely a good idea to use this function to make sure you have no wild `LuaThreads` running around.
__extLuaRousrKillThreads();

#define __luaRousr_event_create
///@function luaRousr_event_create()
///@desc Create event for LuaRousr Object
///@extensionizer { "docs": false }
#region Initialize

#region Debug Init
DebugToConsole = true;
DebugListeners = ds_list_create();

#endregion
#region Initialize Buffers

Buffer_list  = ds_list_create();
Buffer_stack = ds_stack_create();

// helper buffer
Argument_buffer = buffer_create(32767, buffer_fixed, 1);
buffer_write(Argument_buffer, buffer_u32, 32767);

#endregion
#region Bindings

GMLBindings = array_create(__ELuaRousrGMLBindData.Num);
GMLBindings[@ __ELuaRousrGMLBindData.Binds]            = sr_pool_create_pool(__extluarousr_script_index(__luaRousr_pool_alloc_bind));
GMLBindings[@ __ELuaRousrGMLBindData.IdToBind]         = ds_map_create();
GMLBindings[@ __ELuaRousrGMLBindData.NameToBind]       = ds_map_create();
GMLBindings[@ __ELuaRousrGMLBindData.BoundVariables]   = sr_array_create();
GMLBindings[@ __ELuaRousrGMLBindData.ResourceNameMap]  = ds_map_create();
GMLBindings[@ __ELuaRousrGMLBindData.ResourceIndexMap] = ds_map_create();
GMLBindings[@ __ELuaRousrGMLBindData.Payloads]         = ds_list_create();

// Function Binding User Data
ScriptUserData = ds_map_create();

#endregion

#endregion
#region !!! Initialize LuaSr
if (variable_global_exists("___luaRousr") && instance_exists(global.___luaRousr))
  return;

var initialized = __extLuaRousrInit();
if (initialized == 0) {
  show_debug_message("luaRousr - FATAL: Failed to load shared library!");
  global.___luaRousr       = undefined; 
  instance_destroy(id);
  return;
} 

global.___luaRousr       = id; 

#endregion
#region GMLInstance Bind Creation

var builtInData = [
//    string name,             default value,    read-only?   (__ELuaRousrBoundValue)
  [ "id",                    -1,                    true  ], // __ELuaRousrInstanceBuiltIn.Id
  [ "visible",               visible,               false ], // __ELuaRousrInstanceBuiltIn.Visible
  [ "solid",                 solid,                 false ], // __ELuaRousrInstanceBuiltIn.Solid
  [ "persistent",            persistent,            false ], // __ELuaRousrInstanceBuiltIn.Persistent
  [ "depth",                 depth,                 false ], // __ELuaRousrInstanceBuiltIn.Depth
  [ "layer",                 layer,                 false ], // __ELuaRousrInstanceBuiltIn.Layer
  [ "alarm",                 alarm,                 false ], // __ELuaRousrInstanceBuiltIn.Alarm,
  [ "object_index",          object_index,          true  ], // __ELuaRousrInstanceBuiltIn.ObjectIndex,
  [ "sprite_index",          sprite_index,          false ], // __ELuaRousrInstanceBuiltIn.SpriteIndex,
  [ "sprite_width",          sprite_width,          true  ], // __ELuaRousrInstanceBuiltIn.SpriteWidth,
  [ "sprite_height",         sprite_height,         true  ], // __ELuaRousrInstanceBuiltIn.SpriteHeight,
  [ "sprite_xoffset",        sprite_xoffset,        true  ], // __ELuaRousrInstanceBuiltIn.SpriteXoffset,
  [ "sprite_yoffset",        sprite_yoffset,        true  ], // __ELuaRousrInstanceBuiltIn.SpriteYoffset,
  [ "image_alpha",           image_alpha,           false ], // __ELuaRousrInstanceBuiltIn.ImageAlpha,
  [ "image_angle",           image_angle,           false ], // __ELuaRousrInstanceBuiltIn.ImageAngle,
  [ "image_blend",           image_blend,           false ], // __ELuaRousrInstanceBuiltIn.ImageBlend,
  [ "image_index",           image_index,           false ], // __ELuaRousrInstanceBuiltIn.ImageIndex,
  [ "image_number",          image_number,          true  ], // __ELuaRousrInstanceBuiltIn.ImageNumber,
  [ "image_speed",           image_speed,           false ], // __ELuaRousrInstanceBuiltIn.ImageSpeed,
  [ "image_xscale",          image_xscale,          false ], // __ELuaRousrInstanceBuiltIn.ImageXscale,
  [ "image_yscale",          image_yscale,          false ], // __ELuaRousrInstanceBuiltIn.ImageYscale,
  [ "mask_index",            mask_index,            false ], // __ELuaRousrInstanceBuiltIn.MaskIndex,
  [ "bbox_bottom",           bbox_bottom,           true  ], // __ELuaRousrInstanceBuiltIn.BboxBottom,
  [ "bbox_left",             bbox_left,             true  ], // __ELuaRousrInstanceBuiltIn.BboxLeft,
  [ "bbox_right",            bbox_right,            true  ], // __ELuaRousrInstanceBuiltIn.BboxRight,
  [ "bbox_top",              bbox_top,              true  ], // __ELuaRousrInstanceBuiltIn.BboxTop,
  [ "direction",             direction,             false ], // __ELuaRousrInstanceBuiltIn.Direction,
  [ "friction",              friction,              false ], // __ELuaRousrInstanceBuiltIn.Friction,
  [ "gravity",               gravity,               false ], // __ELuaRousrInstanceBuiltIn.Gravity,
  [ "gravity_direction",     gravity_direction,     false ], // __ELuaRousrInstanceBuiltIn.GravityDirection,
  [ "hspeed",                hspeed,                false ], // __ELuaRousrInstanceBuiltIn.Hspeed,
  [ "vspeed",                vspeed,                false ], // __ELuaRousrInstanceBuiltIn.Vspeed,
  [ "speed",                 speed,                 false ], // __ELuaRousrInstanceBuiltIn.Speed,
  [ "xstart",                xstart,                false ], // __ELuaRousrInstanceBuiltIn.Xstart,
  [ "ystart",                ystart,                false ], // __ELuaRousrInstanceBuiltIn.Ystart,
  [ "x",                     x,                     false ], // __ELuaRousrInstanceBuiltIn.X,
  [ "y",                     y,                     false ], // __ELuaRousrInstanceBuiltIn.Y,
  [ "xprevious",             xprevious,             false ], // __ELuaRousrInstanceBuiltIn.Xprevious,
  [ "yprevious",             yprevious,             false ], // __ELuaRousrInstanceBuiltIn.Yprevious,
  [ "path_index",            path_index,            true  ], // __ELuaRousrInstanceBuiltIn.PathIndex,
  [ "path_position",         path_position,         false ], // __ELuaRousrInstanceBuiltIn.PathPosition,
  [ "path_positionprevious", path_positionprevious, false ], // __ELuaRousrInstanceBuiltIn.PathPositionprevious,
  [ "path_speed",            path_speed,            false ], // __ELuaRousrInstanceBuiltIn.PathSpeed,
  [ "path_scale",            path_scale,            false ], // __ELuaRousrInstanceBuiltIn.PathScale,
  [ "path_orientation",      path_orientation,      false ], // __ELuaRousrInstanceBuiltIn.PathOrientation,
  [ "path_endaction",        path_endaction,        false ], // __ELuaRousrInstanceBuiltIn.PathEndaction,
  [ "timeline_index",        timeline_index,        false ], // __ELuaRousrInstanceBuiltIn.TimelineIndex,
  [ "timeline_running",      timeline_running,      false ], // __ELuaRousrInstanceBuiltIn.TimelineRunning,
  [ "timeline_speed",        timeline_speed,        false ], // __ELuaRousrInstanceBuiltIn.TimelineSpeed,
  [ "timeline_position",     timeline_position,     false ], // __ELuaRousrInstanceBuiltIn.TimelinePosition,
  [ "timeline_loop",         timeline_loop,         false ], // __ELuaRousrInstanceBuiltIn.TimelineLoop,
];

var numBuiltIns = array_length_1d(builtInData);
for (var bindIndex = 0; bindIndex < numBuiltIns; ++bindIndex) {
  var builtIn = builtInData[bindIndex];
    
  // look up all of the accessor script functions
  var getter = asset_get_index("__luaRousr_instance_builtin_get_" + string(builtIn[0]));
  var setter = asset_get_index("__luaRousr_instance_builtin_set_" + string(builtIn[0]));

  var success = __luaRousr_bind_member(bindIndex, builtIn[0], builtIn[1], builtIn[2], 
                                      getter, setter, undefined);
  
  // todo: filter bound variables
};

#endregion

luaRousr_bind_script("GMLResource", __extluarousr_script_index(__luaRousr_bind_resource_lua));


#define __luaRousr_event_async_social
///@function __luaRousr_event_async_social(_data)
///@desc Async Social Event for LuaRousr object
///@param {Real} _data   async_load map id
///@extensionizer { "docs": false }
var _data = argument0;

var debug_msg  = ds_map_find_value(_data, "DebugPrint");
if (debug_msg != undefined)
	__luaRousr_debug_print(debug_msg);

#define __luaRousr_event_clean_up
///@function luaRousr_event_clean_up()
///@desc Clean Up event for LuaRousr Object
///@extensionizer { "docs": false }
__extLuaRousrCleanUp();

global.___luaRousr       = noone;

ds_list_destroy(DebugListeners);

sr_pool_destroy_pool(GMLBindings[__ELuaRousrGMLBindData.Binds]);
sr_array_destroy(GMLBindings[__ELuaRousrGMLBindData.BoundVariables]);
ds_map_destroy(GMLBindings[__ELuaRousrGMLBindData.IdToBind]);
ds_map_destroy(GMLBindings[__ELuaRousrGMLBindData.NameToBind]);
ds_map_destroy(GMLBindings[__ELuaRousrGMLBindData.ResourceNameMap]);
ds_map_destroy(GMLBindings[__ELuaRousrGMLBindData.ResourceIndexMap]);
ds_list_destroy(GMLBindings[__ELuaRousrGMLBindData.Payloads]);
GMLBindings = undefined;

ds_map_destroy(ScriptUserData);
ScriptUserData = undefined;

ds_stack_destroy(Buffer_stack);
Buffer_stack = undefined;

while (!ds_list_empty(Buffer_list)) {
  var buffer = Buffer_list[| 0];
  ds_list_delete(Buffer_list, 0);
  buffer_delete(buffer);
}
ds_list_destroy(Buffer_list);
Buffer_list               = undefined;

buffer_delete(Argument_buffer);
Argument_buffer = undefined;

#define __luaRousr_bind_resource_lua
///@function __luaRousr_bind_resource_lua(_lua_name)
///@desc return a registered resource to Lua
///@param {string} _lua_name   name of resource
///@returns {Real} index of resource or noone
///@extensionizer { "docs": false }
var _lua_name = argument0;
var _index = noone;

with (LuaSr) {
  var name_map  = GMLBindings[__ELuaRousrGMLBindData.ResourceNameMap];
  var bind = name_map[? _lua_name];
  
  _index = bind[1];  
}

return _index;

#define __luaRousr_pool_alloc_bind
///@function __luaRousr_alloc_bind(_rousr_pool)
///@desc allocate a new array for binds
///@param {Array} _rousr_pool - pool being allocated for
///@returns {Array} new bind data array
///@extensionizer { "docs": false }
var _rousr_pool = argument0;
return array_create(__ELuaRousrBindEntity.Num);

#define __luaRousr_instance_builtin_get_bbox_right
///@function __luaRousr_instance_builtin_get_bbox_right()
///@desc builtin getter for bbox_right
///@returns {*} builtin
///@extensionizer { "docs": false }
return bbox_right;


#define __luaRousr_instance_builtin_get_bbox_top
///@function __luaRousr_instance_builtin_get_bbox_top()
///@desc builtin getter for bbox_top
///@returns {*} builtin
///@extensionizer { "docs": false }
return bbox_top;


#define __luaRousr_instance_builtin_get_depth
///@function __luaRousr_instance_builtin_get_depth()
///@desc builtin getter for depth
///@returns {*} builtin
///@extensionizer { "docs": false }
return depth;


#define __luaRousr_instance_builtin_get_direction
///@function __luaRousr_instance_builtin_get_direction()
///@desc builtin getter for direction
///@returns {*} builtin
///@extensionizer { "docs": false }
return direction;


#define __luaRousr_instance_builtin_get_friction
///@function __luaRousr_instance_builtin_get_friction()
///@desc builtin getter for friction
///@returns {*} builtin
///@extensionizer { "docs": false }
return friction;


#define __luaRousr_instance_builtin_get_gravity
///@function __luaRousr_instance_builtin_get_gravity()
///@desc builtin getter for gravity
///@returns {*} builtin
///@extensionizer { "docs": false }
return gravity;


#define __luaRousr_instance_builtin_get_gravity_direction
///@function __luaRousr_instance_builtin_get_gravity_direction()
///@desc builtin getter for gravity_direction
///@returns {*} builtin
///@extensionizer { "docs": false }
return gravity_direction;


#define __luaRousr_instance_builtin_get_hspeed
///@function __luaRousr_instance_builtin_get_hspeed()
///@desc builtin getter for hspeed
///@returns {*} builtin
///@extensionizer { "docs": false }
return hspeed;


#define __luaRousr_instance_builtin_get_id
///@function __luaRousr_instance_builtin_get_id()
///@desc builtin getter for id
///@returns {*} builtin
///@extensionizer { "docs": false }
return id;


#define __luaRousr_instance_builtin_get_image_alpha
///@function __luaRousr_instance_builtin_get_image_alpha()
///@desc builtin getter for image_alpha
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_alpha;


#define __luaRousr_instance_builtin_get_image_angle
///@function __luaRousr_instance_builtin_get_image_angle()
///@desc builtin getter for image_angle
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_angle;


#define __luaRousr_instance_builtin_get_image_blend
///@function __luaRousr_instance_builtin_get_image_blend()
///@desc builtin getter for image_blend
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_blend;


#define __luaRousr_instance_builtin_get_image_index
///@function __luaRousr_instance_builtin_get_image_index()
///@desc builtin getter for image_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_index;


#define __luaRousr_instance_builtin_get_image_number
///@function __luaRousr_instance_builtin_get_image_number()
///@desc builtin getter for image_number
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_number;


#define __luaRousr_instance_builtin_get_image_speed
///@function __luaRousr_instance_builtin_get_image_speed()
///@desc builtin getter for image_speed
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_speed;


#define __luaRousr_instance_builtin_get_image_xscale
///@function __luaRousr_instance_builtin_get_image_xscale()
///@desc builtin getter for image_xscale
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_xscale;


#define __luaRousr_instance_builtin_get_image_yscale
///@function __luaRousr_instance_builtin_get_image_yscale()
///@desc builtin getter for image_yscale
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_yscale;


#define __luaRousr_instance_builtin_get_layer
///@function __luaRousr_instance_builtin_get_layer()
///@desc builtin getter for layer
///@returns {*} builtin
///@extensionizer { "docs": false }
return layer;


#define __luaRousr_instance_builtin_get_mask_index
///@function __luaRousr_instance_builtin_get_mask_index()
///@desc builtin getter for mask_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return mask_index;


#define __luaRousr_instance_builtin_get_object_index
///@function __luaRousr_instance_builtin_get_object_index()
///@desc builtin getter for object_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return object_index;


#define __luaRousr_instance_builtin_get_path_endaction
///@function __luaRousr_instance_builtin_get_path_endaction()
///@desc builtin getter for path_endaction
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_endaction;


#define __luaRousr_instance_builtin_get_path_index
///@function __luaRousr_instance_builtin_get_path_index()
///@desc builtin getter for path_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_index;


#define __luaRousr_instance_builtin_get_path_orientation
///@function __luaRousr_instance_builtin_get_path_orientation()
///@desc builtin getter for path_orientation
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_orientation;


#define __luaRousr_instance_builtin_get_path_position
///@function __luaRousr_instance_builtin_get_path_position()
///@desc builtin getter for path_position
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_position;


#define __luaRousr_instance_builtin_get_path_positionprevious
///@function __luaRousr_instance_builtin_get_path_positionprevious()
///@desc builtin getter for path_positionprevious
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_positionprevious;


#define __luaRousr_instance_builtin_get_path_scale
///@function __luaRousr_instance_builtin_get_path_scale()
///@desc builtin getter for path_scale
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_scale;


#define __luaRousr_instance_builtin_get_path_speed
///@function __luaRousr_instance_builtin_get_path_speed()
///@desc builtin getter for path_speed
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_speed;


#define __luaRousr_instance_builtin_get_persistent
///@function __luaRousr_instance_builtin_get_persistent()
///@desc builtin getter for persistent
///@returns {*} builtin
///@extensionizer { "docs": false }
return persistent;


#define __luaRousr_instance_builtin_get_solid
///@function __luaRousr_instance_builtin_get_solid()
///@desc builtin getter for solid
///@returns {*} builtin
///@extensionizer { "docs": false }
return solid;


#define __luaRousr_instance_builtin_get_speed
///@function __luaRousr_instance_builtin_get_speed()
///@desc builtin getter for speed
///@returns {*} builtin
///@extensionizer { "docs": false }
return speed;


#define __luaRousr_instance_builtin_get_sprite_height
///@function __luaRousr_instance_builtin_get_sprite_height()
///@desc builtin getter for sprite_height
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_height;


#define __luaRousr_instance_builtin_get_sprite_index
///@function __luaRousr_instance_builtin_get_sprite_index()
///@desc builtin getter for sprite_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_index;


#define __luaRousr_instance_builtin_get_sprite_width
///@function __luaRousr_instance_builtin_get_sprite_width()
///@desc builtin getter for sprite_width
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_width;


#define __luaRousr_instance_builtin_get_sprite_xoffset
///@function __luaRousr_instance_builtin_get_sprite_xoffset()
///@desc builtin getter for sprite_xoffset
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_xoffset;


#define __luaRousr_instance_builtin_get_sprite_yoffset
///@function __luaRousr_instance_builtin_get_sprite_yoffset()
///@desc builtin getter for sprite_yoffset
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_yoffset;


#define __luaRousr_instance_builtin_get_timeline_index
///@function __luaRousr_instance_builtin_get_timeline_index()
///@desc builtin getter for timeline_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_index;


#define __luaRousr_instance_builtin_get_timeline_loop
///@function __luaRousr_instance_builtin_get_timeline_loop()
///@desc builtin getter for timeline_loop
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_loop;


#define __luaRousr_instance_builtin_get_timeline_position
///@function __luaRousr_instance_builtin_get_timeline_position()
///@desc builtin getter for timeline_position
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_position;


#define __luaRousr_instance_builtin_get_timeline_running
///@function __luaRousr_instance_builtin_get_timeline_running()
///@desc builtin getter for timeline_running
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_running;


#define __luaRousr_instance_builtin_get_timeline_speed
///@function __luaRousr_instance_builtin_get_timeline_speed()
///@desc builtin getter for timeline_speed
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_speed;


#define __luaRousr_instance_builtin_get_visible
///@function __luaRousr_instance_builtin_get_visible()
///@desc builtin getter for visible
///@returns {*} builtin
///@extensionizer { "docs": false }
return visible;


#define __luaRousr_instance_builtin_get_vspeed
///@function __luaRousr_instance_builtin_get_vspeed()
///@desc builtin getter for vspeed
///@returns {*} builtin
///@extensionizer { "docs": false }
return vspeed;


#define __luaRousr_instance_builtin_get_x
///@function __luaRousr_instance_builtin_get_x()
///@desc builtin getter for x
///@returns {*} builtin
///@extensionizer { "docs": false }
return x;


#define __luaRousr_instance_builtin_get_xprevious
///@function __luaRousr_instance_builtin_get_xprevious()
///@desc builtin getter for xprevious
///@returns {*} builtin
///@extensionizer { "docs": false }
return xprevious;


#define __luaRousr_instance_builtin_get_xstart
///@function __luaRousr_instance_builtin_get_xstart()
///@desc builtin getter for xstart
///@returns {*} builtin
///@extensionizer { "docs": false }
return xstart;


#define __luaRousr_instance_builtin_get_y
///@function __luaRousr_instance_builtin_get_y()
///@desc builtin getter for y
///@returns {*} builtin
///@extensionizer { "docs": false }
return y;


#define __luaRousr_instance_builtin_get_yprevious
///@function __luaRousr_instance_builtin_get_yprevious()
///@desc builtin getter for yprevious
///@returns {*} builtin
///@extensionizer { "docs": false }
return yprevious;


#define __luaRousr_instance_builtin_get_ystart
///@function __luaRousr_instance_builtin_get_ystart()
///@desc builtin getter for ystart
///@returns {*} builtin
///@extensionizer { "docs": false }
return ystart;


#define __luaRousr_instance_builtin_set_alarm
///@function __luaRousr_instance_builtin_set_alarm(_val)
///@desc builtin setter for alarm
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
alarm = argument0;


#define __luaRousr_instance_builtin_set_bbox_bottom
///@function __luaRousr_instance_builtin_set_bbox_bottom(_val)
///@desc builtin setter for bbox_bottom
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_bbox_left
///@function __luaRousr_instance_builtin_set_bbox_left(_val)
///@desc builtin setter for bbox_left
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_bbox_right
///@function __luaRousr_instance_builtin_set_bbox_right(_val)
///@desc builtin setter for bbox_right
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_bbox_top
///@function __luaRousr_instance_builtin_set_bbox_top(_val)
///@desc builtin setter for bbox_top
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_depth
///@function __luaRousr_instance_builtin_set_depth(_val)
///@desc builtin setter for depth
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
depth = argument0;


#define __luaRousr_instance_builtin_set_direction
///@function __luaRousr_instance_builtin_set_direction(_val)
///@desc builtin setter for direction
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
direction = argument0;


#define __luaRousr_instance_builtin_set_friction
///@function __luaRousr_instance_builtin_set_friction(_val)
///@desc builtin setter for friction
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
friction = argument0;


#define __luaRousr_instance_builtin_set_gravity
///@function __luaRousr_instance_builtin_set_gravity(_val)
///@desc builtin setter for gravity
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
gravity = argument0;


#define __luaRousr_instance_builtin_set_gravity_direction
///@function __luaRousr_instance_builtin_set_gravity_direction(_val)
///@desc builtin setter for gravity_direction
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
gravity_direction = argument0;


#define __luaRousr_instance_builtin_set_hspeed
///@function __luaRousr_instance_builtin_set_hspeed(_val)
///@desc builtin setter for hspeed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
hspeed = argument0;


#define __luaRousr_instance_builtin_set_id
///@function __luaRousr_instance_builtin_set_id(_val)
///@desc builtin setter for id
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_image_alpha
///@function __luaRousr_instance_builtin_set_image_alpha(_val)
///@desc builtin setter for image_alpha
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
image_alpha = argument0;


#define __luaRousr_instance_builtin_set_image_angle
///@function __luaRousr_instance_builtin_set_image_angle(_val)
///@desc builtin setter for image_angle
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
image_angle = argument0;


#define __luaRousr_instance_builtin_set_image_blend
///@function __luaRousr_instance_builtin_set_image_blend(_val)
///@desc builtin setter for image_blend
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
image_blend = argument0;


#define __luaRousr_instance_builtin_set_image_index
///@function __luaRousr_instance_builtin_set_image_index(_val)
///@desc builtin setter for image_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
image_index = argument0;


#define __luaRousr_instance_builtin_set_image_number
///@function __luaRousr_instance_builtin_set_image_number(_val)
///@desc builtin setter for image_number
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_image_speed
///@function __luaRousr_instance_builtin_set_image_speed(_val)
///@desc builtin setter for image_speed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
image_speed = argument0;


#define __luaRousr_instance_builtin_set_image_xscale
///@function __luaRousr_instance_builtin_set_image_xscale(_val)
///@desc builtin setter for image_xscale
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
image_xscale = argument0;


#define __luaRousr_instance_builtin_set_image_yscale
///@function __luaRousr_instance_builtin_set_image_yscale(_val)
///@desc builtin setter for image_yscale
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
image_yscale = argument0;


#define __luaRousr_instance_builtin_set_layer
///@function __luaRousr_instance_builtin_set_layer(_val)
///@desc builtin setter for layer
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
layer = argument0;


#define __luaRousr_instance_builtin_set_mask_index
///@function __luaRousr_instance_builtin_set_mask_index(_val)
///@desc builtin setter for mask_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
mask_index = argument0;


#define __luaRousr_instance_builtin_set_object_index
///@function __luaRousr_instance_builtin_set_object_index(_val)
///@desc builtin setter for object_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_path_endaction
///@function __luaRousr_instance_builtin_set_path_endaction(_val)
///@desc builtin setter for path_endaction
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_endaction = argument0;


#define __luaRousr_instance_builtin_set_path_index
///@function __luaRousr_instance_builtin_set_path_index(_val)
///@desc builtin setter for path_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_path_orientation
///@function __luaRousr_instance_builtin_set_path_orientation(_val)
///@desc builtin setter for path_orientation
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_orientation = argument0;


#define __luaRousr_instance_builtin_set_path_position
///@function __luaRousr_instance_builtin_set_path_position(_val)
///@desc builtin setter for path_position
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_position = argument0;


#define __luaRousr_instance_builtin_set_path_positionprevious
///@function __luaRousr_instance_builtin_set_path_positionprevious(_val)
///@desc builtin setter for path_positionprevious
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_positionprevious = argument0;


#define __luaRousr_instance_builtin_set_path_scale
///@function __luaRousr_instance_builtin_set_path_scale(_val)
///@desc builtin setter for path_scale
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_scale = argument0;


#define __luaRousr_instance_builtin_set_path_speed
///@function __luaRousr_instance_builtin_set_path_speed(_val)
///@desc builtin setter for path_speed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_speed = argument0;


#define __luaRousr_instance_builtin_set_persistent
///@function __luaRousr_instance_builtin_set_persistent(_val)
///@desc builtin setter for persistent
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
persistent = argument0;


#define __luaRousr_instance_builtin_set_solid
///@function __luaRousr_instance_builtin_set_solid(_val)
///@desc builtin setter for solid
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
solid = argument0;


#define __luaRousr_instance_builtin_set_speed
///@function __luaRousr_instance_builtin_set_speed(_val)
///@desc builtin setter for speed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
speed = argument0;


#define __luaRousr_instance_builtin_set_sprite_height
///@function __luaRousr_instance_builtin_set_sprite_height(_val)
///@desc builtin setter for sprite_height
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_sprite_index
///@function __luaRousr_instance_builtin_set_sprite_index(_val)
///@desc builtin setter for sprite_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
sprite_index = argument0;


#define __luaRousr_instance_builtin_set_sprite_width
///@function __luaRousr_instance_builtin_set_sprite_width(_val)
///@desc builtin setter for sprite_width
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_sprite_xoffset
///@function __luaRousr_instance_builtin_set_sprite_xoffset(_val)
///@desc builtin setter for sprite_xoffset
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_sprite_yoffset
///@function __luaRousr_instance_builtin_set_sprite_yoffset(_val)
///@desc builtin setter for sprite_yoffset
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!


#define __luaRousr_instance_builtin_set_timeline_index
///@function __luaRousr_instance_builtin_set_timeline_index(_val)
///@desc builtin setter for timeline_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
timeline_index = argument0;


#define __luaRousr_instance_builtin_set_timeline_loop
///@function __luaRousr_instance_builtin_set_timeline_loop(_val)
///@desc builtin setter for timeline_loop
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
timeline_loop = argument0;


#define __luaRousr_instance_builtin_set_timeline_position
///@function __luaRousr_instance_builtin_set_timeline_position(_val)
///@desc builtin setter for timeline_position
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
timeline_position = argument0;


#define __luaRousr_instance_builtin_set_timeline_running
///@function __luaRousr_instance_builtin_set_timeline_running(_val)
///@desc builtin setter for timeline_running
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
timeline_running = argument0;


#define __luaRousr_instance_builtin_set_timeline_speed
///@function __luaRousr_instance_builtin_set_timeline_speed(_val)
///@desc builtin setter for timeline_speed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
timeline_speed = argument0;


#define __luaRousr_instance_builtin_set_visible
///@function __luaRousr_instance_builtin_set_visible(_val)
///@desc builtin setter for visible
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
visible = argument0;


#define __luaRousr_instance_builtin_set_vspeed
///@function __luaRousr_instance_builtin_set_vspeed(_val)
///@desc builtin setter for vspeed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
vspeed = argument0;


#define __luaRousr_instance_builtin_set_x
///@function __luaRousr_instance_builtin_set_x(_val)
///@desc builtin setter for x
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
x = argument0;


#define __luaRousr_instance_builtin_set_xprevious
///@function __luaRousr_instance_builtin_set_xprevious(_val)
///@desc builtin setter for xprevious
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
xprevious = argument0;


#define __luaRousr_instance_builtin_set_xstart
///@function __luaRousr_instance_builtin_set_xstart(_val)
///@desc builtin setter for xstart
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
xstart = argument0;


#define __luaRousr_instance_builtin_set_y
///@function __luaRousr_instance_builtin_set_y(_val)
///@desc builtin setter for y
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
y = argument0;


#define __luaRousr_instance_builtin_set_yprevious
///@function __luaRousr_instance_builtin_set_yprevious(_val)
///@desc builtin setter for yprevious
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
yprevious = argument0;


#define __luaRousr_instance_builtin_set_ystart
///@function __luaRousr_instance_builtin_set_ystart(_val)
///@desc builtin setter for ystart
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
ystart = argument0;


#define __luaRousr_instance_builtin_get_alarm
///@function __luaRousr_instance_builtin_get_alarm()
///@desc builtin getter for alarm
///@returns {*} builtin
///@extensionizer { "docs": false }
return alarm;


#define __luaRousr_instance_builtin_get_bbox_bottom
///@function __luaRousr_instance_builtin_get_bbox_bottom()
///@desc builtin getter for bbox_bottom
///@returns {*} builtin
///@extensionizer { "docs": false }
return bbox_bottom;


#define __luaRousr_instance_builtin_get_bbox_left
///@function __luaRousr_instance_builtin_get_bbox_left()
///@desc builtin getter for bbox_left
///@returns {*} builtin
///@extensionizer { "docs": false }
return bbox_left;


#define __luaRousr_check_init
///@function __luaRousr_check_init([_no_warn=false])
///@desc return true if we're ready to roll, warn if not initialized
///@param {Boolean} [_no_warn=false]   pass true it suppress warning
///@returns {Boolean} true if luaRousr is initialized
///@extensionizer { "docs": false }
gml_pragma("forceinline");

with (LuaSr)
   return true;

if (argument_count == 0 || !argument[0])
  show_debug_message("luaRousr Error - not initialized");

return false;

#define __luaRousr_executeGML
///@function __luaRousr_executeGML(_out, _scriptId, _args, _argCount)
///@desc Called from Lua, execute a GML function based passing the arguments from Lua, as necessary, and return value back.
///@param {Real}  _out           output buffer to write returns to.
///@param {Real}  _script_index  script index of the function to execute
///@param {Array} _args          array of arguments
///@param {Real}  _argCount      amount of args
///@extensionizer { "docs": false }
var _out         = argument0;
var _scriptId    = argument1;
var _args        = argument2;
var _argCount    = argument3;

var args = undefined;
var userData = LuaSr.ScriptUserData[? _scriptId];
if (userData != undefined && userData[0] > 0) {
  var userDataCount = userData[0];
  userData = userData[1];
  args = array_create(userDataCount + _argCount);
  array_copy(args, 0, userData, 0, userDataCount);
  array_copy(args, userDataCount, _args, 0, _argCount);
  _argCount += userDataCount;
} else 
  args = _args;

// Execute
var ret = sr_execute(_scriptId, args, _argCount);

var numRet = 1;
buffer_seek(_out, buffer_seek_start, 6);
if (is_string(ret))    { buffer_write(_out, buffer_u8, ERousrData.String); buffer_write(_out, buffer_string, ret); }
else if (is_real(ret)) { buffer_write(_out, buffer_u8, ERousrData.Double); buffer_write(_out, buffer_f64, ret); }
else if (is_bool(ret)) { buffer_write(_out, buffer_u8, ERousrData.Bool);   buffer_write(_out, buffer_u8, ret ? 1 : 0); }
else                   numRet = 0;

buffer_seek(_out, buffer_seek_start, 5);
buffer_write(_out, buffer_u8, numRet);
buffer_seek(_out, buffer_seek_start, 1);
buffer_write(_out, buffer_u32, _scriptId);

#define __luaRousr_process_buffer
///@function __luaRousr_process_buffer(_cmd_buffer, _cmd_id)
///@description process the buffer from Lua, until we're done
///@param {Real} _cmd_buffer   Id of the buffer passed to LuaSr to process Lua commands
///@param {Real} _cmd_id       Id of the command to ensure we didn't corrupt our processing buffer.
///@extensionizer { "docs": false }
var _cmd_buffer = argument0;
var _cmd_id     = argument1;
if (_cmd_id < 0)
  return;

buffer_seek(_cmd_buffer, buffer_seek_start, 0); 

var cmdId = undefined;
var status = __ELuaRousrStatus.Waiting;
while (status != __ELuaRousrStatus.Done) {
  __extLuaRousrWaitForLua(_cmd_id);
  status = buffer_read(_cmd_buffer, buffer_u8);
 	cmdId  = buffer_read(_cmd_buffer, buffer_u32);
  switch (status) {
    case __ELuaRousrStatus.Ready: {
  	  var callType     = buffer_read(_cmd_buffer, buffer_s8);
      
  	  switch(callType) {
        case __ELuaRousrCmd.DebugPrint: {
          var msg = buffer_read(_cmd_buffer, buffer_string);
          __luaRousr_debug_print(msg)
          __luaRousr_reset_buffer(_cmd_buffer, _cmd_id);
        } break;
        
        case __ELuaRousrCmd.GMLBound: {
          __luaRousr_sync_read(_cmd_buffer);
          var script_index = buffer_read(_cmd_buffer, buffer_u32);
					
    			var argCount = buffer_read(_cmd_buffer, buffer_u8);
    			var args = array_create(argCount)
    			for (var argIndex = 0; argIndex < argCount; ++argIndex) {
    				args[argIndex] = sr_buffer_read_val(_cmd_buffer)
    			}
					
    			__luaRousr_executeGML(_cmd_buffer, script_index, args, argCount);
          __luaRousr_reset_buffer(_cmd_buffer, _cmd_id);
        } break;
        
        // Lua requesting an instance sync
        case __ELuaRousrCmd.RequestSync: {
          var num = buffer_read(_cmd_buffer, buffer_u32);
          if (num > 0) {
            for (var i = 0; i < num; ++i) {
              var _id = buffer_read(_cmd_buffer, buffer_f64);
              __luaRousr_write_instance(_id, false);
            }
          } else {
            show_debug_message("luaRousr - warning: Got a sync request with no ids.");
          }
          
          __luaRousr_sync_write(_cmd_buffer);
          __luaRousr_reset_buffer(_cmd_buffer, _cmd_id);
        } break;
      } 
    } break;
    
    case __ELuaRousrStatus.Done: { 
      if (cmdId == _cmd_id) {
        __luaRousr_sync_read(_cmd_buffer);
        break;
      }
      
      show_debug_message("lua Rousr - ERROR: cmd buffer corrupt! Attempting to recover.");
      status = __ELuaRousrStatus.Waiting
    } break;
  } 
  
  buffer_seek(_cmd_buffer, buffer_seek_start, 0);
}

__extLuaRousrFinished(_cmd_id);

#define __luaRousr_reset_buffer
///@desc __luaRousr_reset_buffer - signal the DLL to write more data
///@param {Real} _cmdBuffer - buffer id, buffer that Lua and GML pass
///@param {Real} _cmdId     - id to notify Lua which command buffer this is for. (sanity checks)
///@extensionizer { "docs": false }
var _cmdBuffer = argument0;
var _cmdId     = argument1;
buffer_seek(_cmdBuffer, buffer_seek_start, 0);
buffer_write(_cmdBuffer, buffer_u8, __ELuaRousrStatus.Waiting);
buffer_seek(_cmdBuffer, buffer_seek_start, 0);
__extLuaRousrReadyForLua(_cmdId);

#define __luaRousr_push_buffer
///@desc __luaRousr_push_buffer - push a buffer back onto LuaSr's buffer stack
///@param {Real} _buffer - id of the buffer no longer used in Lua
///@extensionizer { "docs": false }
var _buffer = argument0;
with (LuaSr) {
  ds_stack_push(Buffer_stack, _buffer);
}

#define __luaRousr_pop_buffer
///@fucntion __luaRousr_pop_buffer()
///@desc pop a buffer from the buffer stack, put its size back in it.
///@returns {Real} id of the popped buffer, ready for Lua with its size as the first element
///@extensionizer { "docs": false }
var buffer = undefined;
with(LuaSr) {
  if (!ds_stack_empty(Buffer_stack))
    buffer = ds_stack_pop(Buffer_stack);
  else {
    buffer = buffer_create(_RousrDefaultBufferSize, buffer_fixed, 1);
    ds_list_add(Buffer_list, buffer);
  }
  
  buffer_seek(buffer, buffer_seek_start, 0);
  buffer_write(buffer, buffer_u16, _RousrDefaultBufferSize);
  buffer_seek(buffer, buffer_seek_start, 0);
}

return buffer;

#define __luaRousr_bind_member
///@func __luaRousr_bind_member(_bindIndex, _member_name, _default, _read_only, [_forceType])
///@desc wrapper to bind members to a binding object
///@param {Real}        _bindIndex   - numeric index of the bind to refer to it by in the future.
///@param {String}      _member_name  - name of this member to refer to it by in Lua i.e., `instance._member_name`
///@param {Real|String} _default  - default value of this member, if _forceType is undefined (not passed), the type written is based on this value.
///@param {Boolean}     _read_only    - can you write to this value?
///@param {Real}        _getter        - script_index of getter function
///@param {Real}        _setter        - script_idnex of setter function
///@param {Real|String} [_forceType] - ERousrData or string representing type to optionally force use for this value
///@returns {Boolean} true on success
///@extensionizer { "docs": false }
var _bindIndex  = argument[0];
var _member_name = argument[1];
var _default = argument[2];
var _read_only   = argument[3];
var _getter     = argument[4];
var _setter     = argument[5];
var _forceType  = argument[6];

if (_forceType == undefined) {
	if      (is_bool(_default))    _forceType = ERousrData.Bool;
	else if (is_real(_default))  	_forceType = ERousrData.Double;
	else if (is_string(_default))	_forceType = ERousrData.String;
}

if (is_string(_forceType)) {
  switch (_forceType) {
    case "real":   _forceType = ERousrData.Double; break;
    case "string": _forceType = ERousrData.String; break;
    case "bool":   _forceType = ERousrData.Bool; break;
  }
}


buffer_seek(Argument_buffer, buffer_seek_start, 4);
buffer_write(Argument_buffer, buffer_u32, _bindIndex);
buffer_write(Argument_buffer, buffer_string, _member_name);
sr_buffer_write_val(Argument_buffer, _default, _forceType);
buffer_write(Argument_buffer, buffer_bool, _read_only);

var success = __extLuaRousrBindMember(buffer_get_address(Argument_buffer)) >= 0;
if (!success)
  show_debug_message("luaRousr - warning: can't bind member '" + string(_member_name) + "' invalid type passed for default value (can only be bool/real/string).");
else {
  with (LuaSr) {
    var boundVals = GMLBindings[__ELuaRousrGMLBindData.BoundVariables];
    sr_array_insert(boundVals, _bindIndex, [ _member_name, _default, _read_only, _getter, _setter ]);
  }
}

return success;

#define __luaRousr_read_instance
///#function __luaRousr_read_instance(_id, _contextValues, _dirtyIndices, _dirtyCount)
///@desc internal function to read an instance's values from a sync
///@param {Real}  _id              instance id
///@param {Array} _contextValues   Array of values to set instance built-ins to
///@param {Array} _dirtyIndices    Array of dirty values
///@param {Real}  _dirtyCount      Amount of dirty values in array already, add new dirty indices at the end [deprecated]
///@extensionizer { "docs": false }
var _id            = argument0;
var _contextValues = argument1;
var _dirtyIndices  = argument2;
var _dirtyCount    = argument3;

with (LuaSr) {
  var id_map = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  var bind = id_map[? _id];
  if (bind == undefined)
    return;

  
}

#define __luaRousr_write_instance
///@func __luaRousr_write_instance(_id, _force)
///@desc sync the instance
///@param {Real}         _id   instance to sync
///@param {Boolean}   _force   should we force update every argument (full flush)
///@extensionizer { "docs": false }
var _id        = argument0;
var _force     = argument1;

var variables =  [ ];
var bind = undefined;
with (LuaSr) {
  var boundVariablesData = GMLBindings[__ELuaRousrGMLBindData.BoundVariables];
  var boundVariables     = boundVariablesData[ERousrArray.Data];
  var numBoundVariables  = boundVariablesData[ERousrArray.Count];
  
  var id_map = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  var bind = id_map[? _id];
  
  var syncVals = bind[__ELuaRousrBindEntity.Values];
  var numSync  = sr_array_size(syncVals);

  var valueCount  = max(numSync, numBoundVariables);
  var dirtyValues = array_create(valueCount);	// Create greedy arrays for performance
  var dirtyIndex  = 0;
  
  with (_id) {
    var  variableIndex = 0;
    for (var i = 0; i < numBoundVariables; ++i) {
      var boundVariable = boundVariables[i];
      var getter = boundVariable[__ELuaRousrBoundValue.Getter];
      var val = script_execute(getter, boundVariable[__ELuaRousrBoundValue.Name], variableIndex);
      dirtyIndex = __luaRousr_write_val(syncVals, variableIndex, val, dirtyValues, dirtyIndex, _force);
      variableIndex++;
    }
    
    if (bind != undefined) {  
      var customBinds = bind[__ELuaRousrBindEntity.CustomMembers];
      var numCustom = sr_array_size(customBinds);
      var customVars = sr_array_data(customBinds);
  
      for (var i = 0; i < numCustom ; ++i) {
        var customVariable = customVars[i];
        var getter = customVariable[__ELuaRousrBoundValue.Getter];
        var val = script_execute(getter, customVariable[__ELuaRousrBoundValue.Name], variableIndex);
        dirtyIndex = __luaRousr_write_val(syncVals, variableIndex, val, dirtyValues, dirtyIndex, _force);
        variableIndex++;
      }
    }
  }

  if (dirtyIndex > 0) {
  	var payloads = GMLBindings[__ELuaRousrGMLBindData.Payloads];
  	ds_list_add(payloads, [ _id, dirtyValues, dirtyIndex ]);
  }
}

#define __luaRousr_write_val
///@desc __luaRousr_write_val - compare a value to a context value array's value.
///@param {Array}       _syncVals      - sr_array of values this context was last updated with 
///@param {Real}        _contextIndex  - index in contextValues this value is
///@param {Real|String} _val           - value to update contextValues with
///@param {Array}       _dirtyValues   - array of already dirty values, this contextIndex is added if its dirty
///@param {Real}        _dirtyIndex    - index in the dirtyArray to use
///@param {Boolean}      _force        - force set the value regardless if its dirty
///@returns {Real} the next _dirtyIndex to use for the next contextValue
///@extensionizer { "docs": false }
gml_pragma("forceinline");

var _syncVals      = argument0;
var _contextIndex  = argument1;
var _val           = argument2;
var _dirty         = argument3;
var _dirtyIndex    = argument4;
var _force         = argument5;

var _contextValues = sr_array_data(_syncVals);
var _contextCount  = sr_array_size(_syncVals);

if (_contextIndex < _contextCount) {
	var oldVal = _contextValues[_contextIndex];
	if (!_force && oldVal == _val)
		return _dirtyIndex;

  _contextValues[@ _contextIndex] = oldVal;
} else {
  var insertVal = 0;
  if (is_real(_val) || is_string(_val)) {
  	insertVal = _val;
  } else if (is_bool(_val) && _val) {
    insertVal = _val
  } 
  
  sr_array_insert(_syncVals, _contextIndex, insertVal);
}

_dirty[@ _dirtyIndex++] = _contextIndex;
_dirty[@ _dirtyIndex++] = _val;

return _dirtyIndex;

#define __luaRousr_sync_write
///@function __luaRousr_sync_write(_buffer)
///@desc write all pending payloads to the DLL buffer, and clear the payload list
///@param {Real) buffer   buffer being processed
///@extensionizer { "docs": false }
var buffer = argument0;

with (LuaSr) {
  var payloads     = GMLBindings[__ELuaRousrGMLBindData.Payloads];
  var payloadCount = ds_list_size(payloads);

  buffer_seek(buffer, buffer_seek_start, 1);
  buffer_write(buffer, buffer_u32, payloadCount);

  // Write any dirty contexts since last update
  if (payloadCount > 0) {
    
  	for (var payloadIndex = 0; payloadIndex < payloadCount; ++payloadIndex) {	
  		var payload = payloads[| payloadIndex];
  		var _id         = payload[__ELuaRousrPayload.Id];
  		var vals        = payload[__ELuaRousrPayload.Payload];
      var payloadSize = payload[__ELuaRousrPayload.PayloadSize];
    
    	buffer_write(buffer, buffer_f64, _id);
    	buffer_write(buffer, buffer_u32, payloadSize / 2);

    	for (var i = 0; i < payloadSize - 1; i+=2) {
    		var valIndex = vals[i];
    		var val =      vals[i + 1];
    		buffer_write(buffer, buffer_u32, valIndex);
    		sr_buffer_write_val(buffer, val);
    	}
    }
	
    ds_list_clear(payloads);
  }
}

#define __luaRousr_sync_read
///@func __luaRousr_sync_read(_buffer)
///@desc check and process any updated values from the dll
///@param {Real} buffer   buffer id
///@extensionizer { "docs": false }
var buffer       = argument0;

with (LuaSr) {
  var numWritten = buffer_read(buffer, buffer_u32);
  if (numWritten == 0)
    return;
  
  var id_map = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  var boundVals = GMLBindings[__ELuaRousrGMLBindData.BoundVariables];
  var numBound = sr_array_size(boundVals);
  boundVals = sr_array_data(boundVals);
  
  repeat(numWritten) {
  	var _id    = buffer_read(buffer, buffer_f64);
  	var count  = buffer_read(buffer, buffer_u32);
    
  	var bind = id_map[? _id];
    if (bind == undefined) {
      show_debug_message("__luaRousr_sync_read - warning: Invalid bind! Trying to recover.");
      for (var i = 0; i < count; ++i) {
  	    buffer_read(buffer, buffer_u32);
  		  sr_buffer_read_val(buffer);
      }
      
      continue;
    }
        
  	var syncVals = bind[__ELuaRousrBindEntity.Values];
    var customBinds = bind[__ELuaRousrBindEntity.CustomMembers];
    
    with(_id) {
      repeat(count) {
    		var valIndex = buffer_read(buffer, buffer_u32);
    		var val = sr_buffer_read_val(buffer);
    		syncVals[@valIndex] = val;	
        
        if (valIndex < numBound) {
          var bound  = boundVals[valIndex];
          var setter = bound[__ELuaRousrBoundValue.Setter];
          script_execute(setter, val);
        } else {
          var numCustom = sr_array_size(customBinds);
          var customIndex = valIndex - numBound;

          if (customIndex < numCustom) {
            var customVal = sr_array(customBinds, customIndex);
            var setter = customVal[__ELuaRousrBoundValue.Setter];
            script_execute(setter, val, customVal[__ELuaRousrBoundValue.Name], valIndex);
          } else {
            show_debug_message("luaRousr - warning: __luaRousr_sync_read has an invalid index");
          }
        }
    	}
	  }
  }
}

#define __luaRousr_debug_print
///@func __luaRousr_debug_print(_msg)
///@desc print a message from the Lua debug call
///@param {string} _msg   messge from Lua
///@extensionizer { "docs": false }
var _msg = argument0;
var to_console = true;

with (LuaSr) {
  var num_listeners = ds_list_size(DebugListeners);
  for (var i = 0; i < num_listeners; ++i) {
    script_execute(DebugListeners[| i], _msg);
  }
  to_console = DebugToConsole;
}

if (to_console)
  show_debug_message(_msg);


#define __extluarousr_script_index
///@function __extluarousr_script_index(ext_script_index)
///@desc Returns the actual runtime script index because YYG doesn't know how to do that apparently
///@param {Real} ext_script_index
///@extensionizer { "docs": false, "export": true} 
///@returns {Real} script_index
gml_pragma("forceinline")
gml_pragma("global", "global.__extluarousr_script_index_lookup = array_create(0);global.__extluarousr_script_index_lookup[@ luaRousr_init] = asset_get_index(\"luaRousr_init\");global.__extluarousr_script_index_lookup[@ luaRousr_add_debug_print_listener] = asset_get_index(\"luaRousr_add_debug_print_listener\");global.__extluarousr_script_index_lookup[@ luaRousr_remove_debug_print_listener] = asset_get_index(\"luaRousr_remove_debug_print_listener\");global.__extluarousr_script_index_lookup[@ luaRousr_tick] = asset_get_index(\"luaRousr_tick\");global.__extluarousr_script_index_lookup[@ luaRousr_default_getter] = asset_get_index(\"luaRousr_default_getter\");global.__extluarousr_script_index_lookup[@ luaRousr_default_setter] = asset_get_index(\"luaRousr_default_setter\");global.__extluarousr_script_index_lookup[@ luaRousr_bind_script] = asset_get_index(\"luaRousr_bind_script\");global.__extluarousr_script_index_lookup[@ luaRousr_bind_instance_variable] = asset_get_index(\"luaRousr_bind_instance_variable\");global.__extluarousr_script_index_lookup[@ luaRousr_bind_instance] = asset_get_index(\"luaRousr_bind_instance\");global.__extluarousr_script_index_lookup[@ luaRousr_bind_resource] = asset_get_index(\"luaRousr_bind_resource\");global.__extluarousr_script_index_lookup[@ luaRousr_unbind_instance] = asset_get_index(\"luaRousr_unbind_instance\");global.__extluarousr_script_index_lookup[@ luaRousr_unbind_resource] = asset_get_index(\"luaRousr_unbind_resource\");global.__extluarousr_script_index_lookup[@ luaRousr_set_global] = asset_get_index(\"luaRousr_set_global\");global.__extluarousr_script_index_lookup[@ luaRousr_get_global] = asset_get_index(\"luaRousr_get_global\");global.__extluarousr_script_index_lookup[@ luaRousr_execute_file] = asset_get_index(\"luaRousr_execute_file\");global.__extluarousr_script_index_lookup[@ luaRousr_execute_string] = asset_get_index(\"luaRousr_execute_string\");global.__extluarousr_script_index_lookup[@ luaRousr_call] = asset_get_index(\"luaRousr_call\");global.__extluarousr_script_index_lookup[@ luaRousr_retain_callback] = asset_get_index(\"luaRousr_retain_callback\");global.__extluarousr_script_index_lookup[@ luaRousr_release_callback] = asset_get_index(\"luaRousr_release_callback\");global.__extluarousr_script_index_lookup[@ luaRousr_kill_threads] = asset_get_index(\"luaRousr_kill_threads\");global.__extluarousr_script_index_lookup[@ __luaRousr_event_create] = asset_get_index(\"__luaRousr_event_create\");global.__extluarousr_script_index_lookup[@ __luaRousr_event_async_social] = asset_get_index(\"__luaRousr_event_async_social\");global.__extluarousr_script_index_lookup[@ __luaRousr_event_clean_up] = asset_get_index(\"__luaRousr_event_clean_up\");global.__extluarousr_script_index_lookup[@ __luaRousr_bind_resource_lua] = asset_get_index(\"__luaRousr_bind_resource_lua\");global.__extluarousr_script_index_lookup[@ __luaRousr_pool_alloc_bind] = asset_get_index(\"__luaRousr_pool_alloc_bind\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_bbox_right] = asset_get_index(\"__luaRousr_instance_builtin_get_bbox_right\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_bbox_top] = asset_get_index(\"__luaRousr_instance_builtin_get_bbox_top\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_depth] = asset_get_index(\"__luaRousr_instance_builtin_get_depth\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_direction] = asset_get_index(\"__luaRousr_instance_builtin_get_direction\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_friction] = asset_get_index(\"__luaRousr_instance_builtin_get_friction\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_gravity] = asset_get_index(\"__luaRousr_instance_builtin_get_gravity\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_gravity_direction] = asset_get_index(\"__luaRousr_instance_builtin_get_gravity_direction\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_hspeed] = asset_get_index(\"__luaRousr_instance_builtin_get_hspeed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_id] = asset_get_index(\"__luaRousr_instance_builtin_get_id\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_image_alpha] = asset_get_index(\"__luaRousr_instance_builtin_get_image_alpha\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_image_angle] = asset_get_index(\"__luaRousr_instance_builtin_get_image_angle\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_image_blend] = asset_get_index(\"__luaRousr_instance_builtin_get_image_blend\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_image_index] = asset_get_index(\"__luaRousr_instance_builtin_get_image_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_image_number] = asset_get_index(\"__luaRousr_instance_builtin_get_image_number\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_image_speed] = asset_get_index(\"__luaRousr_instance_builtin_get_image_speed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_image_xscale] = asset_get_index(\"__luaRousr_instance_builtin_get_image_xscale\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_image_yscale] = asset_get_index(\"__luaRousr_instance_builtin_get_image_yscale\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_layer] = asset_get_index(\"__luaRousr_instance_builtin_get_layer\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_mask_index] = asset_get_index(\"__luaRousr_instance_builtin_get_mask_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_object_index] = asset_get_index(\"__luaRousr_instance_builtin_get_object_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_path_endaction] = asset_get_index(\"__luaRousr_instance_builtin_get_path_endaction\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_path_index] = asset_get_index(\"__luaRousr_instance_builtin_get_path_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_path_orientation] = asset_get_index(\"__luaRousr_instance_builtin_get_path_orientation\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_path_position] = asset_get_index(\"__luaRousr_instance_builtin_get_path_position\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_path_positionprevious] = asset_get_index(\"__luaRousr_instance_builtin_get_path_positionprevious\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_path_scale] = asset_get_index(\"__luaRousr_instance_builtin_get_path_scale\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_path_speed] = asset_get_index(\"__luaRousr_instance_builtin_get_path_speed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_persistent] = asset_get_index(\"__luaRousr_instance_builtin_get_persistent\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_solid] = asset_get_index(\"__luaRousr_instance_builtin_get_solid\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_speed] = asset_get_index(\"__luaRousr_instance_builtin_get_speed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_sprite_height] = asset_get_index(\"__luaRousr_instance_builtin_get_sprite_height\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_sprite_index] = asset_get_index(\"__luaRousr_instance_builtin_get_sprite_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_sprite_width] = asset_get_index(\"__luaRousr_instance_builtin_get_sprite_width\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_sprite_xoffset] = asset_get_index(\"__luaRousr_instance_builtin_get_sprite_xoffset\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_sprite_yoffset] = asset_get_index(\"__luaRousr_instance_builtin_get_sprite_yoffset\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_timeline_index] = asset_get_index(\"__luaRousr_instance_builtin_get_timeline_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_timeline_loop] = asset_get_index(\"__luaRousr_instance_builtin_get_timeline_loop\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_timeline_position] = asset_get_index(\"__luaRousr_instance_builtin_get_timeline_position\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_timeline_running] = asset_get_index(\"__luaRousr_instance_builtin_get_timeline_running\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_timeline_speed] = asset_get_index(\"__luaRousr_instance_builtin_get_timeline_speed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_visible] = asset_get_index(\"__luaRousr_instance_builtin_get_visible\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_vspeed] = asset_get_index(\"__luaRousr_instance_builtin_get_vspeed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_x] = asset_get_index(\"__luaRousr_instance_builtin_get_x\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_xprevious] = asset_get_index(\"__luaRousr_instance_builtin_get_xprevious\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_xstart] = asset_get_index(\"__luaRousr_instance_builtin_get_xstart\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_y] = asset_get_index(\"__luaRousr_instance_builtin_get_y\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_yprevious] = asset_get_index(\"__luaRousr_instance_builtin_get_yprevious\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_ystart] = asset_get_index(\"__luaRousr_instance_builtin_get_ystart\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_alarm] = asset_get_index(\"__luaRousr_instance_builtin_set_alarm\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_bbox_bottom] = asset_get_index(\"__luaRousr_instance_builtin_set_bbox_bottom\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_bbox_left] = asset_get_index(\"__luaRousr_instance_builtin_set_bbox_left\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_bbox_right] = asset_get_index(\"__luaRousr_instance_builtin_set_bbox_right\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_bbox_top] = asset_get_index(\"__luaRousr_instance_builtin_set_bbox_top\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_depth] = asset_get_index(\"__luaRousr_instance_builtin_set_depth\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_direction] = asset_get_index(\"__luaRousr_instance_builtin_set_direction\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_friction] = asset_get_index(\"__luaRousr_instance_builtin_set_friction\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_gravity] = asset_get_index(\"__luaRousr_instance_builtin_set_gravity\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_gravity_direction] = asset_get_index(\"__luaRousr_instance_builtin_set_gravity_direction\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_hspeed] = asset_get_index(\"__luaRousr_instance_builtin_set_hspeed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_id] = asset_get_index(\"__luaRousr_instance_builtin_set_id\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_image_alpha] = asset_get_index(\"__luaRousr_instance_builtin_set_image_alpha\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_image_angle] = asset_get_index(\"__luaRousr_instance_builtin_set_image_angle\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_image_blend] = asset_get_index(\"__luaRousr_instance_builtin_set_image_blend\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_image_index] = asset_get_index(\"__luaRousr_instance_builtin_set_image_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_image_number] = asset_get_index(\"__luaRousr_instance_builtin_set_image_number\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_image_speed] = asset_get_index(\"__luaRousr_instance_builtin_set_image_speed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_image_xscale] = asset_get_index(\"__luaRousr_instance_builtin_set_image_xscale\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_image_yscale] = asset_get_index(\"__luaRousr_instance_builtin_set_image_yscale\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_layer] = asset_get_index(\"__luaRousr_instance_builtin_set_layer\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_mask_index] = asset_get_index(\"__luaRousr_instance_builtin_set_mask_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_object_index] = asset_get_index(\"__luaRousr_instance_builtin_set_object_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_path_endaction] = asset_get_index(\"__luaRousr_instance_builtin_set_path_endaction\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_path_index] = asset_get_index(\"__luaRousr_instance_builtin_set_path_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_path_orientation] = asset_get_index(\"__luaRousr_instance_builtin_set_path_orientation\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_path_position] = asset_get_index(\"__luaRousr_instance_builtin_set_path_position\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_path_positionprevious] = asset_get_index(\"__luaRousr_instance_builtin_set_path_positionprevious\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_path_scale] = asset_get_index(\"__luaRousr_instance_builtin_set_path_scale\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_path_speed] = asset_get_index(\"__luaRousr_instance_builtin_set_path_speed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_persistent] = asset_get_index(\"__luaRousr_instance_builtin_set_persistent\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_solid] = asset_get_index(\"__luaRousr_instance_builtin_set_solid\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_speed] = asset_get_index(\"__luaRousr_instance_builtin_set_speed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_sprite_height] = asset_get_index(\"__luaRousr_instance_builtin_set_sprite_height\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_sprite_index] = asset_get_index(\"__luaRousr_instance_builtin_set_sprite_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_sprite_width] = asset_get_index(\"__luaRousr_instance_builtin_set_sprite_width\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_sprite_xoffset] = asset_get_index(\"__luaRousr_instance_builtin_set_sprite_xoffset\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_sprite_yoffset] = asset_get_index(\"__luaRousr_instance_builtin_set_sprite_yoffset\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_timeline_index] = asset_get_index(\"__luaRousr_instance_builtin_set_timeline_index\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_timeline_loop] = asset_get_index(\"__luaRousr_instance_builtin_set_timeline_loop\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_timeline_position] = asset_get_index(\"__luaRousr_instance_builtin_set_timeline_position\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_timeline_running] = asset_get_index(\"__luaRousr_instance_builtin_set_timeline_running\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_timeline_speed] = asset_get_index(\"__luaRousr_instance_builtin_set_timeline_speed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_visible] = asset_get_index(\"__luaRousr_instance_builtin_set_visible\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_vspeed] = asset_get_index(\"__luaRousr_instance_builtin_set_vspeed\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_x] = asset_get_index(\"__luaRousr_instance_builtin_set_x\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_xprevious] = asset_get_index(\"__luaRousr_instance_builtin_set_xprevious\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_xstart] = asset_get_index(\"__luaRousr_instance_builtin_set_xstart\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_y] = asset_get_index(\"__luaRousr_instance_builtin_set_y\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_yprevious] = asset_get_index(\"__luaRousr_instance_builtin_set_yprevious\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_set_ystart] = asset_get_index(\"__luaRousr_instance_builtin_set_ystart\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_alarm] = asset_get_index(\"__luaRousr_instance_builtin_get_alarm\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_bbox_bottom] = asset_get_index(\"__luaRousr_instance_builtin_get_bbox_bottom\");global.__extluarousr_script_index_lookup[@ __luaRousr_instance_builtin_get_bbox_left] = asset_get_index(\"__luaRousr_instance_builtin_get_bbox_left\");global.__extluarousr_script_index_lookup[@ __luaRousr_check_init] = asset_get_index(\"__luaRousr_check_init\");global.__extluarousr_script_index_lookup[@ __luaRousr_executeGML] = asset_get_index(\"__luaRousr_executeGML\");global.__extluarousr_script_index_lookup[@ __luaRousr_process_buffer] = asset_get_index(\"__luaRousr_process_buffer\");global.__extluarousr_script_index_lookup[@ __luaRousr_reset_buffer] = asset_get_index(\"__luaRousr_reset_buffer\");global.__extluarousr_script_index_lookup[@ __luaRousr_push_buffer] = asset_get_index(\"__luaRousr_push_buffer\");global.__extluarousr_script_index_lookup[@ __luaRousr_pop_buffer] = asset_get_index(\"__luaRousr_pop_buffer\");global.__extluarousr_script_index_lookup[@ __luaRousr_bind_member] = asset_get_index(\"__luaRousr_bind_member\");global.__extluarousr_script_index_lookup[@ __luaRousr_read_instance] = asset_get_index(\"__luaRousr_read_instance\");global.__extluarousr_script_index_lookup[@ __luaRousr_write_instance] = asset_get_index(\"__luaRousr_write_instance\");global.__extluarousr_script_index_lookup[@ __luaRousr_write_val] = asset_get_index(\"__luaRousr_write_val\");global.__extluarousr_script_index_lookup[@ __luaRousr_sync_write] = asset_get_index(\"__luaRousr_sync_write\");global.__extluarousr_script_index_lookup[@ __luaRousr_sync_read] = asset_get_index(\"__luaRousr_sync_read\");global.__extluarousr_script_index_lookup[@ __luaRousr_debug_print] = asset_get_index(\"__luaRousr_debug_print\");");

var _index = argument0;return global.__extluarousr_script_index_lookup[@ _index];
