{
    "id": "a05e5dd0-2995-4f44-b90a-3783685763b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_line",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "15bd51a5-7e6f-42eb-b5e3-081e97e728ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "06b9357b-df4f-4964-89c4-b42a27b4fbab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15bd51a5-7e6f-42eb-b5e3-081e97e728ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb542c6b-881c-426b-9e4c-729b1b9c8c83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15bd51a5-7e6f-42eb-b5e3-081e97e728ae",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "0ac8e1e4-b574-4cf3-9834-6596555ce1b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "442f6716-a096-4824-b6ef-d7000b5ebc4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ac8e1e4-b574-4cf3-9834-6596555ce1b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ae38b30-1e5a-401b-b837-6a0405ba90cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ac8e1e4-b574-4cf3-9834-6596555ce1b1",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "77ed38ff-8787-45e9-b782-f2f9cf18a101",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "cd538fb2-0fc9-47f7-897e-d5e34336a659",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77ed38ff-8787-45e9-b782-f2f9cf18a101",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "649d983e-7641-4448-9168-74877515c29d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77ed38ff-8787-45e9-b782-f2f9cf18a101",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "b6d2d444-abb2-433c-beef-b6ecf5364144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "d82b8518-f7a1-4e3a-bdd7-e39f079c412d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6d2d444-abb2-433c-beef-b6ecf5364144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d5c7f86-5298-44e5-af1a-b53a8ac795c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6d2d444-abb2-433c-beef-b6ecf5364144",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "b9de8dfa-fc20-4b26-a67b-11dd8fc254e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "679ac6e2-03e5-4387-9e82-cb71536f20b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9de8dfa-fc20-4b26-a67b-11dd8fc254e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2478313-6df1-4716-933d-844542a0602a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9de8dfa-fc20-4b26-a67b-11dd8fc254e9",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "3a2691a3-f8e3-4c54-a1cf-5207281f12db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "cd36f850-5172-47de-a429-5855bd46f5ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a2691a3-f8e3-4c54-a1cf-5207281f12db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73a5853b-468b-4718-a8f7-bf3b59d7e224",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a2691a3-f8e3-4c54-a1cf-5207281f12db",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "8e63aed6-888d-4b71-bf3b-98231c9216a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "bb65f590-2477-466b-846b-8c6cf1c9b516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e63aed6-888d-4b71-bf3b-98231c9216a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96757346-4538-4ed4-b5f6-40e152cf8914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e63aed6-888d-4b71-bf3b-98231c9216a9",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "d8eb6e96-c630-4ab7-a92b-939b5f4bd753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "6bdcbfa0-0f3c-4184-af15-c921629b9a92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8eb6e96-c630-4ab7-a92b-939b5f4bd753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b089cde-88f7-4f1c-8cb3-ad2ec0a81fae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8eb6e96-c630-4ab7-a92b-939b5f4bd753",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "49e3b455-8a0e-43ab-9c9f-d32017bbd178",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "057bdd43-d90b-45e0-b4fc-86c086e5abd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49e3b455-8a0e-43ab-9c9f-d32017bbd178",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58d3c076-ed2f-4793-b9bc-d2f36232fa0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49e3b455-8a0e-43ab-9c9f-d32017bbd178",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "d3bcaba8-99ef-49b7-816f-42e77835a36b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "c0b1dd4c-0dd8-44f2-b48e-1b368af6c441",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3bcaba8-99ef-49b7-816f-42e77835a36b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2be4f069-a1d8-4315-b2d0-1428e86e6c8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3bcaba8-99ef-49b7-816f-42e77835a36b",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "483b8f72-bb07-4071-bc7f-137580ed67ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "ed460110-6e59-4334-b2e9-ea412932bf90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "483b8f72-bb07-4071-bc7f-137580ed67ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e8ad582-11bf-4b39-91bf-76142dc609be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "483b8f72-bb07-4071-bc7f-137580ed67ef",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "0a69818d-2dde-48ef-ab00-2796b71dd427",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "28c0e94e-b3d2-49ed-b718-ec9b6275853f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a69818d-2dde-48ef-ab00-2796b71dd427",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41dfe9f6-2c16-41fd-942a-0c8f0856d9c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a69818d-2dde-48ef-ab00-2796b71dd427",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "668a57d9-9181-40b6-bd31-812d63769fc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "f3ac401b-754f-4644-9ffb-6495354137ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "668a57d9-9181-40b6-bd31-812d63769fc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a417208c-f064-4dca-be95-4f6708271a8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "668a57d9-9181-40b6-bd31-812d63769fc0",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "b8447e90-e5d5-4839-868a-08826e0877cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "104108f3-7feb-4e5b-ab3d-0a42525adcc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8447e90-e5d5-4839-868a-08826e0877cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e0c626-0f4f-48ec-9236-de8f8fd99839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8447e90-e5d5-4839-868a-08826e0877cd",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "944b7c7a-8cbe-4398-ac9d-1d4bfa52bc52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "891bc137-8ab1-4805-9498-c5c2baa841f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "944b7c7a-8cbe-4398-ac9d-1d4bfa52bc52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8284c13-1517-4194-b956-f606bbdc187d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "944b7c7a-8cbe-4398-ac9d-1d4bfa52bc52",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "b7e3d207-9d60-4428-9b6e-de263c5aa55d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "70197612-d8c3-47f1-abaf-3ab0e2f43766",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7e3d207-9d60-4428-9b6e-de263c5aa55d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26c9b1e4-5b47-4149-bd83-8556a8d8ed27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e3d207-9d60-4428-9b6e-de263c5aa55d",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "6c040701-842d-4a69-867e-780762f6aa11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "8ad86981-1e56-4c3c-98cc-b1140e8b256e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c040701-842d-4a69-867e-780762f6aa11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f2c61424-ba1a-4808-a751-3311c11e220e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c040701-842d-4a69-867e-780762f6aa11",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "9b7f741d-8a5d-4597-94a4-234832ba57d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "6f3bfc26-31ba-4e41-b4d9-45a80491a32e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b7f741d-8a5d-4597-94a4-234832ba57d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38f12ec-8065-4944-85c0-c2d2e25ad965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b7f741d-8a5d-4597-94a4-234832ba57d0",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "0dd3e368-2e1e-4695-919a-7e583bb783b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "47e20565-b893-4411-8955-833bacb744c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dd3e368-2e1e-4695-919a-7e583bb783b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4382e37-8cf2-4fd7-a1a9-666d3107ccc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dd3e368-2e1e-4695-919a-7e583bb783b0",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "2897cc96-abb6-4df8-8eea-3b4a87f0f18a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "df2bb78b-1568-4ffa-9227-c0042f1d03b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2897cc96-abb6-4df8-8eea-3b4a87f0f18a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f392b53-2687-4ac8-abaa-9fdc2dd12755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2897cc96-abb6-4df8-8eea-3b4a87f0f18a",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "05adfaa3-ca7c-42d5-8cd6-295120d1313d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "df300c34-de60-4ab0-9570-32b761bb199b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05adfaa3-ca7c-42d5-8cd6-295120d1313d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "560dd3ed-1aed-4743-8029-887a7af6854a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05adfaa3-ca7c-42d5-8cd6-295120d1313d",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "dc8cd593-957e-4fbb-826a-3496d53b2a4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "d46afde4-f7ea-4977-a7a5-529af06c5af1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc8cd593-957e-4fbb-826a-3496d53b2a4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c390c98-4508-477c-8535-a2111c8a1187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc8cd593-957e-4fbb-826a-3496d53b2a4d",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "745268f6-43de-4dc4-b422-d646ea29b57c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "29669345-44ef-4cb0-9847-fd2dc532d08b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "745268f6-43de-4dc4-b422-d646ea29b57c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "200c8373-cf81-4e18-96c3-6c5b6faeecd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "745268f6-43de-4dc4-b422-d646ea29b57c",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "85fd2016-1f24-44a0-8dd1-a03d5d48fea1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "de740c44-6e43-429c-9352-2a78c509b6bd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85fd2016-1f24-44a0-8dd1-a03d5d48fea1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92740814-731c-40d3-95d5-50b570499455",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85fd2016-1f24-44a0-8dd1-a03d5d48fea1",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "1d723dc9-d846-4f16-a980-0616968462dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "6981b185-dc5a-41f0-96e1-76464752f980",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d723dc9-d846-4f16-a980-0616968462dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d694529-3145-4dfe-bfee-cbd4a1bb3edf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d723dc9-d846-4f16-a980-0616968462dd",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "cbc96d45-3e13-4ab9-ad97-97239fc5ee4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "2ff04fa4-3e36-4c4e-bcbb-e8f6bfdd7abe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbc96d45-3e13-4ab9-ad97-97239fc5ee4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7f4b30ae-9bfc-496e-aecd-39d40ceeddf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbc96d45-3e13-4ab9-ad97-97239fc5ee4b",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "7cf7620f-6c32-464a-a3d5-a134c1b651e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "10924b8f-d57d-4f57-a7a2-a8ae0ebdc1d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cf7620f-6c32-464a-a3d5-a134c1b651e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4ea43ced-b137-47cf-928e-2a0d48ed84da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cf7620f-6c32-464a-a3d5-a134c1b651e3",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "75207bac-6b68-4053-8f06-24d801f3ae38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "d2d0a0c7-0961-42ad-a23d-2fb5d361b5ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75207bac-6b68-4053-8f06-24d801f3ae38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c92876a-bd93-4570-bf05-c87c81f84d98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75207bac-6b68-4053-8f06-24d801f3ae38",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "275ba3c6-5303-4476-b0e6-a9c16e4412b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "2e0a0772-d63b-436c-855b-3059c1c6f2bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "275ba3c6-5303-4476-b0e6-a9c16e4412b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4865b6b4-1b56-49b8-bf12-ef7eedd1d876",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "275ba3c6-5303-4476-b0e6-a9c16e4412b3",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "5409d0da-1292-473a-b8de-39ba7d6b7f5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "b0a37d14-acb6-4266-8d24-15f4212e0dff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5409d0da-1292-473a-b8de-39ba7d6b7f5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45b5318f-dd23-4547-98f3-eb5b613a9a54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5409d0da-1292-473a-b8de-39ba7d6b7f5b",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "61bf9f3f-f021-4d9e-8690-84a93db424ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "8ac95874-ce48-4cf9-9275-0711069dc475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61bf9f3f-f021-4d9e-8690-84a93db424ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "928281e1-987d-4baf-a2fe-e1245179f613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61bf9f3f-f021-4d9e-8690-84a93db424ae",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "b7ec82e9-68bf-4d92-b39f-64b7e10c8aa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "865f95f0-6a93-4848-9b5c-43876d46288f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7ec82e9-68bf-4d92-b39f-64b7e10c8aa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a846c05b-2a1b-48e3-92c8-ca4e1a882e91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7ec82e9-68bf-4d92-b39f-64b7e10c8aa6",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "21cc4fd3-8502-43e8-8f06-943b4f0e8eee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "ffce37cc-9d97-4a03-8b86-108e8a093f35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21cc4fd3-8502-43e8-8f06-943b4f0e8eee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32c80249-c5b6-4973-9737-98d1bacd592b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21cc4fd3-8502-43e8-8f06-943b4f0e8eee",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "72692d27-46f7-401c-9d41-39ea48cc3bab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "b0d20e7a-4967-40fe-87ac-127069d40429",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72692d27-46f7-401c-9d41-39ea48cc3bab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a15d246-f1b5-4761-a53a-bb3a43266783",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72692d27-46f7-401c-9d41-39ea48cc3bab",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        },
        {
            "id": "7812b094-2b94-4b97-ab77-36840476500f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "compositeImage": {
                "id": "d74e6b33-3228-4c6c-a122-435596086622",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7812b094-2b94-4b97-ab77-36840476500f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f7461f5-eda0-4a6a-a904-b6657973f54b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7812b094-2b94-4b97-ab77-36840476500f",
                    "LayerId": "147d2771-83c0-45fb-9eab-e37f984f2a66"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "147d2771-83c0-45fb-9eab-e37f984f2a66",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a05e5dd0-2995-4f44-b90a-3783685763b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}