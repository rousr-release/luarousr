///@function __luaRousr_instance_builtin_get_mask_index()
///@desc builtin getter for mask_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return mask_index;
