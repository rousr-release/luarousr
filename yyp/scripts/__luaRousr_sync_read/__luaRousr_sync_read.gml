///@func __luaRousr_sync_read(_buffer)
///@desc check and process any updated values from the dll
///@param {Real} buffer   buffer id
///@extensionizer { "docs": false }
var buffer       = argument0;

with (LuaSr) {
  var numWritten = buffer_read(buffer, buffer_u32);
  if (numWritten == 0)
    return;
  
  var id_map = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  var boundVals = GMLBindings[__ELuaRousrGMLBindData.BoundVariables];
  var numBound = sr_array_size(boundVals);
  boundVals = sr_array_data(boundVals);
  
  repeat(numWritten) {
  	var _id    = buffer_read(buffer, buffer_f64);
  	var count  = buffer_read(buffer, buffer_u32);
    
  	var bind = id_map[? _id];
    if (bind == undefined) {
      show_debug_message("__luaRousr_sync_read - warning: Invalid bind! Trying to recover.");
      for (var i = 0; i < count; ++i) {
  	    buffer_read(buffer, buffer_u32);
  		  sr_buffer_read_val(buffer);
      }
      
      continue;
    }
        
  	var syncVals = bind[__ELuaRousrBindEntity.Values];
    var customBinds = bind[__ELuaRousrBindEntity.CustomMembers];
    
    with(_id) {
      repeat(count) {
    		var valIndex = buffer_read(buffer, buffer_u32);
    		var val = sr_buffer_read_val(buffer);
    		syncVals[@valIndex] = val;	
        
        if (valIndex < numBound) {
          var bound  = boundVals[valIndex];
          var setter = bound[__ELuaRousrBoundValue.Setter];
          script_execute(setter, val);
        } else {
          var numCustom = sr_array_size(customBinds);
          var customIndex = valIndex - numBound;

          if (customIndex < numCustom) {
            var customVal = sr_array(customBinds, customIndex);
            var setter = customVal[__ELuaRousrBoundValue.Setter];
            script_execute(setter, val, customVal[__ELuaRousrBoundValue.Name], valIndex);
          } else {
            show_debug_message("luaRousr - warning: __luaRousr_sync_read has an invalid index");
          }
        }
    	}
	  }
  }
}