///@function luaRousr_definitions()
///@description luaRousr enums and macros
enum ELuaSr {
  Instance = 0,
  NumTypes,
  
  Num = ELuaSr.NumTypes,
};

#region Internal Definitions
#macro LuaSr (global.___luaRousr)

enum __ELuaRousrStatus {
	Waiting = 0,
	Ready,
	Done
};

enum __ELuaRousrCmd {
	GMLBound        = 0,
	DebugPrint,
  RequestSync,  // Lua requesting an instance sync
  SubmitSync,   // Lua sending an instance sync
};

enum __ELuaRousrBindEntity {
  Id = 0,
  LuaName,
  Values,
  CustomMembers,
  
  Num,
};

enum __ELuaRousrBoundValue { 
  Name = 0,
  Default, 
  ReadOnly, 
  Getter, 
  Setter, 
  ForceType, 
  
  Num 
};

enum __ELuaRousrGMLBindData {
  Binds = 0,
  
  IdToBind,
  NameToBind,
  
  BoundVariables,
  Payloads,
  
  ResourceNameMap,
  ResourceIndexMap,
  
  Num
};

enum __ELuaRousrResourceBind {
  Name = 0,
  Index,
  
  Num
};

enum __ELuaRousrPayload {
	Id = 0,
	Payload,
	PayloadSize,
  
	Num,
};

enum __ELuaRousrExtraData {
  ResourceBinds,
  
  Num
};

#region Instance BuiltIn bindings

enum __ELuaRousrInstanceBuiltIn {
  Begin = 0,
  
  Id = __ELuaRousrInstanceBuiltIn.Begin,
  Visible,
  Solid,
  Persistent,
  Depth,
  Layer,
  Alarm,
  ObjectIndex,
  SpriteIndex,
  SpriteWidth,
  SpriteHeight,
  SpriteXoffset,
  SpriteYoffset,
  ImageAlpha,
  ImageAngle,
  ImageBlend,
  ImageIndex,
  ImageNumber,
  ImageSpeed,
  ImageXscale,
  ImageYscale,
  MaskIndex,
  BboxBottom,
  BboxLeft,
  BboxRight,
  BboxTop,
  Direction,
  Friction,
  Gravity,
  GravityDirection,
  Hspeed,
  Vspeed,
  Speed,
  Xstart,
  Ystart,
  X,
  Y,
  Xprevious,
  Yprevious,
  PathIndex,
  PathPosition,
  PathPositionprevious,
  PathSpeed,
  PathScale,
  PathOrientation,
  PathEndaction,
  TimelineIndex,
  TimelineRunning,
  TimelineSpeed,
  TimelinePosition,
  TimelineLoop,
  
  Last = __ELuaRousrInstanceBuiltIn.TimelineLoop,
  Num = (__ELuaRousrInstanceBuiltIn.Last - __ELuaRousrInstanceBuiltIn.Begin) + 1,
  Count = __ELuaRousrInstanceBuiltIn.Last + 1
};

#endregion 

#endregion