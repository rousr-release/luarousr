///@function luaRousr_release_callback(_callback_id)
///@desc Decrements the reference count for the callbackId. If it's completely released (refcount = 0), it queues the callbackId for deletion. The next `step` called by `LuaRousr` will release this callback from memory.
///@param {Real} _callback_id - callback Id passed to us from Lua
var _callback_id = argument0;
__extLuaRousrReleaseScriptCallback(_callback_id);