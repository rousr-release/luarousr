///@function __luaRousr_instance_builtin_get_direction()
///@desc builtin getter for direction
///@returns {*} builtin
///@extensionizer { "docs": false }
return direction;
