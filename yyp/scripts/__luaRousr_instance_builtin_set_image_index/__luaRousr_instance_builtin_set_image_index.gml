///@function __luaRousr_instance_builtin_set_image_index(_val)
///@desc builtin setter for image_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
image_index = argument0;
