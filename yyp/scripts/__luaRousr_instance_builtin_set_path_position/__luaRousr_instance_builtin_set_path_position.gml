///@function __luaRousr_instance_builtin_set_path_position(_val)
///@desc builtin setter for path_position
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_position = argument0;
