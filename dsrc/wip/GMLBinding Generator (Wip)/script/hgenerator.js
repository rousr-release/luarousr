// hgenerator.js
//  babyjeans
//
///
var tab = '\t';

class HGenerator {
	constructor(boundResource) {
		this.boundResource = boundResource;
		
		let vcxConfig = boundResource.vcxConfig;
		let cppPath   = vcxConfig.exportPath;

		this.fileName   = boundResource.cppFileName + ".h";		
		this.filePath   = cppPath + this.fileName;

		let headerFile = "// " + boundResource.cppFileName + ".h\n//  Auto-Generated bindings for luaRousr extension\n#pragma once\n";
		for (let incIndex in boundResource.headerIncludes) headerFile += "#include \"" + boundResource.headerIncludes[incIndex] + "\"\n";
		headerFile += '\n';
		headerFile += "class " + boundResource.className + " {\n";
		headerFile += "public:\n";
		headerFile += '\t' + "static void RegisterLua(CLuaRousr& _L);\n"
		if (boundResource.hasId) headerFile += "\t" + boundResource.className + "(int _id) : m_id(_id) { ; }\n";
		headerFile += "\n";
		
		this.headerFile = headerFile;
		this.headerPublic = "";
		this.headerPrivate = "";
	}

	processVariable(v) {
		this.headerPublic  += this.addHeaderPublic(v);
		this.headerPrivate += this.addHeaderPrivate(v);
	}
		
	addHeaderPublic(v) {
		let headerPublic  = tab + "// variable: " + v.name + "\n";
	    headerPublic     += tab + v.getReturnType + " " + v.name + "();\n";
		if (!v.flags.readonly)
		    headerPublic    += tab + "void Set" + v.name + "(const " + v.setReturnType + " _val);\n";
		
		headerPublic += "\n";
		return headerPublic;
	}

	addHeaderPrivate(v) {
		return tab + v.memberType + " m_" + v.name + ";\n";
	}

	processFunction(f) {
		let hBinding = tab;
		let returnType = f.return.getReturnType;

		if (f.return.resource) returnType = "std::shared_ptr<" + f.return.resource.className + ">";
		if (f.isStatic || this.boundResource.type == "module") hBinding += "static ";
		hBinding += returnType + " " + f.cppName + "(";
		
		for (let argIndex = 0; argIndex < f.args.length; ++argIndex) {
			let arg = f.args[argIndex];

			hBinding += arg.setReturnType + " ";
			hBinding += "_arg" + argIndex;
			if (argIndex + 1 < f.args.length) hBinding += ", ";
		}
		hBinding += ");\n";
		
		this.headerPublic += hBinding;
	}

	generate() {
		let headerFile = this.headerFile;
		headerFile += this.headerPublic;
		headerFile += "\nprivate: \n";
		headerFile += this.headerPrivate;
		headerFile += "\n};\n"

		return headerFile;
	}
}

module.exports = HGenerator;