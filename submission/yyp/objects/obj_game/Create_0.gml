//Demo by Kyle Askew twitter.com/@net8floz 

//Set lua bindings
randomize();

luaRousr_init();
luaRousr_bind_script("gmlRoomWidth",bound_room_width);
luaRousr_bind_script("gmlRoomHeight",bound_room_height);
luaRousr_bind_script("gmlAddGridForce",grid_apply_directed_force);
luaRousr_bind_script("gmlCreateBall",bound_create_ball);
luaRousr_bind_script("gmlInstanceDestroy",bound_instance_destroy);
luaRousr_bind_script("gmlRandomSign",bound_random_sign);
luaRousr_bind_script("gmlInstancePlacePaddle",bound_instance_place_paddle);
luaRousr_bind_script("gmlSetBallLimit",bound_set_ball_limit);

image_alpha = 0;

//Lua files are expected to be right next to the .exe in this demo - though 
//You don't have to do it that way. To run this demo from GMS2 you must 
//add the lua files to the below _exe_path first. This path is something like
//C:\ProgramData\GMS2\Cache\runtimes\runtime-x-x-x\windows on VM 
// and something ridiculous on YYC. Alternatively just put the lua files on C:/lua and
// use that instead of get_exe_path
//show_message(get_executable_path());

//This code is ran again in the step event for live reload 
exe_path = get_executable_path();
//exe_path = "C:\\lua\\";
lua_scripts = ["math","game","ball","paddle"];
var _i=0;
repeat(4){
	luaRousr_execute_string(otb_file_to_string(exe_path, string(lua_scripts[_i] + ".lua")));
	++_i;
}

//Store balls 
balls = ds_grid_create(1,0);
//Balls to be added via alarm 
ball_queue = ds_list_create();

//Set up camera
view_enabled = true;
view = 0;
view_set_visible(view,true);
camera = camera_create();
camera_zoom = 1.05;

view_set_camera(view,camera);
camera_set_view_size(camera,640*camera_zoom,360*camera_zoom);
window_set_size(1280,720);

window_set_position(
	(display_get_width()-1280)/2,
	(display_get_height()-720)/2
);

camera_set_view_pos(camera,
	(room_width - 640*camera_zoom)/2,
	(room_height - 360*camera_zoom)/2
);

surface_resize(application_surface,window_get_width(),window_get_height());

//Bloom shader
application_surface_draw_enable(false);
uniform_bloom_intensity = shader_get_uniform(shdr_bloom, "intensity");
uniform_blur_size = shader_get_uniform(shdr_bloom, "blurSize"); 
fullscreen = false;

