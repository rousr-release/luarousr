///@function luaRousr_unbind_resource(_index_or_lua_name)
///@desc unbind an object from Lua
///@param {Real|String} _index_or_lua_name - object index OR Lua string Name of the object to unbind
var _index_or_lua_name = argument0;

with (LuaSr) {
  var name_map  = GMLBindings[__ELuaRousrGMLBindData.ResourceNameMap];
  var index_map  = GMLBindings[__ELuaRousrGMLBindData.ResourceIndexMap];
  
  var map = undefined;
  if (is_string(_index_or_lua_name)) map = name_map;
  else if (is_real(_index_or_lua_name)) map = index_map;

  if (map == undefined)
    return;
  
  var bind = map[? _index_or_lua_name];
  if (bind == undefined)
    return;
    
  var _index = bind[@ __ELuaRousrResourceBind.Index];
  var _name  = bind[@ __ELuaRousrResourceBind.Name];

  name_map[? _name]   = undefined;
  index_map[? _index] = undefined;
}