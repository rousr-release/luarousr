///@function __luaRousr_instance_builtin_get_gravity_direction()
///@desc builtin getter for gravity_direction
///@returns {*} builtin
///@extensionizer { "docs": false }
return gravity_direction;
