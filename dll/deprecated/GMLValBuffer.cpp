#include "GMLBuffer.h"

#include "GMLVal.h"

CGMLBuffer::CGMLBuffer(const char* _bufferPtr)
	: mBuffer(const_cast<char*>(_bufferPtr))
	, mSeekPos(0)
	, mBufferSize(static_cast<size_t>(*reinterpret_cast<const uint16_t*>(_bufferPtr))) // If we dont' have a size, assume its passed in the first 2 bytes
{ 
    mBuffer[0] = 0;
}

CGMLBuffer::CGMLBuffer(size_t _bufferSize, const char* _bufferPtr)
	: mBuffer(const_cast<char*>(_bufferPtr))
	, mSeekPos(0)
	, mBufferSize(_bufferSize)
{ }

int8_t CGMLBuffer::PeekByte() const {
	return mBuffer[mSeekPos];
}

int8_t CGMLBuffer::PeekByte(uint32_t _seekPos) const {
	return mBuffer[_seekPos];
}

void CGMLBuffer::Write(std::shared_ptr<IGMLVal> _val) {
	if (_val == nullptr)
		return;

	_val->Write(*this);
}

void CGMLBuffer::WriteString(const std::string& _val, bool _writeType, bool _writeSize) {
	char* _buffer = &mBuffer[mSeekPos];
	if (_writeType) {
		_buffer[0] = static_cast<int8_t>(LuaRousr::DataType::String);
		mSeekPos++;
	}

	_buffer = &mBuffer[mSeekPos];
	size_t length(mSeekPos + _val.length() < mBufferSize ? _val.length() : mBufferSize - mSeekPos);
	std::memcpy(_buffer, _val.c_str(), length);
	mSeekPos += _val.length();
	mBuffer[mSeekPos++] = 0;
}

std::shared_ptr<IGMLVal> CGMLBuffer::ReadVal() {
	if (mSeekPos < mBufferSize) {
		int8_t dataType(*reinterpret_cast<int8_t*>(&mBuffer[mSeekPos++]));
		switch (static_cast<LuaRousr::DataType>(dataType)) {
		case LuaRousr::DataType::Byte:   return std::make_shared<TGMLVal<char>>(this);
		case LuaRousr::DataType::Bool:   return std::make_shared<TGMLVal<bool>>(this);

		case LuaRousr::DataType::Int8:   return std::make_shared<TGMLVal<int8_t>>(this);
		case LuaRousr::DataType::Int16:  return std::make_shared<TGMLVal<int16_t>>(this);
		case LuaRousr::DataType::Int32:  return std::make_shared<TGMLVal<int32_t>>(this);
		case LuaRousr::DataType::Int64:  return std::make_shared<TGMLVal<int64_t>>(this);

		case LuaRousr::DataType::Uint8:  return std::make_shared<TGMLVal<uint8_t>>(this);
		case LuaRousr::DataType::Uint16: return std::make_shared<TGMLVal<uint16_t>>(this);
		case LuaRousr::DataType::Uint32: return std::make_shared<TGMLVal<uint32_t>>(this);
		case LuaRousr::DataType::Uint64: return std::make_shared<TGMLVal<uint64_t>>(this);

		case LuaRousr::DataType::Float:  return std::make_shared<TGMLVal<float>>(this);
		case LuaRousr::DataType::Double: return std::make_shared<TGMLVal<double>>(this);
		case LuaRousr::DataType::String: return std::make_shared<TGMLVal<std::string>>(this);
		default: break;
		}
	} else {
		mSeekPos = mBufferSize;
	}

	CLuaRousr::Error("luaRousr - warning: Invalid Buffer Read Type");
	return std::make_shared<CGMLValVoid>(this);
}

int8_t CGMLBuffer::ReadByte() {
	return mBuffer[mSeekPos++];
}