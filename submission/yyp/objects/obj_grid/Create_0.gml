grid_type = grid_types.normal;
grid_sprite = spr_line_normal;
spacing = 24;
cols = (room_width div spacing)+2;
rows = (room_height div spacing)+1;
points = ds_grid_create(cols,rows);
points_fixed = ds_grid_create(cols,rows);

springs_array = array_create(0);
springs_size = 0;
global.points_size = cols * rows;
global.points_array = array_create(global.points_size);


//Create points
var _gy =0,_gx=0, _i=0;
repeat(rows){
	repeat(cols){
		var _x =  _gx * spacing, _y = _gy * spacing,
			_point = point_create(_x,_y,1),
			_point_fixed = point_create(_x,_y,0);
				
		
		points[# _gx,_gy] = _point;
		points_fixed[# _gx,_gy] = _point_fixed;		
		global.points_array[@ _i] = _point;
		++_i;
		++_gx;
	}
	++_gy;
	_gx=0;
}


//Create springs
_gy =0;_gx=0; _i=0;
repeat(rows){
	repeat(cols){
		if(_gx == 0 || _gy == 0 || _gx == cols-1 || _gy == rows-1){
			springs_array[_i++] = spring_create(points_fixed[# _gx,_gy],points[# _gx,_gy],0.1,0.1)
		}else if(_gx mod 3 == 0 && _gy mod 3 == 0){
			springs_array[_i++] = spring_create(points_fixed[# _gx,_gy],points[# _gx,_gy],0.002,0.02)
		}
		
		var _stiffness = 0.28,
			_damping = 0.06;
			
		if(_gx > 0){
			springs_array[_i++] = spring_create(points[# _gx - 1,_gy],points[# _gx,_gy],_stiffness,_damping)
		}
		
		if(_gy > 0){
			springs_array[_i++] = spring_create(points[# _gx,_gy - 1],points[# _gx,_gy],_stiffness,_damping)
		}
		
		++_gx;
	}
	++_gy;
	_gx=0;
}

springs_size = array_length_1d(springs_array);

timer = 0;
