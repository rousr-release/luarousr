///@function __luaRousr_instance_builtin_set_timeline_speed(_val)
///@desc builtin setter for timeline_speed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
timeline_speed = argument0;
