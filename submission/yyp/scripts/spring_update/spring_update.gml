var _spring = argument0,
	_p1 = _spring[spring.point1],
	_p2 = _spring[spring.point2],
	_dx = (_p1[point.x] - _p2[point.x]),
	_dy = (_p1[point.y] - _p2[point.y]),
	_length = (_dx * _dx) + (_dy * _dy);


if(_length <= _spring[spring.length_squared]){
	exit;
}else{
	_length = sqrt(_length);
}

var _delta_length = _length - _spring[spring.length],
	_dvx = _p2[point.vx] - _p1[point.vx],
	_dvy = _p2[point.vy] - _p1[point.vy],
	_stiffness = _spring[spring.stiffness],
	_damping = _spring[spring.damping],
	_fx,_fy;
	
_dx = (_dx / _length) * _delta_length;
_dy = (_dy / _length) * _delta_length;

_fx = ( _dx * _stiffness) - (_dvx * _damping);
_fy = ( _dy * _stiffness) - (_dvy * _damping);

point_apply_force(_p1,-_fx,-_fy);
point_apply_force(_p2,_fx,_fy);