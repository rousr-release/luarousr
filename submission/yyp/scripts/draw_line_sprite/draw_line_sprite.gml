///@param px1
///@param py1 
///@param px2 
///@param py2 
///@param thickness
///@param frame
//Expensive script - don't do this kinda stuff IRL
var _length = point_distance(argument0,argument1,argument2,argument3),
	_colour = (grid_type == grid_types.normal) ? c_white : irandom(100000000),
	_frame = (grid_type == grid_types.rainbow) ? 0 : argument5;

draw_sprite_ext(
	grid_sprite,
	argument5,
	argument0,
	argument1,
	_length,argument4,
	point_direction(argument0,argument1,argument2,argument3),
	_colour,
	1 
);