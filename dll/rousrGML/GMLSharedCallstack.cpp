#include "GMLSharedCallstack.h"

namespace {
    std::unique_ptr<GMLSharedCallstack> Instance;
}

GMLSharedCallstack::GMLCall::GMLCall(GMLSharedCallstack* _parent, size_t _callId)
    : mParent(_parent)
    , mCallId(_callId)
{ ; }

void GMLSharedCallstack::GMLCall::Call(std::function<void(GMLSharedCallstack::GMLCall&)>&& _callback) {
    mCallback = std::move(_callback);
    mTask = &mParent->mTasks.Run([this](TaskRunner::Task& _task) {
        // Get a buffer
        AllocBuffer();
        if (_task.Cancelled())
            return;

        mCallback(*this);

        Finish();
    });
}

GMLSharedCallstack::GMLCall::~GMLCall() {
    if (mTask && mTask->active) {
        mStatus = rousrGML::Status::Error; 
        mTask->Cancel();
    }
}

void GMLSharedCallstack::GMLCall::Wait(rousrGML::Status _status) {
    using namespace std::literals::chrono_literals;
    
    mStatus = _status;
    mGMLActive = true;
    while (mGMLActive && (mTask == nullptr || !mTask->Cancelled()))
        std::this_thread::yield();
}

void GMLSharedCallstack::GMLCall::Finish() {
    mStatus = rousrGML::Status::Done;
    mGMLActive = true;
}

rousrGML::Status GMLSharedCallstack::GMLCall::Continue(CGMLBuffer&& _buff) {
    using namespace std::literals::chrono_literals;
    
    mBuff = std::move(_buff);
    mGMLActive = false;
    while (!mGMLActive && (mTask == nullptr || !mTask->Cancelled()))
        std::this_thread::yield();
    
    return mStatus;
}

void GMLSharedCallstack::GMLCall::YieldToGML() {
    Wait();
    CGMLBuffer& buff(Buffer());
    buff.Seek(0);
}

double GMLSharedCallstack::Call(std::function<void(GMLCall&)>&& _callback) { return Instance != nullptr ? Instance->Call_Impl(std::forward<std::function<void(GMLCall&)>>(_callback)) : -1.0; }
double GMLSharedCallstack::Call_Impl(std::function<void(GMLCall&)>&& _callback) {
    size_t index(0);
    if (mFreeIndices.empty()) {
        index = mCalls.size(); 
        mCalls.push_back(std::make_unique<GMLSharedCallstack::GMLCall>(this, index));
    } else {
        index = mFreeIndices.top();
        mFreeIndices.pop();
    }

    auto& call(*mCalls[index]);
    call.Call(std::forward<std::function<void(GMLSharedCallstack::GMLCall&)>>(_callback));

    return static_cast<double>(index);
}

double GMLSharedCallstack::WaitingFor(size_t _callId, CGMLBuffer&& _buff) {
    if (Instance == nullptr)
        return -1.0;

    auto call(Instance->FindCall(_callId));
    if (call != nullptr) {
        auto ret(call->Continue(std::forward<CGMLBuffer>(_buff)));
        auto& calls(Instance->mCalls);
        if (ret <= rousrGML::Status::Done) {
            call->Clear();
            Instance->mFreeIndices.push(_callId);
        }

        return static_cast<double>(ret);
    } 
    
    return -2.0; // invalid call
}

GMLSharedCallstack::GMLCall* GMLSharedCallstack::FindCall(size_t _callId) {
    return mCalls[_callId].get();
}

extern "C" {
    DLL_API double InitSharedCallstack() {
        Instance = std::make_unique<GMLSharedCallstack>();
        return 1.0;
    }

    DLL_API double ShutdownSharedCallstack() {
        Instance.reset();
        return 1.0;
    }

    DLL_API double WaitForDLL(double _callId, const char* _buff, double _buffSize) {
        return GMLSharedCallstack::WaitingFor(static_cast<size_t>(_callId), CGMLBuffer(static_cast<size_t>(_buffSize), _buff));;
    }
}