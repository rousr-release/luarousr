///@function __luaRousr_instance_builtin_get_y()
///@desc builtin getter for y
///@returns {*} builtin
///@extensionizer { "docs": false }
return y;
