///@function __luaRousr_instance_builtin_get_id()
///@desc builtin getter for id
///@returns {*} builtin
///@extensionizer { "docs": false }
return id;
