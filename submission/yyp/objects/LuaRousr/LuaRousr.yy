{
    "id": "f3ad24f8-07d9-446d-9613-5d876609dbd3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "LuaRousr",
    "eventList": [
        {
            "id": "258a29db-d6cd-4aeb-9fb9-60efb0295f97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 70,
            "eventtype": 7,
            "m_owner": "f3ad24f8-07d9-446d-9613-5d876609dbd3"
        },
        {
            "id": "41bf8a45-aa6b-4b6f-8cdb-215f1d44f029",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3ad24f8-07d9-446d-9613-5d876609dbd3"
        },
        {
            "id": "3369847d-59f0-4e98-9e02-89346d538468",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "f3ad24f8-07d9-446d-9613-5d876609dbd3"
        },
        {
            "id": "3f79390d-47f3-4e49-bb2a-1a57fd469e05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "f3ad24f8-07d9-446d-9613-5d876609dbd3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}