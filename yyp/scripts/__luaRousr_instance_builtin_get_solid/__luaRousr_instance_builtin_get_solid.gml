///@function __luaRousr_instance_builtin_get_solid()
///@desc builtin getter for solid
///@returns {*} builtin
///@extensionizer { "docs": false }
return solid;
