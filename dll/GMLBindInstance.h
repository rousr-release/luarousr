#pragma once

#include "types.h"

#include "GMLLuaVal.h"

class CGMLBindInstance
{
    static std::set<uint32_t> Dirty;

public:
    CGMLBindInstance(const std::string& _instName, double _gmlId);
	virtual ~CGMLBindInstance() { ; }

	const std::string& Name() const      { return mName; }
    double             Id() const        { return mGmlId; }
    bool               Bound() const     { return mBound; }
    
    void     SetIndex(size_t _index)     { mIndex = _index; }
    size_t             Index() const     { return mIndex;}


    std::shared_ptr<IGMLLuaVal> GetMember(size_t _memberIndex);
    size_t                   GetMemberIndex(const std::string& _memberIndex);

    void Unbind() { mBound = false; }
    void UpdateReadIndex();
    
    size_t AddCustomMember(const std::string& _name, std::shared_ptr<IGMLLuaVal> _val, bool _readOnly);
    void MarkDirty(size_t _memberIndex);
    
    bool Read(CGMLBuffer& _buffer);
    bool Write(CGMLBuffer& _buffer);
    
    // todo:
    // void RemoveCustomMember();
    
private:
    void updateMembers();

private:
	std::string mName;
    double      mGmlId = -1.0;
    bool        mBound = true;
    size_t      mIndex = size_t_max;

    uint32_t    mWriteSequence = 0;
    uint32_t    mReadSequence  = 0;

    std::vector<std::shared_ptr<IGMLLuaVal>> mVals;
    std::vector<std::shared_ptr<IGMLLuaVal>> mLastSync;
    
    std::set<size_t> mDirty;

    using MemberValue = std::tuple<std::string, std::shared_ptr<IGMLLuaVal>, bool>;
    std::vector<MemberValue>                  mCustomMembers;
    std::unordered_map<std::string, size_t>   mCustomMemberMap;
    
    std::unordered_set<std::string> mFiltered;
};