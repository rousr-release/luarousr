///@function luaRousr_add_debug_print_listener(_script_index)
///@desc Adds a script index to the list of debug listeners internal to LuaRousr.  
///      **Note:** Use `with(LuaRousr) DebugToConsole = false` to shut off default print behavior
///@param {real} _script_index   index of script to call... expects `callback(string_parameter)` to be valid
var _script_index = argument0;
with (LuaSr)
  ds_list_add(_script_index);