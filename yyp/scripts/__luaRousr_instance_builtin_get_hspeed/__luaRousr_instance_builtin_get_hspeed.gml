///@function __luaRousr_instance_builtin_get_hspeed()
///@desc builtin getter for hspeed
///@returns {*} builtin
///@extensionizer { "docs": false }
return hspeed;
