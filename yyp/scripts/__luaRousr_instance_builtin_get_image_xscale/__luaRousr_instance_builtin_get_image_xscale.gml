///@function __luaRousr_instance_builtin_get_image_xscale()
///@desc builtin getter for image_xscale
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_xscale;
