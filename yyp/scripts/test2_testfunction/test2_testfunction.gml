///@func test2_testfunction(_num, _func)
///@desc test calling a function from Lua, and storing a call back
var _num = argument0;
var _func = argument1;

var func = luaRousr_retain_callback(_func);
with (TestObject) 
  Callback = func;
  
show_debug_message("test2_callback - gml: _num: " + string(_num));