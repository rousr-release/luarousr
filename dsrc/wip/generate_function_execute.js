var fs = require('fs');


var src = process.argv[2],
    dst = process.argv[3];


var knownBadFunctions = [
    'gml_pragma',
    'font_replace',
    'ds_list_get',
    'ds_map_get'
];


fs.readFile(src, 'utf8', function(err, data) {
    if (err) throw err;

    var output = '',
        input  = data.split('\n'),
        regex  = /([a-zA-Z0-9_]+)\((.+)?\)/;

    output += '/// @description function_execute\n';
    output += '/// @param name\n';
    output += '/// @param [arguments[]]\n\n\n';

    output += 'var _name = argument[0],\n';
    output += '    _args = argument[1];\n\n\n';

    output += 'switch(_name) {\n';

    let defined = [];

    for(var line of input) {
        line = line.trim();

        var matches = line.match(regex);

        if ((line.indexOf('&') == -1) && (!line.indexOf('//') == 0) && (matches != undefined)) {
            var name  = matches[1],
                args  = [],
                multi = false;

            if (!defined.includes(name) && !knownBadFunctions.includes(name)) {
                if (matches[2] != undefined) {
                    args = matches[2].split(',');

                    for(var arg of args) {
                        if (arg.endsWith('...')) {
                            multi = true;
                            break;
                        }
                    }
                }

                if (multi == true) {
                    output += '    case "' + name + '":\n';
                    output += '        switch(array_length_1d(_args)) {\n';

                    for(var i = 0;i <= 32;i ++) {
                        output += '            case ' + i + ':\n';
                        output += '                return ' + name + '(';

                        for(var k = 0;k < i;k ++) {
                            output += '_args[' + k + ']';

                            if (k < (i - 1)) {
                                output += ', ';
                            }
                        }

                        output += ');\n'
                    }

                    output += '            default:\n';
                    output += '                return undefined;\n'
                    output += '        }\n';
                } else {
                    output += '    case "' + name + '":\n';
                    output += '        return ' + name + '(';

                    for(var i = 0, j = args.length;i < j;i ++) {
                        output += '_args[' + i + ']';

                        if (i < (j - 1)) {
                            output += ', ';
                        }
                    }

                    output += ');\n';
                }

                defined.push(name);
            }
        }
    }

    output += '}\n\n';
    output += 'return undefined;';

    fs.writeFile(dst, output);
});