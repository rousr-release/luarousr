///@function __luaRousr_event_async_social(_data)
///@desc Async Social Event for LuaRousr object
///@param {Real} _data   async_load map id
///@extensionizer { "docs": false }
var _data = argument0;

var debug_msg  = ds_map_find_value(_data, "DebugPrint");
if (debug_msg != undefined)
	__luaRousr_debug_print(debug_msg);