///@function luaRousr_remove_debug_print_listener(_script_index)
///@desc Removes a script index from the list of debug listeners internal to LuaRousr.
///      **Note:** Use `with(LuaRousr) DebugToConsole = false` to shut off default print behavior
///@param {Real} _script_index   index of script to remove
var _script_index = argument0;
with (LuaSr) {
  var index = ds_list_find_index(DebugListeners, _script_index);
  if (index >= 0) {
    ds_list_delete(DebugListeners, index);
  }
}