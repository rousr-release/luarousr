///@function __luaRousr_instance_builtin_get_speed()
///@desc builtin getter for speed
///@returns {*} builtin
///@extensionizer { "docs": false }
return speed;
