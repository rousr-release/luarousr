///@function __luaRousr_instance_builtin_set_object_index(_val)
///@desc builtin setter for object_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!
