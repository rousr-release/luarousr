///@function __luaRousr_instance_builtin_get_path_speed()
///@desc builtin getter for path_speed
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_speed;
