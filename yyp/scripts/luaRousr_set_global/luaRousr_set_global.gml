///@func luaRousr_set_global(_name, _val)
///@desc set a value with `_name` in the Lua Globals table (global namespace)
///@param {String}      _name   name of global to be used in Lua
///@param {Real|String} _val    value to assign global
///@returns {Boolean} true if set
if (!__luaRousr_check_init())
  return undefined;
  
var _name = argument0,
    _val = argument1;

if (!is_real(_val)) {
  if (!is_string(_val))
    return undefined;
  
	__extLuaRousrSetGlobalString(_name, _val);
	return true;
}

__extLuaRousrSetGlobalReal(_name, _val);
return true;
