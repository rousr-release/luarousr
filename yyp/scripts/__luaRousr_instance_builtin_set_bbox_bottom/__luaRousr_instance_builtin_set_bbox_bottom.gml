///@function __luaRousr_instance_builtin_set_bbox_bottom(_val)
///@desc builtin setter for bbox_bottom
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!
