///@func __luaRousr_default_custom_getter([_variable_name], [_id_or_index])
///@desc called `with(instance)` 
///@param {String} _variable_name   name given to variable
///@param {Real}           _index   index assigned the variable
///@returns {*} return value
///@notes optionally take the name, or index, as helper ways to identify a gotten variable, 
///       or just use one script per variable

/// by default, variable_instance_get
var _name = argument[0];
var _index = argument[1];

return variable_instance_get(id, _name);