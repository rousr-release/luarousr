///@function __luaRousr_instance_builtin_set_x(_val)
///@desc builtin setter for x
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
x = argument0;
