///@function __luaRousr_instance_builtin_set_depth(_val)
///@desc builtin setter for depth
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
depth = argument0;
