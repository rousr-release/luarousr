///@function __luaRousr_instance_builtin_set_alarm(_val)
///@desc builtin setter for alarm
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
alarm = argument0;
