{
    "id": "8e4df1a5-1932-4cd1-b587-6b4e08a2a581",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "unitTest",
    "eventList": [
        {
            "id": "fede2b1c-8954-4476-9294-7696a4ec3d54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8e4df1a5-1932-4cd1-b587-6b4e08a2a581"
        },
        {
            "id": "fb90f2c4-ee1b-4e20-9c99-7a72c9e2ec5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 9,
            "m_owner": "8e4df1a5-1932-4cd1-b587-6b4e08a2a581"
        },
        {
            "id": "47b6bb8c-46a8-4d61-aaae-04b0311b0031",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 9,
            "m_owner": "8e4df1a5-1932-4cd1-b587-6b4e08a2a581"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}