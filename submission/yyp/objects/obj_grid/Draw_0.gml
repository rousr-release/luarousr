if(grid_type == grid_types.null){
	exit;
}
var _gx = 0,_gy = 0;

repeat(cols){
	repeat(rows){
		var _p = points[# _gx,_gy],
			_px = _p[point.x],
			_py = _p[point.y],
			_up_x = 0,
			_up_y = 0,
			_left_x = 0,
			_left_y = 0;
		
		if(_gx > 1 && _gx < cols-1){
			var _left_p = points[# _gx-1,_gy];
			_left_x = _left_p[point.x];
			_left_y = _left_p[point.y];
			
			var	_dleft_p = points[# _gx-2, _gy],
				_clamped_p = points[# min(_gx+1,cols-1), _gy],
				_thickness = (_gy mod 3 == 1) ? 1.2 : 1,
				_mid_x = spline4(0.5, _dleft_p[point.x],_left_x,_px,_clamped_p[point.x]),
				_mid_y = spline4(0.5, _dleft_p[point.y],_left_y,_py,_clamped_p[point.y]);

			
			draw_line_sprite(_left_x,_left_y,_mid_x,_mid_y,_thickness,timer+_gx);
			draw_line_sprite(_mid_x,_mid_y,_px,_py,_thickness,timer+_gx);
		}
		
		if(_gy > 1 && _gy < rows-1){
			var _up_p = points[# _gx, _gy-1];
			_up_x = _up_p[point.x];
			_up_y = _up_p[point.y];
			var _dup_p = points[# _gx, _gy-2],
				_clamped_p = points[# _gx,min(_gy+1, rows-1)], // might bo _gx+1
				_thickness = (_gx % 3) == 1 ? 1.2 : 1,
				_mid_x = spline4(0.5, _dup_p[point.x],_up_x,_px,_clamped_p[point.x]),
				_mid_y = spline4(0.5, _dup_p[point.y],_up_y,_py,_clamped_p[point.y]);
	
				draw_line_sprite(_up_x,_up_y,_mid_x,_mid_y,_thickness,timer+_gx);
				draw_line_sprite(_mid_x,_mid_y,_px,_py,_thickness,timer+_gx);
		}
		
		if(grid_type == grid_types.fancy){
			if(_gx > 1 && _gy > 1 && _gx < cols-1 && _gy < rows-1){
				var _up_left_p = points[# _gx - 1, _gy -1],
					_up_left_x = _up_left_p[point.x],
					_up_left_y = _up_left_p[point.y],
					_p1x = (_left_x + _px)*0.5,
					_p1y = (_left_y + _py)*0.5,
					_p2x = (_up_left_x + _up_x)*0.5,
					_p2y = (_up_left_y + _up_y)*0.5,
					_p3x = (_up_left_x + _left_x)*0.5,
					_p3y = (_up_left_y + _left_y)*0.5,
					_p4x = (_up_x + _px)*0.5,
					_p4y = (_up_y + _py)*0.5;

				draw_line_sprite(_p1x,_p1y,_p2x,_p2y,0.6,timer+_gx);
				draw_line_sprite(_p3x,_p3y,_p4x,_p4y,0.6,timer+_gx);
			}
		}
			
		++_gy;
	}
	++_gx;
	_gy = 0;
} 
 
