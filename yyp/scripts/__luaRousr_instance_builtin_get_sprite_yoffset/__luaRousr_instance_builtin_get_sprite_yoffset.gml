///@function __luaRousr_instance_builtin_get_sprite_yoffset()
///@desc builtin getter for sprite_yoffset
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_yoffset;
