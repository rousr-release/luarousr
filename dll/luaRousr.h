#pragma once

#include "types.h"

class IGMLVal;
class CLuaThread;
class CGMLLua;
class CLuaContext;
class CGMLBind;
class CmdWorker;

namespace LuaRousr {
	enum class Status : int8_t {
		Waiting = 0,
		Ready,
		Done,
	};

	enum class Cmd : int8_t {
		GMLBound        = 0,
		DebugPrint,
        RequestSync,
        SubmitSync,
	};

	const uint32_t CmdIdResumeThread(0);

	const size_t SeekBegin(0);
	const size_t SeekStatus(0);
	const size_t SeekCmdId(1);
	const size_t SeekData(SeekCmdId + sizeof(uint32_t));

	const size_t LuaCallNumReturn(0);
	const size_t LuaCallReturn(1);

}

class CGMLBuffer;

class CLuaRousr {
public:
	static CLuaRousr&                   Instance();
    static bool                         Initialized();
    
    static void                         GMLReady(uint32_t _cmdId);
    static void                         LuaReady(uint32_t _cmdId);
    static void                         FinishCommand(uint32_t _cmdId);
	
	static void                         WaitForLua(uint32_t _cmdId);
	static bool                         IsLuaReady(uint32_t _cmdId);
    
    static bool                         IsMainThread()   { return std::this_thread::get_id() == mMainThreadID; }
    static bool                         IsShuttingDown() { return Instance().mShuttingDown; }
    static std::shared_ptr<CLuaContext> MainContext()    { return Instance().mMainContext; }
   
    static CGMLBind&                    Bind() { return *Instance().mBind; }

    static uint32_t                     SequenceIndex() { return mSequenceIndex; }
    static void                         IncrementSequence() { mSequenceIndex++; }

    static void                         Error(const std::string& _msg) { CLuaRousr::DebugPrint(_msg, true); }
    static void                         DebugPrint(const std::string& _msg, bool _forceAsync = false);

public:
	CLuaRousr();
	void Shutdown();

	std::shared_ptr<CLuaContext> GetContextFromState(lua_State* L)       { return mMainContext; }
	std::vector<std::shared_ptr<CLuaContext>>& GetContexts()             { return mContexts; }
	const std::vector<std::shared_ptr<CLuaContext>>& GetContexts() const { return mContexts; }
   
private:
    static uint32_t mSequenceIndex;

	static std::thread::id mMainThreadID;
	std::vector<std::shared_ptr<CLuaContext>> mContexts;

	std::shared_ptr<CLuaContext> mMainContext;
	std::atomic<bool> mShuttingDown = { false };
    
    std::stack<uint32_t> mGMLReadyStack;
    std::stack<uint32_t> mLuaReadyStack;

    std::unique_ptr<CGMLBind> mBind;
    std::unique_ptr<CmdWorker> mCmdWorker;
};
