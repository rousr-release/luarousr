#pragma once

#include "dllapi.h"

////
// C++
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

////
// Platform
#ifdef WIN32
#include <Windows.h>
#endif 

////
// STL
#include <memory>
#include <string>
#include <vector>
#include <map>
#include <queue>
#include <set>
#include <unordered_set>
#include <stack>

#include <tuple>
#include <functional>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <atomic>

////
// Lua
#define SOL_USING_CXX_LUA
#define SOL_EXCEPTIONS_SAFE_PROPAGATION 
#include "sol/sol.hpp"
#include "LuaIntf/LuaIntf.h"
#include "LuaError.h"

namespace LuaIntf
{
	LUA_USING_SHARED_PTR_TYPE(std::shared_ptr)
};

////
// Lockless Queue
#include "concurrentqueue/blockingconcurrentqueue.h"
#include "concurrentqueue/concurrentqueue.h"

#include "readerwriterqueue/readerwriterqueue.h"

////
// Constants
#define size_t_max      (static_cast<size_t>(~0))

#if defined (__GNUC__)
#define strncpy_s(a, b, c) strncpy(a, b, c)
#endif 

////
// Exceptions

class MsgException : public std::exception {
public:
    MsgException(const std::string& _msg) : mMsg(_msg) {;}
    const char* what() const noexcept override { return mMsg.c_str(); }
private:
    std::string mMsg;
};

////
// Spinlock
// courtesy: anki3d.org/spinlock
class SpinLock {
public:
	void lock() {
		while(mLock.test_and_set(std::memory_order_acquire)) { ; }
	}

	void unlock() {
		mLock.clear(std::memory_order_release);
	}

private:
	std::atomic_flag mLock = ATOMIC_FLAG_INIT;
};