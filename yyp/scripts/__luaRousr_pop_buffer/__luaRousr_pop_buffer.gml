///@fucntion __luaRousr_pop_buffer()
///@desc pop a buffer from the buffer stack, put its size back in it.
///@returns {Real} id of the popped buffer, ready for Lua with its size as the first element
///@extensionizer { "docs": false }
var buffer = undefined;
with(LuaSr) {
  if (!ds_stack_empty(Buffer_stack))
    buffer = ds_stack_pop(Buffer_stack);
  else {
    buffer = buffer_create(_RousrDefaultBufferSize, buffer_fixed, 1);
    ds_list_add(Buffer_list, buffer);
  }
  
  buffer_seek(buffer, buffer_seek_start, 0);
  buffer_write(buffer, buffer_u16, _RousrDefaultBufferSize);
  buffer_seek(buffer, buffer_seek_start, 0);
}

return buffer;