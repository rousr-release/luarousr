///@function __luaRousr_instance_builtin_set_yprevious(_val)
///@desc builtin setter for yprevious
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
yprevious = argument0;
