///@function __luaRousr_instance_builtin_get_persistent()
///@desc builtin getter for persistent
///@returns {*} builtin
///@extensionizer { "docs": false }
return persistent;
