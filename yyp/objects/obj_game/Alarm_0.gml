if(!ds_list_size(ball_queue)){
	exit;
}

var _data = ball_queue[| 0];

var _i=0;
repeat(ds_grid_height(balls)){
	var _inst = balls[# 0,_i];
	if(_inst == 0 || !instance_exists(_inst)){
		_inst = instance_create_depth(_data[ball_queue_data.x],_data[ball_queue_data.y],0,obj_ball);
		with(_inst){
			//Get lua id from id queue - will be 1 or 2 
			lua_id = _i+1;
			//Creat lua key - will be "ball1.0" or "ball2.0"
			lua_key = "ball"+string(lua_id)+".0";
			//Bind instance and start thread
			luaRousr_bind_instance(lua_key,id); 
			luaRousr_call("ballStart",lua_id);
		}
		balls[# 0,_i] = _inst;
		ds_list_delete(ball_queue,0);
	
		if(ds_list_size(ball_queue)){
			var _data = ball_queue[| 0];
			alarm[0] = _data[ball_queue_data.delay];
		}
		break;
	}
	++_i;
}



