TestObject = GMLInstance("test_object")
TestThread = -1

function testPrint(msg)
    --print("unitTest - Lua: " .. msg)
    print("Lua - " .. msg)
end

function Test1()
    testPrint("Test 1: TestObject.TestVariable: " .. TestObject.TestVariable)
    testPrint("Test 1: TestObject.TestVariable = 6")
    TestObject.TestVariable = 6
end

function Test1_A()
    testPrint("Test 1: TestObject.TestVariable: " .. TestObject.TestVariable)
    testPrint("Test 1: TestObject.TestVariable = TestObject.TestVariable + 1")
    TestObject.TestVariable = TestObject.TestVariable + 1
end

function Test2()
    testPrint("Test 2: Calling test2_testfunction(10, Test2_Callback)")
    test2_testfunction(10, Test2_Callback)
end 

function Test2_Callback(num, str)
    testPrint("Test 2: Test2_Callback (num: " .. num .. " str: " .. str .. ")")
end

function Test3()
    testPrint("Test 3: Pass Test3_Callback to test3_function()")
    test3_function(Test3_Callback)
    return 100
end

function Test3_Callback()
    test3_function2("Success!")
end

function Test4()
    testPrint("Test4: Starting Thread")
    TestThread = thread(function()	
		while (true) do
            testPrint("Test4 - Thread: Tick")
            thread.yield()
        end
	end) 
end

function Test5()
    testPrint("Test5: Returning value: 100")
    return 100
end

function Test6()
    testPrint("Test6: Reading Global 'TestGlobal': " .. TestGlobal)
    testPrint("Test6: Reading Global 'TestGlobalString': " .. TestGlobalString)
    TestGlobal = "Hi From Lua!"
    TestGlobalString = 66
    testPrint("Test6: Global `TestGlobal` set to: " .. TestGlobal)
    testPrint("Test6: Global `TestGlobalString` set to: " .. tostring(TestGlobalString))
end                

function Test7()
    TestObject.sprite_index = GMLResource("ball")
end