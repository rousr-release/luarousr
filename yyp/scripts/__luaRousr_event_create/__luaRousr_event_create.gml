///@function luaRousr_event_create()
///@desc Create event for LuaRousr Object
///@extensionizer { "docs": false }
#region Initialize

#region Debug Init
DebugToConsole = true;
DebugListeners = ds_list_create();

#endregion
#region Initialize Buffers

Buffer_list  = ds_list_create();
Buffer_stack = ds_stack_create();

// helper buffer
Argument_buffer = buffer_create(32767, buffer_fixed, 1);
buffer_write(Argument_buffer, buffer_u32, 32767);

#endregion
#region Bindings

GMLBindings = array_create(__ELuaRousrGMLBindData.Num);
GMLBindings[@ __ELuaRousrGMLBindData.Binds]            = sr_pool_create_pool(__luaRousr_pool_alloc_bind);
GMLBindings[@ __ELuaRousrGMLBindData.IdToBind]         = ds_map_create();
GMLBindings[@ __ELuaRousrGMLBindData.NameToBind]       = ds_map_create();
GMLBindings[@ __ELuaRousrGMLBindData.BoundVariables]   = sr_array_create();
GMLBindings[@ __ELuaRousrGMLBindData.ResourceNameMap]  = ds_map_create();
GMLBindings[@ __ELuaRousrGMLBindData.ResourceIndexMap] = ds_map_create();
GMLBindings[@ __ELuaRousrGMLBindData.Payloads]         = ds_list_create();

// Function Binding User Data
ScriptUserData = ds_map_create();

#endregion

#endregion
#region !!! Initialize LuaSr
if (variable_global_exists("___luaRousr") && instance_exists(global.___luaRousr))
  return;

var initialized = __extLuaRousrInit();
if (initialized == 0) {
  show_debug_message("luaRousr - FATAL: Failed to load shared library!");
  global.___luaRousr       = undefined; 
  instance_destroy(id);
  return;
} 

global.___luaRousr       = id; 

#endregion
#region GMLInstance Bind Creation

var builtInData = [
//    string name,             default value,    read-only?   (__ELuaRousrBoundValue)
  [ "id",                    -1,                    true  ], // __ELuaRousrInstanceBuiltIn.Id
  [ "visible",               visible,               false ], // __ELuaRousrInstanceBuiltIn.Visible
  [ "solid",                 solid,                 false ], // __ELuaRousrInstanceBuiltIn.Solid
  [ "persistent",            persistent,            false ], // __ELuaRousrInstanceBuiltIn.Persistent
  [ "depth",                 depth,                 false ], // __ELuaRousrInstanceBuiltIn.Depth
  [ "layer",                 layer,                 false ], // __ELuaRousrInstanceBuiltIn.Layer
  [ "alarm",                 alarm,                 false ], // __ELuaRousrInstanceBuiltIn.Alarm,
  [ "object_index",          object_index,          true  ], // __ELuaRousrInstanceBuiltIn.ObjectIndex,
  [ "sprite_index",          sprite_index,          false ], // __ELuaRousrInstanceBuiltIn.SpriteIndex,
  [ "sprite_width",          sprite_width,          true  ], // __ELuaRousrInstanceBuiltIn.SpriteWidth,
  [ "sprite_height",         sprite_height,         true  ], // __ELuaRousrInstanceBuiltIn.SpriteHeight,
  [ "sprite_xoffset",        sprite_xoffset,        true  ], // __ELuaRousrInstanceBuiltIn.SpriteXoffset,
  [ "sprite_yoffset",        sprite_yoffset,        true  ], // __ELuaRousrInstanceBuiltIn.SpriteYoffset,
  [ "image_alpha",           image_alpha,           false ], // __ELuaRousrInstanceBuiltIn.ImageAlpha,
  [ "image_angle",           image_angle,           false ], // __ELuaRousrInstanceBuiltIn.ImageAngle,
  [ "image_blend",           image_blend,           false ], // __ELuaRousrInstanceBuiltIn.ImageBlend,
  [ "image_index",           image_index,           false ], // __ELuaRousrInstanceBuiltIn.ImageIndex,
  [ "image_number",          image_number,          true  ], // __ELuaRousrInstanceBuiltIn.ImageNumber,
  [ "image_speed",           image_speed,           false ], // __ELuaRousrInstanceBuiltIn.ImageSpeed,
  [ "image_xscale",          image_xscale,          false ], // __ELuaRousrInstanceBuiltIn.ImageXscale,
  [ "image_yscale",          image_yscale,          false ], // __ELuaRousrInstanceBuiltIn.ImageYscale,
  [ "mask_index",            mask_index,            false ], // __ELuaRousrInstanceBuiltIn.MaskIndex,
  [ "bbox_bottom",           bbox_bottom,           true  ], // __ELuaRousrInstanceBuiltIn.BboxBottom,
  [ "bbox_left",             bbox_left,             true  ], // __ELuaRousrInstanceBuiltIn.BboxLeft,
  [ "bbox_right",            bbox_right,            true  ], // __ELuaRousrInstanceBuiltIn.BboxRight,
  [ "bbox_top",              bbox_top,              true  ], // __ELuaRousrInstanceBuiltIn.BboxTop,
  [ "direction",             direction,             false ], // __ELuaRousrInstanceBuiltIn.Direction,
  [ "friction",              friction,              false ], // __ELuaRousrInstanceBuiltIn.Friction,
  [ "gravity",               gravity,               false ], // __ELuaRousrInstanceBuiltIn.Gravity,
  [ "gravity_direction",     gravity_direction,     false ], // __ELuaRousrInstanceBuiltIn.GravityDirection,
  [ "hspeed",                hspeed,                false ], // __ELuaRousrInstanceBuiltIn.Hspeed,
  [ "vspeed",                vspeed,                false ], // __ELuaRousrInstanceBuiltIn.Vspeed,
  [ "speed",                 speed,                 false ], // __ELuaRousrInstanceBuiltIn.Speed,
  [ "xstart",                xstart,                false ], // __ELuaRousrInstanceBuiltIn.Xstart,
  [ "ystart",                ystart,                false ], // __ELuaRousrInstanceBuiltIn.Ystart,
  [ "x",                     x,                     false ], // __ELuaRousrInstanceBuiltIn.X,
  [ "y",                     y,                     false ], // __ELuaRousrInstanceBuiltIn.Y,
  [ "xprevious",             xprevious,             false ], // __ELuaRousrInstanceBuiltIn.Xprevious,
  [ "yprevious",             yprevious,             false ], // __ELuaRousrInstanceBuiltIn.Yprevious,
  [ "path_index",            path_index,            true  ], // __ELuaRousrInstanceBuiltIn.PathIndex,
  [ "path_position",         path_position,         false ], // __ELuaRousrInstanceBuiltIn.PathPosition,
  [ "path_positionprevious", path_positionprevious, false ], // __ELuaRousrInstanceBuiltIn.PathPositionprevious,
  [ "path_speed",            path_speed,            false ], // __ELuaRousrInstanceBuiltIn.PathSpeed,
  [ "path_scale",            path_scale,            false ], // __ELuaRousrInstanceBuiltIn.PathScale,
  [ "path_orientation",      path_orientation,      false ], // __ELuaRousrInstanceBuiltIn.PathOrientation,
  [ "path_endaction",        path_endaction,        false ], // __ELuaRousrInstanceBuiltIn.PathEndaction,
  [ "timeline_index",        timeline_index,        false ], // __ELuaRousrInstanceBuiltIn.TimelineIndex,
  [ "timeline_running",      timeline_running,      false ], // __ELuaRousrInstanceBuiltIn.TimelineRunning,
  [ "timeline_speed",        timeline_speed,        false ], // __ELuaRousrInstanceBuiltIn.TimelineSpeed,
  [ "timeline_position",     timeline_position,     false ], // __ELuaRousrInstanceBuiltIn.TimelinePosition,
  [ "timeline_loop",         timeline_loop,         false ], // __ELuaRousrInstanceBuiltIn.TimelineLoop,
];

var numBuiltIns = array_length_1d(builtInData);
for (var bindIndex = 0; bindIndex < numBuiltIns; ++bindIndex) {
  var builtIn = builtInData[bindIndex];
    
  // look up all of the accessor script functions
  var getter = asset_get_index("__luaRousr_instance_builtin_get_" + string(builtIn[0]));
  var setter = asset_get_index("__luaRousr_instance_builtin_set_" + string(builtIn[0]));

  var success = __luaRousr_bind_member(bindIndex, builtIn[0], builtIn[1], builtIn[2], 
                                      getter, setter, undefined);
  
  // todo: filter bound variables
};

#endregion

luaRousr_bind_script("GMLResource", __luaRousr_bind_resource_lua);
