///@dfunction luaRousr_unbind_instance(_id_or_name)
///@desc Removes an instance binding so that Lua no longer updates the GML instance.
///      **Note:** Any references to the **Lua** version of the object are still 'valid' in that they will act like a real instance... but they no longer are attached to the GML rendering them useless.
///@param {Real|String} _id_or_name   instance id OR Lua string Name of the instance to unbind
var _id_or_name = argument0;
 
with (LuaSr) {
  var id_map      = GMLBindings[__ELuaRousrGMLBindData.IdToBind];
  var name_map    = GMLBindings[__ELuaRousrGMLBindData.NameToBind];
  
  var bind_map = is_string(_id_or_name) ? name_map : id_map;
  var bind = bind_map[? _id_or_name];

  if (bind != undefined) {
    var _id        = bind[@ __ELuaRousrBindEntity.Id];
    var name       = bind[@ __ELuaRousrBindEntity.LuaName]
  
    // Clear the bind
    var binds  = GMLBindings[__ELuaRousrGMLBindData.Binds];
    sr_pool_release(binds, bind);
  
    // Remove it from datastructures
    ds_map_delete(id_map,      _id);
    ds_map_delete(name_map,    name);
  
    // Notify Lua its unhooked
    __extLuaRousrUnbindInstance(_id);
  }
}