///@function luaRousr_retain_callback(_callback_id) 
///@desc Increments the reference count for a callbackId. As long as the reference count is above 0, the callback is kept in memory.
///@desc When a `callbackId` is passed to GML you must retain it during that step, as it begins with a reference count of 0. If you're going to call it immediately, there's no need to retain it.
///@param {Real} _callback_id   callback Id passed to us from Lua
///@returns {Real} _callback_id or undefined
var _callbackid = argument0;
return __extLuaRousrRetainScriptCallback(_callbackid) >= 0 ? _callbackid : undefined;