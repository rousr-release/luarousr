///@desc otb_init - initialze outsideTheBox and set the sandbox filepath
if (variable_global_exists("___otbInitialized")) {
  if (global.___otbInitialized)
    return 1.0;
}

global.___otbFiles       = [ ];

var cf   = file_text_open_write("otb.cfg");
var path = filename_path("otb.cfg");  // determine if we're %AppData% or %LocalAppData%
file_text_close(cf);

global.___otbInitialized = __oTB_init(path) > 0;
return global.___otbInitialized;