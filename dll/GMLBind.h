#pragma once

#include "types.h"

class IGMLLuaVal;
class CLuaContext;
class CGMLBindInstance;

class CGMLBind
{
    using LuaFunction = std::function<sol::object(CGMLBindInstance&, sol::this_state&)>;

public:
    CGMLBind(const std::string& _moduleName);
	virtual ~CGMLBind() { ; }

    void   LuaBind(std::shared_ptr<CLuaContext> _context);

    const std::string& Name() const { return mBindingName; }
    size_t GetMemberCount() const   { return mMembers.size(); }

    size_t AddMember(uint32_t _index, const std::string& _name, std::shared_ptr<IGMLLuaVal> _val, bool _readOnly);
    size_t BindCustomMember(double _gmlId, std::shared_ptr<CGMLBuffer> _argBuffer);
    size_t GetMemberIndex(const std::string& _name) const;
    
	bool   IsMemberReadOnly(size_t _memberIndex)            const;
	bool   IsMemberReadOnly(const std::string& _memberName) const;

    void   UpdateInstanceMembers(std::vector<std::shared_ptr<IGMLLuaVal>>& _members) const;

    void   AddInstance(std::shared_ptr<CGMLBindInstance> _instance);
    bool   RemoveInstance(double _gmlId);

    bool   SyncRead(CGMLBuffer& _buffer);
    void   SyncWrite(CGMLBuffer& _buffer);

    bool   Dirty() const { return !mDirty.empty(); }
    void   MarkDirty(size_t _instanceIndex) { mDirty.insert(_instanceIndex); }

private:
	std::string mBindingName;

	using MemberValue = std::tuple<std::string, std::shared_ptr<IGMLLuaVal>, bool>;
	std::vector<MemberValue>       mMembers;
	std::map<std::string, size_t>  mMemberMap;

	std::map<std::string, size_t>                  mInstanceNameMap;
    std::map<double, size_t>                       mInstanceIdMap;
    std::vector<std::shared_ptr<CGMLBindInstance>> mInstances;
    std::deque<uint32_t>                           mFreeIndices;

    std::set<size_t>                               mDirty;
};