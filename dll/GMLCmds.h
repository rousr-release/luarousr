#pragma once

#include "GMLCmdLayer.h"
#include "GMLBind.h"
#include "GMLBindInstance.h"

#include "rousrGML/GMLBuffer.h"
#include "GMLLuaVal.h"

namespace GMLCmds {
    inline void GMLCall(const uint32_t _scriptId, std::vector<std::shared_ptr<IGMLLuaVal>>&& _vArgs, const std::function<void(std::shared_ptr<IGMLLuaVal>)>& _returnHandler) {
        GMLCmdLayer::Execute([_scriptId, _vArgs, _returnHandler](GMLCmdLayer& _cmdLayer) {
            auto cmdBuffer(_cmdLayer.CmdBuffer());
            CGMLBind& bind(CLuaRousr::Bind());

            // write function to buffer
            cmdBuffer->Seek(LuaRousr::SeekData);
            cmdBuffer->Write<int8_t>(static_cast<int8_t>(LuaRousr::Cmd::GMLBound));
            bind.SyncWrite(*cmdBuffer);
            cmdBuffer->Write<uint32_t>(_scriptId);
            cmdBuffer->Write<int8_t>(static_cast<int8_t>(_vArgs.size()));
            for (auto& arg : _vArgs) {
                arg->WriteBuffer(*cmdBuffer);
            }
            _cmdLayer.SendBuffer();

            cmdBuffer->Seek(1);
            uint32_t scriptId(cmdBuffer->Read<uint32_t>());
            if (scriptId != _scriptId) {
                CLuaRousr::Error("luaRousr - warning: GMLCall / return mismatch! Expected: " + std::to_string(_scriptId) + " Received: " + std::to_string(scriptId));
                _returnHandler(std::make_shared<GMLLuaValVoid>());
            }
            else {
                int8_t numReturns(cmdBuffer->ReadByte());

                // TODO: Multiple Returns
                _returnHandler(numReturns > 0 ? GMLLua::ReadVal(*cmdBuffer) : std::make_shared<GMLLuaValVoid>());
            }
        });
    }

    inline void DebugMsg(const std::string& _msg) {
        GMLCmdLayer::Execute([_msg](GMLCmdLayer& _cmdLayer) {
            auto cmdBuffer(_cmdLayer.CmdBuffer());
            cmdBuffer->Seek(LuaRousr::SeekData);
            cmdBuffer->Write<int8_t>(static_cast<int8_t>(LuaRousr::Cmd::DebugPrint));
            cmdBuffer->Write<std::string>(_msg);
            _cmdLayer.SendBuffer();
        });
    }
}