///@desc oTB_fileToString - open a file u
///@param {String} [_dataPath=""] - data path to use, i.e., executable path or any filesystem path (using oTB)
///@param {String} _fname         - Can be 1st argument if no datapath is passed, filename to open. will use file_text_open_read if it oTB fails.
///@return {String} undefined on failure, string of the file on success.
var _dataPath  = argument_count >= 2 ? argument[0] : "";
var _fname     = argument_count >= 2 ? argument[1] : argument[0];
var usesOTB = true;

var _file = oTB_fileOpen(string(_dataPath + "/" + _fname), oTB.Read),
	_string = "";
 
if (_file == -1) {
  usesOTB = false;
  _file = file_text_open_read(_fname);

  if (_file == -1)
	  return undefined;
}

while (!file_text_eof(_file)) {
	_string += file_text_readln(_file);
}

if (usesOTB)
  oTB_fileClose(_file);
else
  file_text_close(_file)

return _string;