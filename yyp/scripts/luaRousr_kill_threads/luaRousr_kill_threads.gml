///@function luaRousr_kill_threads()
///@desc Kill all threads created in Lua script using the `thread(function)` Lua function.
///@desc When reloading scripts, it's definitely a good idea to use this function to make sure you have no wild `LuaThreads` running around.
__extLuaRousrKillThreads();