///@function __luaRousr_instance_builtin_get_x()
///@desc builtin getter for x
///@returns {*} builtin
///@extensionizer { "docs": false }
return x;
