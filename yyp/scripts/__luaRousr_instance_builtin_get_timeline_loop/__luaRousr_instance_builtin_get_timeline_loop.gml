///@function __luaRousr_instance_builtin_get_timeline_loop()
///@desc builtin getter for timeline_loop
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_loop;
