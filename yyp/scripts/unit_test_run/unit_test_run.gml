///@desc unitTest - Create - Start and run all the unitTests

#region Initialize Tests
var indent = "   ";
var tag = "unitTest - gml: ";
var trace = "";

if (!instance_exists(LuaRousr))
	instance_create_depth(x, y, depth, LuaRousr);

instance_destroy(TestObject);
instance_create_depth(x, y, depth, TestObject);

var exe_path = get_executable_path();
var lua_scripts = ["unittest/unittest"];
for (var i = 0; i < array_length_1d(lua_scripts); ++i)
	luaRousr_execute_string(otb_file_to_string(exe_path, string(lua_scripts[i] + ".lua")));


#endregion

#region Test 1: Custom Instance Variable Binding
unit_test_write("\n");

unit_test_write("Test 1 - Instance Variable:");

with (TestObject) {
  unit_test_write(tag + indent + "TestObject.TestVariable: " + string(TestVariable));
  unit_test_write(tag + "Execute `Test1()`");
  
  luaRousr_call("Test1");
    
  unit_test_write(tag + "Execution Complete - TestObject.TestVariable: " + string(TestVariable));
  TestVariable++;          
  unit_test_write(tag + "Execute `TestVariable++; TestVariable: " + string(TestVariable) + " - Test1_A()`");

  luaRousr_call("Test1_A");     
  
  unit_test_write("Test 1 - Complete:  - TestObject.TestVariable: " + string(TestVariable));
}
unit_test_write("\n");

#endregion
#region Test 2: Function Call and Callbacks

unit_test_write("Test 2 - Functions and callbacks");

luaRousr_bind_script("test2_testfunction", test2_testfunction)
unit_test_write(tag + "Execute `Test2()`");
luaRousr_call("Test2");
with (TestObject) {
  unit_test_write(tag + indent + "Calling `TestObject.Callback(15, 'TestString')`");
  luaRousr_call(Callback, 15, "TestString");
  unit_test_write(tag + indent + "Releasing `TestObject.Callback`");
  luaRousr_release_callback(Callback);
  Callback = undefined;
}
unit_test_write("Test 2 - Complete");
unit_test_write("\n");

#endregion
#region Test 3: Lua->GML->Lua->GML Call Chain
luaRousr_bind_script("test3_function", test3_function);
luaRousr_bind_script("test3_function2", test3_function2);

unit_test_write("Test 3: Lua->GML->Lua->GML Call Chain");
unit_test_write(tag + "Execute `Test3()`");
luaRousr_call("Test3");
unit_test_write("Test 3 - Complete");
unit_test_write("\n");

#endregion
#region Test 4: kill threads

unit_test_write("Test 4: kill threads");
unit_test_write(tag + "Execute `Test4()`");
luaRousr_call("Test4");
unit_test_write(tag + "Calling luaRousr_tick once...");
luaRousr_tick();
unit_test_write(tag + "Calling `luaRousr_kill_threads()`");
luaRousr_kill_threads();
luaRousr_tick();
unit_test_write("Test 4 - Complete");
unit_test_write("\n");

#endregion
#region Test 5: Kill Threads

unit_test_write("Test 5: luaRousr_Call return value");
unit_test_write(tag + "Execute `Test5()`");
var val = luaRousr_call("Test5");
unit_test_write("Test 5 - Complete: val returned: " + string(val));
unit_test_write("\n");

#endregion

#region Test6: Globals

unit_test_write("Test 6: luaRousr_set_global / luaRousr_get_global");
unit_test_write(tag + "Setting Global 'TestGlobal' to 44, 'TestGlobalString' to 'GML is rad.'");
luaRousr_set_global("TestGlobal", 44);
luaRousr_set_global("TestGlobalString", "GML is rad.");
unit_test_write(tag + "Execute `Test6()`");
luaRousr_call("Test6");
var testGlobal = luaRousr_get_global("TestGlobal");
var testGlobalString = luaRousr_get_global("TestGlobalString");
unit_test_write("Test 6 - Complete - `TestGlobal` is: " + string(testGlobal) + " and `TestGlobalString` is: " + string(testGlobalString));
unit_test_write("\n");

#endregion

#region Test7: GMLResource

unit_test_write("Test 8: GMLResource");
unit_test_write(tag + "Binding spr_ball as 'ball'");
luaRousr_bind_resource("ball", spr_ball);
unit_test_write(tag + "Execute `Test7()`: Set the sprite to spr_ball");
luaRousr_call("Test7");
unit_test_write("Test 7 - Complete");
unit_test_write("\n");

#endregion

// instance_destroy(TestObject);