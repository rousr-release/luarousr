///@desc __luaRousr_write_val - compare a value to a context value array's value.
///@param {Array}       _syncVals      - sr_array of values this context was last updated with 
///@param {Real}        _contextIndex  - index in contextValues this value is
///@param {Real|String} _val           - value to update contextValues with
///@param {Array}       _dirtyValues   - array of already dirty values, this contextIndex is added if its dirty
///@param {Real}        _dirtyIndex    - index in the dirtyArray to use
///@param {Boolean}      _force        - force set the value regardless if its dirty
///@returns {Real} the next _dirtyIndex to use for the next contextValue
///@extensionizer { "docs": false }
gml_pragma("forceinline");

var _syncVals      = argument0;
var _contextIndex  = argument1;
var _val           = argument2;
var _dirty         = argument3;
var _dirtyIndex    = argument4;
var _force         = argument5;

var _contextValues = sr_array_data(_syncVals);
var _contextCount  = sr_array_size(_syncVals);

if (_contextIndex < _contextCount) {
	var oldVal = _contextValues[_contextIndex];
	if (!_force && oldVal == _val)
		return _dirtyIndex;

  _contextValues[@ _contextIndex] = oldVal;
} else {
  var insertVal = 0;
  if (is_real(_val) || is_string(_val)) {
  	insertVal = _val;
  } else if (is_bool(_val) && _val) {
    insertVal = _val
  } 
  
  sr_array_insert(_syncVals, _contextIndex, insertVal);
}

_dirty[@ _dirtyIndex++] = _contextIndex;
_dirty[@ _dirtyIndex++] = _val;

return _dirtyIndex;