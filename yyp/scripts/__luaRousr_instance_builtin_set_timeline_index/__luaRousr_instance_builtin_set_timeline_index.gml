///@function __luaRousr_instance_builtin_set_timeline_index(_val)
///@desc builtin setter for timeline_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
timeline_index = argument0;
