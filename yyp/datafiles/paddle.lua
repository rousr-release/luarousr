function paddleStart(paddleIndex) 
	local instance = GMLInstance("paddle"..paddleIndex)
	
	local paddle = { }
	local startForce = 100
	local dist1 = 360 * 360
	local dist2 = 128 * 128
	local dist3 = 32 * 32
	local minY = 64 
	local maxY = game.roomHeight - 64
	local maxV = 3
	local friction = 0.9

	local function addGridForce(fx,fy,radius)
		gmlAddGridForce(fx,fy,instance.x,instance.y,radius)
	end

	local function init()
		if game.paddles[paddleIndex] == nil then 
			paddle.instance = instance
			paddle.accel = 0
			paddle.velocity = 0
			paddle.placement = sign(instance.x - game.roomCenterX)
			paddle.radiusSquared = 16 * 16
			game.paddles[paddleIndex] = paddle
			addGridForce(startForce*paddle.placement,0,paddle.radiusSquared)
		else 
			paddle = game.paddles[paddleIndex]
			paddle.instance = instance 
		end
	end

	local function findTarget()
		local target = nil
		local min = game.roomWidthSquared/2.5

		for key,ball in pairs(game.balls) do
			if ball.dx == paddle.placement then 
				local dx = ball.instance.x - instance.x
				local dy = ball.instance.y - instance.y
				local len = dx * dx + dy * dy

				if len < min then
					min = len
					target = ball
				end

			end 
		end 
		return target
	end

	local function chaseTarget(target)
		local dx = target.instance.x - instance.x 
		local dy = target.instance.y - instance.y
		local len2 = (dx * dx) + (dy * dy)
		local dir = sign(dy)
		local speed = 0
		if len2 < dist1 then
			speed = 0.5
		end

		if len2 < dist2 then
			maxV = 4
			speed = 1
		end

		if len2 < dist3 then
			maxV = 6
			speed = 1
		end
		return dir * speed
	end

	local function chaseCenter()
		return sign(game.roomCenterY-instance.y)/2
	end

	local function update()
		local target = findTarget()
		local speed = 0
		maxV = 3

		if target then 
			speed = chaseTarget(target)
		elseif math.abs(game.roomCenterY - instance.y) > 10 then 
			speed = chaseCenter()
		end

		paddle.velocity = clamp(paddle.velocity + speed,-maxV,maxV)
		instance.y = instance.y + paddle.velocity
		local lastY = instance.y
		instance.y = clamp(instance.y,minY,maxY)

		if lastY ~= instance.y then
			paddle.velocity = 0
		end

		if speed == 0 then 
			paddle.velocity = paddle.velocity *  friction
		end

		addGridForce(0,paddle.velocity,paddle.radiusSquared)
	end

	local newThread = thread(function()	
		init()
		while(instance ~= nil) do
			update()
			thread.yield()  
		end
		newThread = nil		
	end) 
end