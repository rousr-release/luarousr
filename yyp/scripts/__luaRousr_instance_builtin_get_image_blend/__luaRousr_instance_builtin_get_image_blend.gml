///@function __luaRousr_instance_builtin_get_image_blend()
///@desc builtin getter for image_blend
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_blend;
