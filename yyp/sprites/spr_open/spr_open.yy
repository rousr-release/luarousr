{
    "id": "662eaae7-6b62-48f9-8777-573f08e1abd0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_open",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 0,
    "bbox_right": 639,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "7bfb1008-cefe-46df-af37-2c806f4edc5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "662eaae7-6b62-48f9-8777-573f08e1abd0",
            "compositeImage": {
                "id": "223f2320-1f8c-4fe1-9553-7bce076cbbd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bfb1008-cefe-46df-af37-2c806f4edc5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6a4bab6-acb5-4767-92bb-9beae3d23278",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bfb1008-cefe-46df-af37-2c806f4edc5b",
                    "LayerId": "04185619-b699-42e7-accc-70ec22305f71"
                },
                {
                    "id": "282e0e92-7fc0-45bc-92ef-509d77051df6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bfb1008-cefe-46df-af37-2c806f4edc5b",
                    "LayerId": "616794b3-676d-4b06-ac78-c574818718a5"
                },
                {
                    "id": "3047aa84-5235-449c-ad40-1db15cddcabc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bfb1008-cefe-46df-af37-2c806f4edc5b",
                    "LayerId": "a7af532a-f695-48c0-be62-2a35f6971752"
                }
            ]
        }
    ],
    "gridX": 4,
    "gridY": 4,
    "height": 360,
    "layers": [
        {
            "id": "04185619-b699-42e7-accc-70ec22305f71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "662eaae7-6b62-48f9-8777-573f08e1abd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "616794b3-676d-4b06-ac78-c574818718a5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "662eaae7-6b62-48f9-8777-573f08e1abd0",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "a7af532a-f695-48c0-be62-2a35f6971752",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "662eaae7-6b62-48f9-8777-573f08e1abd0",
            "blendMode": 1,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 72,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}