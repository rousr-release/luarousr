///@desc __luaRousr_push_buffer - push a buffer back onto LuaSr's buffer stack
///@param {Real} _buffer - id of the buffer no longer used in Lua
///@extensionizer { "docs": false }
var _buffer = argument0;
with (LuaSr) {
  ds_stack_push(Buffer_stack, _buffer);
}