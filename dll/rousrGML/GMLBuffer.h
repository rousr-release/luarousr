#pragma once

#include "GMLBufferBase.h"

class CGMLBuffer : public CGMLBufferBase
{
public:
    CGMLBuffer(size_t _bufferSize = 0, const char* _bufferPtr = nullptr) : CGMLBufferBase(_bufferSize, _bufferPtr) { ; }
    CGMLBuffer(const char* _bufferPtr) : CGMLBufferBase(static_cast<size_t>(*(reinterpret_cast<const uint32_t*>(&_bufferPtr[0]))), _bufferPtr) { Seek(sizeof(uint32_t)); }
    virtual ~CGMLBuffer() { ; }

    template <typename T, typename=std::enable_if_t<!std::is_same<T, std::string>::value && std::is_pod<T>::value>>
	void Write(const T& _val) {
        size_t totalSize(mSeekPos + sizeof(T));
		if (totalSize >= mBufferSize)
			return;

		*reinterpret_cast<T*>(&mBuffer[mSeekPos]) = _val;
		mSeekPos += sizeof(T);
	}
	
    template <typename T, typename=std::enable_if_t<std::is_same<T, std::string>::value>>
	void Write(const std::string& _val) {
        size_t length(_val.length());
        if (length == 0 || mSeekPos + length + 1 >= mBufferSize)
            return;

        char* _buffer = &mBuffer[mSeekPos];
        std::memcpy(_buffer, _val.c_str(), length);
        mSeekPos += _val.length();
        mBuffer[mSeekPos++] = 0;
	}
};
