///@desc Close a file opened with otb
///@param {Real} _file_handle - file handle returned by oTB_fileOpen
var _fh = argument0;
oTB_init();

var fileData = __oTB_fileFind(_fh);
if (fileData == undefined) {
  show_debug_message("Unable to find file (handle: " + string(_fh) + ") to close! Data may be lost.");
  return;
}

var flags = fileData[EOTBFile.FileFlags];
var oh = fileData[EOTBFile.OTBHandle];

if ((flags & oTB.Text) == oTB.Text || (flags & oTB.Binary) != oTB.Binary) 
  file_text_close(_fh);
else
  file_bin_close(_fh);

var fdb = global.___otbFiles;
var numFiles = array_length_1d(fdb);
var newFdb = array_create(numFiles - 1);

// We don't need to copy anything if this is the only file.
if (numFiles > 1) {
  var fileIndex = fileData[EOTBFile.FileIndex];
  array_copy(newFdb, 0, fdb, 0, fileIndex);
  array_copy(newFdb, fileIndex, fdb, fileIndex + 1, numFiles - fileIndex - 1);
}

global.___otbFiles = newFdb;
__oTB_closeFile(oh);