{
    "id": "95e7d0df-35fd-42db-9fd3-29c388705675",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWhitePixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "879ceb0e-8c27-4be4-9cfe-a705358deced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95e7d0df-35fd-42db-9fd3-29c388705675",
            "compositeImage": {
                "id": "79ed0724-a7a4-47a6-b53d-0232a1623a52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "879ceb0e-8c27-4be4-9cfe-a705358deced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a77ec48e-dcd0-4f31-aacc-24187062e121",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "879ceb0e-8c27-4be4-9cfe-a705358deced",
                    "LayerId": "0ccefb17-cfc2-4e1a-aeb0-5ce022b82e71"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "0ccefb17-cfc2-4e1a-aeb0-5ce022b82e71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95e7d0df-35fd-42db-9fd3-29c388705675",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}