#include "luaRousr.h"

#include "GMLBind.h"

#include "rousrGML/GMLVal.h"
#include "rousrGML/GMLDSMap.h"

#include "LuaContext.h"
#include "GMLBind.h"

#include "CmdWorker.h"

#include "GMLCmds.h"

std::thread::id CLuaRousr::mMainThreadID;
uint32_t CLuaRousr::mSequenceIndex = 0;

namespace {
    static std::unique_ptr<CLuaRousr> Lua;
    static SpinLock ReadyLock;

    std::mutex mtxWaitForLua;
    std::condition_variable cvGMLReady;
}

CLuaRousr::CLuaRousr()
	: mMainContext(std::make_shared<CLuaContext>())
{
	mMainThreadID = std::this_thread::get_id(); 
	mContexts.push_back(mMainContext);
    mCmdWorker = std::make_unique<CmdWorker>();
    mBind = std::make_unique<CGMLBind>("GMLInstance");
    mBind->LuaBind(mMainContext);
}

void CLuaRousr::Shutdown() {
	mShuttingDown = true;
    ReadyLock.lock();
    mMainContext->Shutdown();
    GMLCmdLayer::Shutdown();
    mCmdWorker.reset();
    cvGMLReady.notify_all();
    ReadyLock.unlock();

	Lua.reset();
}

CLuaRousr& CLuaRousr::Instance() { return *Lua; }

bool CLuaRousr::Initialized() {
    return Lua != nullptr;
}

void CLuaRousr::GMLReady(uint32_t _cmdId) { 
    auto& inst(Instance());
    auto& GMLReadyStack(inst.mGMLReadyStack);
    auto& LuaReadyStack(inst.mLuaReadyStack);
    
    ReadyLock.lock();
    GMLReadyStack.push(_cmdId);

    if (!LuaReadyStack.empty()) {
        if (_cmdId != LuaReadyStack.top())
            CLuaRousr::Error("GMLReady: Stack Mismatch");

        LuaReadyStack.pop();
    }
    
    cvGMLReady.notify_all();
    ReadyLock.unlock();
}

void CLuaRousr::LuaReady(uint32_t _cmdId) { 
    auto& inst(Instance());
    auto& GMLReadyStack(inst.mGMLReadyStack);
    auto& LuaReadyStack(inst.mLuaReadyStack);

    ReadyLock.lock();
    LuaReadyStack.push(_cmdId);
    if (_cmdId != GMLReadyStack.top()) {
        uint32_t top = GMLReadyStack.top();
        CLuaRousr::Error("LuaReady: Stack Mismatch" + std::to_string(top));
        
    }

    GMLReadyStack.pop();
    ReadyLock.unlock();
}

void CLuaRousr::FinishCommand(uint32_t _cmdId) {
    auto& inst(Instance());
    auto& GMLReadyStack(inst.mGMLReadyStack);
    if (GMLReadyStack.empty())
        return;

    if (_cmdId != GMLReadyStack.top())
        CLuaRousr::Error("FinishCommand: Stack Mismatch");

    GMLReadyStack.pop();
}

void CLuaRousr::WaitForLua(uint32_t _cmdId) {
    using namespace std::literals::chrono_literals;
    auto& readyStack(Instance().mGMLReadyStack);
        
    ReadyLock.lock();
    bool isReady(!readyStack.empty() && readyStack.top() == _cmdId);    
    ReadyLock.unlock();
    
    if (!isReady) {
        std::unique_lock<std::mutex> lock(mtxWaitForLua);
        auto& spinLock(ReadyLock); 
        cvGMLReady.wait_for(lock, 10ms, [&spinLock, &readyStack, _cmdId](){
            spinLock.lock();
            bool isReady = (!readyStack.empty() && readyStack.top() == _cmdId) || CLuaRousr::IsShuttingDown();
            spinLock.unlock();
            return isReady || CLuaRousr::IsShuttingDown();            
        });
    }
}

bool CLuaRousr::IsLuaReady(uint32_t _cmdId) { 
    if (IsShuttingDown())
        return true;
    
    auto& readyStack(Instance().mLuaReadyStack);

    ReadyLock.lock();
    bool isReady(!readyStack.empty() && readyStack.top() == _cmdId);
    ReadyLock.unlock();

    return isReady;
}

void CLuaRousr::DebugPrint(const std::string& _msg, bool _forceAsync) {
    if (GMLCmdLayer::IsEmpty() || _forceAsync) {
        CGMLDSMap debugPrint;
        debugPrint.AddString("DebugPrint", _msg);
        debugPrint.TriggerAsyncEvent();
    }
    else {
        GMLCmds::DebugMsg(_msg);
    }
}

extern "C" {
	DLL_API double Init() {
		if (Lua != nullptr)
			return 0.0;
		
        Lua = std::make_unique<CLuaRousr>();
		return 1.0;
	}
    
    DLL_API double SetArgumentBuffer() {
        if (Lua == nullptr)
            return 0.0;

        //Lua->SetArgumentBuffer();
        return 1.0;
    }

	DLL_API double CleanUp() {
		if (Lua == nullptr)
			return 0.0;

		Lua->Shutdown();
		return 1.0;
	}

    DLL_API double WaitForLua(double _cmdId) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        uint32_t cmdId(static_cast<uint32_t>(_cmdId));
        CLuaRousr::WaitForLua(cmdId);
        
        return 1.0;
    }

    DLL_API double ReadyForLua(double _cmdId) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        uint32_t cmdId(static_cast<uint32_t>(_cmdId));
        CLuaRousr::LuaReady(cmdId);

        return 1.0;
    }

    DLL_API double Finished(double _cmdId) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        uint32_t cmdId(static_cast<uint32_t>(_cmdId));
        CLuaRousr::FinishCommand(cmdId);

        return 1.0;
    }
}