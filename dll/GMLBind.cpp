#include "GMLBind.h"

#include "luaRousr.h"

#include "rousrGML/GMLBuffer.h"
#include "rousrGML/GMLTypedBuffer.h"
#include "rousrGML/GMLWrapperBuffer.h"

#include "GMLLuaVal.h"
#include "LuaContext.h"

#include "GMLBindInstance.h"

CGMLBind::CGMLBind(const std::string& _moduleName) 
    : mBindingName(_moduleName) {
}

size_t CGMLBind::GetMemberIndex(const std::string& _name) const {
    size_t memberIndex(~0);
	
    auto itMember(mMemberMap.find(_name));
	if (itMember != mMemberMap.end())
		memberIndex = itMember->second;
	
	return memberIndex;
}

size_t CGMLBind::BindCustomMember(double _gmlId, std::shared_ptr<CGMLBuffer> _buffer) {
    auto itId(mInstanceIdMap.find(_gmlId));
    if (itId == mInstanceIdMap.end())
        return ~0;
    
    auto instance(mInstances[itId->second]);
    
    auto name(_buffer->Read<std::string>());
    auto defaultVal(GMLLua::ReadVal(*_buffer));
    auto readOnly(_buffer->Read<bool>());

    return instance->AddCustomMember(name, defaultVal, readOnly);
}

size_t CGMLBind::AddMember(uint32_t _index, const std::string& _name, std::shared_ptr<IGMLLuaVal> _val, bool _readOnly) {
	auto memberIndex(GetMemberIndex(_name));
    if (memberIndex != ~0u && _index != memberIndex)
        CLuaRousr::DebugPrint("luaRousr - CGMLBind::AddMember: member named " + _name + " found at index: " + std::to_string(memberIndex) + " but trying to be created at index: " + std::to_string(_index));

    
    if (_index >= mMembers.size())
        mMembers.resize(_index + 1);

    auto& memberValue(mMembers[_index]);
    mMemberMap[_name] = _index;

	std::get<0>(memberValue) = _name;
    std::get<1>(memberValue) = _val;
	std::get<2>(memberValue) = _readOnly;

    return _index;
}

bool CGMLBind::IsMemberReadOnly(size_t _memberIndex) const {
    if (_memberIndex >= mMembers.size())
        return false;   // TODO: Error?

    auto& memberValue(mMembers[_memberIndex]);
    return std::get<2>(memberValue);
}

bool CGMLBind::IsMemberReadOnly(const std::string& _memberName) const {
    return IsMemberReadOnly(GetMemberIndex(_memberName));
}

void CGMLBind::UpdateInstanceMembers(std::vector<std::shared_ptr<IGMLLuaVal>>& _members) const {
    size_t bindMemberSize(mMembers.size());
    size_t instanceMembersSize(_members.size());

    if (instanceMembersSize != bindMemberSize)
        _members.resize(bindMemberSize);

    for (size_t i(instanceMembersSize); i < bindMemberSize; ++i) {
        _members[i] = std::dynamic_pointer_cast<IGMLLuaVal>(std::get<1>(mMembers[i])->Clone());
    }
}

void CGMLBind::LuaBind(std::shared_ptr<CLuaContext> _context) {
	sol::state_view L(_context->L());
    std::string bindingName(mBindingName);
    CLuaContext* pContext(_context.get());

    // index metamethod (getter)
    auto index([pContext](CGMLBindInstance& _self, std::string _memberName, sol::this_state _s) {
        // todo: determine if the member value is a double, is it actually a function, so we can skip the Lua `toFunction` call
        auto memberIndex(_self.GetMemberIndex(_memberName));
        auto member(_self.GetMember(memberIndex));
        
        if (member != nullptr)
            return member->AsSolObject(_s);

        return sol::make_object(_s, nullptr);
    });

    // new_index metamethod (setter)
    auto newIndex([pContext, this](CGMLBindInstance& _self, const char* _memberName, sol::stack_object o) {
        if (IsMemberReadOnly(_memberName))
            return;

        auto index(_self.GetMemberIndex(_memberName));
        auto member(_self.GetMember(index));
        if (member == nullptr)
            return;

        auto oldVal(member->Clone());

        LuaIntf::LuaState L(o.lua_state());
        if (L.isFunction(-1))
            member->Set(std::make_shared<TGMLVal<double>>(pContext->CaptureScriptCallback(L)));
        else
            member->FromSolObject(o);

        if (!oldVal->Equals(member))
            _self.MarkDirty(index);
    });
        
    L[bindingName] = [pContext, this](sol::object _arg, sol::this_state _s) -> sol::object {
        lua_State* L(_s.L);

        // Get the arguments
        uint32_t index(~0u);
        if (_arg.is<std::string>()) {
            std::string name(_arg.as<std::string>());
            auto itInstance(mInstanceNameMap.find(name));
            if (itInstance == mInstanceNameMap.end()) {
                CLuaRousr::Error("luaRousr - warning: can't find bound type with name: " + name);
                return sol::make_object(L, nullptr);
            }
            index = itInstance->second;
        } else if (_arg.is<double>()) {
            double id(_arg.as<double>());
            auto itInstance(mInstanceIdMap.find(static_cast<uint32_t>(id)));
            if (itInstance == mInstanceIdMap.end()) {
                CLuaRousr::Error("luaRousr - warning: can't find bound type with id: " + std::to_string(id));
                return sol::make_object(L, nullptr);
            }

            index = itInstance->second;
        }

        if (index == ~0u || index >= mInstances.size()) {
            CLuaRousr::Error("luaRousr - warning: type is missing or corrupt (but did exist).");
            return sol::make_object(L, nullptr);
        }

        return sol::make_object<std::shared_ptr<CGMLBindInstance>>(_s, mInstances[index]);
    };

    L.new_usertype<CGMLBindInstance>(mBindingName + "_class", 
        "new", sol::no_constructor,
        sol::meta_method::index,     index, 
	    sol::meta_method::new_index, newIndex
	);
}

void CGMLBind::AddInstance(std::shared_ptr<CGMLBindInstance> _instance) {
	const auto& name(_instance->Name());
    const auto id(_instance->Id());

	size_t index(mInstances.size());
    if (!mFreeIndices.empty()) {
        index = mFreeIndices.front();
        mFreeIndices.pop_front();
        mInstances[index] = _instance;
    } else {
	    mInstances.push_back(_instance);
	}
    
    if (name.length() > 0)
        mInstanceNameMap[name] = index;
    
    mInstanceIdMap[id] = index;
    _instance->SetIndex(index);
}

bool CGMLBind::RemoveInstance(double _gmlId) {
    auto itId(mInstanceIdMap.find(_gmlId));
    if (itId == mInstanceIdMap.end())
        return false;

    uint32_t instanceIndex(itId->second);
    auto instance(mInstances[instanceIndex]);
    auto itName(mInstanceNameMap.find(instance->Name()));
    
    instance->Unbind();
    mInstanceIdMap.erase(itId);
    mInstanceNameMap.erase(itName);

    mInstances[instanceIndex].reset();
    mFreeIndices.push_back(instanceIndex);

    return true;
}

bool CGMLBind::SyncRead(CGMLBuffer& _buffer) {
    for (uint32_t i(0), count(_buffer.Read<uint32_t>()); i < count; ++i) {
        double _gmlId(_buffer.Read<double>());
        auto itId(mInstanceIdMap.find(_gmlId));
        if (itId == mInstanceIdMap.end()) {
            CLuaRousr::Error(std::string("luaRousr - warning: instance to read isn't bound... gmlID: ") + std::to_string(_gmlId));

            // if we couldn't find it, seek past it still.
            for (uint32_t i = 0, updateCount(_buffer.Read<uint32_t>()); i < updateCount; ++i) {
                size_t valIndex(static_cast<size_t>(_buffer.Read<uint32_t>()));
                auto newVal(GMLLua::ReadVal(_buffer));
            }
        } else {
            auto instance(mInstances[itId->second]);
            instance->Read(_buffer);
        }
    }

    return true;
}

void CGMLBind::SyncWrite(CGMLBuffer& _buffer) {
    if (mDirty.empty()) {
        _buffer.Write<uint32_t>(0);
        return;
    }

    size_t start(_buffer.Tell());
    _buffer.SeekOffset(sizeof(uint32_t));
    uint32_t written(0);
    // Gather the dirty sync's
    for (auto& instanceIndex : mDirty) {
        auto instance(mInstances[instanceIndex]);
        if (instance->Write(_buffer))
            written++;
    }

    size_t end(_buffer.Tell());
    _buffer.Seek(start);
    _buffer.Write<uint32_t>(written);
    mDirty.clear();

    if (written != 0)
        _buffer.Seek(end);  
}


extern "C" {

	DLL_API double BindInstance(const char* _instanceName, double _gmlId) {
        CGMLBind& bind(CLuaRousr::Bind());
        bind.AddInstance(std::make_shared<CGMLBindInstance>(_instanceName, _gmlId));
        return 1.0;
	}

    DLL_API double UnbindInstance(double _gmlId) {
        CGMLBind& bind(CLuaRousr::Bind());
        return bind.RemoveInstance(_gmlId) ? 1.0 : 0.0;
    }

	DLL_API double BindMember(char *_buffer) {
        CGMLBuffer buffer(_buffer);

        // function arguments
        uint32_t memberIndex(buffer.Read<uint32_t>());
        std::string membername(buffer.Read<std::string>());
        auto def(GMLLua::ReadVal(buffer));
        bool readOnly(buffer.Read<bool>());
       
        CGMLBind& bind(CLuaRousr::Bind());
        return static_cast<double>(bind.AddMember(memberIndex, membername, def, readOnly));
	}

    DLL_API double BindCustomMember(double _gmlId, double _bufferSize, char *_buffer) {
        CGMLBind& bind(CLuaRousr::Bind());
        uint32_t memberIndex(bind.BindCustomMember(_gmlId, std::make_shared<CGMLBuffer>(static_cast<uint32_t>(_bufferSize), _buffer)));
        return static_cast<double>(memberIndex != ~0 ? memberIndex : -1.0);
    }
}