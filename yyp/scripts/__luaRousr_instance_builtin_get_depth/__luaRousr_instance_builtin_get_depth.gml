///@function __luaRousr_instance_builtin_get_depth()
///@desc builtin getter for depth
///@returns {*} builtin
///@extensionizer { "docs": false }
return depth;
