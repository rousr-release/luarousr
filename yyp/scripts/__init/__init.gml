//Demo by Kyle Askew twitter.com/@net8floz 
gml_pragma("forceinline");

enum point{
	x,y,
	vx,vy,
	ax,ay,
	inverse_mass,
	damping, 
	__size 
}

enum spring{
	point1,
	point2,
	stiffness,
	damping,
	length,
	length_squared,
	__size
}

enum grid_types{
	normal,
	fancy,
	rainbow,
	null
}

enum ball_queue_data{
	x,y,delay,__size 
}

#macro min_vel 0.001 * 0.001