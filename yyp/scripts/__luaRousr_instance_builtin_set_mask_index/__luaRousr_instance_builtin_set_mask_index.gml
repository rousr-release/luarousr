///@function __luaRousr_instance_builtin_set_mask_index(_val)
///@desc builtin setter for mask_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
mask_index = argument0;
