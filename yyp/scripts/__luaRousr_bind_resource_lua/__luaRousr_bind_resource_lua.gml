///@function __luaRousr_bind_resource_lua(_lua_name)
///@desc return a registered resource to Lua
///@param {string} _lua_name   name of resource
///@returns {Real} index of resource or noone
///@extensionizer { "docs": false }
var _lua_name = argument0;
var _index = noone;

with (LuaSr) {
  var name_map  = GMLBindings[__ELuaRousrGMLBindData.ResourceNameMap];
  var bind = name_map[? _lua_name];
  
  _index = bind[1];  
}

return _index;