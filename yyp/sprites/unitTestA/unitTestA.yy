{
    "id": "fd3a9bc9-fce2-4f2a-8dc5-dd8ffd8cc50b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "unitTestA",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 9,
    "bbox_right": 25,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "163753a2-9661-400b-a02f-12e22ced4f90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd3a9bc9-fce2-4f2a-8dc5-dd8ffd8cc50b",
            "compositeImage": {
                "id": "86bf1e8d-bef1-47f8-8acb-291959718466",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "163753a2-9661-400b-a02f-12e22ced4f90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f383257-f200-490c-9aaf-841d793e956f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "163753a2-9661-400b-a02f-12e22ced4f90",
                    "LayerId": "e3de36e0-a1b9-4ca2-8c64-7393d4a95a99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 34,
    "layers": [
        {
            "id": "e3de36e0-a1b9-4ca2-8c64-7393d4a95a99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd3a9bc9-fce2-4f2a-8dc5-dd8ffd8cc50b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 0,
    "yorig": 0
}