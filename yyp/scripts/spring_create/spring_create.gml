///@param point1 point
///@param point2 point
///@param stiffnesss real
///@param damping real
var _spring = array_create(spring.__size),	
	_p1x = argument0[point.x],
	_p1y = argument0[point.y],
	_p2x = argument1[point.x],
	_p2y = argument1[point.y];
	
_spring[@ spring.point1] = argument0;
_spring[@ spring.point2] = argument1;
_spring[@ spring.stiffness] = argument2;
_spring[@ spring.damping] = argument3;
_spring[@ spring.length] = point_distance(_p1x,_p1y,_p2x,_p2y) * 0.95;
_spring[@ spring.length_squared] = _spring[spring.length] * _spring[spring.length];
return _spring;