///@function __luaRousr_instance_builtin_set_y(_val)
///@desc builtin setter for y
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
y = argument0;
