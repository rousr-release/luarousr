#pragma once
#include "luaRousr.h"

#include <stdexcept>
#include "LuaIntf/LuaIntf.h"

namespace LuaIntf {	class LuaState; }

class LuaError : public std::exception
{
public:
    // Construct using top-most element on Lua stack as error.
    LuaError(LuaIntf::LuaState& L);
    LuaError(const LuaError& other);
	virtual ~LuaError() { }
	const char * what() const throw() override;
	
private:
	LuaIntf::LuaState mL;
	std::string errorMsg;
	std::shared_ptr<LuaIntf::LuaState> mLuaExceptionStack;
	LuaError & operator=(const LuaError & other) = delete; // prevent
};

