///@desc oTB_fileOpen - open a file... outside the box.
///@param {String}   filepath - path to the file
///@param {Real} [_fileFlags] - the flags to use while opening. see oTB
///@returns an oTB file handle
var _filePath  = argument[0];
var _fileFlags = argument_count > 1 ? argument[1] : 0;
oTB_init();

var fileData = __oTB_fileFind(_filePath);
if (fileData != undefined) {
  // error - it's already open?
  return fileData[EOTBFile.GMLHandle];
}

var otbHandle   = __oTB_openFile(_filePath, (_fileFlags & oTB.Write == oTB.Write) || (_fileFlags & oTB.Append == oTB.Append));
if (otbHandle < 0) {
  show_debug_message("Unable to open file: " + string(_filePath));
  return -1;
}

var otbFileName = __oTB_getFileName(otbHandle);
if (otbFileName == 0) {
  show_debug_message("Unable to open file: " + string(_filePath) + ", unable to get local filepath.");
  return -1;
}

var fh = -1;

var textFlag = oTB.Text;
var readFlag = oTB.Read;
var flags = oTB.Text | oTB.Read;

var test1 = flags & oTB.Text;
var test2 = flags & oTB.Read;

if ((_fileFlags & oTB.Text) == oTB.Text || (_fileFlags & oTB.Binary) != oTB.Binary) {
  if ((_fileFlags & oTB.Write) == oTB.Write || (_fileFlags & oTB.Append) == oTB.Append) {
    if ((_fileFlags & oTB.Append) == oTB.Append) 
      fh = file_text_open_append(otbFileName);
    else 
      fh = file_text_open_write(otbFileName);
  } else {
    fh = file_text_open_read(otbFileName);
  
    if ((_fileFlags & oTB.Write) == oTB.Write)
      show_debug_message("Opened for read: Invalid Write Flag - Unable to open text for read and write (binary only)");
    else if ((_fileFlags & oTB.Append) == oTB.Append)
      show_debug_message("Opened for read: Invalid Append Flag - Can't 'append' a text read.");
  }
  
  if ((_fileFlags & oTB.Text) == oTB.Text && (_fileFlags & oTB.Binary) != oTB.Binary) 
    show_debug_message("Can't open file as both Text and Binary - Text opened");
} else if((_fileFlags & oTB.Binary) == oTB.Binary) {
  var mode = undefined;
  if ((_fileFlags & oTB.Read) == oTB.Read)
    mode = 0;
  if ((_fileFlags & oTB.Write) == oTB.Write)
    mode = mode == undefined ? 1 : 2;
    
  fh = file_bin_open(otbFileName, mode);
  
  if ((_fileFlags & oTB.Append) != oTB.Append)
    file_bin_rewrite(fh);
}

if (fh < 0) {
  __oTB_closeFile(otbHandle);
  show_debug_message("Unable to open AppData local file for " + string(otbFileName) + " (" + string(_filePath) + ")");
  return -1;
}

var fileDb      = global.___otbFiles;  
var numFiles = array_length_1d(fileDb);

fileData = array_create(EOTBFile.Num);
fileData[@ EOTBFile.FileIndex] = numFiles;
fileData[@ EOTBFile.FilePath]  = _filePath;
fileData[@ EOTBFile.GMLFile]   = otbFileName;
fileData[@ EOTBFile.GMLHandle] = fh;
fileData[@ EOTBFile.OTBHandle] = otbHandle;
fileData[@ EOTBFile.FileFlags] = _fileFlags;

fileDb[@ numFiles] = fileData;
return fh;