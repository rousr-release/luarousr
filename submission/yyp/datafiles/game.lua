game = { }
game.roomWidth = gmlRoomWidth() 
game.roomHeight = gmlRoomHeight()
game.roomWidthSquared = game.roomWidth * game.roomWidth;
game.roomCenterX = game.roomWidth/2
game.roomCenterY = game.roomHeight/2 
game.balls = { }
game.paddles = { }
game.instancePlacePaddle = function(id,x,y)
	return game.paddles[gmlInstancePlacePaddle(id,x,y)] 
end
game.ballCount = 2

function gameStart()
	gmlSetBallLimit(game.ballCount) 
	for i=1,game.ballCount do
		gmlCreateBall(game.roomWidth/2,game.roomHeight/2,60*i) 
	end
end
