///@function __luaRousr_instance_builtin_set_hspeed(_val)
///@desc builtin setter for hspeed
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
hspeed = argument0;
