///@param point
var _point = argument0,
	_mass = _point[point.inverse_mass],
	_vx,_vy;

//Add accel
_point[@ point.vx] += _point[point.ax] * _mass;
_point[@ point.vy] += _point[point.ay] * _mass;

//Add velocity
_point[@ point.x] += _point[point.vx];
_point[@ point.y] += _point[point.vy];

//Reset accel
_point[@ point.ax] = 0;
_point[@ point.ay] = 0;

//Set velocity to zero if too small
_vx = _point[point.vx];
_vy = _point[point.vy];
	
if( (_vx * _vx ) + (_vy * _vy) < min_vel){
	_point[@ point.vx] = 0;
	_point[@ point.vy] = 0;
}else{
	var _damping = _point[point.damping];
	_point[@ point.vx] *= _damping;
	_point[@ point.vy] *= _damping;
}



