///@function luaRousr_event_clean_up()
///@desc Clean Up event for LuaRousr Object
///@extensionizer { "docs": false }
__extLuaRousrCleanUp();

global.___luaRousr       = noone;

ds_list_destroy(DebugListeners);

sr_pool_destroy_pool(GMLBindings[__ELuaRousrGMLBindData.Binds]);
sr_array_destroy(GMLBindings[__ELuaRousrGMLBindData.BoundVariables]);
ds_map_destroy(GMLBindings[__ELuaRousrGMLBindData.IdToBind]);
ds_map_destroy(GMLBindings[__ELuaRousrGMLBindData.NameToBind]);
ds_map_destroy(GMLBindings[__ELuaRousrGMLBindData.ResourceNameMap]);
ds_map_destroy(GMLBindings[__ELuaRousrGMLBindData.ResourceIndexMap]);
ds_list_destroy(GMLBindings[__ELuaRousrGMLBindData.Payloads]);
GMLBindings = undefined;

ds_map_destroy(ScriptUserData);
ScriptUserData = undefined;

ds_stack_destroy(Buffer_stack);
Buffer_stack = undefined;

while (!ds_list_empty(Buffer_list)) {
  var buffer = Buffer_list[| 0];
  ds_list_delete(Buffer_list, 0);
  buffer_delete(buffer);
}
ds_list_destroy(Buffer_list);
Buffer_list               = undefined;

buffer_delete(Argument_buffer);
Argument_buffer = undefined;