///@function test3_function(_callback)
///@desc called by the Unit Tests
///@param {Real} _callback   callbackid of function to call in unit test
var _callback = argument0;
show_debug_message("unitTest - gml: test3_function - calling callback");
luaRousr_call(_callback);
show_debug_message("unitTest - gml: test3_function - success");