///@function __luaRousr_executeGML(_out, _scriptId, _args, _argCount)
///@desc Called from Lua, execute a GML function based passing the arguments from Lua, as necessary, and return value back.
///@param {Real}  _out           output buffer to write returns to.
///@param {Real}  _script_index  script index of the function to execute
///@param {Array} _args          array of arguments
///@param {Real}  _argCount      amount of args
///@extensionizer { "docs": false }
var _out         = argument0;
var _scriptId    = argument1;
var _args        = argument2;
var _argCount    = argument3;

var args = undefined;
var userData = LuaSr.ScriptUserData[? _scriptId];
if (userData != undefined && userData[0] > 0) {
  var userDataCount = userData[0];
  userData = userData[1];
  args = array_create(userDataCount + _argCount);
  array_copy(args, 0, userData, 0, userDataCount);
  array_copy(args, userDataCount, _args, 0, _argCount);
  _argCount += userDataCount;
} else 
  args = _args;

// Execute
var ret = sr_execute(_scriptId, args, _argCount);

var numRet = 1;
buffer_seek(_out, buffer_seek_start, 6);
if (is_string(ret))    { buffer_write(_out, buffer_u8, ERousrData.String); buffer_write(_out, buffer_string, ret); }
else if (is_real(ret)) { buffer_write(_out, buffer_u8, ERousrData.Double); buffer_write(_out, buffer_f64, ret); }
else if (is_bool(ret)) { buffer_write(_out, buffer_u8, ERousrData.Bool);   buffer_write(_out, buffer_u8, ret ? 1 : 0); }
else                   numRet = 0;

buffer_seek(_out, buffer_seek_start, 5);
buffer_write(_out, buffer_u8, numRet);
buffer_seek(_out, buffer_seek_start, 1);
buffer_write(_out, buffer_u32, _scriptId);