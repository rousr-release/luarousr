#pragma once

#include "GMLVal.h"

namespace GMLLuaVal {


};

class IGMLLuaVal {
    virtual void        Push(LuaIntf::LuaState& L) = 0;
    virtual sol::object AsSolObject(sol::state_view _L) = 0;
    virtual void        FromSolObject(sol::stack_object& _o) = 0;
};

template <typename TVal>
class TGMLLuaVal : public TGMLVal<TVal> {

};