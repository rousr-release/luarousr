///@function __luaRousr_instance_builtin_set_path_endaction(_val)
///@desc builtin setter for path_endaction
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
path_endaction = argument0;
