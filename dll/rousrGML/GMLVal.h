#pragma once

#include "rousrGML.h"

#include "GMLDSMap.h"

//////////////
class IGMLVal {//
public:
	virtual void        WriteToMap(int _mapId, const std::string& _key) = 0; 
	virtual bool        Equals(std::shared_ptr<IGMLVal> _other) = 0;
	virtual bool        Set(std::shared_ptr<IGMLVal> _other) = 0;
    virtual std::shared_ptr<IGMLVal> Clone() const = 0;

protected:
	template <typename T>
	static bool InternalEquals(T& _a, std::shared_ptr<IGMLVal> _b) {
		std::shared_ptr<T> other(std::dynamic_pointer_cast<T>(_b));
		if (other == nullptr)
			return false;

		return _a.Val() == other->Val();
	}
};

//////////////
template <typename T>
class TGMLVal : public IGMLVal {
public:
	TGMLVal(const T& _val) : mVal(_val) { ; }
	TGMLVal() : mVal() { ; }

	T& Val()             { return mVal; }
	const T& Val() const { return mVal; }

	void SetVal(const T& _val) { mVal = _val; }

	void WriteToMap(int _mapId, const std::string& _key) override { CGMLDSMap::WriteDouble(_mapId, _key, static_cast<double>(mVal)); }
    bool Equals(std::shared_ptr<IGMLVal> _other) override         { return IGMLVal::InternalEquals<TGMLVal<T>>(*this, _other); }
	std::shared_ptr<IGMLVal> Clone() const override               { return std::make_shared<TGMLVal<T>>(mVal);  }
    bool Set(std::shared_ptr<IGMLVal> _other) override            {
		auto other(std::dynamic_pointer_cast<TGMLVal<T>>(_other));
		if (other != nullptr) {
			mVal = other->mVal;
			return true;
		}

		return false;
	}

private:
	T mVal;
};

template <> inline void TGMLVal<std::string>::WriteToMap(int _mapId, const std::string& _key) {
    CGMLDSMap::WriteString(_mapId, _key, Val().c_str());
}

//////////////
class CGMLValVoid : public IGMLVal {
public:
	CGMLValVoid(CGMLBuffer* _buffer) { ; }
	CGMLValVoid() { ; }

	bool Equals(std::shared_ptr<IGMLVal> _other) override {
		std::shared_ptr<CGMLValVoid> other(std::dynamic_pointer_cast<CGMLValVoid>(_other));
		return other != nullptr;
	}
	void WriteToMap(int, const std::string&) override { ; }
    std::shared_ptr<IGMLVal> Clone() const override { return std::make_shared<CGMLValVoid>(); }
    bool Set(std::shared_ptr<IGMLVal> _other) override { return std::dynamic_pointer_cast<CGMLValVoid>(_other) != nullptr; }
};
