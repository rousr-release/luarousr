///@function __luaRousr_instance_builtin_get_sprite_index()
///@desc builtin getter for sprite_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_index;
