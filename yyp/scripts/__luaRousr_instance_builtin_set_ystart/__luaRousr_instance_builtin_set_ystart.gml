///@function __luaRousr_instance_builtin_set_ystart(_val)
///@desc builtin setter for ystart
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
ystart = argument0;
