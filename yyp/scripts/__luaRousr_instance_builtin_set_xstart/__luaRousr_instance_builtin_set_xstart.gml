///@function __luaRousr_instance_builtin_set_xstart(_val)
///@desc builtin setter for xstart
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
xstart = argument0;
