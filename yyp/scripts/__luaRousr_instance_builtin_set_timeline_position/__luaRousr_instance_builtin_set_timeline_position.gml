///@function __luaRousr_instance_builtin_set_timeline_position(_val)
///@desc builtin setter for timeline_position
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
timeline_position = argument0;
