#pragma once

#include "types.h"

class CmdWorker {
    static CmdWorker* Instance;

public:
    CmdWorker() {
        Instance = this;
    }

    ~CmdWorker() {
        Instance->mThreads.clear();
        Instance = nullptr;
    }

    static void PushJob(std::function<void()> _job) {
        Instance->PushJobInternal(std::move(_job));
    }
    
private:
    void PushJobInternal(std::function<void()>&& _job);

    class WorkerThread {
    public:
        WorkerThread();
        ~WorkerThread() {
            mShutdown = true;
            mWaitForJob.notify_all();
            while(!mShutdown)
                std::this_thread::yield();
        }

        bool isReady() const { return !mWorking && !mJobPending && mJob == nullptr; }
        void DoWork(const std::function<void()>& _job);
    
    private:
        std::atomic<bool> mWorking    = { false };
        std::atomic<bool> mJobPending = { false };
        std::atomic<bool> mShutdown   = { false };
        std::condition_variable mWaitForJob;

        std::function<void()> mJob;
    };

    std::vector<std::shared_ptr<WorkerThread>> mThreads;
};