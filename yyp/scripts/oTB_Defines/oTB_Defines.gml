///@desc collection of macros / enums used by oTB

// otb flags
enum oTB {
  Text      = (1 << 1),
  Binary    = (1 << 2),
                 
  Read      = (1 << 3),
  Write     = (1 << 4),
                 
  Append    = (1 << 5),
  Overwrite = (1 << 6),
};


enum EOTBFile {
  FileIndex,
  FilePath,
  SaneFilePath,
  GMLFile,
  GMLHandle,
  OTBHandle,
  FileFlags,
  
  Num
};