///@function __luaRousr_instance_builtin_set_image_number(_val)
///@desc builtin setter for image_number
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!
