{
    "id": "66446719-3655-406f-8f56-0eb5f225f9cf",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "extLuaRousr",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 194,
    "date": "2017-13-06 02:02:32",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "57ac44a2-3c2d-4c88-a0eb-30158ce221ab",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                {
                    "id": "063599ed-bb2c-4fc3-ae44-357f81f4f992",
                    "modelName": "GMProxyFile",
                    "mvc": "1.0",
                    "TargetMask": 1,
                    "proxyName": "rousrlua"
                }
            ],
            "constants": [
                
            ],
            "copyToTargets": 194,
            "filename": "rousrlua.dll",
            "final": "",
            "functions": [
                {
                    "id": "ea9493af-7ed6-4eb7-8f0d-6b2a2291fd47",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "",
                    "help": "nofunction",
                    "hidden": false,
                    "kind": 1,
                    "name": "_dummyFunc",
                    "returnType": 1
                },
                {
                    "id": "e843a18e-9f6f-4254-a5b8-981d59592464",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "Init",
                    "help": "Internally Used: pass the command buffer to Lua. Returns -1.0 on failure.",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrInit",
                    "returnType": 2
                },
                {
                    "id": "851819f1-1008-47e9-beba-c79dfec96fd1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "ExecuteLua",
                    "help": "Internally used: Run the string passed to Lua as Lua script. Returns -1.0 on failure.",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrExecuteLua",
                    "returnType": 2
                },
                {
                    "id": "daf82077-9959-461a-913e-55423db9b2f3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1,
                        1,
                        1
                    ],
                    "externalName": "RegisterCallbacks",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "RegisterCallbacks",
                    "returnType": 2
                },
                {
                    "id": "d4c73b36-0cfb-4f26-b365-4543610753b6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "CleanUp",
                    "help": "Internally used: clean up the background processes. Returns -1.0 on failure.",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrCleanUp",
                    "returnType": 2
                },
                {
                    "id": "6c211c24-04ec-41a7-b98c-78e9b44f14d2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1,
                        2,
                        1
                    ],
                    "externalName": "CallLua",
                    "help": "Internally used: call's a lua function and returns the values by writing them to the passed buffer. Returns -1.0 on failure.",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrCallLua",
                    "returnType": 2
                },
                {
                    "id": "531c47a7-c6df-4458-ae6a-e12a7bb6cc28",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "BindGMLFunction",
                    "help": "Internally used: Bind a GML script Id to a named Lua function. Returns -1.0 on failure.",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrBindScriptFunction",
                    "returnType": 2
                },
                {
                    "id": "17e41819-d1a2-4546-9124-27f451c975d2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "ResumeThreads",
                    "help": "Internally used: called every step to update each lua coroutine or thread. Returns -1.0 on failure.",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrResumeThreads",
                    "returnType": 2
                },
                {
                    "id": "0b164d8b-5650-4716-862b-bf68ebe66767",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "KillThreads",
                    "help": "Internally used: kill all threads. returns -1.0 on failure",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrKillThreads",
                    "returnType": 2
                },
                {
                    "id": "c6097444-1f7f-427e-b0bc-607b4a5c5495",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "BindInstance",
                    "help": "Internal Use: bind an instance",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrBindInstance",
                    "returnType": 2
                },
                {
                    "id": "f6c34a65-8f21-467a-a691-98537cd08902",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1
                    ],
                    "externalName": "BindMember",
                    "help": "internal use: binds a named \"member\" value to a bindId",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrBindMember",
                    "returnType": 2
                },
                {
                    "id": "d48c70a2-ae3f-486c-b246-8d696f914912",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "UnbindInstance",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrUnbindInstance",
                    "returnType": 2
                },
                {
                    "id": "b04f759e-8448-4a7c-8608-c8372be1f394",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2,
                        2,
                        1
                    ],
                    "externalName": "CallLuaScriptId",
                    "help": "Internally used: call a lua script callback ID given to us from Lua",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrCallLuaScriptId",
                    "returnType": 2
                },
                {
                    "id": "a156357c-83de-4ce1-9bab-4f9fcf9c0229",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "WaitForLua",
                    "help": "Internally Used: Waits for the cmd buffer to be ready - returns 0 if no Lua initialized.",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrWaitForLua",
                    "returnType": 2
                },
                {
                    "id": "21c819c4-5f20-41a3-9ad8-34a11aeb65fe",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "ReadyForLua",
                    "help": "Internally Used: signal the buffer is ready for Lua to write to",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrReadyForLua",
                    "returnType": 2
                },
                {
                    "id": "1b7d9f6c-9479-4205-892c-80fb48bc5daf",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "ReleaseCallback",
                    "help": "Internally Used: release a callback id",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrReleaseScriptCallback",
                    "returnType": 2
                },
                {
                    "id": "75974402-894e-44d0-9bef-0c27c2712190",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "RetainCallback",
                    "help": "Internally Used: increment the callback ref count",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrRetainScriptCallback",
                    "returnType": 2
                },
                {
                    "id": "776e76f2-c38b-4101-bc3c-8f3d204b9aaa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "CollectGarbage",
                    "help": "Internally Used: collects garbage - currently, no longer ref'd scripts. Returns -1.0 on failure.",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrCollectGarbage",
                    "returnType": 2
                },
                {
                    "id": "d578f0ad-fe61-4e86-815a-34f9c9e69fb1",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2,
                        2,
                        1
                    ],
                    "externalName": "BindCustomMember",
                    "help": "Internally Used: bind a custom member to a context. Returns memberIndex or -1",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrBindCustomMember",
                    "returnType": 2
                },
                {
                    "id": "28ef3b86-c61a-427e-b2a2-b13e82acb064",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2
                    ],
                    "externalName": "SetGlobalDbl",
                    "help": "sets a global with a double val",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrSetGlobalReal",
                    "returnType": 2
                },
                {
                    "id": "d7d13b07-debd-419d-b73a-5a53c01e5a1b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        1
                    ],
                    "externalName": "SetGlobalString",
                    "help": "sets a global with a string type",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrSetGlobalString",
                    "returnType": 2
                },
                {
                    "id": "2c56504d-365a-4c23-95a4-e8054fdf3d3c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        1,
                        2,
                        1
                    ],
                    "externalName": "GetGlobal",
                    "help": "returns a global value",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrGetGlobal",
                    "returnType": 2
                },
                {
                    "id": "775dc6cf-8ea5-423b-82d6-8287fe5d8258",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        2
                    ],
                    "externalName": "Finished",
                    "help": "",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrFinished",
                    "returnType": 2
                },
                {
                    "id": "d2cbc024-dc3d-4cb3-9238-16ea59245fec",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "HasThreads",
                    "help": "returns 1.0 if there's threads pending",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extLuaRousrHasThreads",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 1,
            "order": [
                "ea9493af-7ed6-4eb7-8f0d-6b2a2291fd47",
                "daf82077-9959-461a-913e-55423db9b2f3",
                "e843a18e-9f6f-4254-a5b8-981d59592464",
                "776e76f2-c38b-4101-bc3c-8f3d204b9aaa",
                "17e41819-d1a2-4546-9124-27f451c975d2",
                "851819f1-1008-47e9-beba-c79dfec96fd1",
                "6c211c24-04ec-41a7-b98c-78e9b44f14d2",
                "b04f759e-8448-4a7c-8608-c8372be1f394",
                "a156357c-83de-4ce1-9bab-4f9fcf9c0229",
                "21c819c4-5f20-41a3-9ad8-34a11aeb65fe",
                "531c47a7-c6df-4458-ae6a-e12a7bb6cc28",
                "c6097444-1f7f-427e-b0bc-607b4a5c5495",
                "f6c34a65-8f21-467a-a691-98537cd08902",
                "d578f0ad-fe61-4e86-815a-34f9c9e69fb1",
                "d48c70a2-ae3f-486c-b246-8d696f914912",
                "75974402-894e-44d0-9bef-0c27c2712190",
                "1b7d9f6c-9479-4205-892c-80fb48bc5daf",
                "d4c73b36-0cfb-4f26-b365-4543610753b6",
                "0b164d8b-5650-4716-862b-bf68ebe66767",
                "28ef3b86-c61a-427e-b2a2-b13e82acb064",
                "d7d13b07-debd-419d-b73a-5a53c01e5a1b",
                "2c56504d-365a-4c23-95a4-e8054fdf3d3c",
                "775dc6cf-8ea5-423b-82d6-8287fe5d8258",
                "d2cbc024-dc3d-4cb3-9238-16ea59245fec"
            ],
            "origname": "",
            "uncompress": false
        },
        {
            "id": "b809dbab-ac82-46c6-9bf5-0c47f61abcb1",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 35184372089026,
            "filename": "extLuaRousr.gml",
            "final": "",
            "functions": [
                {
                    "id": "c84b27ba-8331-4543-8847-357261152519",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_init",
                    "help": "create LuaRousr if it doesn't already exist. [helper function] ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_init",
                    "returnType": 2
                },
                {
                    "id": "68d10a56-2c77-4f4e-bd9f-d1ffb53e77b4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_add_debug_print_listener",
                    "help": "Adds a script index to the list of debug listeners internal to LuaRousr.      **Note:** Use `with[LuaRousr] DebugToConsole = false` to shut off default print behavior ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_add_debug_print_listener",
                    "returnType": 2
                },
                {
                    "id": "813e58c7-fd1c-43b1-a22f-f97470b03bcc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_remove_debug_print_listener",
                    "help": "Removes a script index from the list of debug listeners internal to LuaRousr.      **Note:** Use `with[LuaRousr] DebugToConsole = false` to shut off default print behavior ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_remove_debug_print_listener",
                    "returnType": 2
                },
                {
                    "id": "74809d3c-4133-42c2-acf7-2f8133d76388",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_tick",
                    "help": "force lua to process a frame \/ tick \/step ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_tick",
                    "returnType": 2
                },
                {
                    "id": "c3b95516-3204-474c-bf86-27577042426e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_default_getter",
                    "help": "called `with[instance]` (_index)",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_default_getter",
                    "returnType": 2
                },
                {
                    "id": "8f983efc-8354-4164-b011-0292d3fa134b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_default_setter",
                    "help": "called `with[instance]` (_variable_name, _index)",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_default_setter",
                    "returnType": 2
                },
                {
                    "id": "a9e848ed-cec2-4f78-ae8c-4879bf0fe2fb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_bind_script",
                    "help": "Call this for all of your GML script functions you'd like Lua to have access to.      **Note:** This must be done _before_ you load your scripts that use the functions. Otherwise, it'sunreliable that they'll be able to \"see\" your functions.      **Note About Arguments:** In Lua, you're able to treat most anything like a variable and pass it to a function as an argument. At this time, LuaRousr doesn't support accepting `Lua tables` as arguments. You may pass functions, however. If passing a function to GML, you must retain the `callbackid` in order to call it outside of the scope of the bound script function. See `luaRousr_retain_callback` and `luaRousr_release_callback`. (_script_index, [_user_data])",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_bind_script",
                    "returnType": 2
                },
                {
                    "id": "e6e30344-9d99-4130-9f45-d25a4785b289",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_bind_instance_variable",
                    "help": "Binds an `instance` variable to `_member_name` in Lua. Once `instance` is bound in Lua, the variable is accessible by `_member_name.`      In LuaRousr's current version, this feature uses `variable_instance_set` and `variable_instance_get` to keep the value in sync with Lua, so `varName` **must** match the `instance` variable name. (_member_name, _default, [_read_only=false], [_getter], [_setter], [_force_type])",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_bind_instance_variable",
                    "returnType": 2
                },
                {
                    "id": "ba6f5703-f269-436e-b71f-523aac6a0c51",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_bind_instance",
                    "help": "Binds an instance to Lua using the given name. All the members can now be accessed i.e,:      ```      inst.x = 10      print[inst.y]      ```      **NOTE:** You must unbind an instance in its `CleanUp` event if it's still bound at that point. (_instance_id)",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_bind_instance",
                    "returnType": 2
                },
                {
                    "id": "f71b5a0f-00b2-4a38-afeb-a20be5d84690",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_bind_resource",
                    "help": "bind an Object to Lua (_object_index)",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_bind_resource",
                    "returnType": 2
                },
                {
                    "id": "8c4634d0-8c62-4908-b122-149f3470adf4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_unbind_instance",
                    "help": "Removes an instance binding so that Lua no longer updates the GML instance.      **Note:** Any references to the **Lua** version of the object are still 'valid' in that they will act like a real instance... but they no longer are attached to the GML rendering them useless. ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_unbind_instance",
                    "returnType": 2
                },
                {
                    "id": "9ce1454d-0019-4b78-94de-ea3021aa439a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_unbind_resource",
                    "help": "unbind an object from Lua ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_unbind_resource",
                    "returnType": 2
                },
                {
                    "id": "6a15b669-e7ff-42a7-b7c9-79a83346c399",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_set_global",
                    "help": "set a value with `_name` in the Lua Globals table [global namespace] (_val)",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_set_global",
                    "returnType": 2
                },
                {
                    "id": "26c52436-48ce-4571-a3d6-db261c06ff96",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_get_global",
                    "help": "get the value of `_name` in the Lua Globals table [global namespace] ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_get_global",
                    "returnType": 2
                },
                {
                    "id": "37f7df23-c5a0-41cd-b2f9-04820f8f61c8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_execute_file",
                    "help": "Load a .Lua file and call `luaRousr_execute_string` on it ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_execute_file",
                    "returnType": 2
                },
                {
                    "id": "66c047fc-48a5-4d8f-bd0d-62bf9077da92",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_execute_string",
                    "help": "(_script)",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_execute_string",
                    "returnType": 2
                },
                {
                    "id": "a797cded-199f-439e-904b-480808ce00bb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_call",
                    "help": "Call a Lua function with the given name, that simple!      **NOTE: Returns are not functioning in the current version! No returns as of yet.**      [luaRousr_retain_callback][#luaRousr_retain_callback]      [luaRousr_release_callback][#luaRousr_release_callback] ([_args...])",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_call",
                    "returnType": 2
                },
                {
                    "id": "26be5581-9fe1-41c0-9a69-894a29eb2bc9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_retain_callback",
                    "help": "Increments the reference count for a callbackId. As long as the reference count is above 0, the callback is kept in memory. (_callback_id)",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_retain_callback",
                    "returnType": 2
                },
                {
                    "id": "0017e978-a64f-42db-8d24-78689e2e7811",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_release_callback",
                    "help": "Decrements the reference count for the callbackId. If it's completely released [refcount = 0], it queues the callbackId for deletion. The next `step` called by `LuaRousr` will release this callback from memory. ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_release_callback",
                    "returnType": 2
                },
                {
                    "id": "36bdee65-f98c-4cf8-a640-de7ad6bd8388",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "luaRousr_kill_threads",
                    "help": "Kill all threads created in Lua script using the `thread[function]` Lua function. ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "luaRousr_kill_threads",
                    "returnType": 2
                },
                {
                    "id": "9e03c526-a5bc-419f-ba90-799c085c2fcb",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_event_create",
                    "help": "Create event for LuaRousr Object ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_event_create",
                    "returnType": 2
                },
                {
                    "id": "6bb82c09-ff11-482e-be75-e443c14d691e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_event_async_social",
                    "help": "Async Social Event for LuaRousr object ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_event_async_social",
                    "returnType": 2
                },
                {
                    "id": "b375059d-f7d6-4191-8759-aae217941f7a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_event_clean_up",
                    "help": "Clean Up event for LuaRousr Object ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_event_clean_up",
                    "returnType": 2
                },
                {
                    "id": "80732ff7-4d5f-47d2-a596-c435c15d3660",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_bind_resource_lua",
                    "help": "return a registered resource to Lua ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_bind_resource_lua",
                    "returnType": 2
                },
                {
                    "id": "355941d8-1677-4f84-a958-8e17039d4a5a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_pool_alloc_bind",
                    "help": "allocate a new array for binds ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_pool_alloc_bind",
                    "returnType": 2
                },
                {
                    "id": "d7791c69-62e2-419c-bcb3-eb16494df829",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_bbox_right",
                    "help": "builtin getter for bbox_right ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_bbox_right",
                    "returnType": 2
                },
                {
                    "id": "a7b1312b-ff8d-497c-8412-2333f841d164",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_bbox_top",
                    "help": "builtin getter for bbox_top ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_bbox_top",
                    "returnType": 2
                },
                {
                    "id": "1a7cb552-e63f-4471-b1d3-42578e610024",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_depth",
                    "help": "builtin getter for depth ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_depth",
                    "returnType": 2
                },
                {
                    "id": "beab10fc-a760-4bab-a68e-da88a10ecfec",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_direction",
                    "help": "builtin getter for direction ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_direction",
                    "returnType": 2
                },
                {
                    "id": "9a556e24-f5f5-4cdd-904c-5cf7f30b1e5d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_friction",
                    "help": "builtin getter for friction ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_friction",
                    "returnType": 2
                },
                {
                    "id": "67a47393-bc39-4f1b-a87d-781ce29169e3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_gravity",
                    "help": "builtin getter for gravity ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_gravity",
                    "returnType": 2
                },
                {
                    "id": "4a3b90f7-cfa4-4683-9375-118371854583",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_gravity_direction",
                    "help": "builtin getter for gravity_direction ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_gravity_direction",
                    "returnType": 2
                },
                {
                    "id": "9a6eb1fd-dd27-4e04-970c-5e8aa79afb29",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_hspeed",
                    "help": "builtin getter for hspeed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_hspeed",
                    "returnType": 2
                },
                {
                    "id": "a84b2f2e-9fde-4694-a0e5-941bf84664e5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_id",
                    "help": "builtin getter for id ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_id",
                    "returnType": 2
                },
                {
                    "id": "e62b2b82-1932-4d72-a465-83e8f016e3b6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_image_alpha",
                    "help": "builtin getter for image_alpha ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_image_alpha",
                    "returnType": 2
                },
                {
                    "id": "d268624f-18c4-4a4a-a958-a5112154ccbc",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_image_angle",
                    "help": "builtin getter for image_angle ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_image_angle",
                    "returnType": 2
                },
                {
                    "id": "a4e432d9-1c11-4a0b-9a8d-eebd12eeeb14",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_image_blend",
                    "help": "builtin getter for image_blend ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_image_blend",
                    "returnType": 2
                },
                {
                    "id": "3fbaa797-e540-4989-b02d-85ed91b1f16e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_image_index",
                    "help": "builtin getter for image_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_image_index",
                    "returnType": 2
                },
                {
                    "id": "4bf4adea-510b-48a7-bf5d-b217feacb353",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_image_number",
                    "help": "builtin getter for image_number ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_image_number",
                    "returnType": 2
                },
                {
                    "id": "149ac09f-3698-4607-a673-81a21c9355c2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_image_speed",
                    "help": "builtin getter for image_speed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_image_speed",
                    "returnType": 2
                },
                {
                    "id": "d126dea4-1981-46ca-b362-a8c768ade370",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_image_xscale",
                    "help": "builtin getter for image_xscale ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_image_xscale",
                    "returnType": 2
                },
                {
                    "id": "5b112fa0-f9a7-4d9d-8c69-ed9d334131d9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_image_yscale",
                    "help": "builtin getter for image_yscale ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_image_yscale",
                    "returnType": 2
                },
                {
                    "id": "7ffa6949-9711-4627-88d6-63e9e11fc926",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_layer",
                    "help": "builtin getter for layer ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_layer",
                    "returnType": 2
                },
                {
                    "id": "535a8f64-f4ee-45d9-8e75-db73d33afeb5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_mask_index",
                    "help": "builtin getter for mask_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_mask_index",
                    "returnType": 2
                },
                {
                    "id": "0ae1fb75-f999-45c0-9b07-f3069c56bf98",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_object_index",
                    "help": "builtin getter for object_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_object_index",
                    "returnType": 2
                },
                {
                    "id": "cc3740ca-9081-4e57-a1cf-9878062497c5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_path_endaction",
                    "help": "builtin getter for path_endaction ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_path_endaction",
                    "returnType": 2
                },
                {
                    "id": "3907b211-8a38-4113-a0bf-3b85eda61f5c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_path_index",
                    "help": "builtin getter for path_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_path_index",
                    "returnType": 2
                },
                {
                    "id": "bd6ce7a3-e342-4c63-9bef-e62cf42278df",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_path_orientation",
                    "help": "builtin getter for path_orientation ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_path_orientation",
                    "returnType": 2
                },
                {
                    "id": "5047fdf8-7ddd-4f84-9f02-0fa94a772851",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_path_position",
                    "help": "builtin getter for path_position ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_path_position",
                    "returnType": 2
                },
                {
                    "id": "196e140e-2aee-4126-a079-31f41950fd0e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_path_positionprevious",
                    "help": "builtin getter for path_positionprevious ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_path_positionprevious",
                    "returnType": 2
                },
                {
                    "id": "4c5b21fb-194b-4c37-a693-b3d0bea72ad2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_path_scale",
                    "help": "builtin getter for path_scale ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_path_scale",
                    "returnType": 2
                },
                {
                    "id": "c76101c9-6655-49ef-90c3-7639636315a4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_path_speed",
                    "help": "builtin getter for path_speed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_path_speed",
                    "returnType": 2
                },
                {
                    "id": "4392c653-cd05-4479-bcf8-caf7fc2e5627",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_persistent",
                    "help": "builtin getter for persistent ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_persistent",
                    "returnType": 2
                },
                {
                    "id": "69dc646a-b3a7-4aa6-9834-290ed633b12c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_solid",
                    "help": "builtin getter for solid ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_solid",
                    "returnType": 2
                },
                {
                    "id": "4ac48886-5c44-4c4e-964f-49e9974f7c76",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_speed",
                    "help": "builtin getter for speed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_speed",
                    "returnType": 2
                },
                {
                    "id": "e18b6282-c97e-4a54-a962-b6627612ea38",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_sprite_height",
                    "help": "builtin getter for sprite_height ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_sprite_height",
                    "returnType": 2
                },
                {
                    "id": "75e5ed20-9e8b-41d9-8421-8836cc85e90d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_sprite_index",
                    "help": "builtin getter for sprite_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_sprite_index",
                    "returnType": 2
                },
                {
                    "id": "cc914ac9-c1b8-40e1-9791-0bb992a91553",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_sprite_width",
                    "help": "builtin getter for sprite_width ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_sprite_width",
                    "returnType": 2
                },
                {
                    "id": "6a2618c0-210e-4577-a469-a49554351e88",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_sprite_xoffset",
                    "help": "builtin getter for sprite_xoffset ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_sprite_xoffset",
                    "returnType": 2
                },
                {
                    "id": "cbeae14f-cddf-48ca-a080-b7f8d1b9c666",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_sprite_yoffset",
                    "help": "builtin getter for sprite_yoffset ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_sprite_yoffset",
                    "returnType": 2
                },
                {
                    "id": "07748e3b-d8cf-4886-a5d4-fd056b2a35ef",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_timeline_index",
                    "help": "builtin getter for timeline_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_timeline_index",
                    "returnType": 2
                },
                {
                    "id": "7fc33aee-1ca8-4155-99bd-2d88f8157433",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_timeline_loop",
                    "help": "builtin getter for timeline_loop ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_timeline_loop",
                    "returnType": 2
                },
                {
                    "id": "2091a180-c3b2-4db9-8184-ac520c0f71c6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_timeline_position",
                    "help": "builtin getter for timeline_position ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_timeline_position",
                    "returnType": 2
                },
                {
                    "id": "e1010fd0-552d-4266-9865-49e531971fca",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_timeline_running",
                    "help": "builtin getter for timeline_running ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_timeline_running",
                    "returnType": 2
                },
                {
                    "id": "4449db9d-5e98-477e-b24b-0fae56229729",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_timeline_speed",
                    "help": "builtin getter for timeline_speed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_timeline_speed",
                    "returnType": 2
                },
                {
                    "id": "03b19e1c-e0e1-4096-9003-13751291391d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_visible",
                    "help": "builtin getter for visible ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_visible",
                    "returnType": 2
                },
                {
                    "id": "d2cc5708-6cc8-4db5-b60b-af9a480b8cd2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_vspeed",
                    "help": "builtin getter for vspeed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_vspeed",
                    "returnType": 2
                },
                {
                    "id": "45238352-ac08-431e-888d-386e9bd42ae9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_x",
                    "help": "builtin getter for x ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_x",
                    "returnType": 2
                },
                {
                    "id": "f1004367-fa7f-4c25-bcdb-a7cc2ab82d5b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_xprevious",
                    "help": "builtin getter for xprevious ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_xprevious",
                    "returnType": 2
                },
                {
                    "id": "4ae03ee7-cd41-4a22-9f8e-e8d3fa5e8401",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_xstart",
                    "help": "builtin getter for xstart ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_xstart",
                    "returnType": 2
                },
                {
                    "id": "a9b9e493-f4e8-4fe1-a925-86d63816ce7a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_y",
                    "help": "builtin getter for y ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_y",
                    "returnType": 2
                },
                {
                    "id": "7abda83d-eb69-4d65-9a89-abfa6eeb0874",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_yprevious",
                    "help": "builtin getter for yprevious ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_yprevious",
                    "returnType": 2
                },
                {
                    "id": "db739753-2617-4566-8ed0-0bfd43dfd2bd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_ystart",
                    "help": "builtin getter for ystart ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_ystart",
                    "returnType": 2
                },
                {
                    "id": "72e73b4b-32ab-49c0-99c7-ba069fb81af8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_alarm",
                    "help": "builtin setter for alarm ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_alarm",
                    "returnType": 2
                },
                {
                    "id": "9c8dab8c-1f80-4c39-a841-6e99539af19f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_bbox_bottom",
                    "help": "builtin setter for bbox_bottom ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_bbox_bottom",
                    "returnType": 2
                },
                {
                    "id": "645431a0-70a2-448d-9526-e6dee6f1f0e5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_bbox_left",
                    "help": "builtin setter for bbox_left ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_bbox_left",
                    "returnType": 2
                },
                {
                    "id": "9e9de154-4a2f-44d9-bafe-7d09a02b7eb8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_bbox_right",
                    "help": "builtin setter for bbox_right ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_bbox_right",
                    "returnType": 2
                },
                {
                    "id": "575941d6-cb86-4d0f-a128-5796247d0557",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_bbox_top",
                    "help": "builtin setter for bbox_top ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_bbox_top",
                    "returnType": 2
                },
                {
                    "id": "e0db94ea-ef44-495f-a664-1788d095f42f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_depth",
                    "help": "builtin setter for depth ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_depth",
                    "returnType": 2
                },
                {
                    "id": "5cacbeed-ee69-418a-98fb-03dee2c4e2b7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_direction",
                    "help": "builtin setter for direction ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_direction",
                    "returnType": 2
                },
                {
                    "id": "7ab3d04a-794c-4d1b-bd78-ec59dc170767",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_friction",
                    "help": "builtin setter for friction ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_friction",
                    "returnType": 2
                },
                {
                    "id": "6e0ec078-76ad-4bd7-960c-26f35ad75fa6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_gravity",
                    "help": "builtin setter for gravity ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_gravity",
                    "returnType": 2
                },
                {
                    "id": "bc5a357b-8b91-486d-9d13-aadb61d62e51",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_gravity_direction",
                    "help": "builtin setter for gravity_direction ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_gravity_direction",
                    "returnType": 2
                },
                {
                    "id": "aafdcaf3-1f7f-4737-aa12-c74f36edae42",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_hspeed",
                    "help": "builtin setter for hspeed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_hspeed",
                    "returnType": 2
                },
                {
                    "id": "3a9b9ed4-cca9-42a7-884e-b3a7d09269a0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_id",
                    "help": "builtin setter for id ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_id",
                    "returnType": 2
                },
                {
                    "id": "2cae1244-1d27-444f-9a71-4f1763276d1f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_image_alpha",
                    "help": "builtin setter for image_alpha ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_image_alpha",
                    "returnType": 2
                },
                {
                    "id": "f6ec036b-43b2-496f-b4a2-b3ddbeb71d99",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_image_angle",
                    "help": "builtin setter for image_angle ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_image_angle",
                    "returnType": 2
                },
                {
                    "id": "93b20485-dda7-4a7c-8cb2-cf60ce02eb60",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_image_blend",
                    "help": "builtin setter for image_blend ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_image_blend",
                    "returnType": 2
                },
                {
                    "id": "da312869-0268-4b0a-8514-748e29a4a843",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_image_index",
                    "help": "builtin setter for image_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_image_index",
                    "returnType": 2
                },
                {
                    "id": "b7812409-14e1-4d42-8f78-ef6b8e9a00a7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_image_number",
                    "help": "builtin setter for image_number ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_image_number",
                    "returnType": 2
                },
                {
                    "id": "fdf0c2b8-a108-4f79-86d3-87bfc044658d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_image_speed",
                    "help": "builtin setter for image_speed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_image_speed",
                    "returnType": 2
                },
                {
                    "id": "e41a7358-249e-45cd-90d3-abb08ab979a2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_image_xscale",
                    "help": "builtin setter for image_xscale ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_image_xscale",
                    "returnType": 2
                },
                {
                    "id": "558a8c74-76e1-4321-9dd3-6678fc98c0aa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_image_yscale",
                    "help": "builtin setter for image_yscale ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_image_yscale",
                    "returnType": 2
                },
                {
                    "id": "cdcae05f-4367-4aab-8ded-62b5457a6939",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_layer",
                    "help": "builtin setter for layer ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_layer",
                    "returnType": 2
                },
                {
                    "id": "fc5edda4-dde6-4326-974a-9df7f1a0e434",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_mask_index",
                    "help": "builtin setter for mask_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_mask_index",
                    "returnType": 2
                },
                {
                    "id": "e04149e9-6456-4f8e-b5bb-12c2cc6581cd",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_object_index",
                    "help": "builtin setter for object_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_object_index",
                    "returnType": 2
                },
                {
                    "id": "9b845760-ea53-4815-9aa1-7d85086d0383",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_path_endaction",
                    "help": "builtin setter for path_endaction ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_path_endaction",
                    "returnType": 2
                },
                {
                    "id": "0891466b-3c3a-49b0-a90a-9f4e4f6e4b31",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_path_index",
                    "help": "builtin setter for path_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_path_index",
                    "returnType": 2
                },
                {
                    "id": "1494e4e5-af32-4a4e-8a35-c8d9f23e0606",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_path_orientation",
                    "help": "builtin setter for path_orientation ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_path_orientation",
                    "returnType": 2
                },
                {
                    "id": "f6909b0f-5490-4be1-916a-2c4ec5f2563a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_path_position",
                    "help": "builtin setter for path_position ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_path_position",
                    "returnType": 2
                },
                {
                    "id": "e65a6b09-4ef7-45f9-a663-6cc2c405d435",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_path_positionprevious",
                    "help": "builtin setter for path_positionprevious ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_path_positionprevious",
                    "returnType": 2
                },
                {
                    "id": "624927ea-d1a7-4a91-b6e8-f35dcd97677b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_path_scale",
                    "help": "builtin setter for path_scale ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_path_scale",
                    "returnType": 2
                },
                {
                    "id": "ab90036e-3df2-4eb7-9d86-0cccad886d8b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_path_speed",
                    "help": "builtin setter for path_speed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_path_speed",
                    "returnType": 2
                },
                {
                    "id": "6443f954-b118-4526-bb0d-61eae1f20453",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_persistent",
                    "help": "builtin setter for persistent ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_persistent",
                    "returnType": 2
                },
                {
                    "id": "9f1534a4-adcf-4e81-b7f2-d10fb56aeb74",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_solid",
                    "help": "builtin setter for solid ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_solid",
                    "returnType": 2
                },
                {
                    "id": "7e3bb92c-9d52-4a52-997f-decd32b3d7c2",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_speed",
                    "help": "builtin setter for speed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_speed",
                    "returnType": 2
                },
                {
                    "id": "e25d9a32-d44e-41fc-8cb0-84cc006ab95d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_sprite_height",
                    "help": "builtin setter for sprite_height ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_sprite_height",
                    "returnType": 2
                },
                {
                    "id": "1851cef6-e294-41db-92af-9909efe21030",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_sprite_index",
                    "help": "builtin setter for sprite_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_sprite_index",
                    "returnType": 2
                },
                {
                    "id": "bfb690f3-b61c-49ca-ba2f-108d2de0734f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_sprite_width",
                    "help": "builtin setter for sprite_width ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_sprite_width",
                    "returnType": 2
                },
                {
                    "id": "1038e3f3-e448-40f9-b857-84c2c6ff08ac",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_sprite_xoffset",
                    "help": "builtin setter for sprite_xoffset ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_sprite_xoffset",
                    "returnType": 2
                },
                {
                    "id": "e8f54e92-c403-4ac1-b21d-693d86892273",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_sprite_yoffset",
                    "help": "builtin setter for sprite_yoffset ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_sprite_yoffset",
                    "returnType": 2
                },
                {
                    "id": "fd232d6a-408d-4a49-8717-8ee1bf850a7d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_timeline_index",
                    "help": "builtin setter for timeline_index ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_timeline_index",
                    "returnType": 2
                },
                {
                    "id": "f4961881-799e-4795-922f-e106e1219ec5",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_timeline_loop",
                    "help": "builtin setter for timeline_loop ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_timeline_loop",
                    "returnType": 2
                },
                {
                    "id": "2e54dd76-e1ca-4664-b48f-4d39d2768996",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_timeline_position",
                    "help": "builtin setter for timeline_position ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_timeline_position",
                    "returnType": 2
                },
                {
                    "id": "8ae1e7ca-f3eb-469f-8f7f-1e54db16ba78",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_timeline_running",
                    "help": "builtin setter for timeline_running ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_timeline_running",
                    "returnType": 2
                },
                {
                    "id": "2c7c44c0-ab4e-43c4-9804-f2c6079b9467",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_timeline_speed",
                    "help": "builtin setter for timeline_speed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_timeline_speed",
                    "returnType": 2
                },
                {
                    "id": "0596d745-026c-43fc-8b8a-4041385ea1d0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_visible",
                    "help": "builtin setter for visible ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_visible",
                    "returnType": 2
                },
                {
                    "id": "3eaee61c-de85-4418-a322-9291e543a272",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_vspeed",
                    "help": "builtin setter for vspeed ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_vspeed",
                    "returnType": 2
                },
                {
                    "id": "8b862c0e-0842-43dd-8b03-6c4a7cff7084",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_x",
                    "help": "builtin setter for x ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_x",
                    "returnType": 2
                },
                {
                    "id": "981ddd6d-669b-4e25-91aa-eec3037943f6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_xprevious",
                    "help": "builtin setter for xprevious ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_xprevious",
                    "returnType": 2
                },
                {
                    "id": "d8537f3a-5b65-4405-b802-82637b16953f",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_xstart",
                    "help": "builtin setter for xstart ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_xstart",
                    "returnType": 2
                },
                {
                    "id": "454d47c6-0870-4d1e-8596-b28939c8b391",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_y",
                    "help": "builtin setter for y ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_y",
                    "returnType": 2
                },
                {
                    "id": "52349503-fb54-4043-b71c-6cbfbabc98e6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_yprevious",
                    "help": "builtin setter for yprevious ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_yprevious",
                    "returnType": 2
                },
                {
                    "id": "c3df2192-bcbc-4ab7-b31e-d733d4a0ada8",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_set_ystart",
                    "help": "builtin setter for ystart ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_set_ystart",
                    "returnType": 2
                },
                {
                    "id": "4c4ac7c4-0611-49f8-8909-753a9830466c",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_alarm",
                    "help": "builtin getter for alarm ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_alarm",
                    "returnType": 2
                },
                {
                    "id": "5a3a2e26-93f7-41c7-b263-73ca9eaa5be6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_bbox_bottom",
                    "help": "builtin getter for bbox_bottom ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_bbox_bottom",
                    "returnType": 2
                },
                {
                    "id": "a28edec1-f5f2-43be-84bd-8befe424115e",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_instance_builtin_get_bbox_left",
                    "help": "builtin getter for bbox_left ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_instance_builtin_get_bbox_left",
                    "returnType": 2
                },
                {
                    "id": "1c9d9b47-0cd3-40a7-bb87-6a79ea233a70",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_check_init",
                    "help": "return true if we're ready to roll, warn if not initialized ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_check_init",
                    "returnType": 2
                },
                {
                    "id": "50c885c2-6ec7-4d17-8e8d-b751fe78b02b",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_executeGML",
                    "help": "Called from Lua, execute a GML function based passing the arguments from Lua, as necessary, and return value back. (_script_index, _args, _argCount)",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_executeGML",
                    "returnType": 2
                },
                {
                    "id": "ca82fba8-27c6-4c28-9a12-21bae87511b3",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_process_buffer",
                    "help": "(_cmd_buffer, _cmd_id)",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_process_buffer",
                    "returnType": 2
                },
                {
                    "id": "7a72faf9-ea78-4323-9cf4-8b364387dad6",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_reset_buffer",
                    "help": "__luaRousr_reset_buffer - signal the DLL to write more data (_cmdId)",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_reset_buffer",
                    "returnType": 2
                },
                {
                    "id": "9146b4d3-5527-4c54-a313-0a4c134f9160",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_push_buffer",
                    "help": "__luaRousr_push_buffer - push a buffer back onto LuaSr's buffer stack ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_push_buffer",
                    "returnType": 2
                },
                {
                    "id": "e54d9f23-9a2b-4b2f-9a56-2346ec4844fa",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_pop_buffer",
                    "help": "pop a buffer from the buffer stack, put its size back in it. ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_pop_buffer",
                    "returnType": 2
                },
                {
                    "id": "6a00d0ac-4410-4a05-b36a-731885f4a362",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_bind_member",
                    "help": "wrapper to bind members to a binding object (_member_name, _default, _read_only, _getter, _setter, [_forceType])",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_bind_member",
                    "returnType": 2
                },
                {
                    "id": "8400231c-6545-4f4c-8986-d292eeec6ea4",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_read_instance",
                    "help": "internal function to read an instance's values from a sync (_contextValues, _dirtyIndices, _dirtyCount)",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_read_instance",
                    "returnType": 2
                },
                {
                    "id": "316499a6-f274-4d78-934c-0708acd2b26a",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_write_instance",
                    "help": "sync the instance (_force)",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_write_instance",
                    "returnType": 2
                },
                {
                    "id": "2d92b5a1-aeff-455f-bc69-25a1ff089eff",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_write_val",
                    "help": "__luaRousr_write_val - compare a value to a context value array's value. (_contextIndex, _val, _dirtyValues, _dirtyIndex, _force)",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_write_val",
                    "returnType": 2
                },
                {
                    "id": "3b59afeb-d8e3-406f-b094-6dbafdf53104",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_sync_write",
                    "help": "write all pending payloads to the DLL buffer, and clear the payload list ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_sync_write",
                    "returnType": 2
                },
                {
                    "id": "2d3fca6f-6425-4c4b-bcd5-766914ca1231",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_sync_read",
                    "help": "check and process any updated values from the dll ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_sync_read",
                    "returnType": 2
                },
                {
                    "id": "b4c1160a-5221-4140-a97c-bc054cb76271",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__luaRousr_debug_print",
                    "help": "print a message from the Lua debug call ()",
                    "hidden": false,
                    "kind": 1,
                    "name": "__luaRousr_debug_print",
                    "returnType": 2
                },
                {
                    "id": "36a57623-91a6-438a-988e-0a5385706643",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "__extluarousr_script_index",
                    "help": "Returns the actual runtime script index because YYG doesn't know how to do that apparently (ext_script_index)",
                    "hidden": false,
                    "kind": 1,
                    "name": "__extluarousr_script_index",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 2,
            "order": [
                "c84b27ba-8331-4543-8847-357261152519",
                "68d10a56-2c77-4f4e-bd9f-d1ffb53e77b4",
                "813e58c7-fd1c-43b1-a22f-f97470b03bcc",
                "74809d3c-4133-42c2-acf7-2f8133d76388",
                "c3b95516-3204-474c-bf86-27577042426e",
                "8f983efc-8354-4164-b011-0292d3fa134b",
                "a9e848ed-cec2-4f78-ae8c-4879bf0fe2fb",
                "e6e30344-9d99-4130-9f45-d25a4785b289",
                "ba6f5703-f269-436e-b71f-523aac6a0c51",
                "f71b5a0f-00b2-4a38-afeb-a20be5d84690",
                "8c4634d0-8c62-4908-b122-149f3470adf4",
                "9ce1454d-0019-4b78-94de-ea3021aa439a",
                "6a15b669-e7ff-42a7-b7c9-79a83346c399",
                "26c52436-48ce-4571-a3d6-db261c06ff96",
                "37f7df23-c5a0-41cd-b2f9-04820f8f61c8",
                "66c047fc-48a5-4d8f-bd0d-62bf9077da92",
                "a797cded-199f-439e-904b-480808ce00bb",
                "26be5581-9fe1-41c0-9a69-894a29eb2bc9",
                "0017e978-a64f-42db-8d24-78689e2e7811",
                "36bdee65-f98c-4cf8-a640-de7ad6bd8388",
                "9e03c526-a5bc-419f-ba90-799c085c2fcb",
                "6bb82c09-ff11-482e-be75-e443c14d691e",
                "b375059d-f7d6-4191-8759-aae217941f7a",
                "80732ff7-4d5f-47d2-a596-c435c15d3660",
                "355941d8-1677-4f84-a958-8e17039d4a5a",
                "d7791c69-62e2-419c-bcb3-eb16494df829",
                "a7b1312b-ff8d-497c-8412-2333f841d164",
                "1a7cb552-e63f-4471-b1d3-42578e610024",
                "beab10fc-a760-4bab-a68e-da88a10ecfec",
                "9a556e24-f5f5-4cdd-904c-5cf7f30b1e5d",
                "67a47393-bc39-4f1b-a87d-781ce29169e3",
                "4a3b90f7-cfa4-4683-9375-118371854583",
                "9a6eb1fd-dd27-4e04-970c-5e8aa79afb29",
                "a84b2f2e-9fde-4694-a0e5-941bf84664e5",
                "e62b2b82-1932-4d72-a465-83e8f016e3b6",
                "d268624f-18c4-4a4a-a958-a5112154ccbc",
                "a4e432d9-1c11-4a0b-9a8d-eebd12eeeb14",
                "3fbaa797-e540-4989-b02d-85ed91b1f16e",
                "4bf4adea-510b-48a7-bf5d-b217feacb353",
                "149ac09f-3698-4607-a673-81a21c9355c2",
                "d126dea4-1981-46ca-b362-a8c768ade370",
                "5b112fa0-f9a7-4d9d-8c69-ed9d334131d9",
                "7ffa6949-9711-4627-88d6-63e9e11fc926",
                "535a8f64-f4ee-45d9-8e75-db73d33afeb5",
                "0ae1fb75-f999-45c0-9b07-f3069c56bf98",
                "cc3740ca-9081-4e57-a1cf-9878062497c5",
                "3907b211-8a38-4113-a0bf-3b85eda61f5c",
                "bd6ce7a3-e342-4c63-9bef-e62cf42278df",
                "5047fdf8-7ddd-4f84-9f02-0fa94a772851",
                "196e140e-2aee-4126-a079-31f41950fd0e",
                "4c5b21fb-194b-4c37-a693-b3d0bea72ad2",
                "c76101c9-6655-49ef-90c3-7639636315a4",
                "4392c653-cd05-4479-bcf8-caf7fc2e5627",
                "69dc646a-b3a7-4aa6-9834-290ed633b12c",
                "4ac48886-5c44-4c4e-964f-49e9974f7c76",
                "e18b6282-c97e-4a54-a962-b6627612ea38",
                "75e5ed20-9e8b-41d9-8421-8836cc85e90d",
                "cc914ac9-c1b8-40e1-9791-0bb992a91553",
                "6a2618c0-210e-4577-a469-a49554351e88",
                "cbeae14f-cddf-48ca-a080-b7f8d1b9c666",
                "07748e3b-d8cf-4886-a5d4-fd056b2a35ef",
                "7fc33aee-1ca8-4155-99bd-2d88f8157433",
                "2091a180-c3b2-4db9-8184-ac520c0f71c6",
                "e1010fd0-552d-4266-9865-49e531971fca",
                "4449db9d-5e98-477e-b24b-0fae56229729",
                "03b19e1c-e0e1-4096-9003-13751291391d",
                "d2cc5708-6cc8-4db5-b60b-af9a480b8cd2",
                "45238352-ac08-431e-888d-386e9bd42ae9",
                "f1004367-fa7f-4c25-bcdb-a7cc2ab82d5b",
                "4ae03ee7-cd41-4a22-9f8e-e8d3fa5e8401",
                "a9b9e493-f4e8-4fe1-a925-86d63816ce7a",
                "7abda83d-eb69-4d65-9a89-abfa6eeb0874",
                "db739753-2617-4566-8ed0-0bfd43dfd2bd",
                "72e73b4b-32ab-49c0-99c7-ba069fb81af8",
                "9c8dab8c-1f80-4c39-a841-6e99539af19f",
                "645431a0-70a2-448d-9526-e6dee6f1f0e5",
                "9e9de154-4a2f-44d9-bafe-7d09a02b7eb8",
                "575941d6-cb86-4d0f-a128-5796247d0557",
                "e0db94ea-ef44-495f-a664-1788d095f42f",
                "5cacbeed-ee69-418a-98fb-03dee2c4e2b7",
                "7ab3d04a-794c-4d1b-bd78-ec59dc170767",
                "6e0ec078-76ad-4bd7-960c-26f35ad75fa6",
                "bc5a357b-8b91-486d-9d13-aadb61d62e51",
                "aafdcaf3-1f7f-4737-aa12-c74f36edae42",
                "3a9b9ed4-cca9-42a7-884e-b3a7d09269a0",
                "2cae1244-1d27-444f-9a71-4f1763276d1f",
                "f6ec036b-43b2-496f-b4a2-b3ddbeb71d99",
                "93b20485-dda7-4a7c-8cb2-cf60ce02eb60",
                "da312869-0268-4b0a-8514-748e29a4a843",
                "b7812409-14e1-4d42-8f78-ef6b8e9a00a7",
                "fdf0c2b8-a108-4f79-86d3-87bfc044658d",
                "e41a7358-249e-45cd-90d3-abb08ab979a2",
                "558a8c74-76e1-4321-9dd3-6678fc98c0aa",
                "cdcae05f-4367-4aab-8ded-62b5457a6939",
                "fc5edda4-dde6-4326-974a-9df7f1a0e434",
                "e04149e9-6456-4f8e-b5bb-12c2cc6581cd",
                "9b845760-ea53-4815-9aa1-7d85086d0383",
                "0891466b-3c3a-49b0-a90a-9f4e4f6e4b31",
                "1494e4e5-af32-4a4e-8a35-c8d9f23e0606",
                "f6909b0f-5490-4be1-916a-2c4ec5f2563a",
                "e65a6b09-4ef7-45f9-a663-6cc2c405d435",
                "624927ea-d1a7-4a91-b6e8-f35dcd97677b",
                "ab90036e-3df2-4eb7-9d86-0cccad886d8b",
                "6443f954-b118-4526-bb0d-61eae1f20453",
                "9f1534a4-adcf-4e81-b7f2-d10fb56aeb74",
                "7e3bb92c-9d52-4a52-997f-decd32b3d7c2",
                "e25d9a32-d44e-41fc-8cb0-84cc006ab95d",
                "1851cef6-e294-41db-92af-9909efe21030",
                "bfb690f3-b61c-49ca-ba2f-108d2de0734f",
                "1038e3f3-e448-40f9-b857-84c2c6ff08ac",
                "e8f54e92-c403-4ac1-b21d-693d86892273",
                "fd232d6a-408d-4a49-8717-8ee1bf850a7d",
                "f4961881-799e-4795-922f-e106e1219ec5",
                "2e54dd76-e1ca-4664-b48f-4d39d2768996",
                "8ae1e7ca-f3eb-469f-8f7f-1e54db16ba78",
                "2c7c44c0-ab4e-43c4-9804-f2c6079b9467",
                "0596d745-026c-43fc-8b8a-4041385ea1d0",
                "3eaee61c-de85-4418-a322-9291e543a272",
                "8b862c0e-0842-43dd-8b03-6c4a7cff7084",
                "981ddd6d-669b-4e25-91aa-eec3037943f6",
                "d8537f3a-5b65-4405-b802-82637b16953f",
                "454d47c6-0870-4d1e-8596-b28939c8b391",
                "52349503-fb54-4043-b71c-6cbfbabc98e6",
                "c3df2192-bcbc-4ab7-b31e-d733d4a0ada8",
                "4c4ac7c4-0611-49f8-8909-753a9830466c",
                "5a3a2e26-93f7-41c7-b263-73ca9eaa5be6",
                "a28edec1-f5f2-43be-84bd-8befe424115e",
                "1c9d9b47-0cd3-40a7-bb87-6a79ea233a70",
                "50c885c2-6ec7-4d17-8e8d-b751fe78b02b",
                "ca82fba8-27c6-4c28-9a12-21bae87511b3",
                "7a72faf9-ea78-4323-9cf4-8b364387dad6",
                "9146b4d3-5527-4c54-a313-0a4c134f9160",
                "e54d9f23-9a2b-4b2f-9a56-2346ec4844fa",
                "6a00d0ac-4410-4a05-b36a-731885f4a362",
                "8400231c-6545-4f4c-8986-d292eeec6ea4",
                "316499a6-f274-4d78-934c-0708acd2b26a",
                "2d92b5a1-aeff-455f-bc69-25a1ff089eff",
                "3b59afeb-d8e3-406f-b094-6dbafdf53104",
                "2d3fca6f-6425-4c4b-bcd5-766914ca1231",
                "b4c1160a-5221-4140-a97c-bc054cb76271",
                "36a57623-91a6-438a-988e-0a5385706643"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "1.1.1"
}