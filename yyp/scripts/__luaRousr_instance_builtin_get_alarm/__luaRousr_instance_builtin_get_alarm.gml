///@function __luaRousr_instance_builtin_get_alarm()
///@desc builtin getter for alarm
///@returns {*} builtin
///@extensionizer { "docs": false }
return alarm;
