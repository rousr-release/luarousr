///@param t
///@param k0
///@param k1
///@param k2
///@param k3
/// spline4(t,knot1,knot2,knot3,knot4)
//
//  Returns the Catmull-Rom interpolation of the given 
//  knot values at the given parameter position. 
//
//      t           interpolation parameter [0..1], real
//      knot1-4     knot values of spline, real
//
//  This is an optimized version of spline() for the special
//  case of four knots. See spline() for more details.
//
/*
Copyright (c) 2007-2017, GMLscripts.com

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

GMLscripts.com/license
*/
{
    var k0, k3, c3, c2, c1, c0;
    if (argument0 <= 0) return argument2;
    if (argument0 >= 1) return argument3;
    k0 = -0.5 * argument1;
    k3 = 0.5 * argument4;
    c3 = k0 + 1.5 * argument2 - 1.5 * argument3 + k3;
    c2 = argument1 - 2.5 * argument2 + 2 * argument3 - k3;
    c1 = k0 + 0.5 * argument3;
    c0 = argument2;
    return ((c3 * argument0 + c2) * argument0 + c1) * argument0 + c0;
}