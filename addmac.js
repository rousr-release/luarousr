const fs = require('fs-extra');
const uuid = require("uuid/v4");

let extFile = "./yyp/extensions/extLuaRousr/extLuaRousr.yy";

let jsonFile = fs.readFileSync(extFile);
let json = JSON.parse(jsonFile);

extFile = null;

let files = json.files;
for (let file of files) {
    if (file.filename == "rousrlua.dll") {
        extFile = file;
    }
}

if (extFile == null) {
    console.log("Coudln't find the rousrlua.dll");
    return;
}

let macFile = JSON.parse(JSON.stringify(extFile));

macFile.filename = "rousrlua.dylib";
macFile.id = uuid();
macFile.ProxyFiles = new Array();

macFile.order = new Array();
for (let func of     macFile.functions) {
    func.id = uuid();
    macFile.order.push(func.id);
}

files.push(macFile);
fs.writeFile("extLuaRousr.yy", JSON.stringify(json, null, "\t"));