///@function __luaRousr_instance_builtin_get_timeline_speed()
///@desc builtin getter for timeline_speed
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_speed;
