#include "LuaCmd.h"

#include "luaRousr.h"

#include "CmdWorker.h"
#include "GMLCmdLayer.h"
#include "LuaContext.h"

#include "GMLLuaVal.h"

namespace LuaCmd {
    std::atomic<uint32_t> NextID = { 1 };

    std::vector<std::shared_ptr<IGMLLuaVal>> ReadArgs(std::shared_ptr<CGMLBuffer> _argBuffer) {
        std::shared_ptr<IGMLLuaVal> arg(nullptr);
        std::vector<std::shared_ptr<IGMLLuaVal>> args;

        uint32_t argCount(_argBuffer->Read<uint32_t>());
        for (uint32_t argIndex(0); argIndex < argCount; ++argIndex)
            args.push_back(GMLLua::ReadVal(*_argBuffer));

        return args;
    }

    void ResetReturnBuffer(std::shared_ptr<CGMLBuffer> _returnBuffer) {
        _returnBuffer->Seek(LuaRousr::LuaCallNumReturn);
        _returnBuffer->Write<int8_t>(0);
        _returnBuffer->Seek(LuaRousr::SeekBegin);
    }

    uint32_t Execute(CLuaContext& _context, std::shared_ptr<CGMLBuffer> _cmdBuffer, std::function<void(CLuaContext& _context)> _func) {
        uint32_t cmdId(NextID++);
        if (cmdId == 0) cmdId = NextID++; // skip 0, since GML returns 0 for missing Extension functions, I want to avoid _any_ confusion.

        CmdWorker::PushJob([&_context, _cmdBuffer, cmdId, _func]() {
            GMLCmdLayer cmdLayer(_cmdBuffer, cmdId);
            _func(_context);
        });

        return cmdId;
    }
}

extern "C" {
    DLL_API double ExecuteLua(char* _cmdBuffer, const char* _script) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::Instance().MainContext());
        std::shared_ptr<CGMLBuffer> cmdBuffer(std::make_shared<CGMLBuffer>(_cmdBuffer));
        std::string script(_script);

        return static_cast<double>(LuaCmd::Execute(*CLuaRousr::MainContext(), cmdBuffer, [script](CLuaContext& _context) {
            _context.DoString(script);
        }));
    }

    DLL_API double CallLua(char* _cmdBuffer, const char* _luaFunction, double _argBufferLength, const char* _argumentBuffer) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::Instance().MainContext());
        auto cmdBuffer(std::make_shared<CGMLBuffer>(_cmdBuffer));
        auto argBuffer(std::make_shared<CGMLBuffer>(static_cast<size_t>(_argBufferLength), _argumentBuffer));
        
        auto args(LuaCmd::ReadArgs(argBuffer));
        LuaCmd::ResetReturnBuffer(argBuffer);
        std::string functionName(_luaFunction);

        return static_cast<double>(LuaCmd::Execute(*CLuaRousr::MainContext(), cmdBuffer, [functionName, args, argBuffer](CLuaContext& _context) {
            _context.CallLua(functionName, args, argBuffer);
        }));
    }

    DLL_API double CallLuaScriptId(char* _cmdBuffer, double _scriptId, double _argBufferLength, const char* _argumentBuffer) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::Instance().MainContext());
        auto cmdBuffer(std::make_shared<CGMLBuffer>(_cmdBuffer));
        auto argBuffer(std::make_shared<CGMLBuffer>(static_cast<size_t>(_argBufferLength), _argumentBuffer));

        auto args(LuaCmd::ReadArgs(argBuffer));
        LuaCmd::ResetReturnBuffer(argBuffer);

        return static_cast<double>(LuaCmd::Execute(*CLuaRousr::MainContext(), cmdBuffer, [_scriptId, args, argBuffer](CLuaContext& _context) {
            _context.CallLua(static_cast<uint32_t>(_scriptId), args, argBuffer);
        }));
    }

    DLL_API double HasThreads() {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::Instance().MainContext());
        if (mainContext == nullptr || mainContext->EmptyThreads())
            return 0.0;

        return 1.0;
    }

    DLL_API double ResumeThreads(char* _cmdBuffer) {
        if (!CLuaRousr::Initialized())
            return -1.0;

        auto mainContext(CLuaRousr::Instance().MainContext());
        if (mainContext == nullptr || mainContext->EmptyThreads())
            return -1.0;
        
        auto cmdBuffer(std::make_shared<CGMLBuffer>(_cmdBuffer));
        
        return static_cast<double>(LuaCmd::Execute(*CLuaRousr::MainContext(), cmdBuffer, [](CLuaContext& _context) {
            _context.ResumeThreads();
        }));
    }
}