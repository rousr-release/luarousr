///@func __luaRousr_default_custom_setter(_val, [_variable_name], [_id_or_index])
///@desc called `with(instance)`
///@param {*}                _val   value to assign
///@param {String} _variable_name   name given to variable
///@param {Real}           _index   index assigned the variable
///@notes optionally take the name, or index, as helper ways to identify a variable - _val is first and required to be used
///       or just use one script per variable

/// by default, variable_instance_get
var _val = argument[0];
var _name = argument[1];
var _index = argument[2];

return variable_instance_set(id, _name, _val);