if(!fade){
	if(alpha < 1){
		alpha+=0.02/2;
	}else{
		alpha = 1;
		timer+=0.02;
	
		if(sin(timer) < 0 ){
			timer = 0;
		}
	
		if(keyboard_check_pressed(vk_space)){
			fade = true;
			luaRousr_call("gameStart");
		}

	}
}else{
	end_alpha -= 0.02;
	obj_game.image_alpha+=0.02;
	if(end_alpha < 0 && obj_game.image_alpha > 0){
		instance_destroy();
		
	}
}


if(timer mod 0.5 = 0){
	grid_apply_directed_force(-1,-1,0,0,256*256);
	grid_apply_directed_force(-1,-1,room_width,room_height,256*256);
}
