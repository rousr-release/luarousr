///@func __luaRousr_bind_member(_bindIndex, _member_name, _default, _read_only, [_forceType])
///@desc wrapper to bind members to a binding object
///@param {Real}        _bindIndex   - numeric index of the bind to refer to it by in the future.
///@param {String}      _member_name  - name of this member to refer to it by in Lua i.e., `instance._member_name`
///@param {Real|String} _default  - default value of this member, if _forceType is undefined (not passed), the type written is based on this value.
///@param {Boolean}     _read_only    - can you write to this value?
///@param {Real}        _getter        - script_index of getter function
///@param {Real}        _setter        - script_idnex of setter function
///@param {Real|String} [_forceType] - ERousrData or string representing type to optionally force use for this value
///@returns {Boolean} true on success
///@extensionizer { "docs": false }
var _bindIndex  = argument[0];
var _member_name = argument[1];
var _default = argument[2];
var _read_only   = argument[3];
var _getter     = argument[4];
var _setter     = argument[5];
var _forceType  = argument[6];

if (_forceType == undefined) {
	if      (is_bool(_default))    _forceType = ERousrData.Bool;
	else if (is_real(_default))  	_forceType = ERousrData.Double;
	else if (is_string(_default))	_forceType = ERousrData.String;
}

if (is_string(_forceType)) {
  switch (_forceType) {
    case "real":   _forceType = ERousrData.Double; break;
    case "string": _forceType = ERousrData.String; break;
    case "bool":   _forceType = ERousrData.Bool; break;
  }
}


buffer_seek(Argument_buffer, buffer_seek_start, 4);
buffer_write(Argument_buffer, buffer_u32, _bindIndex);
buffer_write(Argument_buffer, buffer_string, _member_name);
sr_buffer_write_val(Argument_buffer, _default, _forceType);
buffer_write(Argument_buffer, buffer_bool, _read_only);

var success = __extLuaRousrBindMember(buffer_get_address(Argument_buffer)) >= 0;
if (!success)
  show_debug_message("luaRousr - warning: can't bind member '" + string(_member_name) + "' invalid type passed for default value (can only be bool/real/string).");
else {
  with (LuaSr) {
    var boundVals = GMLBindings[__ELuaRousrGMLBindData.BoundVariables];
    sr_array_insert(boundVals, _bindIndex, [ _member_name, _default, _read_only, _getter, _setter ]);
  }
}

return success;