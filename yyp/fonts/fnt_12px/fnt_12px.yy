{
    "id": "05715f04-76bf-4c0c-a9b6-6a18c40d2aad",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fnt_12px",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "04b03",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b24c0b96-050b-4dbc-a0e7-3a1f7615ea1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d40c744e-26ee-43c1-b460-9ab22d288225",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 78,
                "y": 56
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "72ace20d-ff72-41d7-8bad-22c9b88873e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 132,
                "y": 38
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "4545de96-7240-4616-afdb-c8f1fb452bf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "3500b789-90b5-42cd-bb3e-5340e317ed5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 38
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "7702925f-9a6f-4fde-9434-6338197aad58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "b443a912-0f25-4985-8848-4b207e249655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c7f4f3f8-d259-4ab9-8a8b-d7274aa41192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 70,
                "y": 56
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c4ce1b2f-f3fa-41ff-a54b-9269604ad238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 42,
                "y": 56
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "eed33683-74d4-4392-9647-9da14a2aac67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 36,
                "y": 56
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "32ea6ae8-db28-43da-a3e7-ec681d86174c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 92,
                "y": 38
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0fdddbf2-a3bf-4887-8e73-29c697745379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 156,
                "y": 38
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c8e1673b-7c30-408b-b8e4-fbeb7d13d825",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 60,
                "y": 56
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "9dd8f9b0-17b7-4860-98ce-8ba6bb317464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 100,
                "y": 38
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d121a498-90c1-400b-8ed3-bb1b7f59505c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 94,
                "y": 56
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b9163df0-3391-4ffe-b8f0-f5daff5eb0e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "25db79aa-8170-401d-b34b-3bd15dce887d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 38
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "8204765f-130a-468d-9e63-8dc3a244926f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 30,
                "y": 56
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "23e8e4a5-a9ba-4720-85f0-b29164d565dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 38
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "8f578b80-8d60-4b0c-abaf-7ca4a0afaebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 232,
                "y": 20
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c97a5cbb-5483-4b4a-b1c3-fe08865aec54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 212,
                "y": 20
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "700107e3-40d4-45ae-b6b9-ad5c03ab3bc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 38
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "32616040-bb05-4e05-8ad9-8bbdfb96ebaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 38
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "2854e9c4-753b-4993-b9d8-62e6cec137cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 192,
                "y": 20
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "f6dcb037-7fc9-494f-8aff-8f1f4b346619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 222,
                "y": 20
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1aaa6e19-6339-459b-a144-e61106c3f449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 242,
                "y": 20
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "866faf81-0c98-4bf1-98c4-3337f6586401",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 74,
                "y": 56
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "65f0b239-f5e2-40e7-b5fa-c6bd15c55b39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 82,
                "y": 56
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f96d6d7c-ce6d-431f-b7c2-0b15c9a3f3b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 236,
                "y": 38
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "acb03833-a54c-4de8-a02b-481c13eb4957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 244,
                "y": 38
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0207c010-12c8-4d23-870b-2b71f500de3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 220,
                "y": 38
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "303fccff-22bc-465f-b1e6-8ad6cc7464f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 38
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b4317844-7696-43b3-84e6-243c152cf248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6da3eb05-afa0-40b1-9169-01a84a93d6bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 38
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "81e5d95f-98a6-43b0-83cd-1ddc87915982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 62,
                "y": 38
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c3a94248-8956-45c8-bea9-be93da7fc7b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 204,
                "y": 38
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8c537b79-f928-4b07-a038-2a1a135f2266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 38
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1598380f-a7df-48b2-89c7-2c7c1debc9c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 188,
                "y": 38
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c9dffed6-d868-439a-becd-a170ad3d9edb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 180,
                "y": 38
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "295e73db-170b-42c5-b7ca-e35ce102c70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 202,
                "y": 20
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "691d73e1-bb21-4a2c-97c2-9b817b1f21d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 162,
                "y": 20
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "d8a6600f-63aa-41bb-9c43-d57a6904b343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b3b716d2-bce8-48ae-877c-a044dd4b8e9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 132,
                "y": 20
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "e1d5bad9-c55d-473c-80e4-310217e1dbdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "248af860-0c56-4b09-8a2c-98aca80094fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 196,
                "y": 38
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "775275d5-185d-4046-9fb2-c32ef5f34436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "cc715c41-ac9b-4c4f-93e9-a8e185d784e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "1cc29fda-473d-46f4-a0a6-a7f3199f5aa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 172,
                "y": 20
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "05341d26-161d-4213-b476-a5527f8d3545",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 192,
                "y": 2
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e76cfb56-34ae-4e79-908d-c4442c5af1a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "14d3620a-2610-47e4-958e-5ff534c949f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 172,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "74587d03-ccae-4759-9dbb-8b6f374cc358",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "efe0e7e6-308a-4dfa-8977-b50edeeedf13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 228,
                "y": 38
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0e0bc096-c97f-4399-87a6-00368a4bfbf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a9949344-5194-4de7-a208-566bf2b05b40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c04e2c7e-caa0-490a-9794-d0e2dcc51eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "f19972a6-6665-4022-b622-913e4291a890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d8baff96-efdd-4e4c-8ed2-5839cc5cc30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2a1a489a-27af-4abf-b39a-4ba542850ba1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 56
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "153e1692-1a11-42ef-9975-f6acfbf1d86a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 24,
                "y": 56
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7944a4ad-74d1-4d7f-97a9-8abf1fea114a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "875f5ca2-0974-4d70-b510-42ce68423b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 18,
                "y": 56
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "deaceced-c986-4ee6-9cf6-ca88c5597e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 172,
                "y": 38
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "8c8afa46-8747-4c5c-a84f-7240f91e549f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "61ec9c6d-355f-4634-9b81-133ee6612177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 54,
                "y": 56
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ad4d6de6-be69-4802-9897-a4204246d4a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 72,
                "y": 20
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e0ba6208-fae0-48c8-a8ef-44cad40b4c87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 20
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "53c19ad7-5d60-4c17-97bf-6776bf60a9ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 164,
                "y": 38
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f0ff924d-e49d-439a-80e6-6f4e912a6d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 142,
                "y": 20
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "8f8b0090-ef3b-4034-ae7a-d33f0f2af064",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "efbe8134-fdf9-4f24-bbe7-802139facec4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 116,
                "y": 38
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "7dd9e827-3d54-4bb5-9b72-ad38ab245595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 122,
                "y": 20
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "dc217d00-6898-4e78-9eac-2345e0a39627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 112,
                "y": 20
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "cc754e45-8d29-496b-86f6-5bf1490b6b52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 90,
                "y": 56
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6ad42717-3804-4ef9-a73b-63eb604dc931",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 16,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 48,
                "y": 56
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b80bab8e-6c2d-471d-a792-8da4d335272b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 102,
                "y": 20
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "99f2c92d-aa5f-4e38-8ecf-649c9a026424",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 66,
                "y": 56
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "366d3418-dcd0-4294-85a0-ae78d1ff3627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "be855e16-a618-431a-a70c-9dfa10dc740f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 92,
                "y": 20
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "82dac13b-5667-4ab7-a05e-cd08dd2f56df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 152,
                "y": 20
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "4e7bcd4e-4487-4343-9896-33526a7456c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 82,
                "y": 20
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d96fa068-6818-4f88-b7a2-3de22aca33c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 62,
                "y": 20
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "a7cdd26c-9f9c-4120-ba3d-a1910e547dfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 148,
                "y": 38
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "58737257-5e29-4ff3-b2aa-1b61ead028b7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 52,
                "y": 20
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a4178f7b-d684-4ad9-b40d-d6fa2779825c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 108,
                "y": 38
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bcaf153b-576f-49c0-9369-f66d9c342dc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 42,
                "y": 20
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "cb072258-4327-401e-a701-f2368269b6b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 32,
                "y": 20
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3a4ce388-e2ed-480e-8ad6-6864ce6226c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 16,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "4667049e-2bde-4679-9d82-dd0f3c81537c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 124,
                "y": 38
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "1975fd1a-d01c-468f-9f1e-fb465b1ff778",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 22,
                "y": 20
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "83b75942-b59f-44ce-9ce5-7cd8bbae6c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 12,
                "y": 20
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "eb734b82-e180-4c56-9246-45887fa8ec92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 140,
                "y": 38
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "683db32d-9a49-4cff-ab4b-33f2d888a753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 16,
                "offset": 0,
                "shift": 4,
                "w": 2,
                "x": 86,
                "y": 56
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e1db36a9-5572-426a-af78-7b6c466a4b0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 16,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 212,
                "y": 38
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6e110b6e-b501-4f2f-8967-9ff538bcb3fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 16,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 182,
                "y": 20
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}