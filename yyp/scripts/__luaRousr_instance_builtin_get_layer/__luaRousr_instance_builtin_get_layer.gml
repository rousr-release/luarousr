///@function __luaRousr_instance_builtin_get_layer()
///@desc builtin getter for layer
///@returns {*} builtin
///@extensionizer { "docs": false }
return layer;
