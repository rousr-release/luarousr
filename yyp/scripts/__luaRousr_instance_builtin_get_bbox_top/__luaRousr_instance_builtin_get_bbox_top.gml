///@function __luaRousr_instance_builtin_get_bbox_top()
///@desc builtin getter for bbox_top
///@returns {*} builtin
///@extensionizer { "docs": false }
return bbox_top;
