#include "luaRousr.h"

#include "LuaThread.h"
#include "LuaContext.h"

void CLuaThread::RegisterLua(CLuaContext& _Lua) {
	LuaIntf::LuaBinding(_Lua.L()).beginClass<CLuaThread>("thread")
		.addFactory(&CLuaContext::NewThread)
		.addStaticFunction("yield", &CLuaThread::YieldThread)
	.endClass();
}

CLuaThread::CLuaThread(LuaIntf::LuaState& _L)
{ 
	mL = _L.newThread();
	addReference();
}

CLuaThread::~CLuaThread() {
	if (mReference)
		removeReference();
}

bool CLuaThread::Resume() {
	int status = LUA_ERRERR;
	mRunning = true;
	
	try {
		status = mL.resume(0);
	} catch (std::exception& e) {
		std::string errorMsg(e.what());
		CLuaRousr::Error("luaRousr - lua error: " + errorMsg);
	} 

	mFinished = status != LUA_YIELD;
	if (mFinished) {
		if (status != LUA_OK) {
            uint32_t top(mL.top());
			std::string errorMsg(mL.getString(-2));
			CLuaRousr::Error("luaRousr - lua error: " + errorMsg);
			mFinished = true;
		}

		removeReference();
	}
	mRunning = false;
	return !mFinished;
}

int CLuaThread::YieldThread(lua_State* _L) {
	try {
		lua_yield(_L, 0);
	} catch (std::exception& e) {
		std::string errorMsg(e.what());
		CLuaRousr::Error("luaRousr - lua error: " + errorMsg);
	} 

	return 0;
}

void CLuaThread::addReference() {
	// add the thread to the global table
    mL.pushRegistryTable();
	mL.push("_luaRousrthreads");
	mL.getTable(-2);
	
	if (mL.isNil(-1)) {
		mL.pop();
		mL.newTable();
		mL.pushRegistryTable();
		mL.pushValueAt(-2);                  // push the table
		mL.setField(-2, "_luaRousrthreads"); // set the registry table field
		mL.pop();                            // pop the registry table
	}

	// add the reference to the end of the array
	int numThreads(mL.tableLen(-1));
	mL.pushThread();
	mL.setField(-2, numThreads + 1);
	
	mL.pop(); // pop the table
	mL.pop(); // pop the registryTable
	mReference = true;
}

void CLuaThread::removeReference() {
	// remove from globals
	uint32_t top(mL.top());
	mL.pushRegistryTable();
	mL.push("_luaRousrthreads");
	mL.getTable(-2);
	
	int numThreads(mL.tableLen(-1));
	int foundIndex = -1;
	for (int i = 1; i <= numThreads; ++i) {
		uint32_t top(mL.top());
		mL.getField(-1, i);
		if (foundIndex >= 0) {
			mL.setField(-2, i - 1);
		}
		else {
			if (mL.isThread(-1) && mL == mL.toThread(-1)) {
				foundIndex = i;
				mL.pop();
				mL.push(nullptr);
				mL.setField(-2, i);
			}
			else
				mL.pop();
		}
	}
	mL.pop(); // remove the table
	mL.pop(); // remove the registrytable
	
	mReference = false;
}