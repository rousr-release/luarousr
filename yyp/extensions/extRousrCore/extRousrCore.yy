{
    "id": "41aa7af4-8ff0-4c74-b128-a0ebf7e056c1",
    "modelName": "GMExtension",
    "mvc": "1.0",
    "name": "extRousrCore",
    "IncludedResources": [
        
    ],
    "androidPermissions": [
        
    ],
    "androidProps": false,
    "androidactivityinject": "",
    "androidclassname": "",
    "androidinject": "",
    "androidmanifestinject": "",
    "androidsourcedir": "",
    "author": "",
    "classname": "",
    "copyToTargets": 194,
    "date": "2017-33-29 08:12:44",
    "description": "",
    "extensionName": "",
    "files": [
        {
            "id": "32df1491-b20f-4ad6-8ac8-a825b3536e2d",
            "modelName": "GMExtensionFile",
            "mvc": "1.0",
            "ProxyFiles": [
                
            ],
            "constants": [
                
            ],
            "copyToTargets": 194,
            "filename": "extRousrCore.gml",
            "final": "",
            "functions": [
                {
                    "id": "83fb9e8a-5f0b-4dec-9ead-fe2dab5ad1f9",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_ensure_singleton",
                    "help": "called `with` object to be singleton - ensure the name passed is a singleton, will call instance destroy on a different id ( _singleton_name )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_ensure_singleton",
                    "returnType": 2
                },
                {
                    "id": "0b6f96c1-481b-4de8-9f21-bae1683e2560",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_ensure_font",
                    "help": "cache the current font ( font )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_ensure_font",
                    "returnType": 2
                },
                {
                    "id": "02f9e558-fd4c-422e-a2a5-983ac76cb474",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_ensure_color",
                    "help": "cache the current color ( _color )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_ensure_color",
                    "returnType": 2
                },
                {
                    "id": "ada0f45e-3996-4952-8fd1-2c88f0a30091",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_color_hex",
                    "help": "[Convert the RGB to BGR][https:\/\/forum.yoyogames.com\/index.php?threads\/why-are-hex-colours-bbggrr-instead-of-rrggbb.16325\/#post-105309] ( color )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_color_hex",
                    "returnType": 2
                },
                {
                    "id": "0d7fcdcb-9c70-40af-a04d-defe333883ce",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_color_glsl",
                    "help": "convert a color to an array float values [0.0 - 1.0] ( _color , [_out_array] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_color_glsl",
                    "returnType": 2
                },
                {
                    "id": "da4d179b-1074-4fe3-8ee8-55e8ab435141",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_next_pot",
                    "help": "return the nearest power of 2 for a given number [integer] ( _va )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_next_pot",
                    "returnType": 2
                },
                {
                    "id": "1d6bb285-0d9a-40cf-9761-9bc3c2482dc7",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_shadow_text",
                    "help": "Draw text with a shadow ( _x , _y , _text , _fg , _bg , [_xoff=1] , [_yoff=1] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_shadow_text",
                    "returnType": 2
                },
                {
                    "id": "8b309ef6-0888-4dca-a0be-79f5742b9359",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_outline_text",
                    "help": "draw text with an oultine ( _x , _y , _text , _fg , _bg )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_outline_text",
                    "returnType": 2
                },
                {
                    "id": "8a8e61ee-e6c7-49ff-b89e-765077ff3737",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_error",
                    "help": "Error wrapper [eventual logging system] ( [_system] , _text )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_error",
                    "returnType": 2
                },
                {
                    "id": "0a06aca4-2acb-4de9-978e-010782254964",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_log",
                    "help": "Log wrapper [eventual logging system] ( [_system] , _text )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_log",
                    "returnType": 2
                },
                {
                    "id": "47bb7ae2-3edf-4f16-80c0-c7745c753a78",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_game_quit",
                    "help": "generic callback to use for quits [button callbacks mainly] (  )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_game_quit",
                    "returnType": 2
                },
                {
                    "id": "3f7e6a29-d16d-4b36-a6e3-1d12e1103068",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_execute",
                    "help": "rousr_execute - call a function with variadic argument support ( _script_index , _params , [paramCount] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_execute",
                    "returnType": 2
                },
                {
                    "id": "aedfaaf9-0359-44bd-8c9b-4252ed4ba41d",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_ds_safe_destroy",
                    "help": "check if `_ds_id` is a valid `_type` and destroy it if it is, returning the new id to use for _ds_id [undefined in most cases] ( _type , _ds_id )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_ds_safe_destroy",
                    "returnType": 2
                },
                {
                    "id": "59bb85fd-7a2e-4c44-b164-3df276c38a03",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_dummy",
                    "help": "it does nothing - used for placeholders in callback systems ( [_any] )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_dummy",
                    "returnType": 2
                },
                {
                    "id": "8fe15cdf-deaa-4163-b27a-243684e9c7f0",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_aabb_contains_line",
                    "help": "courtesy of https:\/\/stackoverflow.com\/questions\/1585525\/how-to-find-the-intersection-point-between-a-line-and-a-rectangle ( _line_x1 , _line_y1 , _line_x2 , _line_y2 , _rect_min_x , _rect_min_y , _rect_max_x , _rect_max_y )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_aabb_contains_line",
                    "returnType": 2
                },
                {
                    "id": "c32357bc-a3f8-4835-aa4b-61bbead4b680",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_sort_quick",
                    "help": "implement a quicksort you can pass a predicate to port from http:\/\/www.algolist.net\/Algorithms\/Sorting\/Quicksort ( list , comparison )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_sort_quick",
                    "returnType": 2
                },
                {
                    "id": "ccd02047-0474-48bc-8659-08af1685b296",
                    "modelName": "GMExtensionFunction",
                    "mvc": "1.0",
                    "argCount": 0,
                    "args": [
                        
                    ],
                    "externalName": "sr_sort_insert",
                    "help": "an insertion sort with a predicateported from http:\/\/www.algolist.net\/Algorithms\/Sorting\/Insertion_sortcause why not? ( list , predicate )",
                    "hidden": false,
                    "kind": 1,
                    "name": "sr_sort_insert",
                    "returnType": 2
                }
            ],
            "init": "",
            "kind": 2,
            "order": [
                "83fb9e8a-5f0b-4dec-9ead-fe2dab5ad1f9",
                "0b6f96c1-481b-4de8-9f21-bae1683e2560",
                "02f9e558-fd4c-422e-a2a5-983ac76cb474",
                "ada0f45e-3996-4952-8fd1-2c88f0a30091",
                "0d7fcdcb-9c70-40af-a04d-defe333883ce",
                "da4d179b-1074-4fe3-8ee8-55e8ab435141",
                "1d6bb285-0d9a-40cf-9761-9bc3c2482dc7",
                "8b309ef6-0888-4dca-a0be-79f5742b9359",
                "8a8e61ee-e6c7-49ff-b89e-765077ff3737",
                "0a06aca4-2acb-4de9-978e-010782254964",
                "47bb7ae2-3edf-4f16-80c0-c7745c753a78",
                "3f7e6a29-d16d-4b36-a6e3-1d12e1103068",
                "aedfaaf9-0359-44bd-8c9b-4252ed4ba41d",
                "59bb85fd-7a2e-4c44-b164-3df276c38a03",
                "8fe15cdf-deaa-4163-b27a-243684e9c7f0",
                "c32357bc-a3f8-4835-aa4b-61bbead4b680",
                "ccd02047-0474-48bc-8659-08af1685b296"
            ],
            "origname": "",
            "uncompress": false
        }
    ],
    "gradleinject": "",
    "helpfile": "",
    "installdir": "",
    "iosProps": false,
    "iosSystemFrameworkEntries": [
        
    ],
    "iosThirdPartyFrameworkEntries": [
        
    ],
    "iosplistinject": "",
    "license": "",
    "maccompilerflags": "",
    "maclinkerflags": "",
    "macsourcedir": "",
    "packageID": "",
    "productID": "",
    "sourcedir": "",
    "version": "0.11.0"
}