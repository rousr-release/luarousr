///@function __luaRousr_instance_builtin_set_solid(_val)
///@desc builtin setter for solid
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
solid = argument0;
