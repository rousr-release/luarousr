///@function __luaRousr_instance_builtin_set_sprite_index(_val)
///@desc builtin setter for sprite_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
sprite_index = argument0;
