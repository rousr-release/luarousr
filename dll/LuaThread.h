#pragma once

#include "types.h"

class CLuaContext;

class CLuaThread {
public:
	static void RegisterLua(CLuaContext& _Lua);
    CLuaThread(LuaIntf::LuaState& _L);
	~CLuaThread();

	bool Resume();	
	bool IsRunning()  const { return mRunning;  }
	bool IsFinished() const { return mFinished; }
	static int YieldThread(lua_State* _L);
	void Kill() { mFinished = true; removeReference(); }

    LuaIntf::LuaState&       L()       { return mL;  }
	const LuaIntf::LuaState& L() const { return mL;  }

private:

	CLuaThread(const CLuaThread& _r) = delete;
	CLuaThread& operator=(const CLuaThread& _r) = delete;
	CLuaThread(const CLuaThread&& _r) = delete;
	CLuaThread& operator=(const CLuaThread&& _r) = delete;

	bool mFinished = false;
	bool mRunning  = false;
	bool mReference = false;
	LuaIntf::LuaState mL;

	void addReference();
	void removeReference();
};