///@function __luaRousr_instance_builtin_get_bbox_bottom()
///@desc builtin getter for bbox_bottom
///@returns {*} builtin
///@extensionizer { "docs": false }
return bbox_bottom;
