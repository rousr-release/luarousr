if(grid_type == grid_types.null){
	exit;
}

var _i=0;
repeat(springs_size){
	spring_update(springs_array[_i]);
	++_i;
}

_i=0;
repeat(global.points_size){
	point_update(global.points_array[_i]);
	++_i;
}

if(grid_type != grid_types.normal){
  timer+=0.3;
}
 