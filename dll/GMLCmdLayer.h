#pragma once

#include "types.h"

class CGMLBuffer;

class GMLCmdLayer {
public: 
	static void          Shutdown();
    static bool          IsShuttingDown() { return mShuttingDown; }
    static bool          IsEmpty()        { return mCmdStack.empty(); }
    static GMLCmdLayer*  Active()         { return IsEmpty() ? nullptr : mCmdStack.top();  }

private:
    static std::stack<GMLCmdLayer*> mCmdStack;
    static std::atomic<bool>        mShuttingDown;

public:
    GMLCmdLayer(std::shared_ptr<CGMLBuffer> _buffer, uint32_t _cmdId);
    ~GMLCmdLayer();
	
    uint32_t CmdId() const                  { return mCmdId; }
    bool IsRunning() const                  { return mRunning; }
    std::shared_ptr<CGMLBuffer> CmdBuffer() { return mBuffer; }

    static void Execute(std::function<void(GMLCmdLayer&)> _action);
    void SendBuffer();

private:
    std::atomic<int>  mRunning       = { false };
    
	uint32_t mCmdId;
    std::shared_ptr<CGMLBuffer> mBuffer;
};
