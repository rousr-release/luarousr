///@function __luaRousr_instance_builtin_get_image_index()
///@desc builtin getter for image_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return image_index;
