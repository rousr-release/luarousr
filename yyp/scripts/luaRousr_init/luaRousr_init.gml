///@function luaRousr_init()
///@desc create LuaRousr if it doesn't already exist. [helper function]
///@returns {Real} LuaRousr instance id
if (instance_number(LuaRousr) != 0) {
  with (LuaRousr)
    return id;
}

return instance_create_depth(0, 0, 0, LuaRousr);