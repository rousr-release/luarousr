///@function test3_function2(_testString)
///@desc called by the Unit Tests
///@param {string} _testString   test string to print in the call chain test
var _testString = argument0;
show_debug_message("unitTest - gml: test3_function2 - _testString = " + _testString);