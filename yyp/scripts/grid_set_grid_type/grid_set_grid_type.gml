///@param grid_type
with(obj_grid){
	switch(argument0){
		case grid_types.normal:
			grid_sprite = spr_line_normal;
			timer = 0;
		break;
		case grid_types.fancy:
			timer = 0;
			grid_sprite = spr_line_normal;
		break;
		case grid_types.rainbow:
			timer = 0
			grid_sprite = spr_line_rainbow;
		break;
		case grid_types.null:
			timer = 0;
		break;
	}
	
	grid_type = argument0;
}