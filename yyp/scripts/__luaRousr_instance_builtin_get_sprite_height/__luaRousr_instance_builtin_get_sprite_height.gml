///@function __luaRousr_instance_builtin_get_sprite_height()
///@desc builtin getter for sprite_height
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_height;
