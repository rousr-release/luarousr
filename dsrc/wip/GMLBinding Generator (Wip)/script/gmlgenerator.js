// gmlgenerator.js
//  babyjeans
//
// handle GML stuffs
///
var fs = require('fs');
var tab = "  ";
class GMLGenerator {

	static getGMLPath(fileName, gmlConfig, noPrefix) {
		let gmlFilePath = fileName;
		if (!noPrefix) gmlFilePath = gmlConfig.filePrefix + fileName;

		try {
			let gmlExistsPath = gmlConfig.path + "scripts/" + gmlFilePath + "/" + gmlFilePath + ".gml";
			if (fs.existsSync(gmlExistsPath)) gmlFilePath = gmlExistsPath;
			else gmlFilePath = gmlConfig.fallbackPath + gmlFilePath + ".gml";
		} catch (err) {	console.log(err); }

		return gmlFilePath;
	}

	constructor(boundResource) {
		this.resource = boundResource;
		
		let gmlConfig   = this.resource.gmlConfig;
		let gmlFilePath = GMLGenerator.getGMLPath(this.resource.name, gmlConfig);

		let gmlFile = "///@desc Auto-Generated GMLBindings for " + this.resource.name + "\n";
		gmlFile += "///@param bindingIndex\n";
		gmlFile += "///@param callId\n";
		gmlFile += "///@param id\n";
		gmlFile += "///@param args\n";
		gmlFile += "\n";
		gmlFile += "var _bindingIndex = argument0;\n";
		gmlFile += "var _callId       = argument1;\n";
		gmlFile += "var _id           = argument2;\n";
		gmlFile += "var _args         = argument3;\n";
		gmlFile += "\n";
		gmlFile += "switch(_bindingIndex) {\n";
		
		let gmlFooter = "  default:break;\n";
		gmlFooter += "}\n\n";
		
		this.filePath = gmlFilePath;
		this.gmlFile = gmlFile;
		this.gmlFooter = gmlFooter;
		this.gmlBindings = "";
	}

	processVariable(v) {
		let bindingIndex = v.bindingIndex;
		v.gmlUpdate.func

		let gmlBinding  = tab + "//Binding " + this.resource.name + "." + v.name + "\n";
		gmlBinding     += tab + "case " + bindingIndex + ": ";
		if (bindingIndex < 10)  gmlBinding += " ";
		if (bindingIndex < 100) gmlBinding += " ";
		
		if (this.resource.hasId) gmlBinding += v.gmlUpdate.func + "(_callId, _id." + v.name + "); break;\n";
		// todo: get value from function
		else                     gmlBinding += tab + tab + "return _id." + v.name + "; break;\n";
		bindingIndex++;
		
		if (!v.flags.readonly) {
			gmlBinding += tab + "//Binding " + this.resource.name + "." + v.name + "\n";
			gmlBinding += tab + "case " + bindingIndex.toString() + ": ";
			if (bindingIndex < 10)  gmlBinding += " ";
			if (bindingIndex < 100) gmlBinding += " ";
			
			if (this.resource.hasId) gmlBinding += "_id." + v.name + " = _args[0]; break;\n";
			// todo: set value from function
			else                     gmlBinding += "_id." + v.name + " = _args[0]; break;\n";

			gmlBinding += "\n";
		}

		this.gmlBindings += gmlBinding;
		return gmlBinding;
	}


	processFunction(f) {
		let args = f.args;
		let gmlFunction = f.gmlName || f.name;
		let gmlUpdate = f.return.gmlUpdate;
		let noparam = gmlUpdate.flags.no_param || false;
		let bindingIndex = f.bindingIndex;

		let gmlBinding = "";
		gmlBinding += tab + "// function: " + f.name + "\n";
		gmlBinding += tab + "case " + bindingIndex + ": ";
		if (bindingIndex < 10)  gmlBinding += " ";
		if (bindingIndex < 100) gmlBinding += " ";

		if (noparam) gmlBinding += "\n" + tab + tab + f.gmlName + "(";
		else         gmlBinding += gmlUpdate.func + "(_callId, " + f.gmlName + "(";
		
		for (let argIndex = 0; argIndex < args.length; ++argIndex) {
			gmlBinding += "_args[" + argIndex + "]"
			if (argIndex + 1 < args.length) gmlBinding += ", ";
		}
		gmlBinding += ")";

		if (noparam) gmlBinding += ";\n" + tab + tab + gmlUpdate.func + "(_callId" + ");\n" + tab + "break;\n\n";
		else gmlBinding += "); break;\n\n"; 

		this.gmlBindings += gmlBinding;
		return gmlBinding;
	}

	generateGMLCall() {
		return "case " + this.resource.index + ": " + this.resource.gmlConfig.filePrefix + this.resource.name + "(_bind, _callId, _id, args); break;";
	}

	generate() {
		return this.gmlFile + this.gmlBindings + this.gmlFooter;
	}
};


module.exports = GMLGenerator;