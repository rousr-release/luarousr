///@function __luaRousr_instance_builtin_get_sprite_xoffset()
///@desc builtin getter for sprite_xoffset
///@returns {*} builtin
///@extensionizer { "docs": false }
return sprite_xoffset;
