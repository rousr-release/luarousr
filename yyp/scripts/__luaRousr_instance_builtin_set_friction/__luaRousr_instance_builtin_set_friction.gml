///@function __luaRousr_instance_builtin_set_friction(_val)
///@desc builtin setter for friction
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
friction = argument0;
