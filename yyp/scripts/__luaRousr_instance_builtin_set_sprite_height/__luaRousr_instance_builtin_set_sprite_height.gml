///@function __luaRousr_instance_builtin_set_sprite_height(_val)
///@desc builtin setter for sprite_height
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!
