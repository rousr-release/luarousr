///@function __luaRousr_instance_builtin_get_visible()
///@desc builtin getter for visible
///@returns {*} builtin
///@extensionizer { "docs": false }
return visible;
