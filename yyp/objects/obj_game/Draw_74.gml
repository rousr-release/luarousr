//Cool blurry stuff
shader_set(shdr_bloom);
if(obj_grid.grid_type != grid_types.fancy){
	shader_set_uniform_f(uniform_blur_size, 0.001);
	shader_set_uniform_f(uniform_bloom_intensity, 1.6);
}else{
	shader_set_uniform_f(uniform_blur_size, 0.01);
	shader_set_uniform_f(uniform_bloom_intensity, 1);
}

draw_surface(application_surface,0,0);
shader_reset();


draw_sprite_stretched_ext(
	spr_game,0,0,0,
	display_get_gui_width(),
	display_get_gui_height(),
	c_white,
	image_alpha-0.2
);

