///@function __luaRousr_instance_builtin_get_timeline_position()
///@desc builtin getter for timeline_position
///@returns {*} builtin
///@extensionizer { "docs": false }
return timeline_position;
