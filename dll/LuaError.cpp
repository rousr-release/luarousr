#include "LuaError.h"
#include "sol/sol.hpp"

LuaError::LuaError(LuaIntf::LuaState& L)
	: mL(L)
	, mLuaExceptionStack(new LuaIntf::LuaState(L), [](auto L) { L->pop(); })
{
	errorMsg = std::string(mL.toString(-1));
}

LuaError::LuaError(const LuaError & other)
	: mL(other.mL), mLuaExceptionStack(other.mLuaExceptionStack)
{
}

const char* LuaError::what() const throw()
{
	return errorMsg.c_str();
}
