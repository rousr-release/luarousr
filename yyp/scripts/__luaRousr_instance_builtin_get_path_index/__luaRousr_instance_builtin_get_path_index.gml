///@function __luaRousr_instance_builtin_get_path_index()
///@desc builtin getter for path_index
///@returns {*} builtin
///@extensionizer { "docs": false }
return path_index;
