///@function __luaRousr_instance_builtin_get_xprevious()
///@desc builtin getter for xprevious
///@returns {*} builtin
///@extensionizer { "docs": false }
return xprevious;
