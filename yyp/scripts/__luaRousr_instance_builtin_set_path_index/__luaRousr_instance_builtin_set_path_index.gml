///@function __luaRousr_instance_builtin_set_path_index(_val)
///@desc builtin setter for path_index
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
// readonly builtin, no set!
