///@function __luaRousr_instance_builtin_set_gravity_direction(_val)
///@desc builtin setter for gravity_direction
///@param {*} _val   val to set builtin to
///@extensionizer { "docs": false }
var _val = argument0;
gravity_direction = argument0;
