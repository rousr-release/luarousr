{
    "id": "084863ca-83f9-4cc2-ad27-532e05de612a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_line_normal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "22a18d4e-7697-4ddd-94be-c536c4fbfa87",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "66dcf935-8d98-4a0a-8438-d98ee2a8e8ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a18d4e-7697-4ddd-94be-c536c4fbfa87",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fbeeca7-2fe7-4bd9-a845-dcf4721d4c9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a18d4e-7697-4ddd-94be-c536c4fbfa87",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "d297a5a0-4556-46ea-bcfc-37529e9d6476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "9877f40c-6222-4745-af66-6c6fb8c59bed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d297a5a0-4556-46ea-bcfc-37529e9d6476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb3e079b-e781-4abb-8969-d58a77ed9a07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d297a5a0-4556-46ea-bcfc-37529e9d6476",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "21f99a42-d929-4345-8a54-98b3b97dbd34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "546df5f5-678a-4db7-b122-3e291fbb7e51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21f99a42-d929-4345-8a54-98b3b97dbd34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a245e57-f4d4-479a-baa4-ea81d27ea46c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21f99a42-d929-4345-8a54-98b3b97dbd34",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "0685913c-9400-48be-9ff0-1f0bb8b420af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "49a135b3-d38e-4ba4-a0de-0180c9018bd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0685913c-9400-48be-9ff0-1f0bb8b420af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbd1d970-4e7e-4d15-b99e-b4d5bf437467",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0685913c-9400-48be-9ff0-1f0bb8b420af",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "1167819b-b13a-4110-bbe6-e2f1bce0b5af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "7db8cd6b-fd6a-458b-82fe-e89ce9fe3f06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1167819b-b13a-4110-bbe6-e2f1bce0b5af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7231d059-1abe-4a09-8b57-8fa301557477",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1167819b-b13a-4110-bbe6-e2f1bce0b5af",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "1af6ee25-9240-49f8-9d76-e509fb64e4e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "a726fe33-be9b-4e9d-bf99-91190ac6b2c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1af6ee25-9240-49f8-9d76-e509fb64e4e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19702913-d4da-4e61-930a-b4332b21f972",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1af6ee25-9240-49f8-9d76-e509fb64e4e8",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "2b01ff47-b236-433c-b194-cfa0a33484d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "b9309dd7-4f4b-4a26-94eb-aa975ac12d3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b01ff47-b236-433c-b194-cfa0a33484d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30a11426-fba4-4f1e-a76b-fc5cf23f82ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b01ff47-b236-433c-b194-cfa0a33484d6",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "b65b295d-96ca-48b0-a993-473d21abf7f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "231074de-de59-42d9-b679-78cff7e38f7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65b295d-96ca-48b0-a993-473d21abf7f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14a5b121-bfec-4f3a-9faa-d8eadc655dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65b295d-96ca-48b0-a993-473d21abf7f1",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "d8626227-cf58-49b6-aba3-19d5df1768a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "2f1de0bc-1cc9-4c5c-9109-5c270e5d01c7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8626227-cf58-49b6-aba3-19d5df1768a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "994ca394-d7f0-4fe5-b3a2-1d2be5e5bd83",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8626227-cf58-49b6-aba3-19d5df1768a7",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "6a5e71aa-4424-45be-a523-785b0835ef80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "2e9f06d1-f9ce-440a-b737-039e1263ce49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5e71aa-4424-45be-a523-785b0835ef80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "066ad752-36ee-43bb-8ec0-2b6f44668508",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5e71aa-4424-45be-a523-785b0835ef80",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "77efb771-265e-4319-b88a-ec0a97e70a16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "f1e3a45e-ce41-4303-9964-8e4fe9f27a95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77efb771-265e-4319-b88a-ec0a97e70a16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f98e8e1-17ca-4868-bd91-a115d3c339b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77efb771-265e-4319-b88a-ec0a97e70a16",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "c16b3f4d-a611-474d-9aaa-3616178bc38a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "50506878-0943-4ef2-923d-86e84ceddd8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c16b3f4d-a611-474d-9aaa-3616178bc38a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0277572a-24da-4a07-a98c-056654771f10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c16b3f4d-a611-474d-9aaa-3616178bc38a",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "b4b662e6-f9a6-46ad-b3e9-0b1b1e7b2e6b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "c345e4f1-71ff-4e54-866d-7b0e814d69a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4b662e6-f9a6-46ad-b3e9-0b1b1e7b2e6b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c66cd58a-757a-46e3-a305-78f505685074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4b662e6-f9a6-46ad-b3e9-0b1b1e7b2e6b",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "40ee1f66-a533-4314-92f5-0b56646dcb89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "cf4eec56-d890-437b-b7aa-ef4331d97d0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40ee1f66-a533-4314-92f5-0b56646dcb89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e410de3-2449-4242-8867-e454a5b4cc39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40ee1f66-a533-4314-92f5-0b56646dcb89",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "0634ea00-887b-4320-adf0-c53e9bffb87c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "a4f6ca01-f928-4c7b-ae5a-32681d282665",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0634ea00-887b-4320-adf0-c53e9bffb87c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1a1603e-b791-419c-b9e3-ab1e307d7ee9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0634ea00-887b-4320-adf0-c53e9bffb87c",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "22a1892c-d016-49e3-960c-6f2ce5600524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "3d7a894f-1dda-4f95-848a-1e798714ee6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a1892c-d016-49e3-960c-6f2ce5600524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0bd54ef-011d-4f59-a76a-eedccd74ca43",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a1892c-d016-49e3-960c-6f2ce5600524",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "ef0144b9-8243-4e13-9f77-bd0101da9b00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "948af2d9-29d3-4bee-af62-3fdfda5764cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef0144b9-8243-4e13-9f77-bd0101da9b00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340b9615-e791-4e5b-9b6f-d827f284051e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef0144b9-8243-4e13-9f77-bd0101da9b00",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "6d81e5e4-f543-4793-b433-e761a8f2a2ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "4a10ec60-2c23-45c9-8b99-0d30a04be95f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d81e5e4-f543-4793-b433-e761a8f2a2ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3dab246-833e-484e-ae81-7d6f00cf6757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d81e5e4-f543-4793-b433-e761a8f2a2ac",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "c30c16b6-d848-40c2-9765-8f88fe8bcda2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "85118f10-3854-41e0-87f7-bcd35ca3633a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c30c16b6-d848-40c2-9765-8f88fe8bcda2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de45400c-980c-486b-8414-f3354b9eab3e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c30c16b6-d848-40c2-9765-8f88fe8bcda2",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "482fb65f-1e0c-45fa-a2f0-d49beebb092d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "0c4096b5-20ce-49e0-8bd4-2b0adeb3d215",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "482fb65f-1e0c-45fa-a2f0-d49beebb092d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "604d9d33-7431-4645-8cbc-585499f889f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "482fb65f-1e0c-45fa-a2f0-d49beebb092d",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "46f2d778-6fc4-4e92-bcc5-2f533e0a342f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "6f319c8c-f606-4fe0-89f5-c43751b09f24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46f2d778-6fc4-4e92-bcc5-2f533e0a342f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b71eec37-081d-40d7-a0ef-1bdb6d97d2f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46f2d778-6fc4-4e92-bcc5-2f533e0a342f",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "031084d8-261c-4cc3-948d-a378afbc528b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "5f56ad40-b639-42c3-81e6-4b5a49bbae69",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "031084d8-261c-4cc3-948d-a378afbc528b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7ee4136-f669-4904-a8be-d491c8ffcf8f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "031084d8-261c-4cc3-948d-a378afbc528b",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "0bd202fb-2bce-4184-8c2a-5f4fc4506485",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "4834f5c7-6374-41ed-86ee-b51d60ec33dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd202fb-2bce-4184-8c2a-5f4fc4506485",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68459174-9492-4e1c-a307-04d88a5a3daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd202fb-2bce-4184-8c2a-5f4fc4506485",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "0265cd06-dbc5-4dff-9c1e-40cfad097dc0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "1566be62-5e49-402b-81cb-37c4231ea9d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0265cd06-dbc5-4dff-9c1e-40cfad097dc0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73f37107-db2f-4680-958f-629c2903c21e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0265cd06-dbc5-4dff-9c1e-40cfad097dc0",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "ba20e239-5769-44e3-80f0-734f3013b734",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "3742fd41-d46d-4eb4-b8e1-cee794fab98c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba20e239-5769-44e3-80f0-734f3013b734",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48e85168-5840-45b6-8b6f-d556bdcd7a22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba20e239-5769-44e3-80f0-734f3013b734",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "b5f0ab0a-c198-4f7a-a49a-a3226c85a72a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "295aceb3-f183-443d-9859-eaae9a23c8d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5f0ab0a-c198-4f7a-a49a-a3226c85a72a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56057e11-f469-460f-95eb-1c1fc2eddd76",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5f0ab0a-c198-4f7a-a49a-a3226c85a72a",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "6f3aa75d-5794-40ae-9ab6-0a979a7398eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "44af1335-9b18-4b0a-8435-52cb86637675",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f3aa75d-5794-40ae-9ab6-0a979a7398eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8b2c3fe-7751-405b-95dd-1d538c6a84e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f3aa75d-5794-40ae-9ab6-0a979a7398eb",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        },
        {
            "id": "36632d34-97ea-449a-a9bc-ea5789de2b99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "compositeImage": {
                "id": "a430afdd-8925-413f-b9ec-67b867ba7fb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36632d34-97ea-449a-a9bc-ea5789de2b99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb470a71-6998-4402-91ba-b857e7ed80fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36632d34-97ea-449a-a9bc-ea5789de2b99",
                    "LayerId": "fc4d4bff-c670-431f-9ceb-3914c43c1720"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "fc4d4bff-c670-431f-9ceb-3914c43c1720",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "084863ca-83f9-4cc2-ad27-532e05de612a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}