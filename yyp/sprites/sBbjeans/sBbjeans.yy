{
    "id": "091ed4fe-ee28-4624-88d6-aa1a822e9ed1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBbjeans",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73fcfe98-254b-42d3-aa03-02529cda7743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "091ed4fe-ee28-4624-88d6-aa1a822e9ed1",
            "compositeImage": {
                "id": "10768387-7e44-4c4e-8120-635124307608",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73fcfe98-254b-42d3-aa03-02529cda7743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ca573fd-18bd-4875-8ee1-8630cb7c35ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73fcfe98-254b-42d3-aa03-02529cda7743",
                    "LayerId": "6ff2611e-493d-4d65-b3c4-5d54e51c21fa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6ff2611e-493d-4d65-b3c4-5d54e51c21fa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "091ed4fe-ee28-4624-88d6-aa1a822e9ed1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 22,
    "xorig": 0,
    "yorig": 0
}