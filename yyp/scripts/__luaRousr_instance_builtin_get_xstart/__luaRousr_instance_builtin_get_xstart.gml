///@function __luaRousr_instance_builtin_get_xstart()
///@desc builtin getter for xstart
///@returns {*} builtin
///@extensionizer { "docs": false }
return xstart;
