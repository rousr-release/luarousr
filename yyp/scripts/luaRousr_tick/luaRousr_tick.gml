///@function luaRousr_tick()
///@desc force lua to process a frame / tick /step

// todo: check if there are threads after calling Lua, and set a "there are threads" flag, rather
//       than hitting the DLL every step

__extLuaRousrCollectGarbage();
if (__extLuaRousrHasThreads()) {
  var cmd_buffer = __luaRousr_pop_buffer();
  __luaRousr_process_buffer(cmd_buffer, __extLuaRousrResumeThreads(buffer_get_address(cmd_buffer)));
  __luaRousr_push_buffer(cmd_buffer);
}