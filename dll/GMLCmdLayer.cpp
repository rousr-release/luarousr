#include "GMLCmdLayer.h"
#include "luaRousr.h"

#include "rousrGML/GMLBuffer.h"
#include "rousrGML/GMLVal.h"
#include "GMLBind.h"

// Initialize statics
std::stack<GMLCmdLayer*>  GMLCmdLayer::mCmdStack;
std::atomic<bool>         GMLCmdLayer::mShuttingDown = { false };

void GMLCmdLayer::Shutdown() {
    mShuttingDown = true;
    while (!mCmdStack.empty())
        std::this_thread::yield();
}

// Command queue
GMLCmdLayer::GMLCmdLayer(std::shared_ptr<CGMLBuffer> _buffer, uint32_t _cmdId) : mBuffer(_buffer), mCmdId(_cmdId) {
    // push on to the command stack
    mCmdStack.push(this);
}

GMLCmdLayer::~GMLCmdLayer() {
    if (mCmdStack.top() != this)
        CLuaRousr::Error("luaRousr - ERROR: CmdLayer trying to pop, but isn't the top most layer! CmdStack corrupt.");
    else 
        mCmdStack.pop();

    // return the command buffer
    auto cmdBuffer(CmdBuffer());
    cmdBuffer->Seek(LuaRousr::SeekData);

    CGMLBind& bind(CLuaRousr::Bind());
    bind.SyncWrite(*cmdBuffer);

    cmdBuffer->Seek(LuaRousr::SeekCmdId);
    cmdBuffer->Write<uint32_t>(CmdId());
    cmdBuffer->Seek(LuaRousr::SeekStatus);
    cmdBuffer->Write<int8_t>(static_cast<uint8_t>(LuaRousr::Status::Done));

    CLuaRousr::GMLReady(CmdId());
}

void GMLCmdLayer::Execute(std::function<void(GMLCmdLayer&)> _action) {
    if (CLuaRousr::IsMainThread())
        throw MsgException("luaRousr - EXCEPTION: Trying to execute GML Cmd from main thread!");

    if (mCmdStack.empty())
        throw MsgException("luaRousr - EXCEPTION: CmdStack empty, but trying to execute.");

    auto top(mCmdStack.top());
    if (top == nullptr || top->mRunning)
        throw MsgException("luaRousr - EXCEPTION: Top Command Layer is unavailable for new commands, missing a new push?");

    top->mRunning = true;
    _action(*top);
    top->mRunning = false;
}

void GMLCmdLayer::SendBuffer() {
	auto cmdBuffer(CmdBuffer());
    
    cmdBuffer->Seek(LuaRousr::SeekCmdId);
	cmdBuffer->Write<uint32_t>(CmdId());
	cmdBuffer->Seek(LuaRousr::SeekStatus);
	cmdBuffer->Write<int8_t>(static_cast<int8_t>(LuaRousr::Status::Ready));

    CLuaRousr::GMLReady(CmdId());
	while (!CLuaRousr::IsLuaReady(CmdId()))
		std::this_thread::yield();
}