![luuuuuuaaaaaaaaah-ah-ah-ah](http://git.rou.sr/release/misc/raw/master/LuaRousr/lua_rousr_top.png)

# LuaRousr

LuaRousr is a GameMaker Studio 2 extension that integrates Lua with GameMaker Studio 2

### Adding to your project

*	Import the entire thing into your project
*	Drop Lua files into your included folder
*	_Optionally_ Use **[OutsideTheBox](http://forum.rou.sr/topic/46/gms2-extension-outsidethebox-saving-files-outside-of-the-sandbox)** to load from anywhere.

### Quick Start

*	Drop an instance of oLuaRousr into your room. (**NOTE:** It's best to keep it persistent)
*	Execute Lua files with `luaRousr_executeFile(filename)`
*	Execute Lua strings with `luaRousr_executeString(scriptstring)`
*	Bind GML script with `luaRousr_bindScript(lua_name, script_index, [optional]noreturns)`
*	Bind/Unbind Instances with `luaRousr_bindInstance(lua_name, instance_id)`/`luaRousr_unbindInstance(instance_id)`

###### _Note_: LuaRousr uses [Lua 5.3](https://www.lua.org/versions.html#5.3)

---

## The Demo
![boingggggg](http://i.imgur.com/yRzgkk7.gif)

###### The [demo]() was lovingly handcrafted, and provided by [@net8floz](https://twitter.com/net8floz)
[Download Here]()

### Messin' with the Demo

First and foremost: Use the `F1`-`F4` keys so that you can experience the latest in Grid fashion.

The demo uses the 4 Lua files, `game.lua`, `paddle.lua`, `ball.lua`, and `math.lua` to pit two AI pong paddles against one another, each of these files can be edited WHILE the game is running to completely change the behavior of the game itself. Once you've edited the Lua files, press `R` and the Lua will reload. Hurray!

### Working with the Demo Example (included with LuaRousr)

Lua files are expected to be right next to the .exe in this demo - though you don't have to do it that way.  To run the demo from GMS2 you must add the lua files to the `executable path` **first**. 

This path is something like the following for VM (and something ridiculous on YYC):
```
C:\ProgramData\GMS2\Cache\runtimes\runtime-x-x-x\windows
```

Alternatively just put the Lua files on `C:/lua` (or anywhere else) and use that instead of `get_executable_path`

**See: obj_game - Create for more instruction**

##### _Makes use of the [spline](http://www.gmlscripts.com/script/spline) from [GMLscripts.com](http://www.gmlscripts.com)_

###### A note about the included extension:
*	######  [OutsideTheBox](http://forum.rou.sr/topic/46/gms2-extension-outsidethebox-saving-files-outside-of-the-sandbox) is provided to allow loading the Lua files from the executable path, but you can use it in your own projects. [Read More](http://forum.rou.sr/topic/46/gms2-extension-outsidethebox-saving-files-outside-of-the-sandbox)

---

## API Reference


`luaRousr_bindScript(lua_name, script_index, has_returns(optional))`  
**lua_name:**     the `string` name to use **in Lua** to call the script.  
**script_index:** the gml script index of the script  
**noreturns:** (_optional_) slightly faster to set this true if you expect there's no returns  
* Call this for all of your GML script functions you'd like Lua to have access to.  
* **Note:** This must be done _before_ you load your scripts that use the functions. Otherwise, it'sunreliable that they'll be able to "see" your functions.


`luaRousr_executeString(string)`  
`luaRousr_executeFile(filename)` - literally file_text_reads the file into a string, and calls `luaRousr_executeString`  
**returns:**   returns a `script_context_id`  
* These functions execute Lua script, and return an id to associate with any threads that may have been started within this execution. See `luaRousr_killThreads`


`luaRousr_call(function_name, arguments...)`  
**function_name:** the `string` name to use **in Lua** to represent the instance.  
**arguments:**    any number of arguments to pass to the lua functoin  
**returns:**      the value Lua returns.  (**NOTE: This is not functioning in the current version! No returns as of yet.**)
* Call a Lua function with the given name, that simple!

  
`luaRousr_killThreads()`  
* Kill all running threads

  
`luaRousr_bindInstance(lua_name, instance_id)`  
**lua_name:**     the `string` name to use **in Lua** to represent the instance.  
**instance_id:**  the instance to bind  
* Binds an instance to Lua using the given name. All the members can now be accessed i.e,:
```
inst.x = 10
print(inst.y)
```
* **NOTE:** You must unbind an instance in its `CleanUp` event if it's still bound at that point.


`luaRousr_unbindInstance(instance_id)`  
**instance_id:**  the instance to stop binding to Lua  
* Removes a bind, the Lua objects still exist, but in an undefined state.

---

### Credit

* [@net8floz](http://www.twitter.com/net8floz) for the friggin cool [demo]()
	* Makes use of [spline](http://www.gmlscripts.com/script/spline) from [GMLscripts.com](http://www.gmlscripts.com)
* [Lua 5.3](http://www.lua.org)!
* [lua-intf](https://github.com/SteveKChiu/lua-intf) for a C++ 11 Lua C API Wrapper!
* [moodycamel:concurrentqueue](https://github.com/cameron314/concurrentqueue) for lockless queues!
* [sol2](https://github.com/ThePHd/sol2) for GMLBindInstance dynamic getter/setter

**Please see LICENSE.md for license information**  
**Extension itself is covered by [YoYo Games's EULA]()**

---

## Changelog
*	v0.7.0RC 
	*	Excellent, superb demo by @net8floz
		*	Added outsideTheBox (v0.9.3) to the demo
	*	Fixed id not being bound to instances.
	*	Fixed rebinding to the same named instance.
	*	Added `luaRousr_unbindInstance`
	*	Added `GMLInstance`
		*	Integrated [sol2](https://github.com/ThePHd/sol2) for dynamic member getter/setter
*   v0.6.1 - Fixed YYC compile
*	v0.6.0
	*	rousr_sync_val system added
	*	Added `luaRousr_bindInstance(instance_id)`
* 	v0.5.1 - Added `luaRousr_killThreads` 
*	v0.5.0 
	*	Initial Version
		*	GML to DLL: Use Async Map to transfer data on a request basis
		*	DLL Lua Integration using [lua-intf](https://github.com/SteveKChiu/lua-intf)

### Upcoming

*	Add a `print` hook to redirect debug output as you see fit.
*	Lua "Language Features"
	*	Ability to pass Instance to function as id (rather than `inst.id`)
	*	Binding Custom Members to Instances
	*	Add `GMLInstance` functions in Lua (i.e., `inst:place_meeting(otherInst)`)
	*	Bind Objects as `GMLObject`
	*	Bind Sprites as 'GMLSprites'
	*	More GML API Binding...
*	Support for Multiple Lua States
	*	Potentially running on threads independent of each other.
*	Optimizations
	*	DLL-Based Peek to sleep the thread, rather than spin on a buffer_peek
	*	Remove `lua-intf` and replace completely with `sol2`
*	Support for more platforms (MacOs, Linux first)

### Known Issues (Expect fixes soon)

*	`luaRousr_call` does not return values or context IDs.
*	`luaRousr_killThread(contextId)` OR `luaRousr_executeFile` are having issues with contextIds.
*	Repeated calls to `luaRousr_executeFile` can cause unexpected results.
*	`layer = -1` disables rendering, currently syncing it is disabled.
*	If LuaRousr is not persistent, recreating it can sometimes cause issues.