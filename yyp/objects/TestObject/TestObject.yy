{
    "id": "923c73ec-d050-4ae2-911a-768a5cb282f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "TestObject",
    "eventList": [
        {
            "id": "238405ff-af44-42f6-8e37-2b8424a0a396",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "923c73ec-d050-4ae2-911a-768a5cb282f8"
        },
        {
            "id": "a16e8391-4eaf-42c4-9944-339b86d0e9c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "923c73ec-d050-4ae2-911a-768a5cb282f8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "fd3a9bc9-fce2-4f2a-8dc5-dd8ffd8cc50b",
    "visible": true
}