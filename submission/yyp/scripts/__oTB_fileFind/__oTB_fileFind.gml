///@desc __oTB_fileFind - find a __fileDB file from a filename or GMS fileHandle
///@param {Real|String} _filePathOrHandle
var _filePath = argument0;

oTB_init();
var fileDb = global.___otbFiles;
var numFiles = array_length_1d(fileDb);
for (var i = 0; i < numFiles; ++i) {
  var fileData = fileDb[i];
  if ( (is_string(_filePath) && fileData[EOTBFile.FilePath] == _filePath) ||
       (is_real(_filePath) && fileData[EOTBFile.GMLHandle] == _filePath) ) {
    return fileData;     
  }
}

return undefined;
