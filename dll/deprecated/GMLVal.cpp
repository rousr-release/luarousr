#include "GMLVal.h"

template<>
void TGMLVal<double>::FromSolObject(sol::stack_object& o)  {
    LuaIntf::LuaState L(o.lua_state());
    if (o.is<double>())
        mVal = o.as<double>();
    else if(o.is<bool>())
        mVal = o.as<bool>() ? 1.0 : 0.0;
}

