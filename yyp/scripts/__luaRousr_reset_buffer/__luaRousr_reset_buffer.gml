///@desc __luaRousr_reset_buffer - signal the DLL to write more data
///@param {Real} _cmdBuffer - buffer id, buffer that Lua and GML pass
///@param {Real} _cmdId     - id to notify Lua which command buffer this is for. (sanity checks)
///@extensionizer { "docs": false }
var _cmdBuffer = argument0;
var _cmdId     = argument1;
buffer_seek(_cmdBuffer, buffer_seek_start, 0);
buffer_write(_cmdBuffer, buffer_u8, __ELuaRousrStatus.Waiting);
buffer_seek(_cmdBuffer, buffer_seek_start, 0);
__extLuaRousrReadyForLua(_cmdId);