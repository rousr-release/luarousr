{
    "id": "6ccf2cda-d39f-42a5-b289-692ffe2b9810",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grid",
    "eventList": [
        {
            "id": "669fe036-dd07-4207-a67f-1a3061c26d41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6ccf2cda-d39f-42a5-b289-692ffe2b9810"
        },
        {
            "id": "6fecab9b-fae6-457d-8c51-e468ba369544",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6ccf2cda-d39f-42a5-b289-692ffe2b9810"
        },
        {
            "id": "29b9fb09-2c6b-4fc0-aae2-dcf942b82301",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6ccf2cda-d39f-42a5-b289-692ffe2b9810"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}