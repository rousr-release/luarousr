#pragma once

#include "./rousrGML/GMLVal.h"

#include "sol/sol.hpp"
#include "LuaIntf/LuaIntf.h"
#include "rousrGML/GMLBuffer.h"
#include "rousrGML/GMLTypedBuffer.h"

class IGMLLuaVal : public IGMLVal {
public:
    virtual void        Push(LuaIntf::LuaState& _L) = 0;
    virtual sol::object AsSolObject(sol::state_view _L) = 0;
    virtual void        FromSolObject(sol::stack_object& _o) = 0;

    virtual void WriteBuffer(CGMLBuffer& _buffer, bool _alterBuffer = true) = 0;
    virtual void WriteBuffer(CGMLTypedBuffer& _buffer) = 0;

};

template <typename T>
class TGMLLuaVal : public IGMLLuaVal {
public:
    TGMLLuaVal(const T& _val) : mVal(_val) { ; }
    TGMLLuaVal() { ; }

    T& Val() { return mVal; }
    const T& Val() const { return mVal; }

    void SetVal(const T& _val) { mVal = _val; }

    void                               Push(LuaIntf::LuaState& _L)          override { _L.push(Val());  }
    sol::object                        AsSolObject(sol::state_view _L)      override { return sol::make_object(_L, Val()); }
    void                               FromSolObject(sol::stack_object& _o) override { SetVal(_o.as<T>());  }

    void WriteToMap(int _mapId, const std::string& _key) override { CGMLDSMap::WriteDouble(_mapId, _key, static_cast<double>(Val())); }
    bool Equals(std::shared_ptr<IGMLVal> _other) override         { return IGMLVal::InternalEquals<TGMLLuaVal<T>>(*this, _other); }
    std::shared_ptr<IGMLVal> Clone() const override               { return std::make_shared<TGMLLuaVal<T>>(Val()); }
    bool Set(std::shared_ptr<IGMLVal> _other) override            {
        auto other(std::dynamic_pointer_cast<TGMLVal<T>>(_other));
        if (other == nullptr)
            return false;
        
        SetVal(other->Val());
        return true;
    }

    // There's no circumstance that we know the Type and want to use the WriteBuffer functions.
    void WriteBuffer(CGMLBuffer& _buffer, bool _alterBuffer = true) override {
        auto typed(CGMLTypedBuffer(_buffer, false));
        WriteBuffer(typed);
        
        if (_alterBuffer)
            _buffer.Seek(typed.Tell());
    }

    void WriteBuffer(CGMLTypedBuffer& _buffer) override {
        _buffer.Write<T>(Val());
    }

private:
    T mVal;
};

template <> inline void TGMLLuaVal<std::string>::WriteToMap(int _mapId, const std::string& _key) {
    CGMLDSMap::WriteString(_mapId, _key, Val().c_str());
}

template <> inline void TGMLLuaVal<double>::FromSolObject(sol::stack_object& _o) {
    LuaIntf::LuaState L(_o.lua_state());
    
    if (_o.is<double>())    SetVal(_o.as<double>());
    else if (_o.is<bool>()) SetVal(_o.as<bool>() ? 1.0 : 0.0);
}

class GMLLuaValVoid : public IGMLLuaVal {
public:
    GMLLuaValVoid(CGMLBuffer* _buffer) { ; }
    GMLLuaValVoid() { ; }

    bool Equals(std::shared_ptr<IGMLVal> _other) override { return std::dynamic_pointer_cast<GMLLuaValVoid>(_other) != nullptr; }
    void WriteToMap(int, const std::string&)     override { ; }
    std::shared_ptr<IGMLVal> Clone() const       override { return std::make_shared<GMLLuaValVoid>(); }
    bool Set(std::shared_ptr<IGMLVal> _other)    override { return std::dynamic_pointer_cast<GMLLuaValVoid>(_other) != nullptr; }

    void        Push(LuaIntf::LuaState& _L)          override { _L.push(nullptr); }
    sol::object AsSolObject(sol::state_view _L)      override { return sol::make_object(_L, sol::nil); }
    void        FromSolObject(sol::stack_object& _o) override { ; }

    void WriteBuffer(CGMLBuffer&, bool)  override { ; }
    void WriteBuffer(CGMLTypedBuffer& )  override { ; }

};

namespace GMLLua {
    inline std::shared_ptr<IGMLLuaVal> ReadVal(CGMLBuffer& _buffer) {

        int8_t dataType(_buffer.Read<int8_t>());
        switch (static_cast<rousrGML::DataType>(dataType)) {
            case rousrGML::DataType::Byte:   return std::make_shared<TGMLLuaVal<char>>(_buffer.Read<char>());
            case rousrGML::DataType::Bool:   return std::make_shared<TGMLLuaVal<bool>>(_buffer.Read<bool>());

            case rousrGML::DataType::Int8:   return std::make_shared<TGMLLuaVal<int8_t>>(_buffer.Read<int8_t>());
            case rousrGML::DataType::Int16:  return std::make_shared<TGMLLuaVal<int16_t>>(_buffer.Read<int16_t>());
            case rousrGML::DataType::Int32:  return std::make_shared<TGMLLuaVal<int32_t>>(_buffer.Read<int32_t>());
            case rousrGML::DataType::Int64:  return std::make_shared<TGMLLuaVal<int64_t>>(_buffer.Read<int64_t>());

            case rousrGML::DataType::Uint8:  return std::make_shared<TGMLLuaVal<uint8_t>>(_buffer.Read<uint8_t>());
            case rousrGML::DataType::Uint16: return std::make_shared<TGMLLuaVal<uint16_t>>(_buffer.Read<uint16_t>());
            case rousrGML::DataType::Uint32: return std::make_shared<TGMLLuaVal<uint32_t>>(_buffer.Read<uint32_t>());
            case rousrGML::DataType::Uint64: return std::make_shared<TGMLLuaVal<uint64_t>>(_buffer.Read<uint64_t>());

            case rousrGML::DataType::Float:  return std::make_shared<TGMLLuaVal<float>>(_buffer.Read<float>());
            case rousrGML::DataType::Double: return std::make_shared<TGMLLuaVal<double>>(_buffer.Read<double>());
            case rousrGML::DataType::String: return std::make_shared<TGMLLuaVal<std::string>>(_buffer.Read<std::string>());
            
            default: break;
        }
        
        return nullptr;
    }
}
